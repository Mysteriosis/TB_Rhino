Deprecated test_help path
You now must require 'rails/test_help' not just 'test_help'.
More information: http://weblog.rubyonrails.org/2009/9/1/gem-packaging-best-practices

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/test/test_helper.rb

New file needed: config/application.rb
You need to add a config/application.rb.
More information: http://omgbloglol.com/post/353978923/the-path-to-rails-3-approaching-the-upgrade

The culprits: 
	- config/application.rb

Soon-to-be-deprecated ActiveRecord calls
Methods such as find(:all), find(:first), finds with conditions, and the :joins option will soon be deprecated.
More information: http://m.onkey.org/2010/1/22/active-record-query-interface

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/annee_academiques_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/annee_civiles_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/application_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/bachelor_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/cahier_charge_assistants_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/charge_effectives_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/charges_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/competence_domaines_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/constantes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/contrat_annuels_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/contrat_mensuels_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/contrats_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/departements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/desiderata_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/desideratas_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/evenement_personnes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/evenement_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/filieres_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/module_unites_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/mois_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/niveau_competences_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/periodes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personnes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/road_maps_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/sous_type_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/statut_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/suivis_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_activites_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_contrats_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_evenement_personnes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_evenement_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_periodes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_personnes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_projet_activites_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_unite_enseignements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/unite_enseignements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/helpers/charges_helper.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/annee_academique.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/contrat.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/desiderata.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/projet.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/unite_enseignement.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/annee_mise_a_jour_competence.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/charge_effectives_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/charges_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/contrats_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/departements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/desideratas_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personne_prives_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personne_sessions_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personnes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/unite_enseignements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/helpers/charges_helper.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/cahier_charge_assistant.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/variable.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charges/index.html.erb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charges/index.html.haml
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/annee_mise_a_jour_competence.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/creation_element_competence_draggable.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/supprime_domaine_competence.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/charge_effectives_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/charges_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/contrat_mensuels_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/contrats_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/departements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/desideratas_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personne_prives_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personne_sessions_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/personnes_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/type_projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/unite_enseignements_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/helpers/charges_helper.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/cahier_charge_assistant.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/contrat.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/annee_mise_a_jour_competence.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/creation_element_competence_draggable.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/supprime_domaine_competence.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/projets_controller.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/helpers/charges_helper.rb

named_scope is now just scope
The named_scope method has been renamed to just scope.
More information: http://github.com/rails/rails/commit/d60bb0a9e4be2ac0a9de9a69041a4ddc2e0cc914

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne.rb

Deprecated AJAX helper calls
AJAX javascript helpers have been switched to be unobtrusive and use :remote => true instead of having a seperate function to handle remote requests.
More information: http://www.themodestrubyist.com/2010/02/24/rails-3-ujs-and-csrf-meta-tags/

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/_unite.html.erb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/_unite.html.haml
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/_unite_without_charge_affectee.html.erb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/_unite_without_charge_affectee.html.haml
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/drop_collaborateur.rjs
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/index.html.erb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/charge_effectives/index.html.haml
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/gestion_desideratas.html.erb
	- /Users/shamu/Projects/rh-ash-rails3/app/views/personnes/gestion_desideratas.html.haml

Old gem bundling (config.gems)
The old way of bundling is gone now.  You need a Gemfile for bundler.
More information: http://omgbloglol.com/post/353978923/the-path-to-rails-3-approaching-the-upgrade

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/config/environment.rb

Deprecated ActionMailer API
You're using the old ActionMailer API to send e-mails in a controller, model, or observer.
More information: http://lindsaar.net/2010/1/26/new-actionmailer-api-in-rails-3

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/app/controllers/activations_controller.rb

Old ActionMailer class API
You're using the old API in a mailer class.
More information: http://lindsaar.net/2010/1/26/new-actionmailer-api-in-rails-3

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne_mailer.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne_mailer.rb
	- /Users/shamu/Projects/rh-ash-rails3/app/models/personne_mailer.rb

Old Rails generator API
A plugin in the app is using the old generator API (a new one may be available at http://github.com/trydionel/rails3-generators).
More information: http://blog.plataformatec.com.br/2010/01/discovering-rails-3-generators/

The culprits: 
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/authlogic/generators/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/authlogic/generators/session/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/authlogic/generators/session/templates/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/css_graphs/generators/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/css_graphs/generators/css_graphs/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/css_graphs/generators/css_graphs/templates/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/role_requirement/generators/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/role_requirement/generators/roles/
	- /Users/shamu/Projects/rh-ash-rails3/vendor/plugins/role_requirement/generators/roles/templates/

Old router API
The router API has totally changed.
More information: http://yehudakatz.com/2009/12/26/the-rails-3-router-rack-it-up/

The culprits: 
	- config/routes.rb

Known broken plugins
At least one plugin in your app is broken (according to the wiki).  Most of project maintainers are rapidly working towards compatability, but do be aware you may encounter issues.
More information: http://wiki.rubyonrails.org/rails/version3/plugins_and_gems

The culprits: 
	- authlogic

require 'rails_helper'

RSpec.describe "commentaires/new", type: :view do
  before(:each) do
    assign(:commentaire, Commentaire.new(
      :texte => "MyText"
    ))
  end

  it "renders new commentaire form" do
    render

    assert_select "form[action=?][method=?]", commentaires_path, "post" do

      assert_select "textarea#commentaire_texte[name=?]", "commentaire[texte]"
    end
  end
end

require 'rails_helper'

RSpec.describe "commentaires/show", type: :view do
  before(:each) do
    @commentaire = assign(:commentaire, Commentaire.create!(
      :texte => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
  end
end

require 'rails_helper'

RSpec.describe "commentaires/edit", type: :view do
  before(:each) do
    @commentaire = assign(:commentaire, Commentaire.create!(
      :texte => "MyText"
    ))
  end

  it "renders the edit commentaire form" do
    render

    assert_select "form[action=?][method=?]", commentaire_path(@commentaire), "post" do

      assert_select "textarea#commentaire_texte[name=?]", "commentaire[texte]"
    end
  end
end

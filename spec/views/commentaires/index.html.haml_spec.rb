require 'rails_helper'

RSpec.describe "commentaires/index", type: :view do
  before(:each) do
    assign(:commentaires, [
      Commentaire.create!(
        :texte => "MyText"
      ),
      Commentaire.create!(
        :texte => "MyText"
      )
    ])
  end

  it "renders a list of commentaires" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end

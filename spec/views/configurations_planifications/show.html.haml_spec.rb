require 'rails_helper'

RSpec.describe "configurations_planifications/show", type: :view do
  before(:each) do
    @configurations_planification = assign(:configurations_planification, ConfigurationsPlanification.create!(
      :description => "Description",
      :valeur => "Valeur"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/Valeur/)
  end
end

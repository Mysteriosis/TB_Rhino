require 'rails_helper'

RSpec.describe "configurations_planifications/index", type: :view do
  before(:each) do
    assign(:configurations_planifications, [
      ConfigurationsPlanification.create!(
        :description => "Description",
        :valeur => "Valeur"
      ),
      ConfigurationsPlanification.create!(
        :description => "Description",
        :valeur => "Valeur"
      )
    ])
  end

  it "renders a list of configurations_planifications" do
    render
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Valeur".to_s, :count => 2
  end
end

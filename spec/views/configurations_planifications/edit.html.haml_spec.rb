require 'rails_helper'

RSpec.describe "configurations_planifications/edit", type: :view do
  before(:each) do
    @configurations_planification = assign(:configurations_planification, ConfigurationsPlanification.create!(
      :description => "MyString",
      :valeur => "MyString"
    ))
  end

  it "renders the edit configurations_planification form" do
    render

    assert_select "form[action=?][method=?]", configurations_planification_path(@configurations_planification), "post" do

      assert_select "input#configurations_planification_description[name=?]", "configurations_planification[description]"

      assert_select "input#configurations_planification_valeur[name=?]", "configurations_planification[valeur]"
    end
  end
end

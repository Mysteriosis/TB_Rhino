require 'rails_helper'

RSpec.describe "configurations_planifications/new", type: :view do
  before(:each) do
    assign(:configurations_planification, ConfigurationsPlanification.new(
      :description => "MyString",
      :valeur => "MyString"
    ))
  end

  it "renders new configurations_planification form" do
    render

    assert_select "form[action=?][method=?]", configurations_planifications_path, "post" do

      assert_select "input#configurations_planification_description[name=?]", "configurations_planification[description]"

      assert_select "input#configurations_planification_valeur[name=?]", "configurations_planification[valeur]"
    end
  end
end

require 'rails_helper'

RSpec.describe "jalons/edit", type: :view do
  before(:each) do
    @jalon = assign(:jalon, Jalon.create!(
      :affectation_projet_id => 1,
      :mail_id => 1,
      :titre => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit jalon form" do
    render

    assert_select "form[action=?][method=?]", jalon_path(@jalon), "post" do

      assert_select "input#jalon_affectation_projet_id[name=?]", "jalon[affectation_projet_id]"

      assert_select "input#jalon_mail_id[name=?]", "jalon[mail_id]"

      assert_select "input#jalon_titre[name=?]", "jalon[titre]"

      assert_select "textarea#jalon_description[name=?]", "jalon[description]"
    end
  end
end

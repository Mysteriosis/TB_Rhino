require 'rails_helper'

RSpec.describe "jalons/show", type: :view do
  before(:each) do
    @jalon = assign(:jalon, Jalon.create!(
      :affectation_projet_id => 1,
      :mail_id => 2,
      :titre => "Titre",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Titre/)
    expect(rendered).to match(/MyText/)
  end
end

require 'rails_helper'

RSpec.describe "jalons/index", type: :view do
  before(:each) do
    assign(:jalons, [
      Jalon.create!(
        :affectation_projet_id => 1,
        :mail_id => 2,
        :titre => "Titre",
        :description => "MyText"
      ),
      Jalon.create!(
        :affectation_projet_id => 1,
        :mail_id => 2,
        :titre => "Titre",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of jalons" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Titre".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end

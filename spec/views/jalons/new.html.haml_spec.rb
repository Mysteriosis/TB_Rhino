require 'rails_helper'

RSpec.describe "jalons/new", type: :view do
  before(:each) do
    assign(:jalon, Jalon.new(
      :affectation_projet_id => 1,
      :mail_id => 1,
      :titre => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new jalon form" do
    render

    assert_select "form[action=?][method=?]", jalons_path, "post" do

      assert_select "input#jalon_affectation_projet_id[name=?]", "jalon[affectation_projet_id]"

      assert_select "input#jalon_mail_id[name=?]", "jalon[mail_id]"

      assert_select "input#jalon_titre[name=?]", "jalon[titre]"

      assert_select "textarea#jalon_description[name=?]", "jalon[description]"
    end
  end
end

require 'rails_helper'

RSpec.describe "planifications/edit", type: :view do
  before(:each) do
    @planification = assign(:planification, Planification.create!())
  end

  it "renders the edit planification form" do
    render

    assert_select "form[action=?][method=?]", planification_path(@planification), "post" do
    end
  end
end

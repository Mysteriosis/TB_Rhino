require 'rails_helper'

RSpec.describe "planifications/new", type: :view do
  before(:each) do
    assign(:planification, Planification.new())
  end

  it "renders new planification form" do
    render

    assert_select "form[action=?][method=?]", planifications_path, "post" do
    end
  end
end

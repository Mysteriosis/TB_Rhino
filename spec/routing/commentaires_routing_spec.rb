require "rails_helper"

RSpec.describe CommentairesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/commentaires").to route_to("commentaires#index")
    end

    it "routes to #new" do
      expect(:get => "/commentaires/new").to route_to("commentaires#new")
    end

    it "routes to #show" do
      expect(:get => "/commentaires/1").to route_to("commentaires#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/commentaires/1/edit").to route_to("commentaires#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/commentaires").to route_to("commentaires#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/commentaires/1").to route_to("commentaires#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/commentaires/1").to route_to("commentaires#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/commentaires/1").to route_to("commentaires#destroy", :id => "1")
    end

  end
end

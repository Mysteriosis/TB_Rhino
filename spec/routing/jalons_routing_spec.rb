require "rails_helper"

RSpec.describe JalonsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/jalons").to route_to("jalons#index")
    end

    it "routes to #new" do
      expect(:get => "/jalons/new").to route_to("jalons#new")
    end

    it "routes to #show" do
      expect(:get => "/jalons/1").to route_to("jalons#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/jalons/1/edit").to route_to("jalons#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/jalons").to route_to("jalons#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/jalons/1").to route_to("jalons#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/jalons/1").to route_to("jalons#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/jalons/1").to route_to("jalons#destroy", :id => "1")
    end

  end
end

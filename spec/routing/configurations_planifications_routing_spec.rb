require "rails_helper"

RSpec.describe ConfigurationsPlanificationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/configurations_planifications").to route_to("configurations_planifications#index")
    end

    it "routes to #new" do
      expect(:get => "/configurations_planifications/new").to route_to("configurations_planifications#new")
    end

    it "routes to #show" do
      expect(:get => "/configurations_planifications/1").to route_to("configurations_planifications#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/configurations_planifications/1/edit").to route_to("configurations_planifications#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/configurations_planifications").to route_to("configurations_planifications#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/configurations_planifications/1").to route_to("configurations_planifications#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/configurations_planifications/1").to route_to("configurations_planifications#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/configurations_planifications/1").to route_to("configurations_planifications#destroy", :id => "1")
    end

  end
end

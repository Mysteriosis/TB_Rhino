require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe CommentairesController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Commentaire. As you add validations to Commentaire, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # CommentairesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all commentaires as @commentaires" do
      commentaire = Commentaire.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:commentaires)).to eq([commentaire])
    end
  end

  describe "GET #show" do
    it "assigns the requested commentaire as @commentaire" do
      commentaire = Commentaire.create! valid_attributes
      get :show, {:id => commentaire.to_param}, valid_session
      expect(assigns(:commentaire)).to eq(commentaire)
    end
  end

  describe "GET #new" do
    it "assigns a new commentaire as @commentaire" do
      get :new, {}, valid_session
      expect(assigns(:commentaire)).to be_a_new(Commentaire)
    end
  end

  describe "GET #edit" do
    it "assigns the requested commentaire as @commentaire" do
      commentaire = Commentaire.create! valid_attributes
      get :edit, {:id => commentaire.to_param}, valid_session
      expect(assigns(:commentaire)).to eq(commentaire)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Commentaire" do
        expect {
          post :create, {:commentaire => valid_attributes}, valid_session
        }.to change(Commentaire, :count).by(1)
      end

      it "assigns a newly created commentaire as @commentaire" do
        post :create, {:commentaire => valid_attributes}, valid_session
        expect(assigns(:commentaire)).to be_a(Commentaire)
        expect(assigns(:commentaire)).to be_persisted
      end

      it "redirects to the created commentaire" do
        post :create, {:commentaire => valid_attributes}, valid_session
        expect(response).to redirect_to(Commentaire.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved commentaire as @commentaire" do
        post :create, {:commentaire => invalid_attributes}, valid_session
        expect(assigns(:commentaire)).to be_a_new(Commentaire)
      end

      it "re-renders the 'new' template" do
        post :create, {:commentaire => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested commentaire" do
        commentaire = Commentaire.create! valid_attributes
        put :update, {:id => commentaire.to_param, :commentaire => new_attributes}, valid_session
        commentaire.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested commentaire as @commentaire" do
        commentaire = Commentaire.create! valid_attributes
        put :update, {:id => commentaire.to_param, :commentaire => valid_attributes}, valid_session
        expect(assigns(:commentaire)).to eq(commentaire)
      end

      it "redirects to the commentaire" do
        commentaire = Commentaire.create! valid_attributes
        put :update, {:id => commentaire.to_param, :commentaire => valid_attributes}, valid_session
        expect(response).to redirect_to(commentaire)
      end
    end

    context "with invalid params" do
      it "assigns the commentaire as @commentaire" do
        commentaire = Commentaire.create! valid_attributes
        put :update, {:id => commentaire.to_param, :commentaire => invalid_attributes}, valid_session
        expect(assigns(:commentaire)).to eq(commentaire)
      end

      it "re-renders the 'edit' template" do
        commentaire = Commentaire.create! valid_attributes
        put :update, {:id => commentaire.to_param, :commentaire => invalid_attributes}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested commentaire" do
      commentaire = Commentaire.create! valid_attributes
      expect {
        delete :destroy, {:id => commentaire.to_param}, valid_session
      }.to change(Commentaire, :count).by(-1)
    end

    it "redirects to the commentaires list" do
      commentaire = Commentaire.create! valid_attributes
      delete :destroy, {:id => commentaire.to_param}, valid_session
      expect(response).to redirect_to(commentaires_url)
    end
  end

end

# -*- encoding : utf-8 -*-
module Fonctions
  #--------------------------------------------------------------------------------------------------------
  # Nom: facteur_actuel
  # But: Retourne le facteur souhaite a une annee academique souhaitée
  # Parametre: description => description de la constante shouaitee
  #            id_annee => id de l'annee académique souhaitee
  #--------------------------------------------------------------------------------------------------------
  def Fonctions.facteur_actuel(description, id_annee)   
      facteur = Constante.find(:first, :conditions => ["description =? AND annee_academique_id =? ", description, id_annee])
      return facteur.valeur.to_f
  end

  
end

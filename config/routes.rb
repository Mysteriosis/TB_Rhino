RhAshRails3::Application.routes.draw do

  # match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :method => :get, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  # get '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}

  resources :jour_academiques do
    collection do
      get :index_pour_annee
      put :index_pour_annee   #Quand l'invocation vient du formulaire de saisie de l'année
      patch :index_pour_annee   #Quand l'invocation vient du formulaire de saisie de l'année
    end
  end

  root :to => 'pages#index'
  
  # ressources représentant des types : 
  resources :type_projets do
    collection do
      get :new_categorie
    end
  end
  resources :type_budgets
  resources :type_id_externes
  
  devise_for :personnes

  #resources :affectation_projets
  
  resources :personnes do
    collection do
      get :droits
      get :resume_heures_collaborateurs_sem1
      get :resume_heures_collaborateurs_sem2
      get :resume_heures_professeurs_sem1
      get :resume_heures_professeurs_sem2
      get :new_personne_fictive
    end
    member do 
      get :resume_heures_annee_precedente
      # get :old_resume_heures
      # put :old_resume_heures
      get :resume_heures
      put :resume_heures #Quand l'invocation vient du formulaire de saisie de l'année
      patch :resume_heures #Quand l'invocation vient du formulaire de saisie de l'année
      get :edit_roles
      put :update_roles
      patch :update_roles
      get :gestion_desideratas
      get :resume_conso
      get :resume_conso_graph_data
      get :enregistrer_desiderata
      get :edit_horaire
      put :update_horaire
      patch :update_horaire
    end
    resources :contrats do
      collection do
        get :historique
      end
    end
    resources :cahier_charge_assistants
    resources :affectation_projets
    resources :affectation_projet_academiques, :controller=>:affectation_projets 
    resources :plage_horaire_saisies do 
      collection do
        get :semaine
        get :jours
        get :add
        get :add_mois
        get :cloner_semaine_precedente
      end
    end
  end
  
  resources :plage_contractuelles do
    collection do
      get :resume_heures
      put :resume_heures
      patch :resume_heures
      put :ajuster_report_heures_sup
      patch :ajuster_report_heures_sup
      put :ajuster_report_heures_vacances
      patch :ajuster_report_heures_vacances
      get :get_calendrier_details
      get :heures_supplementaires
      post :annuler_heures_supplementaires
    end
  end
  
  resources :contrats do
    collection do
      get :stats
      get :index_global
      get :nouvel_engagement
      get :update_engagement
      get :create_engagement
      get :destroy_engagement
    end
  end
  
  resources :projets do
    member do 
      get :resume_conso
      get :resume_conso_graph_data
      get :activer
      get :desactiver
      get :archiver
      get :desarchiver
    end
    collection do
     get :essai
   end
    resources :budgets
    resources :affectation_projets do
      member do
        get :activer
        get :desactiver
        get :supprimer_heures_saisies
      end
    end
    resources :affectation_projet_academiques, :controller=>:affectation_projets 
  end

  
  # match '/personnes/:personne_id/plage_horaire_saisies/:year/:month' => 'plage_horaire_saisies#mois', :as => :calendar, :method => :get#, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  # match 'configuration', :to => "pages#configuration", :as => "configuration", :method => :get
  # match 'enseignement',  :to => "pages#enseignement",  :as => "enseignement", :method => :get
  # match 'exportation_heures_sagex', :to => "pages#exportation_heures_sagex", :as => "exportation_heures_sagex", :method => :get
  # match 'do_exportation_heures_sagex', :to => "pages#do_exportation_heures_sagex", :as => "do_exportation_heures_sagex", :method => :post
    
  get '/personnes/:personne_id/plage_horaire_saisies/:year/:month' => 'plage_horaire_saisies#mois', :as => :calendar#, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  get 'configuration', :to => "pages#configuration", :as => "configuration"
  get 'enseignement',  :to => "pages#enseignement",  :as => "enseignement"
  get 'exportation_heures_sagex', :to => "pages#exportation_heures_sagex", :as => "exportation_heures_sagex"
  post 'do_exportation_heures_sagex', :to => "pages#do_exportation_heures_sagex", :as => "do_exportation_heures_sagex"
  
  # AJAX specific routes
  # match '/affectation_projets/get_engagements_select/:personne_id/:projet_id', :to => "affectation_projets#get_engagements_select", :method => :get
  # match '/affectation_projets/project_specific_fields/:projet_id/:personne_id', :to => "affectation_projets#project_specific_fields", :method => :get
  # match '/affectation_projets/get_consommation_graph_data', :to => "affectation_projets#get_consommation_graph_data", :method => :get
  # match '/affectation_projets/:id/get_details', :to => "affectation_projets#get_details", :method => :get
  # match '/get_type_projet_field', :to => "projets#get_type_fields", :method => :get
  
  get '/affectation_projets/get_engagements_select/:personne_id/:projet_id', :to => "affectation_projets#get_engagements_select"
  get '/affectation_projets/project_specific_fields/:projet_id/:personne_id', :to => "affectation_projets#project_specific_fields"
  get '/affectation_projets/get_consommation_graph_data', :to => "affectation_projets#get_consommation_graph_data"
  get '/affectation_projets/:id/get_details', :to => "affectation_projets#get_details"
  get '/get_type_projet_field', :to => "projets#get_type_fields"
  
  # Legacy ajax routes
  # match '/cahier_charge_assistant/heures_modifiees/:id', :to => "cahier_charge_assistants#heures_modifiees"

  # TODO : refacor and nest into personnes
  resources :personne_prives
  
  resources :variables
  resources :salles
  resources :personne_charge_enseignements
  resources :charge_enseignements
  resources :road_map_actions
  resources :road_maps do
    collection do
      get :doit
      get :dupliquer
      get :supprimer
      get :creer_projets_labos
      get :reset_heures
      get :reset_heures_dune_personne
      get :reset_global_des_heures
    end
  end

  resources :interet_unites
  #resources :cahier_charge_assistants
  #match 'cahier_charge_assistant/heures_modifiees/:id', :controller => 'cahier_charge_assistants', :action => 'heures_modifiees'
  #match 'cahier_charge_assistants/:id', :controller => 'cahier_charge_assistants', :action => 'show'
  #match 'cahier_charge_assistants/:id/edit', :controller => 'cahier_charge_assistants', :action => 'edit'
  resources :charge_assistants
  resources :axe_strategiques
  #map.connect 'personne_prives/:id/edit', :controller => 'personne_prives', :action => 'edit'
  #map.connect 'personnes/update_assignement/:id', :controller => 'personnes', :action => 'ajout_assignement', :id => :id
  resources :date_evenement_personne
  
  resources :charges do
    collection do 
      get :charge_professeurs_global
      get :charge_des_assistants
      get :charge_des_professeurs
      get :get_prev_details
      get :get_details
      get :get_personne_details
      get :update_effectiv_flag
      get :create_charge_effective
      get :liste_eduardo
      get :change_assistanat_reconnu
      get :graphe_affectations
      get :graphe_affectations_data
    end
  end
  
  resources :unite_enseignements do
    member do
      get :charges
      get :nouvelle_charge
      get :change_assistanat_reconnu
      get :inc_groupes_th
      get :inc_groupes_th
      get :dec_groupes_th
      get :inc_groupes_labo
      get :dec_groupes_labo     
      post :create_charge
      get :edit_charge
      put :update_charge
      patch :update_charge
      get :destroy_charge         
      get :up_groupe   
    end
  end
  
  resources :charge_effectives
  resources :type_activites
  resources :module_unites
  resources :modules
  resources :desiderata_projets
  resources :type_categories
  resources :suivis
  resources :type_engagements
  resources :contrat_mensuels
  resources :mois
  resources :constantes
  resources :cst_heures
  resources :desideratas
  resources :competence_domaines
  resources :drag_drop
  resources :niveau_competences
  resources :type_personnes
  resources :type_periodes
  resources :evenement_personnes
  resources :type_evenement_personnes
  resources :data_annee_civiles
  resources :annee_civiles
  resources :periodes
  resources :type_unite_enseignements
  resources :orientations
  resources :filieres
  resources :annee_academiques
  resources :departements

  # Ajout du module de planification
  resources :configurations_planifications
  resources :planifications do
    collection do
      get 'getRessources', :defaults => { :format => 'json' }
      get 'getProjets', :defaults => { :format => 'json' }
    end
  end

  resources :notifications
  resources :commentaires
  resources :jalons



  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # TODO : supprimer ça une fois le refactoring terminé pour rendre l'application RESTful
  # match ':controller(/:action(/:id(.:format)))', :method => :get
  get ':controller(/:action(/:id(.:format)))'

end

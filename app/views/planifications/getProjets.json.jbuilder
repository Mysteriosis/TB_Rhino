json.projets @projets do |projet|
  # Garde uniquement les projets planifiables
  if !projet.est_de_type_absence? and !projet.est_un_projet_academique? and !projet.est_un_projet_enseignement then
    json.id projet.id
    json.nom projet.id_interne
    json.type do
      json.extract! projet.type_projet, :id, :nom, :genre_projet
    end

    # Pour les projets dépliés...
    if @deplier.include? projet.id.to_s then

      # Récupère les affectations planifiables dans la fourchette de temps. Sépare les multiples affectations pour la même personne
      json.affectations projet.affectation_projets.order(display_label: :asc).joins(:projet => :type_projet).where{((date_debut.lteq my{@limite_a}) | (date_debut.eq nil)) & ((date_fin.gteq my{@limite_de}) | (date_fin.eq nil)) & (affectation_projets.est_active.eq 1) & (type_projets.nom.not_eq 'Absences')} do |affectation|
        if !affectation.engagement.nil? then
          json.id affectation.id
          json.label affectation.display_label

          json.type_engagement do
            json.id affectation.engagement.type_engagement_id
            json.nom affectation.engagement.display_name
          end

          json.ressource do
            json.id affectation.personne.id
            json.nom affectation.personne.initiale

            # Récupère les planifications dans la fourchette de temps
            json.planif affectation.planifications.where{(date.gteq my{@limite_de}) & (date.lteq my{@limite_a})} do |planification|
              json.id planification.id
              json.date planification.date
              json.semaine planification.date.strftime("%W")
              json.valeur planification.valeur
              json.commentaire do
                if !planification.commentaire.nil? then
                  json.extract! planification.commentaire, :id, :texte
                else
                  json.nil!
                end
              end
            end

            # Récupère les saisies d'heures dans la fourchette de temps
            json.hSaisies affectation.plage_horaire_saisies.where{(debut.gteq my{@limite_de}) & (fin.lteq my{@limite_a})} do |saisie|
              json.date saisie.debut.to_date.beginning_of_week
              json.semaine saisie.debut.to_date.beginning_of_week.strftime("%W")
              json.de saisie.debut.to_date
              json.a saisie.fin.to_date
              json.commentaire saisie.commentaire
              json.pourcentage_journalier CstHeure.pourcentage_journalier(saisie.heures).round(0)
              json.pourcentage_hebdomadaire CstHeure.pourcentage_hebdomadaire(saisie.heures).round(0)
            end

            # Récupère les jalons dans la fourchette de temps
            json.jalons Jalon.joins{affectation_projet}.where{(date.gteq my{@limite_de}) & (date.lteq my{@limite_a}) & (affectation_projets.id.eq my{affectation.id})} do |jalon|
              json.date jalon.date
              json.semaine jalon.date.to_date.beginning_of_week.strftime("%W")
              json.titre jalon.titre
              json.description jalon.description
            end
          end
        end
      end
    else
      json.affectations []
    end
  end
end

# Infos temporelles
json.dates do
  json.semaines @semaines do |semaine|
    json.extract! semaine, :de, :a, :num, :jours
  end
  json.now Date.today.strftime("%W")
  json.de @limite_de
  json.a @limite_a
end

# Paramètres de configuration de l'application
json.appInfos do
  json.user current_user.initiale
  json.config do
    if(@filtre) then
      json.filtre @filtre
    else
      json.filtre []
    end
    json.date @semaine
    json.edit @edit
    json.deplier @deplier
    json.mode @mode
    json.appFormat @appFormat
    json.heuresCalcul ConfigurationsPlanification.where{variable.eq 'heures_calcul'}.first!.valeur.to_i
    json.colorisation !ConfigurationsPlanification.where{variable.eq 'colorisation'}.first!.valeur.to_i.zero?
  end
end
json.ressources @ressources do |ressource|
  json.id ressource.id
  json.nom ressource.nom
  json.prenom ressource.prenom
  json.initiale ressource.initiale

  if @deplier.include? ressource.initiale then

    # Récupère les affectations planifiables dans la fourchette de temps.
    json.affectations ressource.affectation_projets.order(display_label: :asc).joins(:projet => :type_projet).where{((date_debut.lteq my{@limite_a}) | (date_debut.eq nil)) & ((date_fin.gteq my{@limite_de}) | (date_fin.eq nil)) & (affectation_projets.est_active.eq 1) & (type_projets.nom.not_eq 'Absences')} do |affectation|
      if !affectation.engagement.nil? && !affectation.projet.is_archive? then
        json.id affectation.id

        json.type_engagement do
          json.id affectation.engagement.type_engagement_id
          json.nom affectation.engagement.display_name
        end

        json.projet do
          json.id affectation.projet.id
          json.nom affectation.display_label

          # Récupère les planifications dans la fourchette de temps
          json.planif affectation.planifications.where{(date.gteq my{@limite_de}) & (date.lteq my{@limite_a})} do |planification|
              json.id planification.id
              json.date planification.date
              json.semaine planification.date.strftime("%W")
              json.valeur planification.valeur
              json.commentaire do
                if !planification.commentaire.nil? then
                  json.extract! planification.commentaire, :id, :texte
                else
                  json.nil!
                end
              end
          end

          # Récupère les saisies d'heures dans la fourchette de temps
          json.hSaisies affectation.plage_horaire_saisies.where{(debut.gteq my{@limite_de}) & (fin.lteq my{@limite_a})} do |saisie|
            json.date saisie.debut.to_date.beginning_of_week
            json.semaine saisie.debut.to_date.beginning_of_week.strftime("%W")
            json.de saisie.debut.to_datetime
            json.a saisie.fin.to_datetime
            json.commentaire saisie.commentaire
            json.pourcentage_journalier CstHeure.pourcentage_journalier(saisie.heures).round(0)
            json.pourcentage_hebdomadaire CstHeure.pourcentage_hebdomadaire(saisie.heures).round(0)
          end

          # Récupère les jalons dans la fourchette de temps
          json.jalons Jalon.joins{affectation_projet}.where{(date.gteq my{@limite_de}) & (date.lteq my{@limite_a}) & (affectation_projets.id.eq my{affectation.id})} do |jalon|
            json.date jalon.date
            json.semaine jalon.date.to_date.beginning_of_week.strftime("%W")
            json.titre jalon.titre
            json.description jalon.description
          end
        end
      end
    end

    # Récupère les affectations aux projets de gestion des absences dans la fourchette de temps.
    json.affectations_absences ressource.affectation_projets.joins(:projet => :type_projet).distinct.where{((date_debut.lteq my{@limite_a}) | (date_debut.eq nil)) & ((date_fin.gteq my{@limite_de}) | (date_fin.eq nil)) & (projets.est_archive.eq nil) & (type_projets.nom.eq 'Absences')} do |affectation_abs|
      json.id affectation_abs.id
      json.projet do
        json.nom affectation_abs.display_label

        # Récupère les saisies d'heures (absences planifiées) dans la fourchette de temps
        json.absences affectation_abs.plage_horaire_saisies.where{(debut.gteq my{@limite_de}) & (fin.lteq my{@limite_a})} do |absence|
          json.date absence.debut.to_date.beginning_of_week
          json.semaine absence.debut.to_date.beginning_of_week.strftime("%W")
          json.de absence.debut.to_datetime
          json.a absence.fin.to_datetime
          json.pourcentage_journalier CstHeure.pourcentage_journalier(absence.heures).round(0)
          json.pourcentage_hebdomadaire CstHeure.pourcentage_hebdomadaire(absence.heures).round(0)
        end
      end
    end
  else
    json.affectations []
    json.affectations_absences []
  end
end

# Infos temporelles
json.dates do
  json.semaines @semaines do |semaine|
    json.extract! semaine, :de, :a, :num, :jours
  end
  json.now Date.today.strftime("%W")
  json.de @limite_de
  json.a @limite_a
end

# Paramètres de configuration de l'application
json.appInfos do
  json.user current_user.initiale
  json.config do
    if(@filtre) then
      json.filtre @filtre
    else
      json.filtre []
    end
    json.date @semaine
    json.edit @edit
    json.deplier @deplier
    json.mode @mode
    json.appFormat @appFormat
    json.heuresCalcul ConfigurationsPlanification.where{variable.eq 'heures_calcul'}.first!.valeur.to_i
    json.colorisation !ConfigurationsPlanification.where{variable.eq 'colorisation'}.first!.valeur.to_i.zero?
  end
end
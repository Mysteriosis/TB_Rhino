# -*- encoding : utf-8 -*-
class CstHeure < ActiveRecord::Base
  	def self.HJ(annee_civile)
    # Retourne l'horaire journalier officiel
    	
    	_HJ = where({:sigle => "HJ", :annee_civile => annee_civile}).first
    	if !_HJ
      		raise "Erreur : pas de constante \"HJ: Horaire journalier officiel\" "
    	end
    	return _HJ.valeur.to_f
  	end

    def self.seuil_veteran(annee_civile)
    # Retourne l'age charnière à partir duquel on devient vétéran
      
      seuil_veteran = where({:sigle => "Seuil_veteran", :annee_civile => annee_civile}).first
      if !seuil_veteran
          raise "Erreur : pas de constante \"Seuil_veteran: Age charnière pour bénéficier de la tranche 2 des vacances\" "
      end
      return seuil_veteran.valeur.to_i
    end

  	def self.JV_1(annee_civile)
    # Retourne le nombre de jour de vacances pour la première tranche (en dessous de l'âge véréran)
    	
    	_JV_1 = where({:sigle => "JV_1", :annee_civile => annee_civile}).first
    	if !_JV_1
      		raise "Erreur : pas de constante \"JV_1: Nombre de jours de vacances pour la tranche 1 - < 60 ans\" "
      end
      return _JV_1.valeur.to_i
    end

    def self.JV_2(annee_civile)
    # Retourne le nombre de jour de vacances pour la première tranche (à partir de l'âge véréran)
    	
    	_JV_2 = where({:sigle => "JV_2", :annee_civile => annee_civile}).first
    	if !_JV_2
      		raise "Erreur : pas de constante \"JV_1: Nombre de jours de vacances pour la tranche 2 - >= 60 ans\" "
      end
      return _JV_2.valeur.to_i
    end

    def self.compensation_obligatoire?(annee_civile)
    compensation = where({:sigle => "Compensation", :annee_civile => annee_civile}).first
    if !compensation
      raise "Erreur : pas de constante \"Compensation\" "
    end
    return compensation.valeur.eql?("Obligatoire")
  end

  def self.compensation_supprimee?(annee_civile)
    compensation = where({:sigle => "Compensation", :annee_civile => annee_civile}).first
    if !compensation
      raise "Erreur : pas de constante \"Compensation\" "
    end
    return compensation.valeur.eql?("Supprimee")
  end

  def self.compensation_libre?(annee_civile)
    compensation = where({:sigle => "Compensation", :annee_civile => annee_civile}).first
    if !compensation
      raise "Erreur : pas de constante \"Compensation\" "
    end
    return compensation.valeur.eql?("Libre")
  end

  #Ajouts par PAC pour le module de planification
  def self.pourcentage_hebdomadaire(valeur)
    self.pourcentage_journalier(valeur/5)
  end

  def self.pourcentage_journalier(valeur)
    charge = self.HJ(Date.today.year)
    if charge != 0
      return (valeur * 100 / charge)
    else
      return 0
    end
  end
end
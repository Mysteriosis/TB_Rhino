# -*- encoding : utf-8 -*-
class DataFile < ActiveRecord::Base
  
  #---------------------------------------------------------------------------------------
  # Nom : .save
  # But : Permet d'enregistrer le pdf sur le serveur
  #---------------------------------------------------------------------------------------
  def self.save(upload, extension, abreviation)
    
    directory = "public/pdf/fiches_unites"
    name =  abreviation.to_s + "." + extension
    
    # Création du chemin avec le pdf
    path = File.join(directory, name)

    # Dépose le pdf sur le serveur
    File.open(path, "wb") { |f| f.write(upload.read) }
    
  end
end

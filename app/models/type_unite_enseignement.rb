# -*- encoding : utf-8 -*-
class TypeUniteEnseignement < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un type d'unité d'enseignement concerne plusieurs unités d'enseignement
  has_many :unite_enseignements
  
  def TypeUniteEnseignement.isCoursAChoix?(unite_enseignement)
    return unite_enseignement.type_unite_enseignement.id==2
  end
end

# -*- encoding : utf-8 -*-
class AnneeAcademique < ActiveRecord::Base

  has_one :departement
  has_one :filiere
  has_one :orientation
  has_one :module_unite
  has_one :periode
  has_one :constante
  has_one :desiderata_projet
  has_many :periodes
  
  def self.courante
    Variable.annee_academique_actuelle
  end
   
  def annee_civile
    AnneeCivile.where({:annee => annee[0..3]})
  end
  
  def display_name
    annee
  end
  
   #--------------------------------------------------------------------------------------------------------
   # Nom: liste_annees_academiques
   # But: retourne la liste des années académiques avec comme premier élément l'année courante
   # NOTE REFACTORING: Ancien nom = AnneeAcademique.order(:annee)
   #--------------------------------------------------------------------------------------------------------
   def AnneeAcademique.liste_annees_academiques
     annees_academiques = AnneeAcademique.find(:all)
     annee_ajd = AnneeAcademique.courante
     # Ajoute l'année actuelle à la première position
     tab_annees_academiques = []
     tab_annees_academiques[0]=annee_ajd
     i=1
     for annee in annees_academiques
       if annee.id != annee_ajd.id
         tab_annees_academiques[i]= annee
         i+=1
       end
     end

     return tab_annees_academiques

   end

   #--------------------------------------------------------------------------------------------------------
   # Nom: liste_annees_civiles_actuelles
   # But: retourne la liste des années civiles avec comme premier élément l'année courante
   #--------------------------------------------------------------------------------------------------------
   def AnneeAcademique.liste_annees_civiles_actuelles
     annees_civiles = AnneeCivile.find(:all)
     annee_ajd = AnneeAcademique.courante
     
     # Ajoute l'année actuelle à la première position
     tab_annees_civiles = []
     tab_annees_civiles[0]=annee_ajd
     i=1
     for annee in annees_civiles
       if annee.id != annee_ajd.id
         tab_annees_civiles[i]= annee
         i+=1
       end
     end

     return tab_annees_civiles

   end

   #--------------------------------------------------------------------------------------------------------
   # Nom: liste_annees_choix
   # But: retourne la liste des années academiques avec comme premier élément l'année souhaitée
   #--------------------------------------------------------------------------------------------------------
   def AnneeAcademique.liste_annees_academiques_choix(id_annee)
     annees_academiques = AnneeAcademique.find(:all)
     #annee_ajd = AnneeAcademique.annee_academique_actuelle
     
     # Ajoute l'année choisie en première position
     tab_annees_academiques = []
     i=1
     for annee in annees_academiques
       if annee.id != id_annee.to_i
         tab_annees_academiques[i]= annee
         i+=1
       else
         tab_annees_academiques[0]= annee
       end
     end

     return tab_annees_academiques

   end
   
end

# -*- encoding : utf-8 -*-
class TypeProjet < ActiveRecord::Base
  
  belongs_to :parent, :class_name => "TypeProjet"
  belongs_to :type_id_externe
  has_many :enfants_directs, :class_name => "TypeProjet", :foreign_key => :parent_id
  has_many :projets
  before_destroy :can_be_destroyed?
  after_create :instanciate_singleton
  
  def self.modes_calcul_charge
    ["Heures Effectuées", "Facteur", "Forfait"]
  end
  
  validates_presence_of :nom, :genre_projet, :message => "Champ requis"

  validates_uniqueness_of :nom, :message => "Un type de projet portant ce nom existe déjà"
  
  def has_enfants?
    enfants_directs.count > 0
  end
  
  # retourne tout les enfants directs et indirects
  def enfants
    enfants_directs + enfants_directs.collect{|ed| ed.enfants_directs}.flatten
  end
  
  # retourne true si le type de projet possède des projets actifs appartenant directement au type ou à un sous-type
  def has_descendance?(mode)
    has=false
    projets.each do |p|
      if mode.eql?("active")
        test=p.en_activite?
      elsif mode.eql?("inactive")
        test=!p.en_activite?
      elsif mode.eql?("archive")
        test=p.is_archive?
      end
      if test
          has=true
          break
      end
    end
    if has 
      return true          
    else
      has_descendance = false
      enfants_directs.each do |tpc|
        if tpc.has_descendance?(mode)
          has_descendance=true
          break
        end
      end
      return has_descendance
    end
  end
  
  # retourne tout les parents directs et indirects
  def parents
    parent ? parent.parents + [parent] : []
  end
  
  def parents_potentiels
    new_record? ? TypeProjet.where{id.gteq 0} : TypeProjet.where{id.not_in enfants.collect{|e| e.id} + [id]}
    #new_record? ? TypeProjet.where(:id.gteq => 0) : TypeProjet.where(:id.not_in => enfants.collect{|e| e.id} + [id])
  end
  
  def self.racines
    where{parent_id.eq nil}
  end
  
  # This has a complexity of O(n**2), if performance falls, get a O(n) 
  # complexity with denormalization of children count with a counter
  # and make it a relation filter with AREL
  def self.feuilles(array)
    array.select{|tp| tp.enfants_directs.count == 0}
  end
  
  def self.sans_singletons
    where{(est_singleton.eq false) | (est_singleton.eq nil)}
  end
  
  def self.genre(genre)
  # Retourne les genres de projets (catégories de haut-niveau)
    where{genre_projet.eq my{genre}}
  end
  
  def display_name
    nom
  end

  def validate
    
    if in_loop?
      errors.add(:parent, "Erreur de hiérarchie : boucle dans l'arbre des types de projets")
    end
    
    if erreur_genre?
      errors.add(:parent, "Erreur de hiérarchie : le genre du projet doit correspondre au genre du parent")
    end
  
  end
  
  def can_be_destroyed?
  
    # on ne peux pas supprimer un type de projet pour lequel il existe des projets
    if projets.count > 0
      if est_singleton?
        return projets.first.destroy
      end
      return false
    end
    
    # on ne peut pas supprimer un type de projet qui a un ou plusieurs sous-types de projet
    if enfants_directs.count > 0
      return false
    end
    
    return true
  end
  
  
private
  
  def instanciate_singleton
    if est_singleton? && valid?
      projets.create!(:id_interne => nom)
    end
  end
  
  def in_loop?
    current_node = parent
    while !current_node.nil?
      if current_node.parent == self
        return true
        errors.add(:parent, "Erreur de hiérarchie : boucle dans l'arbre des types de projets")
        break
      end
      current_node = current_node.parent
    end
  end
  
  def erreur_genre?
      type_parent = self.parent
        
      if !type_parent.nil?
          if type_parent.genre_projet.eql? self.genre_projet
            return false # OK: on reste dans le même genre
          else        
            case type_parent.genre_projet
              when "Projet"
                  if (self.genre_projet.eql?"ProjetEnseignement" or self.genre_projet.eql?"These")
                    return false # OK - Hiérarchie de genre respectée
                  else
                    return true
                  end
              when "ProjetEnseignement"
                  return true # ERREUR (pas de sous-type)
              when "These"
                  return true # ERREUR (pas de sous-type)
            end
          end
      end
      return false
  end

  
end

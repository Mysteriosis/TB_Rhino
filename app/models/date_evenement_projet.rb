# -*- encoding : utf-8 -*-
class DateEvenementProjet< ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # Une date d'événement de projet comprend l'événement et le projet
  belongs_to :projet
  belongs_to :evenement_projet
end

# -*- encoding : utf-8 -*-
class TypePersonne < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un type de personne concerne plusieurs personnes
  has_many :personnes
  
  def display_name
    typePersonne
  end
end

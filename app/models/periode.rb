# -*- encoding : utf-8 -*-
class Periode < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une période concerne une année académique
  belongs_to :annee_academique
  # une période concerne un type de période
  belongs_to :type_periode
  # une période concerne plusieurs unités d'enseignement
  has_many :unite_enseignements
  
  def is_en_hiver?
      return saison=='Hiver'
  end
  
  def annee_academique_de_base
    no_annee_scolaire=annee               # 1, 2, 3, ..
    if annee
      debut_annee_academique_courante= annee_academique.debut       # 2010, 2011, ...
      debut_annee_academique_origine= debut_annee_academique_courante - (annee - 1)
      return AnneeAcademique.where{debut.eq debut_annee_academique_origine}.first
    else
      return nil
    end
  end

  def is_en_ete?
    return saison=='Ete'
  end
end

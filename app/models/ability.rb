# -*- encoding : utf-8 -*-
class Ability
  include CanCan::Ability

  def initialize(personne)
    
    if personne.roles.include? :responsable_rh
      can :manage, Projet
    end
    if personne.roles.include? :professeur
      can [:update,:read], Projet, :chef => {:id => personne.id}
      # can [:read], Projet, :chef => {:id => personne.id}   # FOR IICT Dominique
    end

    
    if personne.has_role?(:collaborateur) ||
       personne.has_role?(:professeur) ||
       personne.has_role?(:secretaire)
      can :read, Personne
      can [:edit, :read, :update], PersonnePrive, :personne => {:id => personne.id}
      can [:read, :historique], Contrat, :personne => { :id => personne.id }
      can :read, CahierChargeAssistant, :personne => { :id => personne.id }
      can :gestion_competences, Personne, :id => personne.id
      can :read, UniteEnseignement
      can :read, InteretUnite
      can :consulter_heures_saisies, Personne, :id => personne.id
      can :modifier_heures_saisies, Personne, :id => personne.id
      can :resume_heures, Personne, :id => personne.id
      can :resume_heures, PlageContractuelle, :personne_id => personne.id
      can :get_calendrier_details, PlageContractuelle
      can :resume_heures_annee_precedente, Personne, :id => personne.id
      can :resume_conso, Personne, :id => personne.id
      can :resume_conso_graph_data, Personne, :id => personne.id
      can :show_charges, Personne, :id => personne.id
      can :show, AffectationProjet, :personne_id => personne.id
    end
    
    if personne.roles.include? :professeur
      can [:read, :index_global], Contrat
      can :read, CahierChargeAssistant
      can :show_comp, Personne
      can :read, AffectationProjet
      can :manage, AffectationProjet, :projet => {:chef_id => personne.id}
      # can :read, AffectationProjet, :projet => {:chef_id => personne.id}   # FOR IICT Dominique
      can :consulter_heures_saisies, Personne

      # Vision des charges effectives et prévisionnelles
      can :manage, Charge

      # Plan d'enseignement
      can :read, :enseignement
      can :read, UniteEnseignement
      can :liste_eduardo, UniteEnseignement
      can :read, Departement
      can :read, Filiere
      can :read, Orientation

      # Vision des projets
      can :read, Projet
      can :read, Budget
      can :read, AffectationProjet
    end
    
    if personne.roles.include? :charge_de_cours
      can :edit, Personne, :id => personne.id
    end
    
    if personne.roles.include? :diplomant
      can :edit, Personne, :id => personne.id
    end
    
    if personne.roles.include? :secretaire
      can [:read, :historique], Contrat
      can :read, CahierChargeAssistant
      can [:read], Personne
      can :read, PersonnePrive
      can :read, Projet
      can :read, Budget
      can :read, AffectationProjet
    end
    
    if personne.roles.include? :responsable_rh
      can :manage, Contrat
      can :manage, Engagement
      can :manage, CahierChargeAssistant
      can :manage, Budget
      can :manage, Charge
      can :manage, AffectationProjet
      can :manage, :affectations_en_general
      can :manage, Personne
      cannot [:edit_roles, :droits], Personne
      can :manage, PersonnePrive
      can :magage, PlageHoraireSaisie
      can :manage, :exportation_heures_sagex
      can :manage, :configurationRH
      can :manage, :creationChargesEffectives
      can :manage, :supprimer_heures_saisies
      can :edit_horaire, Personne
      can :manage, :gestion_des_heures
      can :manage, PlageContractuelle

      # Visibilité sur l'enseignement
      can :read, :enseignement
      can :read, UniteEnseignement
      can :liste_eduardo, UniteEnseignement
      can :read, Departement
      can :read, Filiere
      can :read, Orientation
    end
    
    
    if personne.roles.include? :responsable_enseignement
      can :edit, Personne, :id => personne.id
      can :manage, :enseignement
      can :manage, Charge
      can :manage, UniteEnseignement
      can :manage, Departement
      can :manage, Filiere
      can :manage, Orientation
      can :manage, UniteEnseignementOrientation
      can :read, Desiderata
      can :manage, ChargeEffective
      can :manage, RoadMap
      can :show_charges, Personne
      can :gestion_desideratas, Personne
      can :update_comp, Personne
      can :manage, :configurationEnseignement  
      can :manage, :personnes_fictives
    end
    
    if personne.roles.include? :admin
      can [:read,:droits,:edit_roles,:update_roles], Personne
      can :manage, :configuration
      can :manage, [Periode,
                    TypeIdExterne, 
                    TypeProjet, 
                    TypeBudget, 
                    AnneeAcademique, 
                    AnneeCivile, 
                    Moi, 
                    TypePersonne, 
                    TypeEvenementPersonne, 
                    TypeEngagement,
                    TypeUniteEnseignement, 
                    TypePeriode,
                    NiveauCompetence,
                    InteretUnite,
                    TypeActivite,
                    CompetenceDomaine,
                    AxeStrategique,
                    Constante,
                    CstHeure,
                    JourAcademique]
    end


    # Ajout du module de planification
    if personne.roles.include? :collaborateur
      can :read, [Planification,
                  Jalon,
                  Commentaire]
    end

    if personne.roles.include? :professeur
      can :read, [Planification,
                  Jalon,
                  Commentaire]
    end

    if personne.isChefProjet?
      can :manage, [Planification,
                    Jalon,
                    Commentaire]
    end

    if personne.roles.include? :responsable_rh or
       personne.roles.include? :admin
      can :manage, [Planification,
                    Jalon,
                    Commentaire,
                    Notification,
                    Mail]
    end

  end
end

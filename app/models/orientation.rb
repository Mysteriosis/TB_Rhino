# -*- encoding : utf-8 -*-
class Orientation < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une orientation concerne une année
  belongs_to :annee_academique
  # une orientation possède plusieurs unite d'enseignements
  has_many :unite_enseignements, :through =>:unite_enseignement_orientations
  has_many :unite_enseignement_orientations
  # une orientation concerne une filière
  belongs_to :filiere 
  
end

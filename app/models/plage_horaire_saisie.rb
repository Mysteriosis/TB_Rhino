# -*- encoding : utf-8 -*-
class PlageHoraireSaisie < ActiveRecord::Base
  has_event_calendar :start_at_field => :debut, :end_at_field => :fin
  
  belongs_to :affectation_projet
  
  validates_presence_of :heures, :affectation_projet, :debut, :fin, :message => "Champ requis"
  validates_numericality_of :heures, :message => "les heures doivent être un nombre"
  
  validate :fin_apres_debut
  validate :dans_limites_affectation
  validate :max_24h_par_jour

  #validate :dans_plage_saisie_autorisee
  #validate :pas_de_chevauchement
  
  # retourne les plages horaires saisies entre deux moments
  def self.entre(h_debut, h_fin)
    where{
      (debut.gteq my{h_debut.to_time}) & (fin.lteq my{h_fin.to_time})
    }
  end
  
  def color
    self.affectation_projet.display_color
  end
  
  def self.heures_pour_select(personne)
    
    time = Time.parse("01.01.01")
    if personne.personne_prive.saisie_par_pas_de_15mn
      increment = 15
    else
      increment = 1
    end
    res = []
    
    (0..1439).each do |m|
      if m % increment == 0
        res << [(time+m.minutes).strftime("%Hh%M"),m]
      end
    end
    
    return res
    
  end
  
  def self.fichier_excel_sagex(date_debut, date_fin, avec_commentaires)
    collaborateurs_avec_saisie = Hash.new
    book = Spreadsheet::Workbook.new
    sheet = book.create_worksheet
    sheet.name = 'Heures'
    sheet.row(0).push "N° école \n(number5)", 
                      "Collaborateur", 
                      "N° Interne \nCollaborateur \n(Number 5)", 
                      "N° Sagex \nHesso \n(number 5)", 
                      "Nom projet",
                      "Type projet", 
                      "N° Activité\n(number 5)", 
                      "Heures", 
                      #"Heures (en centième)", 
                      "Date\n(jj.mm.aaaa)",
                      "Nom",
                      "Prenom", 
                      "Heures", 
                      "Contrat"
    if avec_commentaires
      sheet.row(0).push "Commentaires"
    end
    i = 1
    AffectationProjet.pour_exportation_sagex.each do |ap|
      plages_horaires = ap.plage_horaire_saisies.entre(date_debut, date_fin)
      heures = 0.0
      commentaires =""
      plages_horaires.each do |plage|
        heures += plage.heures
        if plage.commentaire and plage.commentaire !=""
          commentaires=commentaires + "\n" + plage.debut.in_time_zone.to_formatted_s(:jour_mois_annee) + " " + self.heure_en_format_heures_minutes(plage.heures) + " " + plage.commentaire
        end
      end
  
      if heures > 0.0
        collaborateurs_avec_saisie[ap.personne.initiale]=true
        sheet.row(i).push 3, 
                          ap.personne.initiale, 
                          ap.personne.id_sagex, 
                          ap.projet.id_sagex, 
                          ap.projet.id_interne,
                          nil, 
                          nil, 
                          heures.round(2), 
                          date_fin.strftime("%d.%m.%Y"), 
                          ap.personne.nom,
                          ap.personne.prenom,
                          "#{heures.truncate}h#{((heures%1)*60).to_i}m",  
                          ap.engagement ? ap.engagement.type_engagement.libelle : "Aucun"
        if avec_commentaires
          sheet.row(i).push commentaires
        end
                          
        i += 1
      end
    end 
    
    # Afficher les collaborateurs actifs qui n'ont pas saisi d'heures pendant cette période
    if Configuration.feuille_sagex_avec_personnes_sans_saisie?
      collaborateurs_actifs = Personne.collaborateurs_actifs_de_linstitut
      collaborateurs_actifs.each do |collabo|
        if !collaborateurs_avec_saisie.has_key?(collabo.initiale)
            heures=0.0
            sheet.row(i).push 3, 
                              collabo.initiale, 
                              collabo.id_sagex, 
                              nil, 
                              nil,
                              nil, 
                              nil, 
                              0, 
                              nil, 
                              collabo.nom,
                              collabo.prenom,
                              "#{heures.truncate}h#{((heures%1)*60).to_i}m",  
                              nil
            i += 1
        end
      end
    end
    ## -------------------------------------------------------------------------------------
    
    return book
  end
  
  def calculer_heures
    self.heures = (fin - debut) / 3600    
  end
  
  def saisie_dans_plage_gelee
  # Voir méthode suivante
    erreur_validation=Hash.new
    date_saisie_autorisee_min=Variable.date_saisie_autorisee_min
    date_saisie_autorisee_max=Variable.date_saisie_autorisee_max
    if debut.to_date < date_saisie_autorisee_min
      erreur_validation[:debut]= "Saisie gelée ( < "+date_saisie_autorisee_min.strftime("%d %b %Y")+")"
    end
    if fin.to_date > date_saisie_autorisee_max
      erreur_validation[:fin]= "Saisie gelée ( > "+date_saisie_autorisee_max.strftime("%d %b %Y")+")"
    end
    return erreur_validation
  end

private 

  # on ne check que le début et la fin, en présuposant que le début est avant la fin de toute manière
  def dans_limites_affectation   
    if affectation_projet.date_debut && affectation_projet.date_debut > debut.to_date
      errors.add(:debut, "Hors dates affectation")
    end
    
    
    if affectation_projet.date_fin && affectation_projet.date_fin < fin.to_date
      errors.add(:fin, "Hors dates affectation")
    end
  end
  
   def dans_plage_saisie_autorisee
     date_saisie_autorisee_min=Variable.date_saisie_autorisee_min
     date_saisie_autorisee_max=Variable.date_saisie_autorisee_max
     if debut.to_date < date_saisie_autorisee_min
       errors.add(:debut, "Saisie gelée ( < "+date_saisie_autorisee_min.strftime("%d %b %Y")+")")
     end
     if fin.to_date > date_saisie_autorisee_max
       errors.add(:debut, "Saisie gelée ( > "+date_saisie_autorisee_max.strftime("%d %b %Y")+")")
     end
   end
  
  # il ne peut pas y avoir deux plages horaire avec un zone temporelle commune pour la même personne
  # (à part pour le Dr. Manathan !) => (P.A. Ca s'écrit "Manhattan"...)
  def pas_de_chevauchement
    if affectation_projet.personne.plage_horaire_saisies.where{
        (debut.lt fin) & (fin.gt debut) & (id.not_eq my{id})
      }.count > 0
      errors.add(:fin, "Chevauchement")
    end
    
  end
  
  def max_24h_par_jour
    total_heures=heures + affectation_projet.personne.plage_horaire_saisies.where{
      (id.not_eq id) & 
      (debut.gteq debut.at_midnight) & 
      (fin.lt (debut+1.day).at_midnight)}.sum(:heures)
    
    if total_heures > Configuration.max_saisie_par_jour
      errors.add(:fin, "#{heures}h - ""Plus de #{Configuration.max_saisie_par_jour }h en 1 jour")
    end
  end

  
  def fin_apres_debut
    if debut && fin && debut > fin
      errors.add(:fin, "Le début doit être avant la fin")
    end
  end
  
  def self.heure_en_format_heures_minutes(heure)
    format_sortie=""
    heure=heure.to_f
    heure_negative=heure<0
    if heure_negative 
      heure=-heure
    end
    hh=heure.to_i
    mm=((heure%1)*60).to_i
    if hh==0
      if mm==0 
        format_sortie= "0"
      else 
        format_sortie= "#{mm}m" 
      end
    end
    if hh!=0 
      if mm==0 
        format_sortie= "#{hh}h"  
      else
        format_sortie= "#{hh}h#{mm}"
      end
    end
    if heure_negative
      format_sortie="-"+format_sortie
    end
    return format_sortie
  end

end

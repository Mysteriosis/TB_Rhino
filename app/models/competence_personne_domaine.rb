# -*- encoding : utf-8 -*-
class CompetencePersonneDomaine < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une compétence d'un domaine concerne un niveau de compétence
  belongs_to :niveau_competence
end

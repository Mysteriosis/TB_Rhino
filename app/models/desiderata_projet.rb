# -*- encoding : utf-8 -*-
class DesiderataProjet < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un desiderata de projet concerne une année académique
  belongs_to :annee_academique
  # un desiderata de projet concerne une personne
  belongs_to :personne
end

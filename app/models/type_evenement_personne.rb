# -*- encoding : utf-8 -*-
class TypeEvenementPersonne < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un type d'événement concerne plusieurs événements
  has_one :evenement_personne
end

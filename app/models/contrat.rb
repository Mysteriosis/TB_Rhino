# -*- encoding : utf-8 -*-
class Contrat < ActiveRecord::Base
  
  belongs_to :engagement
  belongs_to :personne
  
  validates_presence_of :engagement_id, :start_date, :message => "Champ requis"
  validates_numericality_of :pourcentage, :only_integer => true, :message => "Doit être un nombre entier"
  validates_inclusion_of :pourcentage, :in => 10..100, :message => "Doit être compris entre 10 et 100"
  
  def self.en_cours(date = Date.today)
    where do
      (start_date.lteq date) & 
      ((end_date.eq nil) | (end_date.gteq date))
    end
  end

  def self.en_cours_dannee(annee)
  # Retourne les contrats qui recouvrent une année spécifique
    premier_janvier = Date.new(annee, 1, 1)
    trente_et_un_decembre = Date.new(annee, 12, 31)
    (where{
      ((start_date.lteq premier_janvier) & (end_date.eq nil)) |
      ((start_date.lteq premier_janvier) & (end_date.not_eq nil) & (end_date.gteq premier_janvier)) |
      ((start_date.gteq premier_janvier) & (start_date.lteq trente_et_un_decembre)) 
    }).sort_by(&:start_date)
  end
  
  def self.en_cours_ou_futurs(date = Date.today)
    where{
      (end_date.eq nil) | (end_date.gteq date)
    }
  end
  
  def self.futurs(date = Date.today)
    where{
      (start_date.gt date) &
      ((end_date.eq nil) | (end_date.gteq date))
    }
  end 
  
  def self.actuels
    self.en_cours
  end 
  
  def self.taux_activite_actuel
    res=0
    self.en_cours.each do |contrat|
      res+=contrat.pourcentage
    end
    return res
  end
   
  def self.actuels_ou_futurs
    self.en_cours_ou_futurs
  end
  
  def display_name
    "#{engagement.type_engagement.libelle} #{pourcentage}%"
  end
  
  def Contrat.types_engagements_possibles(personne)
    # Retourne les types d'engagements enocre possible pour la personne
    types_engagements_possibles=Array.new
    types_engagements_de_la_personne=Hash.new
    personne.engagements.each do |eng|
      types_engagements_de_la_personne[eng.type_engagement.sigle]=1
    end
    TypeEngagement.all.each do |te|
      if types_engagements_de_la_personne[te.sigle]==nil
        types_engagements_possibles<<te
      end
    end
    return types_engagements_possibles
  end

 
  #def Contrat.types_contrats_possibles_pour_collaborateur_pendant_périodes?(collabo, date1, date2, indetermine)
  #  # Retourner les types de contrats possibles pendant la période donnée, à savoir tous les types de contrat sauf ceux dàjà utilisés
  #   #Récupérer les contrats de la personne chevauchant la période par un jour au moins
  #   if (date2==nil or indetermine)
  #     contrats_de_la_personne = Contrat.find(:all, :conditions => ["personne_id = ? AND (end_date IS ? OR end_date >= ? )", collabo, nil, date1])
  #   else
  #     contrats_de_la_personne = Contrat.find(:all, :conditions => ["personne_id = ? AND ((end_date >= ? AND end_date <= ?) OR (end_date >= ? AND start_date <= ?) OR (end_date IS ? AND start_date <= ?))", collabo, date1, date2, date2, date2, nil, date2])
  #   end
  #   #Liste contenant tous les types de contrats possibles
  #   types_de_contrats = TypeContrat.find(:all)
#
  #   #Liste contenant tous les types de nouveaux contrats autorisés pour la personne concernée
  #   types_de_contrats_possibles = []
#
  #   i=0
  #   for typeContrat in types_de_contrats
  #     contrat_pris = false
  #     for contrat in contrats_de_la_personne
  #       if contrat.type_contrat_id == typeContrat.id
  #         contrat_pris = true
  #       end
  #     end
  #     if contrat_pris == false
  #       types_de_contrats_possibles[i] = typeContrat
  #       i+=1
  #     end
  #   end
  #   return types_de_contrats_possibles
  #end
  #
  #def Contrat.types_contrats_possibles_pour_collaborateur_pendant_périodes_minus_contrat?(contrat, collabo, date1, date2, indetermine)
  #  # Retourner les types de contrats possibles pendant la période donnée, sans tenir compte du contrat "contrat" indidué en paramètre
  #  # REFACTORER: le code diffère de la précédente méthode uniquement dans les find sql
  #    #Récupérer les contrats de la personne chevauchant la période par un jour au moins
  #    if (date2==nil or indetermine)
  #      contrats_de_la_personne = Contrat.find(:all, :conditions => ["id != ? AND personne_id = ? AND (end_date IS ? OR end_date >= ? )", contrat.id, collabo, nil, date1])
  #    else
  #      contrats_de_la_personne = Contrat.find(:all, :conditions => ["id != ? AND personne_id = ? AND ((end_date >= ? AND end_date <= ?) OR (end_date >= ? AND start_date <= ?) OR (end_date IS ? AND start_date <= ?))", contrat.id, collabo, date1, date2, date2, date2, nil, date2])
  #    end
#
  #    #Liste contenant tous les types de contrats possibles
  #    types_de_contrats = TypeContrat.find(:all)
#
  #    #Liste contenant tous les types de nouveaux contrats autorisés pour la personne concernée
  #    types_de_contrats_possibles = []
#
  #    i=0
  #    for typeContrat in types_de_contrats
  #      contrat_pris = false
  #      for contrat in contrats_de_la_personne
  #        if contrat.type_contrat_id == typeContrat.id
  #          contrat_pris = true
  #        end
  #      end
  #      if contrat_pris == false
  #        types_de_contrats_possibles[i] = typeContrat
  #        i+=1
  #      end
  #    end
  #    return types_de_contrats_possibles
  # end
  
  #def Contrat.types_de_contrats?
  #  return Contrat.find(:all)
  #end
end



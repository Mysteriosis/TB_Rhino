# -*- encoding : utf-8 -*-
class Suivi < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un suivi concerne un projet
  belongs_to :projet
  # un suivi concerne une personne
  belongs_to :personne
  # un suivi concerne un mois
  belongs_to :moi
  # un suivi possède une activité
  belongs_to :type_activite
  

end

# -*- encoding : utf-8 -*-
class CahierChargeAssistant < ActiveRecord::Base
  
  belongs_to :personne
  belongs_to :annee_civile
  
#  validates_numericality_of :heures_ens,    :greater_than_or_equal_to => 0, :less_than => 1871, :message => "Erreur, doit être numérique positif plus petit ou égal  1870"
#  validates_numericality_of :heures_infra,  :greater_than_or_equal_to => 0, :less_than => 1871, :message => "Erreur, doit être numérique positif plus petit ou égal  1870"
#  validates_numericality_of :heures_RD,     :greater_than_or_equal_to => 0, :less_than => 1871, :message => "Erreur, doit être numérique positif plus petit ou égal  1870"
  
  # Permet de connaitre les cahiers des charges pour une année civile
  def self.pour_annee_civile(annee_civile)
    where(:annee_civile_id => annee_civile.id)
  end
  
end
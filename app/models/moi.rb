# -*- encoding : utf-8 -*-
class Moi < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un mois concerne une année civile
  belongs_to :annee_civile
  # un mois concerne plusieurs contrats
  has_one :contrat_mensuel
  # un mois concerne plusieurs contrats
  has_one :contrat_annuel
  # un mois concerne plusieurs suivi
  has_one :suivi
  
  def debut
    Date.new(annee_civile.annee.to_i,num.to_i,1).to_time
  end
  
  def fin
    Date.new(annee_civile.annee.to_i,num.to_i,1)+1.month-1.second
  end
  
  def self.courant
    self.where{(num.eq Date.today.month.to_s) & (annee_civile_id.eq AnneeCivile.courante.id)}.first
  end

  def self.display_name(no_mois)
  # Retourne le nom du mois, pour affichage
  # Le no_mois étant un entier valant 1..12
    labels=["Janv", "Fev", "Mars", "Avr", "Mai", "Juin", "Juil", "Août", "Sept", "Oct", "Nov", "Dec"]
    return labels[no_mois-1]
  end
  
end

# -*- encoding : utf-8 -*-
class AxeStrategique < ActiveRecord::Base
  has_many :personnes
  
  validates_presence_of :nom, :sigle, :message => "Champ requis"

  def display_name
    nom
  end

  
  def suppression_empechements
    empechements =""
    if personnes.count > 0 then 
      empechements+= (personnes.count.to_s() + " personne(s) associée(s)")
    end
    empechements=="" ? nil : empechements
  end
end

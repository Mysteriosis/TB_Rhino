# -*- encoding : utf-8 -*-
class Configuration < ActiveRecord::Base
  # ----------------------------------------------------------------
  # Constantes de configuration
  # Indépendantes des années académiques et civiles 
  # -----------------------------------------------------------------
  # Création ELS - 5 Nov 2012
  # -----------------------------------------------------------------
  

  def self.max_saisie_par_jour 
    max_saisie = where({:description => "Max saisie par jour"}).first
    if !max_saisie
      raise "Erreur : pas de constante \"Max saisie par jour\" "
    end
    return max_saisie.valeur.to_f
  end
  
  def self.max_saisie_sup_24?
    return Configuration.max_saisie_par_jour > 24.0 
  end
  
  def self.saisie_avec_commentaire? 
    avec_commentaire = where({:description => "Saisie avec commentaire"}).first
    if !avec_commentaire
      raise "Erreur : pas de constante \"Saisie avec commentaire\" "
    end
    return avec_commentaire.valeur.eql?("true")
  end

  def self.afficher_heures_a_compenser? 
    afficher_heures_a_compenser = where({:description => "Afficher heures a compenser"}).first
    if !afficher_heures_a_compenser
      raise "Erreur : pas de constante \"Afficher heures a compenser\" "
    end
    return afficher_heures_a_compenser.valeur.eql?("true")
  end

  def self.feuille_sagex_avec_personnes_sans_saisie? 
    avec_personnes_sans_saisie = where({:description => "Sagex avec personnes sans saisie"}).first
    if !avec_personnes_sans_saisie
      raise "Erreur : pas de constante \"Sagex avec personnes sans saisie\" "
    end
    return avec_personnes_sans_saisie.valeur.eql?("true")
  end

  def self.annee_civile_initiale
    # Retourne un entier: 2009, 2010, etc..
    annee_civile = where({:description => "Annee civile initiale"}).first
    if !annee_civile
      raise "Erreur : pas de constante \"Annee civile initiale\" "
    end
    return annee_civile.valeur.to_i
  end

  def self.saisie_journaliere_par_defaut
    # Retourne la valeur par défaut d'une saisie d'heures en mode Mois
      
      saisie_journaliere = where({:description => "Saisie journaliere par defaut"}).first
      if !saisie_journaliere
          raise "Erreur : pas de constante \"Saisie journaliere par defaut\" "
      end
      return saisie_journaliere.valeur.to_f
  end


end
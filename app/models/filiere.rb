# -*- encoding : utf-8 -*-
class Filiere < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une filière concerne un département
  belongs_to :departement
  # une filière concerne une année académique
  belongs_to :annee_academique
  # une filière possède plusieurs orientations
  has_many :orientations
end

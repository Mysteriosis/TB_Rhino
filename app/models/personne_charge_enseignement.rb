# -*- encoding : utf-8 -*-
class PersonneChargeEnseignement < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # Un desiderata de personne comprend la personne et le desiderata
  belongs_to :personne
  belongs_to :charge_enseignement
end

# -*- encoding : utf-8 -*-
class NiveauCompetence < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une niveau de compétence concerne plusieurs compétences de domaines
  has_one :competence_personne_domaine
end

class PlanningRessourcesProjet < ActiveRecord::Base

  belongs_to :affectation_projet

  # Returns the planning entries for a specific affectation in the dates range
  def self.planning_weeks(affectation,date_from,date_to)
    PlanningRessourcesProjet.where{(date.gteq date_from) & (date.lteq date_to) & (affectation_projet_id.eq affectation.id)}  
  end

  # Returns the planning entry for the specific affectation and date (has to be a monday)
  def self.planning_week(affectation,date)
    PlanningRessourcesProjet.where{(date.eq date) & (affectation_projet_id.eq affectation.id)}
  end

  # Checks if the table contains any data (used to define if this feature is used or not)
  def self.is_empty?
    PlanningRessourcesProjet.count.zero?
  end 

end

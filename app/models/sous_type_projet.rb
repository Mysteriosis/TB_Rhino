# -*- encoding : utf-8 -*-
class SousTypeProjet < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un sous-type de projet concerne plusieurs types
  has_one :type_projet
end

# -*- encoding : utf-8 -*-
class TypeActivite < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une activité concerne plusieurs suivis
  has_many :suivis
  # un type d'activité concerne plusieurs types de projet
  has_many :type_projet_activites
  
  has_many :type_projets, :through => :type_projet_activites
end

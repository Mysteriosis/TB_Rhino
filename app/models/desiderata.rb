# -*- encoding : utf-8 -*-
class Desiderata < ActiveRecord::Base
  require 'fonctions'
  self.table_name = 'desideratas'
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  
  # un desiderata concerne une unité d'enseignement
  belongs_to :unite_enseignement
  
  # un desiderata est émis par un prof
  belongs_to :personne 
  # un desiderata peut être rattachés à plusieurs assistants
  has_many :personne_desideratas #NEW
  
  #has_many :personnes, :through => :personne_desideratas #NEW mais qui marhe pas
  #has_many :personne_desiderata  #OLD
  
  #--------------------------------------------------------------------------------------------------------
   # Nom: desideratas
   # But: Retourne tous les desiderata en relation avec les unites
   # Parametres : desiderata => liste des desiderata
   #              unites => liste des unites (relatives à une année académique spécifique)
   #--------------------------------------------------------------------------------------------------------
   def Desiderata.desideratas (desiderata, unites)
     # Liste de tous les desideratas liés aux unités d'ensignement
     tabble_desideratas=[]
     i=0
     for desideratum in desiderata
       for unite in unites
         if desideratum.unite_enseignement_id == unite.id
           table_desideratas[i]=desideratum
           i+=1
         end
       end
     end 
     return table_desideratas
   end
   
   def Desiderata.desideratas_annee (prof, annee)
     # Liste de tous les desiderata d'un prof pour une annéée académique donnéée
     table_desideratas=[]
     i=0
     les_desideratas=prof.desideratas
     for un_desiderata in les_desideratas 
        if un_desiderata.unite_enseignement.periode.annee_academique.id==annee.id
          table_desideratas[i]=un_desiderata
          i+=1
        end
     end
     return table_desideratas
   end
   
   def Desiderata.desideratas(annee_academique)
     desideratas= Desiderata.find(:all)
     liste_desideratas_annee=[]
     for desiderata in desideratas
       if desiderata.unite_enseignement.periode.annee_academique.id==annee_academique.id
         liste_desideratas_annee<<desiderata
       end
     end
     return liste_desideratas_annee
   end
   
   #--------------------------------------------------------------------------------------------------------
   # Nom: somme_heure_desideratum
   # But: Somme le nombre d'heures total d'un desideratum
   # Parametre : desideratum : desideratum a sommer
   #-------------------------------------------------------------------------------------------------------- 
   def Desiderata.somme_heures_desideratum (desideratum)
     # Constante concernée par le desideratum
     constante_concernee = Fonctions.facteur_actuel("Facteur_#{desideratum.unite_enseignement.type_unite_enseignement.type_unite}",desideratum.unite_enseignement.periode.annee_academique_id)
     # Calcul des heures
      unite=desideratum.unite_enseignement
       if unite.coursBloc
         valeur=
           (unite.nbPeriodesTheoriquesTotal*desideratum.tauxTheorique.to_f*
           desideratum.nbGroupes.to_f * constante_concernee)/100.0 +
           (unite.nbPeriodesPratiquesTotal*desideratum.tauxPratique.to_f*
           desideratum.nbGroupes.to_f * constante_concernee)/100.0 
         else
           valeur=
           (unite.nbPeriodesTheoriquesSemaine.to_f * 
           desideratum.tauxTheorique.to_f * unite.periode.dureeSemaine.to_f *
           desideratum.nbGroupes.to_f * constante_concernee)/100.0 +
           (unite.nbPeriodesPratiquesSemaine.to_f *
           desideratum.tauxPratique.to_f * unite.periode.dureeSemaine.to_f *
           desideratum.nbGroupes.to_f * constante_concernee)/100.0
       end
       return valeur
   end
   
   def Desiderata.somme_periodes_desideratum (desideratum)
     # Constante concernée par le desideratum
     constante_concernee = Fonctions.facteur_actuel("Facteur_#{desideratum.unite_enseignement.type_unite_enseignement.type_unite}",desideratum.unite_enseignement.periode.annee_academique_id)
     # Calcul des heures
      unite=desideratum.unite_enseignement
       if unite.coursBloc
         valeur=
           (unite.nbPeriodesTheoriquesTotal * desideratum.tauxTheorique.to_f * desideratum.nbGroupes.to_f)/100.0 + 
           (unite.nbPeriodesPratiquesTotal * desideratum.tauxPratique.to_f * desideratum.nbGroupes.to_f)/100.0 
         else
           valeur=
           (unite.nbPeriodesTheoriquesSemaine.to_f * 
           desideratum.tauxTheorique.to_f * unite.periode.dureeSemaine.to_f *
           desideratum.nbGroupes.to_f)/100.0 +
           (unite.nbPeriodesPratiquesSemaine.to_f *
           desideratum.tauxPratique.to_f * unite.periode.dureeSemaine.to_f *
           desideratum.nbGroupes.to_f)/100.0
       end
       return valeur
   end
   
    #--------------------------------------------------------------------------------------------------------
    # Nom: charge_examen_desideratum
    # But: Retourne la charge d'examen en points associée à une unité
    # Parametre : desideratum --> desideratum concernée
    # à savoir: nombre de périodes désirées sur sur nombre de périodes total de l'unité.
    #--------------------------------------------------------------------------------------------------------
    def Desiderata.charge_examen_desideratum (desideratum)
      unite=desideratum.unite_enseignement
      charge = unite.chargeExamen
      if charge==nil or charge==0
        return 0
      end
      if !unite.coursBloc
        charge_totale_unite= (unite.nbPeriodesTheoriquesSemaine+unite.nbPeriodesTheoriquesSemaine).to_f
        charge_utilisee=unite.nbPeriodesTheoriquesSemaine*desideratum.tauxTheorique/100.0 +
                        unite.nbPeriodesTheoriquesSemaine*desideratum.tauxPratique/100.0
        taux_global=(charge_utilisee/charge_totale_unite)
      else #Cours bloc
        charge_totale_unite= (unite.nbPeriodesTheoriquesTotal+unite.nbPeriodesTheoriquesTotal).to_f
        charge_utilisee=unite.nbPeriodesTheoriquesTotal*desideratum.tauxTheorique/100.0 + 
                        unite.nbPeriodesTheoriquesTotal*desideratum.tauxPratique/100.0
        taux_global=(charge_utilisee/charge_totale_unite)
      end
      return charge*taux_global*desideratum.nbGroupes.to_i
    end
    
    #--------------------------------------------------------------------------------------------------------
    # Nom: charge_examen_desideratum_heures
    # But: Retourne la charge d'examen en heures associée à une unité
    # Parametre : desideratum --> desideratum concernée
    # La charge est calculée en fonction du facteur "Facteur_Examen", et tient compte du taux demandé
    # à savoir: nombre de périodes désirées sur sur nombre de périodes total de l'unité.
    #--------------------------------------------------------------------------------------------------------
    def Desiderata.charge_examen_desideratum_heures(desideratum)
      constante_concernee = Fonctions.facteur_actuel("Facteur_Examen",desideratum.unite_enseignement.periode.annee_academique_id)
      return Desiderata.charge_examen_desideratum(desideratum)*constante_concernee
    end
  
end

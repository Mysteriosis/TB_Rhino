# -*- encoding : utf-8 -*-
class UniteEnseignement < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------  

  # une unité d'enseignement est connue par plusieurs personnes
  has_many :personnes, :through => :competence_personne_unites
  # une unité d'enseignement est désirée par plusieurs personnes
  has_many :personnes, :through => :desir_personne_unites
  # une unité d'enseignement est supervisée par une personne
  belongs_to :personne
  # une unité d'enseignement concerne une période
  belongs_to :periode
  # une unité d'enseignement concerne un type
  belongs_to :type_unite_enseignement
  # une unité d'enseignement concerne plusieurs desideratas
  has_many :desideratas
  #Une unitée d'enseignement concerne plusieurs charges effectives
  has_one :charge_effective
  # Une unité d'enseignement peut être associée à plusieurs projets
  has_many :projets
  # Charges d'enseignement et d'assistanat
  has_many :charge_enseignements
  has_many :charge_assistanats

  # Filiation dans l'arborescence
  has_many :orientations, :through =>:unite_enseignement_orientations
  has_many :unite_enseignement_orientations

  
   # une unité d'enseignement concerne plusieurs orientations

  
  validates :type_unite_enseignement, :presence=>{:message => "Champ requis"}
  validates :abreviation, :presence=>{:message => "Champ requis"}  
  validates :periode, :presence=>{:message => "Champ requis"} 
  validates :chargeExamen, :presence=>{:message => "Champ requis"}
  validates :nbGroupesTh, :presence=>{:message => "Champ requis"}
  validates :nbGroupesLab, :presence=>{:message => "Champ requis"}
  validates :nbPeriodesTheoriquesSemaine, :presence=>{:message => "Champ requis"}
  validates :nbPeriodesPratiquesSemaine, :presence=>{:message => "Champ requis"}
      
  def pour_annee(annee)
    where{annee.eq my{annee}}
  end
  
  def display_name
    "#{abreviation} - #{nom}"
  end
  
  def display_name_short
    return "#{abreviation}"
  end
  
  #---------------------------------------------------------------------------------------------------
   # Nom : existe_theorie
   # But : Retourne un boolean indiquant si l'unite doit avoir un projet theorique
   #---------------------------------------------------------------------------------------------------
   def existe_theorie 
     return nbPeriodesTheoriquesSemaine != "" && nbPeriodesTheoriquesSemaine != nil && nbPeriodesTheoriquesSemaine != 0
   end
   #---------------------------------------------------------------------------------------------------
   # Nom : existe_pratique
   # But : Retourne un boolean indiquant si l'unite doit avoir un projet pratique
   #---------------------------------------------------------------------------------------------------
   def existe_pratique
     return nbPeriodesPratiquesSemaine != "" && nbPeriodesPratiquesSemaine != nil && nbPeriodesPratiquesSemaine != 0
   end
   #---------------------------------------------------------------------------------------------------
   # Nom : existe_examen
   # But : Retourne un boolean indiquant si l'unite doit avoir un projet examen
   #---------------------------------------------------------------------------------------------------
   def existe_examen 
     return chargeExamen != "" && chargeExamen != nil && chargeExamen != 0
   end
   
   def texte_orientations_concernees
     orientations_concernees=self.orientations
     texte=""
     orientations_concernees.each do |orientation|
       texte+=orientation.abreviation+" "
     end
     return texte
   end
   
   #--------------------------------------------------------------------------------------------------------
    # Nom: unites_annee
    # But: Retourne toutes les unites liees a l'annee souhaitee
    # Parametre: id_annee => id de l'annee academique souhaitee
    #--------------------------------------------------------------------------------------------------------
    def UniteEnseignement.unites_annee_souhaitee(annee_academique_id)
      # Liste des unités
      periodes = Periode.where{annee_academique_id.eq annee_academique_id}
      unites = UniteEnseignement.where{periode_id.in periodes.select{id}}.sort_by(&:abreviation)
      return unites
    end

    def UniteEnseignement.unites_actives_de_lannee(annee_academique_id)
      # Liste des unités
      #unites = UniteEnseignement.where{periode_id.in (Periode.where{~annee_academique_id.eq my{annee_academique_id}}.select(:id)) & (reconductible.eq true)}.sort_by(&:abreviation)
      periodes = Periode.where(:annee_academique_id => annee_academique_id)
      unites = UniteEnseignement.where{(periode_id.in periodes.select{id}) & (reconductible.eq true)}.sort_by(&:abreviation)
      return unites
    end
    
    def UniteEnseignement.unites_actives_avec_assistanat_reconnu_de_lannee(annee_academique_id)
      # Liste des unités
      #unites = UniteEnseignement.where{(periode_id.in (Periode.where{~annee_academique_id.eq my{annee_academique_id}}.selec{id})) & (reconductible.eq true) & (assistanat_reconnu.eq true)}.sort_by(&:abreviation)
      periodes = Periode.where(:annee_academique_id => annee_academique_id)
      unites = UniteEnseignement.where{(periode_id.in periodes.select{id}) & (reconductible.eq true) & (assistanat_reconnu.eq true)}.sort_by(&:abreviation)
      return unites
    end

    def isOption?
      return self.type_unite_enseignement.type_unite=="Bachelor_A_Choix"
    end
    def isMaster?
      return self.type_unite_enseignement.type_unite=="Master"
    end

    def est_active?
      return reconductible
    end

    def assistanat_reconnu?
      return assistanat_reconnu
    end
      
    # -------------------------------------------
    # Filiation
    # -------------------------------------------
    def departements
      liste_departements = Array.new
      orientations.each do |orientation|
        liste_departements << orientation.filiere.departement
      end
      return liste_departements.uniq
    end

    def appartient_a_departement?(departement)
      return departements.include?(departement)
    end

    # -------------------------------------------
    # Calcul Etudiants potentiels
    # -------------------------------------------
    def annee_academique_de_base
      return periode.annee_academique_de_base
    end

    def nb_etudiants_potentiels
      nombre = 0
      orientations.each do |orientation|
        if annee_academique_de_base
          orientation_origine = Orientation.where{(abreviation.eq my{orientation.abreviation}) & (annee_academique_id.eq my{annee_academique_de_base.id})}.first
          if orientation_origine
            nombre_etudiants_orientation =  orientation_origine.nb_etudiants
            if nombre_etudiants_orientation
              nombre+=nombre_etudiants_orientation
            else
              nombre = nil
              break
            end
          else
            nombre = nil
            break
          end
        else
          nombre = nil
          break  
        end        
      end    
      return nombre  
    end

    # -------------------------------------------
    # Charges 
    # --------------------------------------------
    def charges_associees_pour_groupe(noGroupe, type_charge, type_groupe)
      # Retourne les charges d'enseignement ou d'assistanat, 
      # pour un groupe de type Labo ou enseignement
      # type_charge: type de charge, 'enseignement' ou 'assistanat'
      # typ_groupe: type de groupe, 'L' (Labo) ou 'T' (théorie)
      if type_charge.eql?("enseignement")
        return charge_enseignements.where{(numGroupe.eq my{noGroupe}) & (typeGroupe.eq my{type_groupe})}
      else # ("assistanat")
        return charge_assistanats.where{(numGroupe.eq my{noGroupe}) & (typeGroupe.eq my{type_groupe})}
      end
    end

    
    def charges_associees(type_charge, type_groupe)
      # Retourne les charges d'enseignement ou d'assistanat, 
      # Pour l'ensemble des groupes
      # type_charge: type de charge, 'enseignement' ou 'assistanat'
      # typ_groupe: type de groupe, 'L' (Labo) ou 'T' (théorie)
      if type_charge.eql?("enseignement")
        return charge_enseignements.where{typeGroupe.eq my{type_groupe}}
      else # ("assistanat")
        return charge_assistanats.where{typeGroupe.eq my{type_groupe}}
      end
    end

    def professeurs_responsables_pour_groupe (numero_groupe, type_groupe)
      # Retourne un texte comprenant les professeurs responsables du labo
      charges_responsables=self.charges_associees_pour_groupe(numero_groupe, 'enseignement', type_groupe);
      texte=""
      charges_responsables.each do |charge|
        texte+= charge.personne.initiale
        texte+= " "
      end

      return texte
    end
    
    def taux_charge_pour_groupe(noGroupe, type_charge, type_groupe)
      # Retourne le taux de charge total pour un groupe de type Labo ou  enseignement
      # type_charge: type de charge, 'enseignement' ou 'assistanat'
      # typ_groupe: type de groupe, 'L' (Labo) ou 'T' (théorie)
      taux = 0
      charges_associees_pour_groupe(noGroupe, type_charge, type_groupe).each do |charge|
        if charge.taux
          taux+= charge.taux
        end
      end
      return taux
    end

    def taux_charge_OK?
      # Contrôle si la prise en charge d'une unite d'enseignement est OK

      return taux_charge_theorie_OK? && taux_charge_labo_OK?
    end 
  
    def taux_charge_theorie_OK?
      # Retourne le taux de charge total pour un groupe de type Labo ou  enseignement
      # type_charge: type de charge, 'enseignement' ou 'assistanat'
      # typ_groupe: type de groupe, 'L' (Labo) ou 'T' (théorie)
      nb_groupes_th=nbGroupesTh
      taux_charge_ok=true

      no_groupe='A'
      (1..nb_groupes_th).each do |groupe|
        if taux_charge_pour_groupe(no_groupe, 'enseignement', 'T').round != 100
          taux_charge_ok=false
        end
        no_groupe = no_groupe.succ
      end
      if taux_charge_pour_groupe(no_groupe, 'enseignement', 'T').round != 0  # un groupe de trop!
        taux_charge_ok=false
      end 
      return taux_charge_ok
    end

    def taux_charge_labo_OK?
      # Retourne le taux de charge total pour un groupe de type Labo ou  enseignement
      # type_charge: type de charge, 'enseignement' ou 'assistanat'
      # typ_groupe: type de groupe, 'L' (Labo) ou 'T' (théorie)
      nb_groupes_lab=nbGroupesLab
      taux_charge_ok=true

      no_groupe='A'
      (1..nb_groupes_lab).each do |groupe|
        if taux_charge_pour_groupe(no_groupe, 'enseignement', 'L').round != 100
          taux_charge_ok=false
        end
        if (taux_charge_pour_groupe(no_groupe, 'assistanat', 'L').round != 100) && assistanat_reconnu
          taux_charge_ok=false
        end
        no_groupe = no_groupe.succ
      end
      if (taux_charge_pour_groupe(no_groupe, 'enseignement', 'L').round != 0) || (taux_charge_pour_groupe(no_groupe, 'assistanat', 'L').round != 0)  # un groupe de trop!
        taux_charge_ok=false
      end 
      return taux_charge_ok
    end
    
    def heures_charge_pour_groupe(noGroupe, type_charge, type_groupe)
      # Retourne la somme totale des heures de charge pour un groupe de type Labo ou  enseignement
      # type_charge: type de charge, 'enseignement' ou 'assistanat'
      # typ_groupe: type de groupe, 'L' (Labo) ou 'T' (théorie)
      heures = 0
      charges_associees_pour_groupe(noGroupe, type_charge, type_groupe).each do |charge|
        if charge.heures
          heures+= charge.heures
        end
      end
      return heures
    end 

    
    def self.charges_assistanat(personne, annee_academique_id)
     # Retourne les charges d'assistanat pour une personne
     unites=UniteEnseignement.unites_actives_avec_assistanat_reconnu_de_lannee(annee_academique_id)
     charges=Array.new
     unites.each do |ue|
       ue.charge_assistanats.each do |charge|
         if charge.personne_id==personne.id
           charges << charge
         end
       end
     end
     return charges
   end
   
   def self.charges_assistanat_en_texte(personne, annee_academique_id)
      # Retourne une texte contenant les charges d'assistanat pour une personne
      unites=UniteEnseignement.unites_actives_avec_assistanat_reconnude_lannee(annee_academique_id)
      texte=""
      unites.each do |ue|
        ue.charge_assistanats.each do |charge|
          if charge.personne_id==personne.id
            texte+= charge.unite_enseignement.display_name_short
            texte+= " "
          end
        end
      end
      if texte.eql?("")
        texte="--"
      end
      return texte
    end

    def self.charges_enseignement(personne, annee_academique_id)
    # Retourne les charges d'enseignement pour une personne
      unites=UniteEnseignement.unites_actives_de_lannee(annee_academique_id)
      charges=Array.new
      unites.each do |ue|
        ue.charge_enseignements.each do |charge|
          if charge.personne_id==personne.id
            charges << charge
          end
        end
      end
      return charges
    end
    
    def prochainGroupeLibre(type_groupe)  
      # Retourne le numéro du prochain groupe libre du point de vue charges d'enseignement
      charges=charges_associees('enseignement', type_groupe)
      noGroupeLibre='A'
      charges.each do |charge|
        noGroupe=charge.numGroupe
        if noGroupe[0].ord >= noGroupeLibre[0].ord
          noGroupeLibre=(noGroupe[0].ord+1).chr
        end
      end
      return noGroupeLibre
    end

    def tableau_des_no_de_groupe(type)
      # Tableau des charges par groupe, de théorie (si type == 'T'), d'assistanat (si type == 'L')
      tabAllGroupes = ('A'..'Z').to_a
      if type.eql?('T') 
        nbGroupes=nbGroupesTh
      else
        nbGroupes=nbGroupesLab
      end
      tabGroupes=Array.new
      (1..nbGroupes).each {|no| tabGroupes << tabAllGroupes[no-1] }
      charges_associees('enseignement', type).each do |charge|
        tabGroupes << charge.numGroupe
      end 
      charges_associees('assistanat', type).each do |charge|
        tabGroupes<<charge.numGroupe
      end
      return tabGroupes.uniq.sort
    end

    def liste_orientations
      # Texte décrivant la liste des orientations de la filieres
      tab_orientations = orientations.collect{|o| o.abreviation}.sort.join(" ")
      return tab_orientations
    end
  
end

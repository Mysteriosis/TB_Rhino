# -*- encoding : utf-8 -*-
class Engagement < ActiveRecord::Base
  has_many :contrats
  belongs_to :type_engagement
  belongs_to :personne
  has_many :affectation_projets
  
  def display_name
    return type_engagement.libelle
  end
  
  def has_contrats_actuels_ou_futurs?
    contrats_actuels=Array.new
    if contrats.size >0 
      contrats_actuels=contrats.where{(end_date.gteq Date.today) | (end_date.eq nil)}
    end
    return !contrats_actuels.size==0
  end
  
  def self.premier_engagement_avec_contrats_actuels_ou_futurs(personne)
    # Retourne le premier engagement qui possède des contrats actueks ou futurs
    # S'il n'en existe pas, retourne le premier engagement de la liste
    # S'il n'y a aucun engagement, retourne nil
    ses_engagements=personne.engagements
    ses_engagements.each do |eng|
      if eng.has_contrats_actuels_ou_futurs?
        return eng
      end
    end
    return ses_engagements.first
  end

  def self.nombre_engagement_avec_contrats_actuels_ou_futurs(personne)
    # Retourne le nombre des engagements de la personne possédant des contrats actuels ou futurs
    nombre = 0
    ses_engagements=personne.engagements
    ses_engagements.each do |eng|
      if eng.has_contrats_actuels_ou_futurs?
        nombre += 1
      end
    end
    return nombre 
  end
    
end



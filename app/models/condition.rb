class Condition < ActiveRecord::Base

  belongs_to :critere1, :class_name => 'Critere'
  belongs_to :critere2, :class_name => 'Critere'
  has_one :condition
  has_one :notification

end
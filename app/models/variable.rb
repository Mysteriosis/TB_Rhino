# -*- encoding : utf-8 -*-
class Variable < ActiveRecord::Base
  def Variable.annee_academique_actuelle
    #annee_academique_actuelle=Variable.find(:first, :conditions=>{:nom=>"annee_academique_actuelle"})
    annee_academique_actuelle=Variable.where(:nom => "annee_academique_actuelle").first
    return AnneeAcademique.find_by_id(annee_academique_actuelle.valeur)
  end
  def Variable.phase_planification_enseignement?
    #etat_planification=Variable.find(:first, :conditions=>{:nom=>"planification_enseignement"})
    etat_planification=Variable.where(:nom => "planification_enseignement").first
    return etat_planification.valeur=='true'
  end
  def Variable.date_saisie_autorisee_min
    #date_min=Variable.find(:first, :conditions=>{:nom=>"date_saisie_autorisee_min"})
    date_min=Variable.where(:nom => "date_saisie_autorisee_min").first
    return Date.parse(date_min.valeur)
  end
  def Variable.date_saisie_autorisee_max
    #date_max=Variable.find(:first, :conditions=>{:nom=>"date_saisie_autorisee_max"})
    date_max=Variable.where(:nom => "date_saisie_autorisee_max").first
    return Date.parse(date_max.valeur)
  end
end

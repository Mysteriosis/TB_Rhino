# -*- encoding : utf-8 -*-
require 'digest/sha1'

class Personne < ActiveRecord::Base

  has_many :affectation_projets
  has_many :projets, :through => :affectation_projets
  has_many :engagements
  has_many :contrats, :through => :engagements
  has_many :plage_horaire_saisies, :through => :affectation_projets
  has_many :charge_enseignements
  has_many :charge_assistanats

  # Calcul des heures
  has_many :plage_mensuelles, :through => :plage_contractuelles
  has_many :plage_contractuelles
  has_many :info_heures

  # Module de planification
  has_many :commentaires
  has_many :jalons
  has_and_belongs_to_many :mails_planifications

  before_save :update_roles_for_status

  validates_presence_of :type_personne, :message => "Type de la personne requis (collaborateur, professeur, ..)"
  validates_presence_of :username, :message => "Login requis"
  validates_presence_of :prenom, :message => "Prenom requis"
  validates_presence_of :nom, :message => "Nom requis"
  validates_presence_of :initiale, :message => "Intiales requises"
  
  validate :horaire_match_activite
  
  ## Include devise modules
  devise :ldap_authenticatable, :rememberable, :trackable

  # Setup accessible (or protected) attributes for your model
  # attr_accessible :password, :password_confirmation, :remember_me
  
  ROLES = [:collaborateur, :professeur, :charge_de_cours, :diplomant, :secretaire, :responsable_rh, :responsable_enseignement, :admin]
  ROLES_NAMES = {ROLES[0] => "Collaborateur",
                 ROLES[1] => "Professeur",
                 ROLES[2] => "Chargé de cours",
                 ROLES[3] => "Diplômant",
                 ROLES[4] => "Secrétaire",
                 ROLES[5] => "Responsable RH",
                 ROLES[6] => "Responsable enseignement",
                 ROLES[7] => "Administrateur"}


  def roles=(roles)
    self.roles_mask = (roles & ROLES).map {|r|2**ROLES.index(r)}.sum 
  end  

  def roles  
    ROLES.reject{|r|((roles_mask || 0) & 2**ROLES.index(r)).zero?}    
  end
  
  def has_role?(role)
    roles.include? role
  end
  
  def self.avec_role(role)
    where "roles_mask & #{2**ROLES.index(role.to_sym)} > 0"
  end

  # the default meta_search options for this model
  def self.default_search_options 
    {actif_true: 1, institut_true: 1, est_fictive_false: 1}
  end

  def display_name
    if !est_fictive
      return "#{nom} #{prenom} (#{initiale})"
    else
      return "#{initiale}"
    end
  end

  # une personne peut superviser un departement
  has_one :departement 
  # une personne supervise plusieurs projets
  #has_many :projets
  #Une personne a plusieurs contrats

  #Une personne appartient à un axe
  belongs_to :axe_strategique
  #Un collaborateur possède plusieurs cahiers des charges
  has_many :cahier_charge_assistants
  # une personne possède plusieurs suivis
  has_many :suivis
  # une personne possède plusieurs desideratas de projets
  has_many :desiderata_projets
  # une personne peut avoir plusieurs charges
  has_many :charge_effectives
  # une personne possède un type
  belongs_to :type_personne 
  # un collaborateur a des compétences sur des domaines
  has_many :competence_domaines, :through => :competence_personne_domaines
  # un collaborateur a des désirs sur des unités d'enseignement
  has_many :unite_enseignements, :through => :desir_personne_unites
  # une personne peut être composée de plusieurs événements
  has_many :date_evenement_personnes
  has_many :evenement_personnes, :through => :date_evenement_personnes
  # une personne supervise plusieurs untiés d'enseignement
  has_many :unite_enseignements
  # un collaborateur a des compétences sur des unités d'enseignement
  has_many :unite_enseignements, :through => :competence_personne_unites
  # un professeur peut être concerné par plusieurs desideratas de cours
  has_many :personne_desideratas #NEW
  has_many :desideratas #NEW
  #has_many :desideratas, :through => :personne_desideratas #NEW mais qui pose problème
  #has_one :desiderata #OLD
  #has_many :personne_desiderata #OLD
  # une personne est en relation ternaire avec un projet et une annee_civile
  has_many :charges
  #has_many :projets, :through  => :charges
  has_many :annee_civiles, :through  => :charges
  #Une personne a une partie privée
  has_one :personne_prive
  #Une personne a un responsable
  belongs_to :personne

  # TODO : améliorer la performance avec AREL pour obtenir la liste en 1 requête
  # Si les performances s'effondrent
  def self.avec_heures_a_saisir
    res = []
    order(:nom).all.each do |pers|
      pers.affectation_projets.each do |ap|
        if ap.saisie_heures_possible?
          res << pers
          break
        end
      end
    end
    return res
  end

  
  # le nombre d'heures à effectuer entre deux dates
  def heures_a_effectuer_entre(date_debut,date_fin)
    
    if date_debut.year != date_fin.year
      raise "Erreur : la date de début et de fin doivent être sur la même année"
    end
    
    ac = AnneeCivile.find_by_annee(date_debut.year.to_s)
    
    charge_journaliere = CstHeure.HJ(ac.annee)
    
    return jours_a_effectuer_entre(date_debut, date_fin) * (charge_journaliere)
    
  end

  # le nombre de jours à effectuer entre deux dates
  # NOTE : tiens compte des jours fériés, des dates de début 
  # et de fin des contrats ainsi que de leurs pourcentages
  def jours_a_effectuer_entre(date_debut,date_fin)
    
    res = 0
    contrats.all.each do |contrat|
      if contrat.end_date.nil? 
        fin = (date_fin+1.day-1.second).to_time
      else
        fin = [contrat.end_date+1.day-1.second, date_fin+1.day-1.second].min
      end
      debut = [contrat.start_date.to_time, date_debut.to_time].max
      
    
      # on compte les jours fériés entre ces deux dates
      nb_jours_feries = 0
      JourAcademique.where{((type_jour.eq "Ferie") | (type_jour.eq "Compense"))}.entre(debut, fin).all.each do |ja|
        if ja.periode == "Jour entier"
          nb_jours_feries += 1
        else
          nb_jours_feries += 0.5
        end
      end
      
      nb_jours_potentiellement_ouvrables = 0
      (debut.to_date..fin.to_date).each do |day|
        # on élimine les samedis et dimanches
        if day.wday != 6 && day.wday != 0
          nb_jours_potentiellement_ouvrables += 1
        end
      end
        
      res += (contrat.pourcentage.to_f / 100) * (nb_jours_potentiellement_ouvrables - nb_jours_feries) 
        
    end

    return res
  
  end
  
  # le nombre d'heure à effectuer pour un mois donnée selon les contrats et le calendrier académique
  def heures_a_effectuer_pour_mois(mois)
    heures_a_effectuer_entre(mois.debut.to_date,mois.fin.to_date)
  end

  # Le nombre de jours à effectuer pour un mois donné en tenant compte 
  # des jours fériés et des dates de fin / début des contrats ainsi que de leurs taux
  def jours_a_effectuer_pour_mois(mois)
    jours_a_effectuer_entre(mois.debut.to_date, mois.fin.to_date)
  end

  def heures_a_effectuer_pour_lannee_courante
    total = 0
    if is_collaborateur?
      annee_civile=AnneeCivile.courante
      mois = Moi.where{annee_civile_id.eq annee_civile.id}.joins(:annee_civile).order("annee_civiles.annee, num").all
      mois.each do |mois|
        heures = heures_a_effectuer_pour_mois(mois)
        total += heures
      end
    end
    return total
  end
  
  #def heures_effectuees_pour_lannee_courante
  #  annee_civile=AnneeCivile.courante
  #  mois = Moi.where(:annee_civile_id => annee_civile.id).joins(:annee_civile).order("annee_civiles.annee, num").all
  #  total=0
  #  mois.each do |mois|
  #    total_mois=0
  #    affectation_projets.each do |ap|
  #      heures = ap.heures_pour_mois(mois)
  #      total_mois += heures
  #    end
  #    total += total_mois
  #  end
  #  return total
  #end
  
  def heures_effectuees_pour_lannee_courante
    annee_civile=AnneeCivile.courante
    total=0
    affectation_projets.each do |ap|
        heures = ap.heures_pour_annee(annee_civile)
        total += heures
    end
    return total
  end
    
  #Les options de l'anthentification
  #acts_as_authentic do |c|
  #  c.login_field = :login
  #  c.validate_password_field = false
  #  c.merge_validates_length_of_login_field_options :message => "minimum 3 caractères"
  #  c.merge_validates_format_of_login_field_options :message => "doit être composé de lettres minuscules et peut contenir des chiffres"
  #end
  
  #Methode de controle  de login
  #def self.check_login(login)
  #  #Contrôle si l'utilisateur est déjà présent dans la BDD
  #  personne = find( :first, :conditions=> { :login => login })
  #  if (personne)
  #    #Retourne de la personne
  #    return personne
  #  end
  #  #Login échoué
  #  return false
  #end
  
  #Methode de controle de password
  #def valid_ldap_credentials?(password)
  #  #Définition du mot de passe si celui-ci est vide
  #  if (password.empty?) then
  #      password = 'empty'
  #  end
  #
  #  #Récupération de la personne en BDD
  #  personne = Personne.find( :first, :conditions=> { :login => login })
  #  
  #  #TEMPORAIRE
  #  if(password == "secretaire" and personne.login == "secretaire" )
  #    return personne
  #  elsif (password == "admin" and personne.login == "admin")
  #    return personne
  #  elsif (password == "prof" and personne.login == "prof")
  #    return personne
  #  elsif (password == "assistant" and personne.login == "assistant")
  #    return personne
  #  elsif (password == "resp_rh" and personne.login == "resp_rh")
  #    return personne
  #  elsif (password == "resp_ens" and personne.login == "resp_ens")
  #    return personne
  #  end
  #
  #  #Connexion à LDAP
  #  ldap = Net::LDAP.new()
  #  ldap.host = 'einet.ad.eivd.ch'
  #  ldap.auth('einet\\' + login, password)
  #
  #  #Création des filtres de recherche
  #  treebase = "ou=personnel,dc=einet,dc=ad,dc=eivd,dc=ch"
  #  filter = Net::LDAP::Filter.eq( "sAMAccountName", login )
  #  
  #  #Contrôle de connection à LDAP
  #  if ldap.bind
  #    # Définition des valeurs par défaut des variables
  #    initiale = nil
  #    prenom = nil
  #    nom = nil
  #    email = nil
  #    
  #    #Mise à jour des information de notre BD avec les infos de LDAP
  #    ldap.search( :base => treebase, :filter => filter ) do |entry|
  #      initiale = entry.initials
  #      prenom = entry.givenName
  #      nom = entry.sn
  #      email = entry.mail          
  #    end
  #    
  #    #Mise à jour des attributs
  #    personne.update_attributes(
  #        :initiale => initiale.to_s,
  #        :prenom => prenom.to_s,
  #        :nom => nom.to_s,
  #        :emailTravail => email.to_s)
  #    
  #   return personne
  #  else
  #    # authentication failed
  #      return false
  #  end
  #end

  # Autres méthodes
  def is_collaborateur?
    return self.type_personne.typePersonne=="Collaborateur"
  end

  def dans_institut?
    return self.institut
  end
  
  
  def is_professeur?
    return self.type_personne.typePersonne=="Professeur"
  end

  def self.collaborateurs
    where{(type_personne_id.eq 1) & (est_fictive.eq false)}
    #return Personne.find(:all, :conditions =>{:type_personne_id => '1'}, :order => "initiale")
  end
  
  def self.professeurs
    where{(type_personne_id.eq 2) & (est_fictive.eq false)}
    #return Personne.find(:all, :conditions =>{:type_personne_id => '2'}, :order => "initiale")
  end
  
  def self.personnes_actives_de_linstitut
    where{(actif.eq true) & (institut.eq true) & (est_fictive.eq false)}.sort_by(&:initiale)
  end
  
  def self.professeurs_actifs_de_linstitut
    where{(type_personne_id.eq 2) & (actif.eq true) & (institut.eq true) & (est_fictive.eq false)}
  end
  
  def self.professeurs_actifs
    where{(type_personne_id.eq 2) & (actif.eq true) & (est_fictive.eq false)}
  end
  
  def self.collaborateurs_actifs_de_linstitut
    where{(type_personne_id.eq 1) & (actif.eq true) & (institut.eq true) & (est_fictive.eq false)}
  end
  
  def self.professeurs_et_collaborateurs_actifs_de_linstitut
    # where("(type_personne_id=2 or type_personne_id=1) and actif=true and institut=true and est_fictive=false")
    where{((type_personne_id.eq 2) | (type_personne_id.eq 1)) & (actif.eq 1) & (institut.eq 1) & (est_fictive.eq 0)}
  end
  
  def self.collaborateurs_fictifs
    where{(type_personne_id.eq 1) & (est_fictive.eq true)}
  end
  
  def self.professeurs_fictifs
    where{(type_personne_id.eq 2) & (est_fictive.eq true)}
  end
  
  def self.professeurs_et_collaborateurs_fictifs
    # where("(type_personne_id=2 or type_personne_id=1) and est_fictive=true")
    where{((type_personne_id.eq 2) | (type_personne_id.eq 1)) & (est_fictive.eq 1)}
  end
  
  def self.collaborateurs_fictifs_institut
    where{(type_personne_id.eq 1) & (est_fictive.eq true) & (institut.eq true)}
  end
  
  def self.professeurs_fictifs_institut
    where{(type_personne_id.eq 2) & (est_fictive.eq true) & (institut.eq true)}
  end
  
  def self.assistant_nobody
    return Personne.get_by_initiale('---')
  end
  
  def isNobody?
    return self.initiale=="---"
  end
  
  def isIL?
    return self.initiale=="IL"
  end
  
  def isTR?
    return self.initiale=="TR"
  end
  
  def isIE?
    return self.initiale=="IE"
  end
  
  def isTIC?
    return self.initiale=="TIC"
  end
  
  def mode_saisie_simple?
    # retourne vrai si le mode de saisie est simple
    return personne_prive_correspondante.mode_saisie_simple?
  end
  
  def compense_les_ponts?
    if personne_prive_correspondante
      return personne_prive_correspondante.compense_les_ponts?
    else
      return false
    end
  end

  def personne_prive_correspondante
    return PersonnePrive.where{personne_id.eq self.id}.first
  end
  
  def travaille_lundi_am
    return self.horaire[0]=='1'
  end
  def travaille_lundi_pm
    return self.horaire[1]=='1'
  end
  def travaille_mardi_am
    return self.horaire[2]=='1'
  end
  def travaille_mardi_pm
    return self.horaire[3]=='1'
  end
  def travaille_mercredi_am
    return self.horaire[4]=='1'
  end
  def travaille_mercredi_pm
    return self.horaire[5]=='1'
  end
  def travaille_jeudi_am
    return self.horaire[6]=='1'
  end
  def travaille_jeudi_pm
    return self.horaire[7]=='1'
  end
  def travaille_vendredi_am
    return self.horaire[8]=='1'
  end
  def travaille_vendredi_pm
    return self.horaire[9]=='1'
  end
  
  def affectations_hors_absences
    # Retourne les affectations de projets autres que les absences
    affectations=Array.new
    affectation_projets.order(:id).each do |ap|
      if !ap.projet.est_de_type_absence?
        affectations<<ap
      end
    end
    return affectations
  end
  
  def affectations_absences
    # Retourne les affectations de projets associées aux absences
    affectations=Array.new
    affectation_projets.order(:id).each do |ap|
      if ap.projet.est_de_type_absence?
        affectations<<ap
      end
    end
    return affectations
  end

  def isChefProjet?
    !Projet.where{chef_id.eq my{self.id}}.empty?
  end
  
private
 
  def update_roles_for_status
    
    if new_record? || type_personne_id_changed?
      # on supprime tout les roles qui existent en tant que type de personne
      TypePersonne.all.each do |tp|
        if ROLES_NAMES.rassoc(tp.typePersonne)
          self.roles -= [ROLES_NAMES.rassoc(tp.typePersonne)[0]]
        end
      end
      
      # on affecte le role du type de personne
      self.roles += [ROLES_NAMES.rassoc(type_personne.typePersonne)[0]]
    end
  end
  
  def horaire_match_activite
    taux_activite_hebdomadaire=0
    (0..9).each do |plage_hebdo|
      if horaire[plage_hebdo]=='1'
        taux_activite_hebdomadaire+=10
      end
    end
    if taux_activite_hebdomadaire!=100 and (taux_activite_hebdomadaire!=self.contrats.taux_activite_actuel)
      errors.add('Saisie incorrecte:', "L'horaire choisi, - #{taux_activite_hebdomadaire}% -, ne correspond pas au taux d'activité")
    end
  end
  
end

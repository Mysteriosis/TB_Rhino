# -*- encoding : utf-8 -*-
class Constante < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une constantes possède soit une année acdémique soit une année civile
  belongs_to :annee_academique
  belongs_to :annee_civile
  

  
  def Constante.facteur(annee_academique, type_unite)
    # Retourne le facteur periodes-heures pour un certain type d'unité
    if type_unite.eql?("Bachelor")
      facteur = Constante.where{(description.eq "Facteur_Bachelor") & (annee_academique_id.eq my{annee_academique})}.first
    elsif type_unite.eql?("Bachelor_A_Choix")
      facteur = Constante.where{(description.eq "Facteur_Bachelor_A_Choix") & (annee_academique_id.eq my{annee_academique})}.first
    elsif type_unite.eql?("Master")
      facteur = Constante.where{(description.eq "Facteur_Master") & (annee_academique_id.eq my{annee_academique})}.first
    else
      facteur = 0.0
    end
    return facteur.valeur.to_f
  end
  
  def Constante.facteur_examen(annee_academique)
    return Constante.where{(description.eq "Facteur_Examen") & (annee_academique_id.eq my{annee_academique})}.first.valeur.to_f
  end
end

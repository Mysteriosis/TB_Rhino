# -*- encoding : utf-8 -*-
class EvenementPersonne < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un événement concerne un type d'événement
  belongs_to :type_evenement_personne
  # Les événements concernent plusieurs personnes
  has_many :date_evenement_personnes
  has_many :personne, :through => :date_evenement_personnes
end

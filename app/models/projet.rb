# -*- encoding : utf-8 -*-
class Projet < ActiveRecord::Base

  # The colors of the projects
  COLORS = ["#FF5ADC","#31528E","#368942","#B63634","#FF7940","#76AB01","#0E6A00","#083500","#042200","#067778"]
  #COLORS = ["#00FFFF","#000000","#0000FF","#FF00FF","#808080","#00800","#00FF00","#800000","#000080","#808000", "#800080", "#FF0000", "#C0C0C0","#008080", "#FFFFFF", "#FFFF00"]
  belongs_to :type_projet
  belongs_to :chef, :class_name => "Personne"
  
  has_many :affectation_projets
  has_many :personnes, :through => :affectation_projets
  has_many :plage_horaire_saisies, :through => :affectation_projets
  has_many :budgets, :dependent => :destroy

  validates :id_interne, :presence=>{:message => "Champ requis"}, :uniqueness=>{:message => "Cet identifiant est déjà pris"}
  validates :type_projet_id, :presence=>{:message => "Champ requis"}
  validates :unite_enseignement_id, :presence=>{:message => "Champ requis" , :if => :est_un_projet_enseignement}

  accepts_nested_attributes_for :budgets, :allow_destroy => true
  
  def display_name
    id_interne
  end
  
  def color
    COLORS[id%COLORS.count]
  end
  
  def validate
    
    if try(:type_projet).try(:type_id_externe) && !id_externe
      errors.add(:id_externe, "Champ requis")
    end
    
  end

  # retourne le genre du projet pour l'affichage
  def genre
    Projet.genres_projets.rassoc(self.class.to_s)[0]
  end
  
  def self.genres_projets
    [["R&D, Support & divers", "Projet"], ["Projet académique", "ProjetAcademique"], ["Projet d\'enseignement académique", "ProjetEnseignement"], ["Conduite de thèse académique", "These"] ]
  end
  
  # Un projet est actif dans la mesure où son flag "est_actif" vaut true, et dans la mesure où ce dernier n'est pas "hors date"
  def en_activite?
    return !hors_date? && self.est_actif
  end
  
  def hors_date?
    ok=true
    if date_debut
      if  Date.today < date_debut
        ok=false
      end
    end
    if date_fin
      if  Date.today > date_fin
        ok=false
      end
    end
    return !ok
  end
  
  def is_archive?
    return est_archive
  end
  
  def self.projets_non_archives
    Projet.find(:all, :conditions=>"est_archive=0 or (est_archive is NULL)")
  end
  
  def est_de_type_absence?
    return type_projet.nom.eql?("Absences")
  end
  
  def est_un_projet_academique?
  # Projet associé à une année académique
    return self.kind_of? ProjetAcademique
  end

  def est_un_projet_enseignement
  # Projet académique associé en plus à une unité d'enseignement spécifique
    return self.kind_of? ProjetEnseignement
  end
  
end

class ProjetAcademique < Projet
  belongs_to :annee_academique_realisation, :class_name => "AnneeAcademique"

  def identifiant_interne_to_update
  # Retourne le nom du projet, sans le suffixe "2013-14"
    return self.id_interne.sub(" #{self.annee_academique_realisation.annee}", '')
  end

end

class These < ProjetAcademique

end

class ProjetEnseignement < ProjetAcademique
  belongs_to :unite_enseignement
  
  def identifiant_interne_to_update
  # Retourne le dernier caractère de l'identifiant de projet, c.a.d le no de groupe 'A', 'B', 'C', etc..
    return (self.id_interne.reverse)[0]
  end

  def identifiant_interne(no_groupe)
  # Retourne l'identifiant final interne du projet, celui qui sera enregistré 
    projet=self
    ue=projet.unite_enseignement
    if projet.type_projet.nom.eql? ("Labo-Bachelor") 
      identifiant="Labo"
    elsif projet.type_projet.nom.eql?("Cours-Bachelor")
      identifiant="Cours"
    elsif projet.type_projet.nom.eql?("Cours-Master")
      identifiant="Cours"
    elsif projet.type_projet.nom.eql?("Labo-Master")
      identifiant="Labo"
    elsif projet.type_projet.nom.eql?("Préparation-Cours-Bachelor")
      identifiant="Prep-Cours"
    elsif projet.type_projet.nom.eql?("Préparation-Labo-Bachelor")
      identifiant="Prep-Labo"
    elsif projet.type_projet.nom.eql?("Préparation-Cours-Master")
      identifiant="Prep-Cours"
    elsif projet.type_projet.nom.eql?("Préparation-Labo-Master")
      identifiant="Prep-Labo"
    else
      identifiant="XXX"
    end
    identifiant+=projet.unite_enseignement.periode.annee_academique.annee+"-"+projet.unite_enseignement.abreviation+"-"+no_groupe 
    return identifiant
  end

  def identifiant_externe (no_groupe)
  # Retourne l'identifiant final externe du projet, celui qui sera enregistré 
    projet=self
    identifiant=projet.unite_enseignement.abreviation+"-"+no_groupe
    ue=projet.unite_enseignement
    if projet.type_projet.nom.eql? ("Labo-Bachelor") 
      identifiant+="-L"
    elsif projet.type_projet.nom.eql?("Cours-Bachelor")
      identifiant+="-C"
    elsif projet.type_projet.nom.eql?("Cours-Master")
      identifiant+="-C"
    elsif projet.type_projet.nom.eql?("Labo-Master")
      identifiant+="-L"
    elsif projet.type_projet.nom.eql?("Préparation-Cours-Bachelor")
      identifiant+="-C"
    elsif projet.type_projet.nom.eql?("Préparation-Labo-Bachelor")
      identifiant+="-L"
    elsif projet.type_projet.nom.eql?("Préparation-Cours-Master")
      identifiant+="-C"
    elsif projet.type_projet.nom.eql?("Préparation-Labo-Master")
      identifiant+="-L"
    else
      identifiant+="-XXX"
    end
    return identifiant
  end
end

# -*- encoding : utf-8 -*-
class TypeEvenementProjet < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un type d'événement de projet concerne plusieurs événements de projet
  has_one :evenement_projet
end

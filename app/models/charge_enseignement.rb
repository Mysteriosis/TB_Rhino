# -*- encoding : utf-8 -*-
class ChargeEnseignement < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Charge d'enseignement associé à une unité d'enseignement
  # -----------------------------------------------------------------


  belongs_to :unite_enseignement
  
  # La personne correspondante
  belongs_to :personne
  
  validates :personne, :presence => {:message => "Champ requis"}
  validates :unite_enseignement, :presence => {:message => "Champ requis"}
  validate  :controle_taux_heures


  def controle_taux_heures
    if !taux and !heures
      errors.add(:taux, "Le taux doit être signalé si les heures ne le sont pas")
      errors.add(:heures, "Les heures doivent être signalées si le taux ne l'est pas")
    end
    if taux and heures
      errors.add(:taux, "Signaler soit le taux de charge, soit la charge en heures")
      errors.add(:heures, "Signaler soit le taux de charge, soit la charge en heures")
    end
    if taux and taux <= 0
      errors.add(:taux, "Le taux doit être supérieur à 0")
    end
    if heures and heures <= 0
      errors.add(:heures, "Les heures doivent être supérieures à 0")
    end
  end
  

  def is_charge_de_theorie?
    return typeGroupe.eql?("T")
  end
  
  def is_charge_de_labo?
    return typeGroupe.eql?("L")
  end
  

   def periodes
     # Retourne le nombre de période de présence pour une charge donnée
     # Taux d'assistanat pris en compte
     nombre_semaines=unite_enseignement.periode.dureeSemaine
     if unite_enseignement.coursBloc?
       if is_charge_de_theorie?
         return unite_enseignement.nbPeriodesTheoriquesTotal*taux/100
       else
         return unite_enseignement.nbPeriodesPratiquesTotal*taux/100
       end
     else
       if is_charge_de_theorie?
         return nombre_semaines*unite_enseignement.nbPeriodesTheoriquesSemaine*taux/100
       else
         return nombre_semaines*unite_enseignement.nbPeriodesPratiquesSemaine*taux/100
       end
     end
   end

   def heures_a_charge
     if !taux  # Le taux n'est pas signalé: le nombre total d'heures est alors signalé
       return heures
     else
       return periodes*Constante.facteur(unite_enseignement.periode.annee_academique.id, unite_enseignement.type_unite_enseignement.type_unite)
     end
   end

  def est_effective?
    return est_effective
  end

  def set_effective(value)
    est_effective=value
  end

  # !! Spécifique à ChargeEnseignement
  def charge_examen
    if taux
      return unite_enseignement.chargeExamen*taux/100
    else
      return 0
    end
  end
  
  # !! Spécifique à ChargeEnseignement
  def self.total_heures(personne, annee_academique_id)
    # Retourne le total des heures d'enseignement pour une personne donnée
    periodes = Periode.where(:annee_academique_id => annee_academique_id).select(:id)
    unitesEnseignement = UniteEnseignement.where{(periode_id.in periodes) & (reconductible.eq true)}
    charges = personne.charge_enseignements.where{unite_enseignement_id.in unitesEnseignement}

    total=0.0
    charges.each do |charge|
      total+=charge.heures_a_charge
    end
    return total
  end

  def self.total_heures_avec_examens(personne, annee_academique_id)
    # Retourne le total des heures d'enseignement - y compris les heures d'examen - pour une personne donnée
    periodes = Periode.where(:annee_academique_id => annee_academique_id).select(:id)
    unitesEnseignement = UniteEnseignement.where{(periode_id.in periodes) & (reconductible.eq true)}
    charges = personne.charge_enseignements.where{unite_enseignement_id.in unitesEnseignement}

    total=0.0
    charges.each do |charge|
      total+=charge.heures_a_charge
      if charge.is_charge_de_theorie?
        total+=charge.charge_examen*Constante.facteur_examen(AnneeAcademique.courante.id)
      end
    end
    return total
  end
  
  # !! Spécifique à ChargeEnseignement
  def self.total_charge_examen(personne, annee_academique_id)
    # Retourne le total des charges d'examen (avec facteur 16) pour une personne donnée
    periodes = Periode.where(:annee_academique_id => annee_academique_id).select(:id)
    unitesEnseignement = UniteEnseignement.where{periode_id.in periodes}
    charges = personne.charge_enseignements.where{unite_enseignement_id.in unitesEnseignement}

    total=0.0
    charges.each do |charge|
      if charge.is_charge_de_theorie?
        total+=charge.charge_examen*Constante.facteur_examen(AnneeAcademique.courante.id)
      end
    end
    return total
  end
  
  # !! Spécifique à ChargeEnseignement
  def assistants_associes
    charges_assistanats=unite_enseignement.charge_assistanats.where(:numGroupe => numGroupe, :typeGroupe => typeGroupe)
    personnes=""
    charges_assistanats.each do |charge|
      personnes+= charge.personne.display_name
    end
    return personnes
  end
  
  
end

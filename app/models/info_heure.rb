# -*- encoding : utf-8 -*-
class InfoHeure < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Associations
  # -----------------------------------------------------------------

  # Un report d'heures se rapporte à une personne


  belongs_to :personne

  def self.report_des_heures(personne, annee_civile)
  	# L'année civile est un entier (2013, 2014, ..)
    # Retourne le solde de vacances et le cumul des heures supplémentaires de l'année précédente.
    # Au cas où le calcul correspondant n'a pas été effectué, une mise à jour des heures des années précédentes est commandé,
    # et ainsi de suite jusqu'à l'année civile initiale
  	info_heures=personne.info_heures.where(:annee_civile => annee_civile).first

  	if !info_heures
      # La structure n'existe pas, la créer 
      # S'il s'agit de l'année civile initiale, cette structure est créée avec un "report_ok = true"
      # et des heures reportées égales à 0 
      info_heures = InfoHeure.creer(personne, annee_civile)
    end

    if !info_heures.report_ok
      # Opérer une mise à jour complète des années précédentes
      PlageContractuelle.update_heures_complet(personne, annee_civile-1)

      # Relire la structure info_heures
      info_heures=personne.info_heures.where(:annee_civile => annee_civile).first
    end

    report_heures=Hash.new

    # Report du cumul des heures sup
  	if info_heures.report_heures_sup_impose
  		report_heures[:sup]=info_heures.report_heures_sup_impose
      report_heures[:sup_impose]=true
  	else 
  		report_heures[:sup]=info_heures.report_heures_sup
      report_heures[:sup_impose]=false
  	end
    report_heures[:sup_calcule]=info_heures.report_heures_sup

    # Report du solde de vacances
  	if info_heures.report_heures_vacances_impose
  		report_heures[:vacances]=info_heures.report_heures_vacances_impose
      report_heures[:vacances_impose]= true
  	else
  		report_heures[:vacances]=info_heures.report_heures_vacances
      report_heures[:vacances_impose]= false
  	end
    report_heures[:vacances_calcule]=info_heures.report_heures_vacances

  	return report_heures
  end

  def self.reporter_le_bilan_des_heures_sur_lannee_suivante(personne, annee_civile_suivante, cumul_heures_sup, solde_vacances)
    # L'année civile suivante est un entier (2013, 2014, ..)
    info_heures=personne.info_heures.where(:annee_civile => annee_civile_suivante).first
    if !info_heures
      info_heures = InfoHeure.creer(personne, annee_civile_suivante)
    end
    info_heures.report_heures_sup = cumul_heures_sup
    info_heures.report_heures_vacances = solde_vacances
    info_heures.report_ok = true
    info_heures.save!
    InfoHeure.set_report_KO_des_annees_suivantes(personne, annee_civile_suivante)
  end

  def self.set_report_KO_des_annees_suivantes(personne, annee_civile)
    # L'année civile suivante est un entier (2013, 2014, ..)
    # Passe le flag "report_ok" à false pour toutes les années,
    # à partir de l'année civile passée en paramètre 
    infos_heures=personne.info_heures.where{annee_civile >= (my{annee_civile}+1)}
    infos_heures.each do |info_heures|
      info_heures.report_ok = false   
      info_heures.save!
    end
  end

    def self.set_report_KO_des_annees_suivantes_pour_tous(annee_civile)
    # L'année civile suivante est un entier (2013, 2014, ..)
    # Passe le flag "report_ok" à false pour toutes les années,
    # à partir de l'année civile passée en paramètre 
    infos_heures=where{annee_civile >= (my{annee_civile}+1)}
    infos_heures.each do |info_heures|
      info_heures.report_ok = false   
      info_heures.save!
    end
  end

  def self.plages_OK?(personne, annee_civile)
    # L'année civile est un entier (2013, 2014, ..)
    if annee_civile < AnneeCivile.annee_prochaine
      info_heures=personne.info_heures.where(:annee_civile => annee_civile).first
      if info_heures
        return info_heures.plages_ok 
      else
        return false
      end
    else
      return true
    end
  end

  def self.report_OK?(personne, annee_civile)
    # L'année civile est un entier (2013, 2014, ..)
    if annee_civile < (AnneeCivile.annee_prochaine + 1)
      info_heures=personne.info_heures.where(:annee_civile => annee_civile).first
      if info_heures
        return info_heures.report_ok 
      else
        return false
      end
    else
      return true
    end
  end

  def self.set_plages_OK(personne, annee_civile)
  # L'année civile est un entier (2013, 2014, ..)
    info_heures=personne.info_heures.where(:annee_civile => annee_civile).first
    if !info_heures
      # La structure n'existe pas, la créer 
      # S'il s'agit de l'année civile initiale, cette structure est créée avec un "report_ok = true"
      # et des heures reportées égales à 0 
      info_heures = InfoHeure.creer(personne, annee_civile)
    end
    info_heures.plages_ok = true
    info_heures.save!
  end

  def self.set_plages_KO(personne, annee_civile)
  # L'année civile est un entier (2013, 2014, ..)
    infos_heures=personne.info_heures.where(:annee_civile >= annee_civile)
    infos_heures.each do |info_heures|
      if info_heures.annee_civile == annee_civile
        info_heures.plages_ok = false  # Invalider les plages de l'année civile
      else
        info_heures.report_ok = false  # Invalider le report des heures pour les autres
      end
      info_heures.save!
    end
  end

  def self.set_plages_KO_pour_tous(annee_civile)
  # L'année civile est un entier (2013, 2014, ..)
    infos_heures=InfoHeure.where(:annee_civile >= annee_civile)
    infos_heures.each do |info_heures|
      if info_heures.annee_civile == annee_civile
        info_heures.plages_ok = false  # Invalider les plages de l'année civile
      else
        info_heures.report_ok = false  # Invalider le report des heures pour les autres
      end
      info_heures.save!
    end
  end

  def self.set_plages_KO_pour_toutes_les_annees(personne)
    annee_civile_initiale=Configuration.annee_civile_initiale
    infos_heures=personne.info_heures.where{annee_civile.gteq annee_civile_initiale}
    infos_heures.each do |info_heures|
      info_heures.plages_ok = false  # Invalider les plages de l'année
      if !(info_heures.annee_civile == annee_civile_initiale)
        info_heures.report_ok = false   # Invalider le report des heures pour toutes les années sauf l'année initiale
        info_heures.report_heures_sup = 0
        info_heures.report_heures_vacances = 0
      end
      info_heures.save!
    end
  end

  def self.creer(personne, annee_civile)
    # L'année civile est un entier (2013, 2014, ..)
    if annee_civile==Configuration.annee_civile_initiale
      info_heures = InfoHeure.new(\
          {:report_heures_sup => 0.0, :report_heures_sup_impose => nil, \
          :report_heures_vacances => 0.0, :report_heures_vacances_impose => nil, \
          :plages_ok => false, \
          :report_ok => true,
          :annee_civile => annee_civile, \
          :personne_id => personne.id})
    else
      info_heures = InfoHeure.new(\
          {:report_heures_sup => nil, :report_heures_sup_impose => nil, \
          :report_heures_vacances => nil, :report_heures_vacances_impose => nil, \
          :plages_ok => false, \
          :report_ok => false,
          :annee_civile => annee_civile, \
          :personne_id => personne.id})
    end
    info_heures.save!
    return info_heures
  end

end
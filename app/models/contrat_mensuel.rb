# -*- encoding : utf-8 -*-
class ContratMensuel < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # Un contrat se fait pour une personne sur un mois
  belongs_to :personne
  belongs_to :moi
  
  
  
  belongs_to :contrat_annuel
  
  
  
  # Un contrat peut être composé de plusieurs types
  has_many :charge_travail
  has_many :type_contrat, :through => :charge_travail
end

# -*- encoding : utf-8 -*-
class AnneeCivile < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une année civile concerne plusieurs constantes
  has_many :constantes
  # une année civile concerne plusieurs mois
  has_many :mois
  has_many :contrat_annuels
  # une annee civile est en relation ternaire avec une personne et un projet
  has_many :charge
  has_many :personne, :through  => :charge
  has_many :projet, :through  => :charge
  has_many :cahier_charge_assistants
  
  def year
  # Retourne le nombre entier (2013, 2014, etc..) représentant l'année d'un objet de type AnneeCivile
    return annee.to_i
  end
  
  def self.courante
  # Retourne  l'objet de type AnneeCivile représentant l'année courante
    where{(annee.eq Time.now.year)}.first
  end

  def self.precedente
  # Retourne  l'objet de type AnneeCivile représentant l'année précédente
    annee_civile_precedente_val=AnneeCivile.courante.annee.to_i-1
    return AnneeCivile.where("annee = ?", "#{annee_civile_precedente_val}").first
  end

  def self.prochaine
  # Retourne  l'objet de type AnneeCivile représentant l'année prochaine
    annee_civile_prochaine_val=AnneeCivile.courante.annee.to_i+1
    return AnneeCivile.where("annee = ?", "#{annee_civile_prochaine_val}").first
  end
  
  def self.annee_courante
  # Retourne un entier: 2013, 2014, etc.. représentant l'année courante
    return Date.today.year
  end

  def self.annee_prochaine
  # Retourne un entier: 2013, 2014, etc.. représentant l'année prochaine
    return Date.today.year + 1
  end
  
  def self.liste_des_annees_civiles
    #return la liste des objets de type AnneeCivile depuis l'année civile initiale jusqu'à et y compris l'année civile courante
    annee_initiale=Configuration.annee_civile_initiale.to_s
    annees_civiles=AnneeCivile.where{(annee.gteq annee_initiale) & (annee.lteq (AnneeCivile.courante.annee.to_i).to_s)}.sort_by(&:annee)
    return annees_civiles
  end


  # Methodes générales ######################################


  def self.jours_dans_mois_entre(date_debut, date_fin) 
  # Retourne le nombre de jours entre deux dates d'un même mois, limites des deux dates comprises,
    return  date_fin.day - date_debut.day + 1
  end 

  def self.jours_ouvrables_entre(date_debut, date_fin)   
    # Retourne le nombre de jours ouvrables entre deux dates (type "Date")
    # NOTE : tient compte des WE 
      jours_ouvrables = 0
      (date_debut..date_fin).each do |jour|
        if !jour.to_time.sunday?  && !jour.to_time.saturday?
              jours_ouvrables += 1
          end
      end
      return jours_ouvrables
  end
  
  def self.jours_feries_entre(date_debut, date_fin)
    # on compte les jours fériés entre ces deux dates
      nb_jours_feries = 0
      JourAcademique.where{type_jour.eq "Ferie"}.entre(date_debut, date_fin).all.each do |ja|
          if ja.periode == "Jour entier"
            nb_jours_feries += 1
          else
            nb_jours_feries += 0.5
          end
      end
      return nb_jours_feries
  end
  
  def self.jours_compenses_entre(date_debut, date_fin)
    # on compte les jours compensés entre ces deux dates
      nb_jours_compenses = 0
      JourAcademique.where{(type_jour.eq "Compense") | (type_jour.eq "Compense")}.entre(date_debut, date_fin).all.each do |ja|
          if ja.periode == "Jour entier"
            nb_jours_compenses += 1
          else
            nb_jours_compenses += 0.5
          end
      end
      return nb_jours_compenses
  end
  
  
  # Le nombre de jours à effectuer pour un mois donné en tenant compte des WE
  def self.jours_a_effectuer_pour_mois(no_mois, annee_civile)
    AnneeCivile.jours_a_effectuer_entre(AnneeCivile.debut_du_mois(no_mois, annee_civile), AnneeCivile.debut_du_mois(no_mois, annee_civile))
  end
    
  def self.debut_du_mois(no_mois, annee_civile)
    Date.new(annee_civile, no_mois, 1)
  end
  
  def self.fin_du_mois(no_mois, annee_civile)
    Date.new(annee_civile, no_mois, 1)+1.month-1.second
  end
end

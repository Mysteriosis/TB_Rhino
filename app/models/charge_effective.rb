# -*- encoding : utf-8 -*-
class ChargeEffective < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une charge concerne une personne
  belongs_to :personne
  # un charge concerne une unité d'enseignement
  belongs_to :unite_enseignement
  
end

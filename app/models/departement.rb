# -*- encoding : utf-8 -*-
class Departement < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un département concerne une année
  belongs_to :annee_academique
  # un département est supervisé par une personne
  belongs_to :personne
  # un département possède plusieurs filières
  has_many :filieres
  has_many :orientations, :through => :filieres
end

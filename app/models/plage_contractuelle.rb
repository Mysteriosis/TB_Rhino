6678# -*- encoding : utf-8 -*-
class PlageContractuelle < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Associations
  # -----------------------------------------------------------------

  # une plage contractuelle se rattache à une personne
  belongs_to :personne
  has_many :plage_mensuelles

  def horaire_compact?
  	return !(horaire.eql?'1111111111')
  end

  def compensation?
  	return compensation
  end

  def self.entetes_plages_contractuelles(personne, annee_civile)
    # Retourne les plages contractuelles d'une personne pendant une année, juste les entêtes, sans les heures
    # Méthode utilisée à l'occasion de l'affichage des contrats
    # Ces plages sont construites à partir des tables plage_contractuelles & plage_mensuelles
    
    plages = Array.new
    personne.plage_contractuelles.where(:annee_civile=>annee_civile).each do |plage_contractuelle|
      nouvelle_plage=Hash.new
      plages << nouvelle_plage
      nouvelle_plage[:no_plage]=plage_contractuelle.no_plage
      nouvelle_plage[:%]=plage_contractuelle.pourcentage
      nouvelle_plage[:date_debut]=plage_contractuelle.date_debut
      nouvelle_plage[:date_fin]=plage_contractuelle.date_fin
      nouvelle_plage[:hj]=plage_contractuelle.hj
      nouvelle_plage[:horaire]=plage_contractuelle.horaire
      nouvelle_plage[:compensation]=plage_contractuelle.compensation
      nouvelle_plage[:mois]= Array.new
      plage_contractuelle.plage_mensuelles.each do |heures_des_mois|
        nouvelle_plage[:mois] << heures_des_mois
      end
    end

    structure = Hash.new
    structure[:plages]=plages

    return structure
  end

  def self.tableau_des_heures(personne, annee_civile)
    # Retourne le tableau des heures d'une personne pendant une année civile (un entier: 2013, 2014, ..)
    # Si, pour l'année civile concernée, n'ont pas été reportés le solde des vacances et le cumul des heures de l'année précédente, 
    # une mise à jour des heures des années précédentes est effectué, et ainsi de suite jusqu'à l'année civile initiale

    # Ce tableau est construit à partir des tables plage_contractuelles & plage_mensuelles et des heuers saisies
    # Description ........
    # Hash constitué de:
    # [:plages]
    #    Tableau de toutes les plages contractuelles, où chaque plage est elle-même un Hash, constituée de:
    #       [:no_plage]                 Un entier, commençant par 1
    #       [:%]                        Le % d'activité
    #       [:date_debut]               Début de la plage, de type Date
    #       [:date_fin]                 Fin de la plage, de type Date  
    #       [:hj]                       Horaire journalier de la plage
    #       [:horaire]                  Horaire individuel de la plage (char(10), '1111111111' pour un plein temps)  
    #       [:compensation]             Booléen indiquant si la personne compense les ponts   
    #       [:report_heures_sup]        Report calculé des heures sup de la plage contractuelle précédente  
    #       [:report_vacances]          Report calculé des heures de vacances de la plage contractuelle précédente  
    #                                   Pour la première plage, ces reports proviennent des données de l'année précédente
    #       [:mois]                     Le tableau des plages mensuelles où chaque plage mensuelle est un Hash, constitué de:
    #             [:no_mois]            Un entier, commençant par 1
    #             [:date_debut]         Début du mois, de type Date
    #             [:date_fin]           Début du mois, de type Date    
    #             [:jo]                 Nombre de jours ouvrables, adapté à la personne, en fonction de son horaire particulier
    #             [:jf]                 Nombre de jours fériés, adapté à la personne, en fonction de son horaire particulier
    #             [:jp]                 Nombre de ponts (jours à compenser), adapté à la personne, en fonction de son horaire particulier
    #             [:jo_]                Non utilisé
    #             [:jt_]                Jours de travail à effectuer, sans tenir compte des vacances, en tenant compte de la compensation
    #             [:th_]                Heures à effectuer dans le mois, si les vacances ne sont pas prises
    #             [:total_projets]        Le total saisi pour les projets, en heures, pour le mois considéré
    #             [:total_absences]       Le total saisi pour les absences, en heures, pour le mois considéré
    #             [:a_faire_si_absences]  Heures à effectuer dans le mois, en tenant compte des vacances et des absences
    #             [:heures_sup]           Le total des heures supplémentaires, en heures, pour le mois considéré
    #             [:cumul_heures_sup]     Le cumul des heures supplémentaires, en heures, en tenant compte du mois précédent, 
    #                                     ou du report des heures supplémentaires de l'année précédente s'il s'agit du pèremier mois de l'année
    #             [:solde_vacances]       Le solde des heures de vacances, en heures, en tenant compte du solde du mois précédent, 
    #                                     ou du solde l'année précédente s'il s'agit du pèremier mois de l'année    
    #              
    # [:totaux]  Pour les affichages de la dernière colonne..
    #   Hash constitué de
    #   [:th_]                        Total sur l'année des heures à effectuer, si les vacances ne sont pas prises
    #   [:solde_vacances]             Ce qui reste de vacances au final
    #   [:cumul_heures_sup]           Bilan des heures supplémentaires
    #   [:total_heures_sup]           Total des heures supplémentaires sur l'année
    #   [:total_a_faire_si_absences]  Total sur l'année des heures à effectuer, en tenant compte des vacances proses et autres absences

    # [:projets]                       Voir méthode plage_contractuelle.projets
    # [:absences]                      Voir méthode plage_contractuelle.absences
    # [:report_heures_sup]             Report calculé des heures sup de l'année précédente  
    # [:report_heures_vacances]        Report calculé des heures de vacances de l'année précédente 
    #                                  Ces reports proviennent de la table "info_heures", qui, pour chaque année enregistre les
    #                                  reports résultant de l'année précédente.
    #                                  Ces reports résultent d'un calcul issu de l'année précédente..
    #                                  ou sont, le cas échéant, imposés par le RH (prioritaire par rapport à la valeur calculée)
    #                                  Si non imposés par le RH (valeur null trouvée), cela retourne la valeur résultant du calcul
    #                                  ET si aucune valeur calculée disponible (valeur null trouvée encore une fois), cela retourne 0
    # [:report_heures_sup_calcule]     Report calculé des heures sup de l'année précédente (si non imposé par le RH)
    # [:report_heures_vacances_calcule] Report calculé des heures de vacances de l'année précédente (si non imposé par le RH)
    # [:report_heures_sup_impose]      true si report_heures_sup imposé par RH, false sinon
    # [:report_vacances_sup_impose]    true si report_heures_vacances imposé par RH, false sinon    
    # [:total_vacances]                Total des vacances octroyées pour l'année
    
    structure = Hash.new

    # La détermination du report des heures peut impliquer une mise à jour des heures des années précédentes
    # Voir la méthode "InfoHeure.report_des_heures"
    report_des_heures= InfoHeure.report_des_heures(personne, annee_civile)

    structure[:report_heures_sup]= report_des_heures[:sup]
    structure[:report_heures_vacances]= report_des_heures[:vacances]
    structure[:report_heures_sup_calcule]= report_des_heures[:sup_calcule]
    structure[:report_heures_vacances_calcule]= report_des_heures[:vacances_calcule]
    structure[:report_heures_sup_impose]= report_des_heures[:sup_impose]
    structure[:report_heures_vacances_impose]= report_des_heures[:vacances_impose]
    structure[:total_vacances]= 0.0

    plages = Array.new
    personne.plage_contractuelles.where(:annee_civile => annee_civile).sort_by(&:no_plage).each do |plage_contractuelle|    
      # Les plages contractuelles d'une personne sont directement tirées du lien "has_many"
      nouvelle_plage=Hash.new
      plages << nouvelle_plage
      nouvelle_plage[:no_plage]=plage_contractuelle.no_plage
      nouvelle_plage[:%]=plage_contractuelle.pourcentage
      nouvelle_plage[:date_debut]=plage_contractuelle.date_debut
      nouvelle_plage[:date_fin]=plage_contractuelle.date_fin
      nouvelle_plage[:hj]=plage_contractuelle.hj
      nouvelle_plage[:horaire]=plage_contractuelle.horaire
      nouvelle_plage[:compensation]=plage_contractuelle.compensation
      nouvelle_plage[:mois]= Array.new
      plage_contractuelle.plage_mensuelles.sort_by(&:no_mois).each do |plage_mensuelle|
          structure[:total_vacances] += plage_mensuelle.hv
           # Les plages mensuelles d'une plage contractuelle sont directement tirées du lien "has_many"

          plage_mensuelle[:total_projets]=0   # Initialisation du total des heures de projets hors absences saisis dans le mois
                                              # Mis à jour par la méthode "projets"
          plage_mensuelle[:total_absences]=0  # Initialisation du total des heures de projets de type absence saisis dans le mois
                                              # Mis à jour par la méthode "absences"
          plage_mensuelle[:total_vacances]=0  # Initialisation du total des heures de vacances prises dans le mois
                                              # Mis à jour par la méthode "absences"                                            
          nouvelle_plage[:mois] << plage_mensuelle
      end
    end

    structure[:plages]=plages

    structure[:projets]= PlageContractuelle.projets(personne, plages, annee_civile)
    structure[:absences]= PlageContractuelle.absences(personne, plages, annee_civile)

    # Calcul des totaux
    total_des_heures_a_faire_dans_lannee=0.0    # Si vacances non prises
    total_des_heures_a_faire_si_absences=0.0
    bilan_des_heures_supplementaires=0.0
    total_des_heures_supplementaires=0.0
    solde_de_vacances_au_final=0.0


    plages.each do |plage|
      index_plage= plage[:no_plage]-1
      index_mois=0
      plage[:mois].each do |mois|
        a_faire_dans_le_mois=mois[:th_]
        total_des_heures_a_faire_dans_lannee+= a_faire_dans_le_mois

        donnees_du_mois=structure[:plages][index_plage][:mois][index_mois]

        total_projets=  donnees_du_mois[:total_projets]
        total_absences= donnees_du_mois[:total_absences]
        vacances_prises= donnees_du_mois[:total_vacances]

        donnees_du_mois[:a_faire_si_absences]= a_faire_dans_le_mois - total_absences
        total_des_heures_a_faire_si_absences += a_faire_dans_le_mois - total_absences

        total_heures_sup=total_projets - (a_faire_dans_le_mois - total_absences)
        donnees_du_mois[:heures_sup]=total_heures_sup
        total_des_heures_supplementaires += total_heures_sup


        if index_mois >=1 
          donnees_du_mois_precedent= structure[:plages][index_plage][:mois][index_mois-1]
          donnees_du_mois[:cumul_heures_sup]= donnees_du_mois_precedent[:cumul_heures_sup] + total_heures_sup
          donnees_du_mois[:solde_vacances]  = donnees_du_mois_precedent[:solde_vacances] - vacances_prises
        elsif index_plage >= 1
          donnees_du_mois_precedent= structure[:plages][index_plage-1][:mois][(structure[:plages][index_plage-1][:mois].size) - 1]
          donnees_du_mois[:cumul_heures_sup]= donnees_du_mois_precedent[:cumul_heures_sup] + total_heures_sup 
          donnees_du_mois[:solde_vacances]  = donnees_du_mois_precedent[:solde_vacances] - vacances_prises
        else # C'est le premier mois de la première plage
          donnees_du_mois[:cumul_heures_sup]= structure[:report_heures_sup] + total_heures_sup
          donnees_du_mois[:solde_vacances]  = structure[:report_heures_vacances] + structure[:total_vacances] - vacances_prises
        end    
        if (index_plage == (plages.size - 1))  && (index_mois==(plage[:mois].size - 1))
          # C'est le dernier mois de la dernière plage
          solde_de_vacances_au_final= donnees_du_mois[:solde_vacances]
          bilan_des_heures_supplementaires= donnees_du_mois[:cumul_heures_sup]
        end
        index_mois+=1
      end
    end
    structure[:totaux]=Hash.new
    structure[:totaux][:th_]=total_des_heures_a_faire_dans_lannee
    structure[:totaux][:solde_vacances] = solde_de_vacances_au_final
    structure[:totaux][:cumul_heures_sup]= bilan_des_heures_supplementaires
    structure[:totaux][:total_heures_sup]= total_des_heures_supplementaires   
    structure[:totaux][:total_a_faire_si_absences]=total_des_heures_a_faire_si_absences

    return structure
  end




  def self.build_plages_contractuelles(personne, contrats, annee)
  # Construit les plages contractuelles correspondant à la liste des contrats couvrant une annee civile
  # Structure retournée:
  # 
  # Tableau de x plages, où chaque plage est un hash comprenant:
  #   <'no_plage' => un int>
  #   <'date_debut' => une Date>
  #   <'date_fin' => une Date>
  #   <'%' => un int>
  #   < 'horaire' = > '1111111111'    (pour l'instant, voir plus bas "PAR LA SUITE")
  #   < 'compensation' => true        (pour l'instant, voir plus bas "PAR LA SUITE")
  #   < 'hj' => un double>            Horaire journalier de la personne
  #   < 'mois' => un tableau dont chaque élément contient les données de chaque mois, à savoir un Hash:
  #       <'no_mois' => un int>     Correspondant au no du mois dans l'année (1 pour janvier, 12 pour decembre)
  #       <'jo' => un double>       Nombre de Jours ouvrables
  #       <'jf' => un double>       Nombre de Jours fériés     
  #       <'jp' => un double>       Nombre de Jours "pont", à compenser
  #       <'jt_' => un double>      Nombre de Jours de travail potentiels
  #       <'th_' => un double>      Total des heures à effectuer si les vacances ne sont pas prises

  # !!! PAR LA SUITE- A faire---
  # Cette méthode doit faire le comparatif avec la liste courante des plages contractuelles et permettre:
  # - D'éliminer les anciennes plages qui ne sont plus à jour
  # - De spécifier pour chaque nouvelle plage si l'horaire spécifique indiqué est valide ou non et, si non, 
  #   qu'il nécessiterait une spécification par le RH

    # Effacement préalable (si existent) ses plages contractuelles et menuelles exsitantes de la personne pour l'année considérée
    personne.plage_contractuelles.where{annee_civile.eq my{annee}}.each do |plage|
      plage.plage_mensuelles.destroy_all
      plage.destroy
    end



    premier_janvier = Date.new(annee, 1, 1)
    trente_et_un_decembre = Date.new(annee, 12, 31)
  
    # Ajout d'un contrat vide (taux nul) convrant toute l'année
    contrat_vide = {:date_debut => premier_janvier, :date_fin => trente_et_un_decembre, :% => 0 }
    
    # Rabotage de tous les contrats à une durée ne sortant pas des limites d'une année 
    contrats_rabotes = Array.new
    contrats_rabotes << contrat_vide
    contrats.each do |contrat|  
      un_contrat_rabote = {:date_debut => contrat.start_date, :date_fin => contrat.end_date, :% => contrat.pourcentage }
      if un_contrat_rabote[:date_debut]<=premier_janvier
        un_contrat_rabote[:date_debut]=premier_janvier
      end
      if (un_contrat_rabote[:date_fin]==nil) or (un_contrat_rabote[:date_fin]>=trente_et_un_decembre)
        un_contrat_rabote[:date_fin]=trente_et_un_decembre
      end
      contrats_rabotes << un_contrat_rabote
    end
  
    # Détermination des dates "pilier" 
    dates = Array.new
    contrats_rabotes.each do |contrat| 
      dates << contrat[:date_debut]
      dates << contrat[:date_fin]
    end
    dates=dates.sort.uniq
  
    piliers = Array.new
    dates.each do |date|
      nouveau_pilier= {:date => date, :est_un_depart => false, :est_une_fin => false}
      contrats_rabotes.each do |contrat| 
        if contrat[:date_debut]== date
          nouveau_pilier[:est_un_depart]=true
        end
        if contrat[:date_fin]== date
          nouveau_pilier[:est_une_fin]=true
        end
      end
      piliers << nouveau_pilier
    end
  
    # Détermination des plages en fonction des piliers
    plages = Array.new
  
    piliers.each do |pilier|
      if pilier[:est_un_depart]
        nouvelle_plage = {:date_debut => pilier[:date]}  # C'est le début d'une nouvelle plage
        plages << nouvelle_plage
        if plages.size > 1 # Une plage précédente existe et doit prendre fin le jour précédent
          date_fin=(pilier[:date]-1.day)
          if date_fin < plages[plages.size-2][:date_debut]  # La plage qui avait été créée n'a même pas duré un jour
            plages.delete_at(plages.size-2)
          else
            plages[plages.size-2][:date_fin]=date_fin
          end
        end
      end
      if pilier[:est_une_fin] 
        plages[plages.size-1][:date_fin]=(pilier[:date]).to_date # La plage actuelle se termine
        if pilier[:date]< trente_et_un_decembre # Une nouvelle plage commence le jour d'après
          nouvelle_plage = {:date_debut => pilier[:date]+1.day} 
          plages << nouvelle_plage
        end
      end
    end
  
    # Introduction du no de plage et calcul des taux d'activité résultants
    no_plage=1
    plages.each do |plage|
      pourcent=0
      contrats_rabotes.each do |contrat| 
        date=plage[:date_debut]
        if contrat[:date_debut]<= date && contrat[:date_fin]>=date
          pourcent += contrat[:%]
        end
      end
      plage[:%]=pourcent
      plage[:no_plage]=no_plage
      no_plage+=1
    end

    # Introduction de l'Horaire
    # !!! Imposé pour l'instant comme étant un horaire étalé, et la compensation est une constante générale, de l'institut puis de l'individu
    plages.each do |plage|
      plage[:horaire]='1111111111'
      if CstHeure.compensation_obligatoire?(annee) || (CstHeure.compensation_libre?(annee) && personne.compense_les_ponts?)
        plage[:compensation]=true
      else
        plage[:compensation]=false
      end
    end

    # Les données de base pour les calculs ultérieurs
    horaire_journalier_officiel= CstHeure.HJ(annee)
    personne_date_naissance=personne.personne_prive_correspondante.date_naissance
    if personne_date_naissance
      if annee - personne_date_naissance.to_date.year >= CstHeure.seuil_veteran(annee)
        jours_de_vacances_par_annee= CstHeure.JV_2(annee) 
      else
        jours_de_vacances_par_annee= CstHeure.JV_1(annee) 
      end
    else
        jours_de_vacances_par_annee= CstHeure.JV_1(annee) 
    end 

    # Introduction des différents mois constitutifs de la plage contractuelle
    plages.each do |plage|

      # Les données de base relatives aux plages
      horaire= plage[:horaire]
      horaire_compact= !horaire.eql?('1111111111')
      avec_compensation= plage[:compensation]


      plage[:mois]=Array.new
      premier_mois = plage[:date_debut].month
      dernier_mois = plage[:date_fin].month

      # Etablissement des données de chacun des mois de la période
      (premier_mois..dernier_mois).each do |no_mois|
        nouveau_mois = Hash.new
        nouveau_mois[:no_mois]=no_mois

        # Détermination des dates de début et de fin du mois en question
        if premier_mois==dernier_mois
            date_debut=plage[:date_debut]
            date_fin=plage[:date_fin]
        else
            if no_mois==premier_mois
                date_debut=plage[:date_debut]
                date_fin=Date.new(annee, no_mois, 1)+1.month-1.day
            elsif no_mois==dernier_mois
                date_debut=Date.new(annee, no_mois, 1)
                date_fin=plage[:date_fin]  
            else
                date_debut=Date.new(annee, no_mois, 1)
                date_fin=Date.new(annee, no_mois, 1)+1.month-1.day
            end   
        end 
        date_debut=date_debut.to_date
        date_fin=date_fin.to_date
        nouveau_mois[:date_debut]=date_debut
        nouveau_mois[:date_fin]=date_fin


        # Calculs ####################################################################
   
        # Données pour un 100% #######################################################
        _JO=AnneeCivile.jours_ouvrables_entre(date_debut, date_fin).to_f
        _JF=AnneeCivile.jours_feries_entre(date_debut, date_fin).to_f
        _JP=AnneeCivile.jours_compenses_entre(date_debut, date_fin).to_f

        # Jours de travail sans tenir compte des vacances
        _JT_=_JO-_JF                         

        # Jours de vacances
        debut_du_mois= Date.new(annee, no_mois, 1)
        fin_du_mois= Date.new(annee, no_mois, 1) + 1.month - 1.day
        _JV= jours_de_vacances_par_annee/12.0 * (AnneeCivile.jours_dans_mois_entre(date_debut, date_fin).to_f / AnneeCivile.jours_dans_mois_entre(debut_du_mois, fin_du_mois).to_f)
        
        # Jours de travail, vacances déduites
        _JT=_JT_-_JV    

        # Total des heures à effectuer, vacances déduites, sans tenir compte de la compensation
        _TH= (_JT)*CstHeure.HJ(annee)   



        # Données adaptées au cas particulier ##############################################
        # Jours ouvrables, fériés et compensés, en tenant compte de l'horaire compact ou non
        if !horaire_compact
          jo = _JO
          jf = _JF
          jp = _JP
        else
          # !!! Adapter en fonction de l'horaire compact
          jo = _JO
          jf = _JF
          jp = _JP
        end

        # Jours de travail sans tenir compte des vacances, en tenant compte de la compensation
        if avec_compensation
          jt_=jo-jf-jp
        else
          jt_=jo-jf
        end

        # Jours de vacances ocroyés pour le mois
        if horaire_compact
          jv = _JV*plage[:%]/100
        else
          jv=_JV
        end

        # Jours de travail, vacances déduites, en tenant compte de la compensation
        jt = jt_ - jv

        # Jours de travail, vacances déduites, sans tenir compte de la compensation
        jtx = jo - jf - jv

        # Heures à effectuer, vacances déduites, sans tenir compte de la compensation
        if horaire_compact
          th=jtx*horaire_journalier_officiel
        else
          th=_TH*plage[:%]/100.0 
        end

        # Introduction dans le mois
        nouveau_mois[:jo]=jo
        nouveau_mois[:jf]=jf
        nouveau_mois[:jp]=jp
        nouveau_mois[:jtx]=jtx
        nouveau_mois[:jt_]=jt_
        nouveau_mois[:jt]=jt
        nouveau_mois[:TH]=_TH
        nouveau_mois[:th]=th
        nouveau_mois[:jv]=jv

        nouveau_mois[:jo_]=jours_de_vacances_par_annee/12.0   # Utilisé pour el debugging

        # Ajout du mois
        plage[:mois] << nouveau_mois

      end

      # Calcul de l'horaire journalier, tenant compte de la compensation
      somme_th=0.0
      somme_jt=0.0
      plage[:mois].each do |mois|
        somme_th+= mois[:th]
        somme_jt+= mois[:jt]
      end # For each mois

      if somme_jt != 0  # Aucun jour de travail sur la plage!
        horaire_journalier= somme_th/somme_jt
      else
        horaire_journalier=0
      end
      plage[:hj]= horaire_journalier

      # Calcul de "th_" & "hv"
      # "th_"   Total des heures à effectuer sur le mois, tenant compte de la compensation, si vacances non prises
      # "hv"    Total des heures de vacances octroyées pour le mois
      plage[:mois].each do |mois|
        mois[:th_]= mois[:jt_]*horaire_journalier
        mois[:hv]= mois[:jv]*horaire_journalier
      end



    end # For each plage

    return plages;
end


def self.projets(personne, plages, annee_civile)
# Retourne un Hash, constitué de:
# [:total_global]    Total des heures sur l'année saisies pour tous les projets (sauf absences)
# [:detail]          Un tableau projet par projet, où, pour chaque projet on trouve un Hash, constitué de:
#                    [:nom]     Nom interne du projet
#                    [:total]   Total des heures sur l'année saisies pour ce projet
#                    [:heures]  Un tableau, plage contractuelle par plage contractuelle, où, pour chaque plage,
#                               on trouve un tableau, qui, pour chaque mois de la plage, contient le total des heures saisies
# EFFET DE BORD:
#   Cette méthode met à jour le total des projets sur le mois considéré: mois[:total_projets]
#
# Note implémentation: 
# Cette méthode est qui identique à la suivante, self.absences, si ce n'est les différences marquées
# par uncommentaire spécial 

    projets = Hash.new
    projets[:debug]='VIDE'

    projets[:total_global]= 0

    projets[:detail]= Array.new


    no_projet=0
    affectations_projets=personne.affectations_hors_absences   # INSTRUCTION DIFFERENTE de SELF.absences
    affectations_projets.each do |ap|
      total_du_projet_dans_lannee=0
      projet=Hash.new
      projet[:heures]=Array.new
      plages.each do |plage|
        heures_de_la_plage = Array.new
        plage[:mois].each do |mois|
          heures_du_mois=ap.heures_saisies_entre(mois[:date_debut], mois[:date_fin])
          heures_de_la_plage << heures_du_mois
          total_du_projet_dans_lannee += heures_du_mois
          mois[:total_projets] += heures_du_mois  # INSTRUCTION DIFFERENTE de SELF.absences
        end
        projet[:heures] << heures_de_la_plage
      end
      if (total_du_projet_dans_lannee > 0) or (annee_civile==Date.today.year && ap.en_activite?)
        # On garde le projet, il sera affiché
        nom_projet= ap.display_name
        taille_max_autorisee = 20
        depassement= nom_projet.length - taille_max_autorisee
        if depassement > 0 
          nom_projet[(taille_max_autorisee-8)..(taille_max_autorisee-7+depassement)]=".."
        end
        projet[:nom] = nom_projet
        projet[:total] = total_du_projet_dans_lannee

        projet[:heures].each do |heures_saisies_pendant_plage|
          heures_saisies_pendant_plage.each do |heures_saisies_pendant_mois|
            projets[:total_global] += heures_saisies_pendant_mois
          end
        end

        projets[:detail][no_projet] = projet
        no_projet +=1
      end
    end
    return projets
end

def self.absences(personne, plages, annee_civile)
# Retourne un Hash, constitué de:
# [:total_global]    Total des heures sur l'année saisies pour tous les projets de type absences
# [:detail]          Un tableau projet par projet, où, pour chaque projet on trouve un Hash, constitué de:
#                    [:nom]     Nom interne du projet
#                    [:total]   Total des heures sur l'année saisies pour ce projet
#                    [:heures]  Un tableau, plage contractuelle par plage contractuelle, où, pour chaque plage,
#                               on trouve un tableau, qui, pour chaque mois de la plage, contient le total des heures saisies
# EFFET DE BORD:
#   Cette méthode met à jour le total des projets sur le mois considéré mois[:total_projets]

# Note implémentation: 
# Cette méthode est qui identique à la précédente, self.projets, si ce n'est les différences marquées
# par uncommentaire spécial

  projets = Hash.new
  projets[:debug]='VIDE'

  projets[:total_global]= 0

  projets[:detail]= Array.new


  no_projet=0
  affectations_projets=personne.affectations_absences  # INSTRUCTION DIFFERENTE de SELF.PROJETS
  affectations_projets.each do |ap|
    total_du_projet_dans_lannee=0
    projet=Hash.new
    projet[:heures]=Array.new
    plages.each do |plage|
      heures_de_la_plage = Array.new
      plage[:mois].each do |mois|
        heures_du_mois=ap.heures_saisies_entre(mois[:date_debut], mois[:date_fin])
        heures_de_la_plage << heures_du_mois
        total_du_projet_dans_lannee += heures_du_mois
        mois[:total_absences] += heures_du_mois             # INSTRUCTION DIFFERENTE de SELF.projets
        if (ap.projet.id_interne[0..7]).eql?("Vacances")    # INSTRUCTION DIFFERENTE de SELF.projets
          mois[:total_vacances] += heures_du_mois           # INSTRUCTION DIFFERENTE de SELF.projets
        end                                                 # INSTRUCTION DIFFERENTE de SELF.projets     
      end
      projet[:heures] << heures_de_la_plage
    end
    if (total_du_projet_dans_lannee > 0) or (annee_civile==Date.today.year && ap.en_activite?)
      # On garde le projet, il sera affiché

      projet[:nom] = ap.display_name
      projet[:total] = total_du_projet_dans_lannee

      projet[:heures].each do |heures_saisies_pendant_plage|
        heures_saisies_pendant_plage.each do |heures_saisies_pendant_mois|
          projets[:total_global] += heures_saisies_pendant_mois
        end
      end

      projets[:detail][no_projet] = projet
      no_projet +=1
    end
  end
  return projets
end

def self.update_avec_nouveau_contrat(personne, contrat)
  self.reset_plages_contractuelles_pour_toutes_les_annees(personne) 
  # Mise à jour de la moitié des heures
  self.update_heures_partiel(personne)
end

def self.update_apres_mise_a_jour_contrat(personne, contrat)
  self.reset_plages_contractuelles_pour_toutes_les_annees(personne) 
  # Mise à jour de la moitié des heures
  self.update_heures_partiel(personne)
end

def self.update_apres_suppression_contrat(personne, contrat)
  self.reset_plages_contractuelles_pour_toutes_les_annees(personne) 
  # Mise à jour de la moitié des heures
  self.update_heures_partiel(personne)
end

def self.update_plages_contractuelles(personne, annee_civile)
# Où "annee_civile" est un entier (2013, 2014, ..)
# Mise à jour des plages contractuelles pour une personne, pour une année spécifique
# Un contrôle de mise à jour est opéré au préalable
  if !InfoHeure.plages_OK?(personne, annee_civile)
  
      # Construction des nouvelles "plage_contractuelles"
      contrats_de_lannee = personne.contrats.en_cours_dannee(annee_civile)
      plages_contractuelles = PlageContractuelle.build_plages_contractuelles(personne, contrats_de_lannee, annee_civile)
    
      # Mise à jour des tables "plage_contractuelles" et "plage_mensuelles"
      plages_contractuelles.each do |plage|
        plage_ctr = PlageContractuelle.new(\
          {:date_debut => plage[:date_debut], :date_fin => plage[:date_fin],\
           :no_plage => plage[:no_plage],\
           :hj => plage[:hj],\
           :pourcentage => plage[:%],\
           :compensation => plage[:compensation],\
           :horaire => plage[:horaire], \
           :annee_civile => annee_civile})
        plage_ctr.personne=personne
        
        plage_ctr.save!
    
        plage[:mois].each do |mois|
            nouvelle_plage_mensuelle = PlageMensuelle.new(\
              {:no_mois=>mois[:no_mois], \
               :date_debut => mois[:date_debut], :date_fin => mois[:date_fin], \
               :jo_=>mois[:jo_], \
               :jo=>mois[:jo], \
               :jf=>mois[:jf], \
               :jp=>mois[:jp], \
               :jt_ => mois[:jt_], \
               :th_ => mois[:th_], \
               :hv  => mois[:hv]})
    
            nouvelle_plage_mensuelle.plage_contractuelle=plage_ctr
    
            nouvelle_plage_mensuelle.save!
        end
      end
    
      InfoHeure.set_plages_OK(personne, annee_civile)

  end
end

def self.update_heures(personne, annee_civile)
# Où "annee_civile" est un entier (2013, 2014, ..)
# Mise à jour des plages contractuelles pour une personne, pour une année spécifique,
# avec report des heures sur l'année suivante
# Si les heures de l'année précédente n'ont pas été reportées, 
# Un update des années précédentes est indirectement opéré, et ainsi de suite
# Un contrôle de mise à jour est opéré au préalable..
# Retourne le tableau des heures de l'année si ce dernier a été calculé, retourne nil sinon

# Algorithme
# ##########
# Si les plages contractuelles ne sont pas mises à jour,
#   a) le faire, 
#   b) calculer les heures: avec un update éventuel des années précédentes
#   c) reporter les heures (vacances, heures suppl.) sur l'année suivante
# Sinon, 
#   Si l'année suivante signale que le report des heures n'est pas à jour,
#      a) Calculer les heures: avec un update éventuel des années précédentes
#      b) reporter les heures (vacances, heures suppl.) sur l'année suivante
#   FIN si
# FIN si


  if !InfoHeure.plages_OK?(personne, annee_civile)

    PlageContractuelle.update_plages_contractuelles(personne, annee_civile)

    # Opérer le calcul des heures pour mettre à jour le report des heures sur l'année suivante
    # Le calcul des heures peut entrainer indirectement un calcul des heures des années précédentes
    # si le report des heures de l'année précédente n'a pas été effectué
    tableau_des_heures=PlageContractuelle.tableau_des_heures(personne, annee_civile)
    PlageContractuelle.reporter_le_bilan_des_heures_sur_lannee_suivante(tableau_des_heures, personne, annee_civile)

  else
    if !InfoHeure.report_OK?(personne, annee_civile+1)
      # Opérer le calcul des heures pour mettre à jour le report des heures sur l'année suivante
      # Le calcul des heures peut entrainer indirectement un calcul des heures des années précédentes
      # si le report des heures de l'année précédente n'a pas été effectué
      tableau_des_heures=PlageContractuelle.tableau_des_heures(personne, annee_civile)
      PlageContractuelle.reporter_le_bilan_des_heures_sur_lannee_suivante(tableau_des_heures, personne, annee_civile)
    end
  end
  return tableau_des_heures
end




def self.update_heures_complet(personne, annee_civile)
# Opérer une mise à jour complète des heures d'une personne, depuis l'année civile initiale,
# jusqu'à l'année civile passée en paramètre
  annee_civile_a_mettre_a_jour = Configuration.annee_civile_initiale
  while annee_civile_a_mettre_a_jour <= annee_civile do
    PlageContractuelle.update_heures(personne, annee_civile_a_mettre_a_jour)
    annee_civile_a_mettre_a_jour += 1
  end
end

def self.update_heures_complet_pour_toutes_les_annees(personne)
# Opérer une mise à jour complète des heures d'une personne, depuis l'année civile initiale,
# jusqu'à l'année civile courante
  PlageContractuelle.update_heures_complet(personne, Date.today.year)
end

def self.update_heures_partiel(personne)
# Opérer une mise à jour des heures d'une personne, sur deux-trois ans depuis l'année civile initiale,
    annee_civile = Configuration.annee_civile_initiale
    annee_finale = annee_civile + (AnneeCivile.annee_prochaine - annee_civile)/2
    while annee_civile < annee_finale do
      PlageContractuelle.update_heures(personne, annee_civile)
      annee_civile += 1
    end
end

def self.reset_plages_contractuelles_pour_toutes_les_annees(personne) 
    # Destructions des données relatives à la personne
    # Détruire en premier les heures, qui dépendent des plages contractuelles
    personne.plage_contractuelles.each do |plage|
      plage.plage_mensuelles.destroy_all
      plage.destroy
    end
  
  
    InfoHeure.set_plages_KO_pour_toutes_les_annees(personne)

    # Mise à jour de toutes les plages contractuelles
    annee_civile = Configuration.annee_civile_initiale
    annee_courante = AnneeCivile.annee_courante
    while annee_civile <= annee_courante do
      PlageContractuelle.update_plages_contractuelles(personne, annee_civile)
      annee_civile += 1
    end

end

####################################################################
private

  def self.reporter_le_bilan_des_heures_sur_lannee_suivante(tableau_des_heures, personne, annee_civile)
    # Mise à jour en base de données dans la table "info_heures" du bilan des heures à reporter pour l'année suivante
    # A savoir: 1. solde des heures de vacances
    #           2. Cumul des heures supplémentaires
    # annee_civile: année actuelle - Un entier comme 2013, 2014, ..
    InfoHeure.reporter_le_bilan_des_heures_sur_lannee_suivante(personne, annee_civile+1, \
                                  tableau_des_heures[:totaux][:cumul_heures_sup], \
                                  tableau_des_heures[:totaux][:solde_vacances]) 
  end

end
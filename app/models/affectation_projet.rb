# -*- encoding : utf-8 -*-
class AffectationProjet < ActiveRecord::Base
  
  # Couleurs des affectations
  COLORS = ["#FF5ADC","#31528E","#368942","#B63634","#FF7940","#76AB01","#0E6A00","#083500","#067778"]
  #COLORS = ["#FF5ADC","#31528E","#368942"]
  belongs_to :projet
  belongs_to :personne
  belongs_to :contrat
  belongs_to :engagement
  has_many   :plage_horaire_saisies

  #Module de planificartion
  has_many :planifications
  has_many :jalons
  
  #validate :affectation_projet_personne_unique
  validate :dans_limites_projet
  validate :debut_avant_fin
  
  validates :projet, :presence=>{:message => "Champ requis"}
  validates :personne, :presence=>{:message => "Champ requis"}
  validates :display_name, :presence=>{:message => "Champ requis"}
  #validates :engagement, :presence=>{:message => "Champ requis" , :if => :contrat_requis?}
  validates :engagement, :presence=>{:message => "Champ requis"}
  validates :charge_horaire, :numericality=>{:message => "Doit être un nombre" , :if => Proc.new { |ap| ap.charge_horaire }}
  
  attr_accessor :budget
 
  def init_color
    affectations=self.personne.affectation_projets
    taux_utilisation_des_couleurs=Hash.new
    COLORS.each do |c|
      taux_utilisation_des_couleurs[c]=0
    end
    affectations.each do |af|
      if af.display_color
        if taux_utilisation_des_couleurs.has_key?(af.display_color)
            taux_utilisation_des_couleurs[af.display_color]+=1
        end
      end
    end
    couleur_la_plus_oubliee=COLORS[0]
    taux_utilisation_de_la_couleur_la_plus_oubliee=taux_utilisation_des_couleurs[COLORS[0]]
    COLORS.each do |c|
      if taux_utilisation_des_couleurs[c]<taux_utilisation_de_la_couleur_la_plus_oubliee
        taux_utilisation_de_la_couleur_la_plus_oubliee=taux_utilisation_des_couleurs[c]
        couleur_la_plus_oubliee=c
      end
    end
    self.display_color=couleur_la_plus_oubliee
  end

  # Retourne le champ display_name de la base de données
  def display_name
    self.display_label
  end
  
  def display_name=(nom)
    self.display_label=nom
  end
  
  def set_display_label(nom)
    self.display_label=nom
  end
  
  def display_color
    self.display_couleur
  end
  
  def display_color=(couleur)
    self.display_couleur=couleur
  end
  
  
  def heures_saisies
    self.plage_horaire_saisies.sum(:heures)
  end

  def contrat_requis?
    projet=self.projet
    return projet.type_projet.contrat_a_renseigner
  end
  
  def saisie_heures_possible?
  # Voir méthode suivnate
    if self.en_activite?
      return (personne.type_personne == TypePersonne.find_by_typePersonne("Collaborateur") && projet.type_projet.saisie_heures_possible_collaborateurs) || (personne.type_personne == TypePersonne.find_by_typePersonne("Professeur") && projet.type_projet.saisie_heures_possible_profs)
    else
      return false
    end
  end
  
  def saisie_heures_possible_by_RH?
  # A la différence de la méthode précédente, la saisie est possible même si l'affectation n'est pas active
  # A savoir, la saisie est possible même si:
  #   - l'affectation est hors-date
  #   - l'affectation est desactivée
  #   - le projet correspondant est hors-date ou desactivé
  
    if self.projet.is_archive?
        return false
    else
      return (personne.type_personne == TypePersonne.find_by_typePersonne("Collaborateur") && projet.type_projet.saisie_heures_possible_collaborateurs) || (personne.type_personne == TypePersonne.find_by_typePersonne("Professeur") && projet.type_projet.saisie_heures_possible_profs)
    end
  end
  
  def est_academique?
  # Est associée à un projet de type academique?
    return self.projet.est_un_projet_academique?
  end
  def pour_annee_academique?(annee_ac)
  # Est associée à une certaine année académique?
    return self.projet.annee_academique_realisation.id == annee_ac.id
  end

  def AffectationProjet.affectations (personne, annee_ac, type_prj)
    # Retourne toutes les affectations de la personne "personne" pour l'année academique "annee_ac" et pour un type donné de projet "type_prj"
    #affectations=personne.affectation_projets.where(:projet_id => Projet.where({:type_projet_id => TypeProjet.where(:nom=>type_prj).first.id})
    affectations=personne.affectation_projets.select{|aff| aff.est_academique?}
    if affectations.size>0
      affectations=affectations.select{|aff| aff.pour_annee_academique?(annee_ac)}
    end
    return affectations
  end

  def AffectationProjet.affectations_academiques (personne, annee_ac)
    # Retourne toutes les affectations académiques de la personne "personne" pour l'année academique "annee_ac"
    return AffectationProjet.affectations(personne, annee_ac, "Académique")
  end

  def AffectationProjet.charge_effective_totale_en_heures(personne, annee_ac)
    # Retourne le total des heures d'affectations de projets academiques pour une personne donnée
    affectations = AffectationProjet.affectations_academiques(personne, annee_ac)
    total = 0.0
    affectations.each do |aff|
      if aff.charge_horaire
        total+=aff.charge_horaire
      end
    end
    return total
  end

  def plages_horaires_saisies_dans_lannee_courante
    aujourdhui=Time.now
    heure_debut=aujourdhui.beginning_of_year
    heure_fin=aujourdhui.end_of_year
    plages_saisies=PlageHoraireSaisie.where(["affectation_projet_id =? and debut >= ? and fin <= ?", self.id, heure_debut, heure_fin]).order(:debut)  
    return plages_saisies
  end
  
  def plages_horaires_saisies_dans_premier_semestre_courant
    aujourdhui=Time.now
    heure_debut=aujourdhui.beginning_of_year
    heure_fin=Time.new(aujourdhui.year,6,30, 24,0,0) # 30 Juin minuit
    plages_saisies=PlageHoraireSaisie.where(["affectation_projet_id =? and debut >= ? and fin <= ?", self.id, heure_debut, heure_fin]).order(:debut)  
    return plages_saisies
  end
  
  def plages_horaires_saisies_dans_second_semestre_courant
    aujourdhui=Time.now
    heure_debut=Time.new(aujourdhui.year,7,1, 0,0,0) # 1er Juillet 
    heure_fin=aujourdhui.end_of_year
    plages_saisies=PlageHoraireSaisie.where(["affectation_projet_id =? and debut >= ? and fin <= ?", self.id, heure_debut, heure_fin]).order(:debut)  
    return plages_saisies
  end
  
  def plages_horaires_saisies_pendant_les_mois (no_mois_debut, no_mois_fin)
    heure_debut=Time.new(Time.now.year, no_mois_debut, 1).at_beginning_of_month
    heure_fin=Time.new(Time.now.year, no_mois_fin, 1).at_end_of_month
    plages_saisies=PlageHoraireSaisie.where(["affectation_projet_id =? and debut >= ? and fin <= ?", self.id, heure_debut, heure_fin]).order(:debut)  
    return plages_saisies
  end
  
  def heures_saisies_dans_lannee?(annee_civile)
    total=0
    mois = Moi.where{annee_civile_id.eq annee_civile.id}.joins(annee_civile).order("annee_civiles.annee, num").all
    mois.each do |mois|
      heures = heures_pour_mois(mois)
      total += heures
    end
    return total!=0
  end

  def self.pour_exportation_sagex
    where{export_sagex.eq true}
  end
  
  def heures_pour_mois(mois)
    plage_horaire_saisies.entre(mois.debut, mois.fin).sum(:heures)
  end
  
  def heures_pour_mois_vrapide(mois)
  # Version améliorée question efficacité de heures_pour_mois(mois)
      return PlageHoraireSaisie.where{
        (affectation_projet_id.eq self.id) & (debut.gteq mois.debut.to_time) & (fin.lteq mois.fin.to_time)
      }.sum(:heures)
  end

  def heures_saisies_entre(date_debut, date_fin)
  # date_debut et date_fin de type Date!
      return PlageHoraireSaisie.where{
        (affectation_projet_id.eq self.id) & (debut.gteq date_debut.to_time.beginning_of_day) & (fin.lteq date_fin.to_time.end_of_day)
      }.sum(:heures)
  end
  
  def heures_pour_annee(annee_civile)
    plage_horaire_saisies.entre(Date.new(annee_civile.annee.to_i,1,1).to_time, Date.new(annee_civile.annee.to_i,12,1)+1.month-1.second).sum(:heures)
  end
  
  def charge_enseignement
    projet=self.projet
    ue=projet.unite_enseignement
    if projet.type_projet.nom.eql? ("Labo-Bachelor") 
      facteur = 2.2
      nbPeriodesParSemaine=ue.nbPeriodesPratiquesSemaine
      nbSemaines=ue.periode.dureeSemaine
      charge=nbPeriodesParSemaine*nbSemaines*facteur
    elsif projet.type_projet.nom.eql?("Cours-Bachelor")
      facteur = 2.2
      nbPeriodesParSemaine=ue.nbPeriodesTheoriquesSemaine
      nbSemaines=ue.periode.dureeSemaine
      charge=nbPeriodesParSemaine*nbSemaines*facteur
    elsif projet.type_projet.nom.eql?("Cours-Master")
      facteur = 3
      nbPeriodesParSemaine=ue.nbPeriodesTheoriquesSemaine
      nbSemaines=ue.periode.dureeSemaine
      charge=nbPeriodesParSemaine*nbSemaines*facteur
    elsif projet.type_projet.nom.eql?("Labo-Master")
      if self.personne.is_collaborateur?
        nbSemaines=ue.periode.dureeSemaine
        charge=ue.nbPeriodesTheoriquesSemaine*nbSemaines/2
      else
        facteur = 2.2
        nbPeriodesParSemaine=ue.nbPeriodesTheoriquesSemaine
        nbSemaines=ue.periode.dureeSemaine
        charge=nbPeriodesParSemaine*nbSemaines*facteur
      end
    end
    if projet.type_projet.nom.eql?("Examen-Bachelor") or  projet.type_projet.nom.eql?("Examen-Master")
      charge=16*ue.chargeExamen
    end    
    return charge
  end
  
  # Une affectation est active dans la mesure où son flag "est_actif" vaut true, et dans la mesure où cette dernière n'est pas "hors date courante"
  # et encore si le projet correspondant est lui-même "actif"
  def en_activite?
    if self.projet.is_archive?
      return false
    else
      if self.projet.en_activite?
        return !hors_date? && !est_desactivee?
      else
        return false
      end
    end
  end
  
  def hors_date?
    ok=true
    if date_debut
      if  Date.today < date_debut
        ok=false
      end
    end
    if date_fin
      if  Date.today > date_fin
        ok=false
      end
    end
    return !ok
  end
  
  def est_desactivee?
    return !self.est_active
  end
  
  def hors_limites_contrats
  # Pour contrôle: L'affectation se trouve-t'elle hors des limites de tous les contrats associés à l'engagement?
    contrats = self.engagement.contrats
    contrats.each do |contrat|
      contrat_ok=true
      if contrat.start_date  && date_debut && date_debut < contrat.start_date
        contrat_ok=false
      elsif contrat.end_date && date_fin && date_fin > contrat.end_date
        contrat_ok=false
      end
      if contrat_ok == true
        return false
      end
    end
    return true
  end

private
  def affectation_projet_personne_unique
    if (new_record? or projet_id_changed? or personne_id_changed?) and personne and projet and AffectationProjet.where{(projet_id.eq projet.id) & (personne_id.eq personne.id)}.first
      errors.add(:personne, "Cette personne est déjà affectée à ce projet")
      errors.add(:projet, "Cette personne est déjà affectée à ce projet")
    end
  end
  
  def debut_avant_fin
    if date_debut && date_fin && date_debut > date_fin
      errors.add(:date_debut, "Doit être plus petit que la date de fin")
      errors.add(:date_fin, "Doit être plus grand que la date de début")
    end
  end
  
  #def dans_limites_contrat
  ## Pour validation: contrôle si l'affectation se trouve dans les limites du contrat
  ## Cette validation n'est plus opérée à partir du 24 septembre 2012
  ## Méthode datant de l'époque où les affectations etient associées à des contrats plutot qu'à des projets
  ## Un simple avertissement est généré
  #  if contrat
  #    if contrat.start_date  && date_debut && date_debut < contrat.start_date
  #      errors.add(:date_debut, "Le contrat ne commence que le #{contrat.start_date.strftime('%d.%m.%Y')}")
  #    end
  #    if contrat.end_date && date_fin && date_fin > contrat.end_date
  #      errors.add(:date_fin, "Le contrat se termine le #{contrat.end_date.strftime('%d.%m.%Y')}")
  #    end
  #  end
  #end

  
  def dans_limites_projet
    if projet
      if projet.date_debut && date_debut && date_debut < projet.date_debut
        errors.add(:date_debut, "Le projet ne commence que le #{projet.date_debut.strftime("%d.%m.%Y")}")
      end
      if projet.date_fin && date_fin && date_fin > projet.date_fin 
        errors.add(:date_fin, "Le projet se termine le #{projet.date_fin.strftime("%d.%m.%Y")}")
      end
    end
  end
  
  def display_name_non_vide
   if display_name
     if display_name.strip.empty?
         errors.add(:display_name, "Ce champ ne doit pas être vide")
     end
   end
  end


  
end

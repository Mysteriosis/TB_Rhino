class Critere < ActiveRecord::Base

  belongs_to :element1, :class_name => 'Element'
  belongs_to :element2, :class_name => 'Element'
  has_one :critere1, :class_name => 'Condition'
  has_one :critere2, :class_name => 'Condition'

end
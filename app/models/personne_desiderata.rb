# -*- encoding : utf-8 -*-
class PersonneDesiderata < ActiveRecord::Base
  self.table_name = 'personne_desideratas'
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # Un desiderata de personne comprend la personne et le desiderata
  belongs_to :personne
  belongs_to :desiderata
end

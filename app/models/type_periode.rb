# -*- encoding : utf-8 -*-
class TypePeriode < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un type de période concerne plusieurs périodes
  has_one :periode
end

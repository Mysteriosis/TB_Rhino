# -*- encoding : utf-8 -*-
class EvenementProjet < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un événement de projet possède un type d'événement de projet
  belongs_to :type_evenement_projet
  # Les événements concernent plusieurs projets
  has_many :date_evenement_projet
  has_many :projet, :through => :date_evenement_projet

end

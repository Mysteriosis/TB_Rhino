# -*- encoding : utf-8 -*-
class Charge < ActiveRecord::Base
  validates_presence_of :nbHeuresAnnuelles, :message => "Erreur, ne peut pas être vide"
  validates_numericality_of :nbHeuresAnnuelles, :greater_than_or_equal_to => 1, :less_than => 1871, :message => "Erreur, doit être numérique positif, plus petit que 1871"
  
  validates_presence_of :coutHoraire, :message => "Erreur, ne peut pas être vide"
  validates_numericality_of :coutHoraire, :greater_than_or_equal_to => 1, :message => "Erreur, doit être numérique positif"
  
  
  
# # -----------------------------------------------------------------
# # Association
# # -----------------------------------------------------------------
# # une charge concerne une personne
# belongs_to :personne
# # une charge concerne un projet
# belongs_to :projet
# # une charge concerne une annee civile
# belongs_to :annee_civile
# 

   #---------------------------------------------------------------------------------------------------
   # Nom : charge_examen
   # But : Cette fonction retourne le nombre d'heure consacrées au examens pour un professeur
   # Parametre : personne => le professeur dont on souhaite connaitre le nombre d'heure
   #             annee => L'année accadémique pour laquelle les heures doivent être calculées
   #---------------------------------------------------------------------------------------------------
   def Charge.charge_examen(professeur, annee)
     return 0
   end

#  #---------------------------------------------------------------------------------------------------
#   # Nom : heures_annuelles_total
#   # But : Cette fonction retourne le nombre d'heure total pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le nombre d'heure
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def heures_annuelles_total (personne, annee)
#     return Charge.sum(:nbHeuresAnnuelles, :conditions => {:personne_id => personne.id, :annee_civile_id => annee.id})
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : age
#   # But : Cette fonction retourne l'age d'une personne
#   # Parametre : date_naissance => La date de naissance de la personne
#   #---------------------------------------------------------------------------------------------------  
#   def age (date_naissance)
#
#     if date_naissance != nil
#       age = Date.today.year - date_naissance.year
#       if Date.today.month < date_naissance.month || 
#             (Date.today.month == date_naissance.month && date_naissance.day >= Date.today.day)
#         age = age - 1
#       end
#       return age
#     else
#       return 0
#     end
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : solde
#   # But : Cette fonction retourne le solde des heures pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le solde
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def solde (personne, annee)
#     # Si la personne a moins de 60 ans, elle doit effectuer 1'870 heure sinon, 1'830 heures
#     @effectue = Charge.sum(:nbHeuresAnnuelles, :conditions => {:personne_id => personne.id, :annee_civile_id => annee.id})
#     personne_priv = PersonnePrive.find(:first, :conditions=>{:personne_id => personne.id})
#     if (age( personne_priv.date_naissance ) < 60)
#       @a_faire = 1870
#     else
#       @a_faire = 1830
#     end
#     return @effectue - @a_faire
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : heures_annuelles_enseignement
#   # But : Cette fonction retourne le nombre d'heure réservé à l'enseignement pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le nombre d'heure
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def heures_annuelles_enseignement (personne, annee)
#
#   return Charge.sum(:nbHeuresAnnuelles, :include => [:projet => :type_projet], 
#                       :conditions => ['charges.personne_id = :personne AND 
#                       (type_projets.typeProjet LIKE :labo OR type_projets.typeProjet LIKE :theo)', 
#                       {:personne => personne.id, :labo => "Laboratoire", :theo => "Théorie"}])
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : heures_annuelles_bachelor
#   # But : Cette fonction retourne le nombre d'heure consacrées au diplomes bachelor pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le nombre d'heure
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def heures_annuelles_bachelor (personne, annee)
#
# #    return Charge.sum(:nbHeuresAnnuelles, :from => "charges, projets, type_projets as parent, type_projets as enfant, personnes", 
# #                                          :conditions => "charges.projet_id = projets.id and
# #                                          projets.type_projet_id = enfant.id and
# #                                          parent.id = enfant.type_projet_id and 
# #                                          personnes.id = charges.personne_id and
# #                                          parent.typeProjet LIKE 'Diplôme Bachelor'  
# #                                          and personnes.id = #{id_personne}")
#
#     # Version RoR  /!\ Ne peut pas récupérer le parent du type de projet /!\ 
# #    return Charge.sum(:nbHeuresAnnuelles, :joins => [:projet => :type_projet], 
# #                      :conditions => {:personne_id => id_personne, "type_projets.typeProjet" => "Diplôme Bachelor",
# #                      :annee_civile_id => annee})
#
#     liste_charges = Charge.find(:all, :conditions => {:personne_id => personne.id, :annee_civile_id => annee.id})
#     nb_heure = 0;
#
#     liste_charges.each do |charge|
#       if charge.projet.type_projet.typeProjet == "Temps plein B." || charge.projet.type_projet.typeProjet == "Formation en emploi B."
#         nb_heure += charge.nbHeuresAnnuelles
#       end
#     end
#
#     return nb_heure
#
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : heures_annuelles_master
#   # But : Cette fonction retourne le nombre d'heure consacrées au diplomes master pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le nombre d'heure
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def heures_annuelles_master (personne, annee)
#
#     return Charge.sum(:nbHeuresAnnuelles, :joins => [:projet => :type_projet], 
#                       :conditions => {:personne_id => personne.id, "type_projets.typeProjet" => "Diplôme Master",
#                       :annee_civile_id => annee.id})
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : heures_annuelles_retd
#   # But : Cette fonction retourne le nombre d'heure consacrées à la R&D pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le nombre d'heure
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def heures_annuelles_retd (personne, annee)
#
#     return Charge.sum(:nbHeuresAnnuelles, :joins => [:projet => :type_projet], 
#                       :conditions => {:personne_id => personne.id, "type_projets.typeProjet" => "Recherche et Développement",
#                       :annee_civile_id => annee.id})
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : heures_annuelles_mission
#   # But : Cette fonction retourne le nombre d'heure consacrées au missions pour une personne
#   # Parametre : personne => la personne dont on souhaite connaitre le nombre d'heure
#   #             annee => L'année civile pour laquelle les heures doivent être calculées
#   #---------------------------------------------------------------------------------------------------
#   def heures_annuelles_mission (personne, annee)
#
#     return Charge.sum(:nbHeuresAnnuelles, :joins => [:projet => :type_projet], 
#                       :conditions => {:personne_id => personne.id, "type_projets.typeProjet" => "Mission",
#                       :annee_civile_id => annee.id})
#   end
#

#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : annees_enseignement
#   # But : Cette fonction retourne la liste des années chargées pour une personne
#   # Parametre : personne => personne dont on souhaite connaitre la liste des années chargées
#   #---------------------------------------------------------------------------------------------------
#   def annees_enseignement (personne, annee)
#
#     return AnneeCivile.find(:all, :select => 'DISTINCT annee_civiles.*', :joins => [:charge], :order => 'annee_civiles.annee ASC',
#                       :conditions => ['charges.personne_id = :id_personne AND annee_civiles.annee <= :annee_annee', {:id_personne => personne.id, :annee_annee => annee.annee }])
#
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : charges_annuelles
#   # But : Cette fonction retourne la liste des charges pour une personne à une année donnée
#   # Parametre : personne => personne dont on souhaite connaitre la liste des charges
#   #             annee => année pour laquelle le calcul doit être effectuer
#   #---------------------------------------------------------------------------------------------------
#   def charges_annuelles (personne, annee)
#     return Charge.find(:all, :joins [:annee_civile], :conditions => {:personne_id => personne.id, :annee_civile_id => annee.id})
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : detail_enseignement
#   # But : Cette fonction retourne la liste des enseignement correspondant au filtre pour une 
#   #       personne à une année donnée
#   # Parametre : personne => personne dont on souhaite connaitre la liste des enseignements
#   #             annee => année academique pour laquelle la liste doit être récupéré
#   #             filter => type d'UE a filtrer (Bachelor, Master)
#   #---------------------------------------------------------------------------------------------------
#   def detail_enseignement (personne, annee, type_ue)
#     #return Charge.find(:all, :from => ['charges, projets, unite_enseignements, type_unite_enseignements, periodes, annee_academiques'], 
#     #                  :joins => [:projet => :unite_enseignement, :unite_enseignement => :type_unite_enseignement, :unite_enseignement => :periode, :periode => :annee_academique],
#     #                  :conditions => ['annee_academiques.id = :id_annee AND type_unite_enseignements.id = :id_type AND charges.personne_id = :id_personne', 
#     #                  {:id_annee => annee.id, :id_type => type_ue.id, :id_personne => personne.id}])
#     return UniteEnseignement.find(:all, :from => "charges, projets, unite_enseignements, periodes, type_periodes, annee_academiques, type_unite_enseignements",
#                                   :conditions => [#Jointures
#                                                   'charges.projet_id = projets.id AND
#                                                   projets.unite_enseignement_id = unite_enseignements.id AND
#                                                   unite_enseignements.type_unite_enseignement_id = type_unite_enseignements.id AND
#                                                   unite_enseignements.periode_id = periodes.id AND
#                                                   periodes.type_periode_id = type_periodes.id AND
#                                                   periodes.annee_academique_id = annee_academiques.id AND
#
#                                                   charges.personne_id = :id_personne AND
#                                                   annee_academiques.id = :id_annee AND
#                                                   type_unite_enseignements.id = :id_type', 
#                                                   {:id_personne => personne.id, :id_annee => annee.id, :id_type => type_ue.id}])
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : type_ue
#   # But : Cette fonction retourne le type d'UE recherché
#   # Parametre : type_description => le type à rechercher (ex: bachelor, master, ...)
#   #---------------------------------------------------------------------------------------------------
#   def type_ue (type_description)
#     return TypeUniteEnseignement.find(:first, :conditions => ['type_unite LIKE :desc', {:desc => type_description}])
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : heures_hebdo
#   # But : Cette fonction retourne le nombre d'heure par semaine attribuées à une UE.
#   #       Par exemple, si une UE est trimestrielle, il faudrat diviser son nombre d'heure par 2
#   # Parametre : unite => l'e type à rechercher (ex: bachelor, master, ...)'ue à rechercher
#   #             type => le type d'enseignement de l'ue (labo ou théorie)
#   #---------------------------------------------------------------------------------------------------
#   def heures_ue_par_an (unite, type)
#
#     heures_hebdo = 0
#     if unite.periode.type_periode.type == "Trimestriel"
#       if type == "labo"
#         heures_hebdo = unite.nbPeriodesPratiquesSemaine / 2
#       else
#         heures_hebdo = unite.nbPeriodesTheoriquesSemaine / 2
#       end
#     else
#       if type == "labo"
#         heures_hebdo = unite.nbPeriodesPratiquesSemaine
#       else
#         heures_hebdo = unite.nbPeriodesTheoriquesSemaine
#       end
#     end
#
#     return heures_hebdo * unite.periode.dureeSemaine
#   end
#
#   #---------------------------------------------------------------------------------------------------
#   # Nom : recupere_coef
#   # But : Cette fonction retourne le coeficient attribué a une UE en fonction de son type (examen, labo, ...)
#   # Parametre : unite => l'unite d'enseignement à chercher
#   #             type => le type d'enseignement de l'ue (labo ou théorie)
#   #---------------------------------------------------------------------------------------------------
#   def recupere_coef(unite, type)
#
#   end
# 
end

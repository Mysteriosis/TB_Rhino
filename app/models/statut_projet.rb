# -*- encoding : utf-8 -*-
class StatutProjet < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un statut concerne plusieurs projets
  has_one :statut_projet
end

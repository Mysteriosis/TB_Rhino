# -*- encoding : utf-8 -*-
class CompetenceDomaine < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # un domaine est connu par des personnes
  has_many :personnes, :through => :competence_personne_domaines
  has_many :competence_personne_domaines
  #has_and_belongs_to_many :personne, :join_table => 'competence_personne_domaines'
end

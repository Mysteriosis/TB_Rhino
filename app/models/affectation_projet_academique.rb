# -*- encoding : utf-8 -*-
class AffectationProjetAcademique < AffectationProjet
  validates_presence_of :taux, :message => "champ obligatoire"
  validates_numericality_of :taux, :only_integer => true, :message => "doit être un nombre entier"
  validates_inclusion_of :taux, :in => 10..100, :message => "doit être compris entre 10 et 100"
end

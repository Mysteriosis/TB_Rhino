# -*- encoding : utf-8 -*-
class JourAcademique < ActiveRecord::Base
  
  PERIODES = ["Jour entier", "Matin", "Apres-midi"]
  TYPES = ["Ferie","Compense"]

  validates_presence_of :type_jour, :periode, :message => "Champ obligatoire"
  validates_inclusion_of :type_jour, :in => TYPES, :message => "est invalide"
  validates_inclusion_of :periode, :in => PERIODES, :message => "est invalide"

  def display_name
    date.strftime("%d.%m.%Y")+": #{periode} #{type_jour}"
  end
  
  # retourne les jours fériés entre deux dates ( comprises )
  def self.entre(h_debut, h_fin)
    where{
      (date.gteq my{h_debut.to_date}) & (date.lteq my{h_fin.to_date})
    }
  end

  # retourne les jours académiques entre deux dates ( comprises ), triés par date
  def self.entre_deux_dates(date_debut, date_fin)
    where{
      (date.gteq my{date_debut}) & (date.lteq my{date_fin})
    }.sort_by(&:date)
  end

  # retourne les jours fériés entre deux dates ( comprises ), triés par date
  def self.feries_entre_deux_dates(date_debut, date_fin)
    where{
      (date.gteq my{date_debut}) & (date.lteq my{date_fin}) & (type_jour.eq "Ferie")
    }.sort_by(&:date)
  end

  # retourne les jours académiques entre deux dates ( comprises ), triés par date
  def self.compenses_entre_deux_dates(date_debut, date_fin)
    where{
      (date.gteq my{date_debut}) & (date.lteq my{date_fin}) & (type_jour.eq "Compense")
    }.sort_by(&:date)
  end
end

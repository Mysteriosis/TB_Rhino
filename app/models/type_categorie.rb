# -*- encoding : utf-8 -*-
class TypeCategorie < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # une catégorie concerne plusieurs suivis
  has_one :suivi
end

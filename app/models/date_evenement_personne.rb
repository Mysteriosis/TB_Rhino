# -*- encoding : utf-8 -*-
class DateEvenementPersonne< ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # Une date d'événement de personne comprend l'événement et la personne
  belongs_to :personne
  belongs_to :evenement_personne
end

# -*- encoding : utf-8 -*-
class TypeIdExterne < ActiveRecord::Base
  
  validates_presence_of :nom, :message => "Champ requis"
  
  has_many :projets
  
  def display_name
    nom
  end
  
end

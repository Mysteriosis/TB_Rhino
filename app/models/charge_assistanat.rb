# -*- encoding : utf-8 -*-
class ChargeAssistanat < ActiveRecord::Base
  # -----------------------------------------------------------------
  # Charge d'assistanat associé à une unité d'enseignement
  # -----------------------------------------------------------------

  belongs_to :unite_enseignement
  
  # La personne correspondante
  belongs_to :personne
  
  validates :personne, :presence => {:message => "Champ requis"}
  validates :unite_enseignement, :presence => {:message => "Champ requis"}
  validate  :controle_taux_heures

  # !! Spécifique à ChargeAssistanat
  validate  :controle_priorite
  
  # !! Spécifique à ChargeAssistanat
   def controle_priorite
    if !priorite
      errors.add(:priorite, "La priorité doit être signalée")
    end
    if (priorite and priorite < 1) or  (priorite and priorite > 2)
      errors.add(:priorite, "La priorité doit être comprise entre 1 et 2")
    end
  end

  
  def controle_taux_heures
    if !taux and !heures
      errors.add(:taux, "Le taux doit être signalé si les heures ne le sont pas")
      errors.add(:heures, "Les heures doivent être signalées si le taux ne l'est pas")
    end
    if taux and heures
      errors.add(:taux, "Signaler soit le taux de charge, soit la charge en heures")
      errors.add(:heures, "Signaler soit le taux de charge, soit la charge en heures")
    end
    if taux and taux <= 0
      errors.add(:taux, "Le taux doit être supérieur à 0")
    end
    if heures and heures <= 0
      errors.add(:heures, "Les heures doivent être supérieures à 0")
    end
  end
  
 

  def is_charge_de_theorie?
    return typeGroupe.eql?("T")
  end
  
  def is_charge_de_labo?
    return typeGroupe.eql?("L")
  end
  

   def periodes
     # Retourne le nombre de période de présence pour une charge donnée
     # Taux d'assistanat pris en compte
     nombre_semaines=unite_enseignement.periode.dureeSemaine
     if unite_enseignement.coursBloc?
       if is_charge_de_theorie?
         return unite_enseignement.nbPeriodesTheoriquesTotal*taux/100
       else
         return unite_enseignement.nbPeriodesPratiquesTotal*taux/100
       end
     else
       if is_charge_de_theorie?
         return nombre_semaines*unite_enseignement.nbPeriodesTheoriquesSemaine*taux/100
       else
         return nombre_semaines*unite_enseignement.nbPeriodesPratiquesSemaine*taux/100
       end
     end
   end

   def heures_a_charge
     if !taux  # Le taux n'est pas signalé: le nombre total d'heures est alors signalé
       return heures
     else
       return periodes*Constante.facteur(unite_enseignement.periode.annee_academique.id, unite_enseignement.type_unite_enseignement.type_unite)
     end
   end

  def est_effective?
    return est_effective
  end

  def set_effective(value)
    est_effective=value
  end

  # !! Spécifique à ChargeAssistanat
  def controle_priorite
    if !priorite
      errors.add(:priorite, "La priorité doit être signalée")
    end
    if (priorite and priorite < 1) or  (priorite and priorite > 2)
      errors.add(:priorite, "La priorité doit être comprise entre 1 et 2")
    end
  end

 
  # !! Spécifique à ChargeAssistanat
  def self.total_heures(personne, annee_academique_id)
     # Retourne le total des heures d'assistanat pour une personne donnée
     charges=personne.charge_assistanats.where{unite_enseignement_id.in UniteEnseignement.where{(periode_id.in Periode.where(:annee_academique_id => annee_academique_id).select{id}) & (reconductible.eq true) & (assistanat_reconnu.eq true)}}
     total=0.0
     charges.each do |charge|
       total+=charge.heures_a_charge
     end
     return total
   end
  
end
# -*- encoding : utf-8 -*-
class Budget < ActiveRecord::Base
  
  def self.prioritaires_possibles
    ["Horaire", "Financier"]
  end
  
  belongs_to :projet
  belongs_to :type_budget
  validates_presence_of :type_budget, :message => "Champ requis"
  validate :champ_remplis

  def value
    self.call(prioritaire.underscore)
  end

  def display_name
  
    "#{prioritaire} : #{value}"
  
  end
  
private

  def champ_remplis
    if (horaire.nil? || horaire.empty?) && (financier.nil? || financier.empty?)
      errors.add(:horaire, "Saisir un des champs 'horaire' ou 'financier' ")
      errors.add(:financier, "Saisir un des champs 'horaire' ou 'financier' ")
    end
    
  end  

end

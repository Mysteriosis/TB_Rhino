# -*- encoding : utf-8 -*-
class TypeEngagement < ActiveRecord::Base
  has_many :engagements
  
  validates_presence_of :sigle, :libelle, :message => "Champ requis"
  

  
  def suppression_empechements
    empechements =""
    if engagements.count > 0 then 
      empechements+= (engagements.count.to_s() + " engagement(s) associé(s)")
    end
    empechements=="" ? nil : empechements
  end
  
end



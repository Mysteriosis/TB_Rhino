class Planification < ActiveRecord::Base

  belongs_to :affectation_projet
  has_one :commentaire

  validates_numericality_of :valeur, :message => "Les valeurs des planification doivent être un nombre"


  ##########################################################################
  # Methodes récupérées sur planning_ressources_projet, ajouté pour les REDS
  ##########################################################################

  # Returns the planning entries for a specific affectation in the dates range
  def self.planning_weeks(affectation, date_from, date_to)
    Planification.where{(date.gteq my{date_from}) & (date.lteq my{date_to}) & (affectation_projet_id.eq my{affectation.id})}
  end

  # Returns the planning entry for the specific affectation and date (has to be a monday)
  def self.planning_week(affectation, dateOf)
    Planification.where{(date.eq my{dateOf}) & (affectation_projet_id.eq my{affectation.id})}
  end

  # Checks if the table contains any data (used to define if this feature is used or not)
  def self.is_empty?
    Planification.count.zero?
  end

end
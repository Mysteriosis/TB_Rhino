# -*- encoding : utf-8 -*-
class ChargeTravail< ActiveRecord::Base
  # -----------------------------------------------------------------
  # Association
  # -----------------------------------------------------------------
  # Une charge de travail comprend le contrat et son type
  belongs_to :contrat
  belongs_to :type_contrat
end

# -*- encoding : utf-8 -*-
class TypeBudget < ActiveRecord::Base
  
  validates_presence_of :nom
  
  has_many :budgets
  
  def display_name
    nom
  end
    
end

# -*- encoding : utf-8 -*-
module PlageHoraireSaisiesHelper
  def month_link(month_date)
    link_to(I18n.localize(month_date, :format => "%B"), {:month => month_date.month, :year => month_date.year})
  end
  
  # custom options for this calendar
  def event_calendar_opts
    { 
      :year => @year,
      :month => @month,
      :event_strips => @event_strips,
      :first_day_of_week => 1,
      :month_name_text => I18n.localize(@shown_month, :format => "%B %Y"),
      :previous_month_text => "<< " + month_link(@shown_month.prev_month),
      :next_month_text => month_link(@shown_month.next_month) + " >>"    }
  end

  def event_calendar
    # args is an argument hash containing :event, :day, and :options
    calendar event_calendar_opts do |args|
      plage = args[:event]
      "#{heure_en_format_heures_minutes(plage.heures)} #{plage.affectation_projet.display_name}"
      #%(<a href="/events/#{event.id}" title="#{h(event.id)}">#{h(event.id)}</a>)
    end
  end
  
  def heure_en_format_heures_minutes(heure)
    format_sortie=""
    heure=heure.to_f
    heure_negative=heure<0
    if heure_negative 
      heure=-heure
    end
    hh=heure.to_i
    mm=((heure%1)*60).round
    if mm==60 
      hh += 1
      mm= 0
    end
    if hh==0
      if mm==0 
        format_sortie= "0"
      else 
        format_sortie= "#{mm}m" 
      end
    end
    if hh!=0 
      if mm==0 
        format_sortie= "#{hh}h"  
      else
        format_sortie= "#{hh}h#{mm}"
      end
    end
    if heure_negative
      format_sortie="-"+format_sortie
    end
    return format_sortie
  end

  def heure_en_format_heures_minutes_vx(heure, afficher_si_0 = false)
  # Version spécifique pour l'affichage du résumé individuel des heures de la nouvelle version 2014
  # N'affiche rien (un texte vide) si l'heure vaut 0, à moins que le flag afficher_si_0 soit à true
    format_sortie=""
    heure=heure.to_f
    heure_negative=heure<0
    if heure_negative 
      heure=-heure
    end
    hh=heure.to_i
    mm=((heure%1)*60).round

    if mm==60 
      hh += 1
      mm= 0
    end
    if hh==0
      if mm==0 
        format_sortie= afficher_si_0 ? "0h00" : ""
      else 
        format_sortie= "#{mm}m" 
      end
    end
    if hh!=0 
      if mm==0 
        format_sortie= "#{hh}h00"  
      else
        format_sortie= mm < 10 ? "#{hh}h0#{mm}" : "#{hh}h#{mm}"
      end
    end
    if heure_negative
      format_sortie="-"+format_sortie
    end
    return format_sortie
  end
  
 def heure_en_format_jours_heures_minutes_vx(heure, horaire_journalier,  format_complet=false)
  # Version spécifique pour l'affichage du résumé individuel des heures de la nouvelle version 2014
  # Le paramètre "horaire_journalier" permet de convertir des heures de travail en nombre de jours
  # Affiche en mode Heures minutes si le flag avec_jours vaut false
    format_sortie=""
    heure=heure.to_f
    heure_negative=heure<0
    if heure_negative 
      heure=-heure
    end
    hh=heure.to_i
    mm=((heure%1)*60).round
    if mm==60 
      hh += 1
      mm= 0
    end
    nb_jours= (heure/horaire_journalier).to_i
    jours_affiches=false
    if nb_jours > 0
      heure-=nb_jours*horaire_journalier
      hh=heure.to_i
      mm=((heure%1)*60).round
      if mm==60 
        hh += 1
        mm= 0
      end
      if  (hh==horaire_journalier.to_i) && (mm==((horaire_journalier%1)*60).round)
          nb_jours += 1
          hh=0
          mm=0
      end
      format_sortie= "#{nb_jours}j"
      jours_affiches=true
    elsif format_complet
      format_sortie= "0j"
    end

    if hh==0
      if mm==0 
        if format_complet
          format_sortie+= "-0h00"
        elsif jours_affiches
          format_sortie+= ""
        else
          format_sortie+= "0"
        end
      else 
        if format_complet
          format_sortie+= mm < 10 ? "-0h0#{mm}" : "-0h#{mm}"
        elsif jours_affiches
          format_sortie+= "-#{mm}m"
        else
          format_sortie+= "#{mm}m"
        end 
      end
    end
    if hh!=0
      if format_complet
        format_sortie+= "-"
      elsif jours_affiches
        format_sortie+= "-"
      end
      if format_complet
        format_sortie+= mm < 10 ? "#{hh}h0#{mm}" : "#{hh}h#{mm}"
      elsif mm==0 
        format_sortie+= "#{hh}h"  
      else
        format_sortie+= "#{hh}h#{mm}"
      end
    end
    if heure_negative
      format_sortie="-"+format_sortie
    end
    return format_sortie
  end
  
  def heure_en_format_jours_heures_minutes(heure, avec_jours)
    format_sortie=""
    heure=heure.to_f
    heure_negative=heure<0
    if heure_negative 
      heure=-heure
    end
    charge_journaliere=CstHeure.HJ(AnneeCivile.courante.annee)
    hh=heure.to_i
    mm=((heure%1)*60).round
    if mm==60 
      hh += 1
      mm= 0
    end
    nb_jours= (heure/charge_journaliere).to_i
    jours_affiches=false
    if avec_jours
      if nb_jours > 0
        format_sortie= "#{nb_jours}j"
        heure-=nb_jours*charge_journaliere
        hh=heure.to_i
        mm=((heure%1)*60).round
        if mm==60 
          hh += 1
          mm= 0
        end
        jours_affiches=true
      end
    end
    if hh==0
      if mm==0 
        if jours_affiches
           format_sortie+= ""
        else
          format_sortie+= "0"
        end
      else 
        if jours_affiches
           format_sortie+= "-"
        end
        format_sortie+= "#{mm}m" 
      end
    end
    if hh!=0
      if jours_affiches
         format_sortie+= "-"
      end
      if mm==0 
        format_sortie+= "#{hh}h"  
      else
        format_sortie+= "#{hh}h#{mm}"
      end
    end
    if heure_negative
      format_sortie="-"+format_sortie
    end
    return format_sortie
  end
  
  def extract_heures(heure)
    heure=heure.to_f
    return heure.to_i
  end
  
  def extract_minutes(heure)
    heure=heure.to_f
    mm=((heure%1)*60).round
    return mm
  end
  
end

# -*- encoding : utf-8 -*-
module PlageContractuellesHelper

	# Couleurs palette
	# Verts
	#     - Vert Clair #05F0C0
	#     - Vert Foncé #05D0C0

	# Gris
	#     - Gris clair #DDDDDD
	#     - Gris foncé #C2D0DA

	# Bleus
	#      - Bleu foncé marine #1060F0

	# Oranges
	#      - Orange clair #F0B000

	# Rouge
	#       - Rouge vif #FF0000

	# Violet
	#       - Mauve #9999FF

	# Nouvelle palette
	##################
	# RR - VV - BB
	# Textes Bleu gris  #41ACE7
	# Textes Gris foncé #333333
	# Gris
	#     -  Gris clair #DDDDDD
	#     -  Gris foncé #C2D0DA
	#     -  Gris très clair #EEEEEE
	# Gris - BleuVert
	#     -  Gris clair #DDBBBB  
	#     -  Gris foncé #C2B0B8 
	# Gris - Rouge
	#     -  Gris clair #BBDDDD 
	#     -  Gris foncé #A0D0DA 

	# Bleus #2618B1
	#      -  Bleus foncé #7B6EFA
	#      -  Bleus clair #9D94FA

	# Orange #FF8C00
	#      -  Orange foncé #FFB151
	#      -  Orange clair #FFD184

	# Vert-Gris #00AF64
	#      -  Vert-Gris foncé #36D792
	#      -  Vert-Gris clair #61D7A4

	def cell_color(row, no_plage, pourcent)
        parite_row = (row % 2 == 0)
        parite_column= (no_plage % 2 == 0)
        case !parite_row 
          when true then  # Rangée impaire
            case !parite_column                                            # RR - VV - BB
              when true then couleur_style="background-color:#BBDDDD"     # Plage Impaire   - Gris Rouge Clair
              when false then couleur_style="background-color:#DDCCCC"    # Plage Paire     - Gris BleuVert Clair
            end
            if pourcent==0 then
             couleur_style="background-color:#DDDDDD"  # % Activite=0     - Clair 
         	end
          when false then # Rangée paire
            case !parite_column
              when true then couleur_style="background-color:#A0D0DA"    # Plage Impaire    - Gris Rouge Foncé
              when false then couleur_style="background-color:#C2B0B8"   # Plage Paire      - Gris BleuVert Foncé
            end
            if pourcent==0 then
             couleur_style="background-color:#C2D0DA"  # % Activite=0     - Foncé 
         	end
        end
        return couleur_style
  	end

  	def cell_color_entete_plages(no_plage)
        parite_column= (no_plage % 2 == 0)
        case !parite_column                                            # RR - VV - BB
              when true then couleur_style="background-color:#EEEEEE"  #  - Gris très clair
              when false then couleur_style="background-color:#EEEEEE"  # - Vert-Gris clair
         end
        return couleur_style
  	end

end

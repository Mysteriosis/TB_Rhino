# -*- encoding : utf-8 -*-
module ApplicationHelper
  def set_onload_remote_function(options)
    return content_tag(:script, "Event.observe(window, 'load', #{remote_function(options)});", :language => :javascript)
  end
  
  def menu_item(label, path, action, controller, has_access, params, link_to_options = {})
    
    if !(params[:action] == action.to_s and params[:controller] == controller.to_s) and has_access
         #  Le menu apparait ssi l'action et le contrôleur actuel (info obtenue depuis params) est différente de l'action et du contrôeur demandé
      return link_to label, path, link_to_options.merge(:class => "menu")
    end
    return ""
  end
  
  def show_value(label,value)
    if value.respond_to? :display_name
      value = value.display_name
    end
    content_tag(:li) do
      content_tag(:label, label)+
      content_tag(:div, value, :class => "value")
    end  
  end
  
  def show_image(label,image, size)
    content_tag(:li) do
      content_tag(:label, label)+
      image_tag(image, :size=> size)
    end  
  end
  
  def ligne_vide
    content_tag(:li) do
      content_tag(:label, "")
    end  
  end
  
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")".html_safe)
  end

  def d(date, format = :normal)
    if date
      case format
      when :normal then date.strftime("%d.%m.%Y")
      when :long then date.strftime("prout %d.%m.%Y")
      end
    else
      ""
    end    
  end
  
  def hidden_div(attribute_name,value)
    content_tag(:div, value, :id => attribute_name, :style => "display:none")
  end
  
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {:search => params[:search], :sort => column, :direction => direction}, {:class => css_class}
  end

  def aff_double(nombre, precision)
    return number_with_precision(nombre, :precision=>precision, :separator=>'.', :strip_insignificant_zeros => true)
  end


  def cell_precise_color(couleur)
    case couleur
      when :black then couleur = "background-color:#111111"
      else couleur = "background-color:#FFFFFF"  # Blanc
    end
  end

  def cell_design(position, marge_gauche, largeur)
    return "text-align:#{position}; padding-left: #{marge_gauche}px; width: #{largeur}px;"
  end
end

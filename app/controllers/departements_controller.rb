# -*- encoding : utf-8 -*-
class DepartementsController < ApplicationController
  
  load_and_authorize_resource
  
  # GET /departements
  # GET /departements.xml
  def index
    
    # Année actuelle
    @annee_academique = AnneeAcademique.courante
    
    # Liste de tous les départements
    # !!!!! - 4 Juillet 2012
    # @departements =Departement.find(:all)   -- Si on veut le choix de l'année académique avec ajax, les prendre tous
    # Sinon:
    #@departements = Departement.find_all_by_annee_academique_id(@annee_academique.id, :order => 'abreviation')
    @departements = Departement.where(annee_academique_id: @annee_academique.id).order('abreviation ASC')  

    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee)
    

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @departements }
    end
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Met a jour les departements en fonction des annees academiques
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    # Liste de tous les départements
    # !!!!! - 4 Juillet 2012
    # @departements =Departement.find(:all)   -- Si on veut le choix de l'année académique avec ajax, les prendre tous
    # Sinon:
    #@departements = Departement.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @departements = Departement.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')

    # Liste de tous les départements concernant l'année souhaitée
    #@departements_annee = Departement.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @departements_annee = Departement.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')
  end
  
  # GET /departements/1
  # GET /departements/1.xml
  def show
    @departement = Departement.find(params[:id])
  
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @departement }
    end
  end
  
  # GET /departements/new
  # GET /departements/new.xml
  def new
    type_professeur = TypePersonne.find(:first, :conditions => "typePersonne='Professeur'")
    type_professeur_id = type_professeur.id
    @departement = Departement.new   
    # Liste des professeurs pouvant superviser un département 
    @personnes = Personne.find(:all, :conditions => ["type_personne_id = ?", type_professeur_id], :order => "initiale")
    @personnes.insert(0, Personne.new(:id => nil, :initiale => "---"))
    
    # Liste de toutes les annnes académiques avec comme premier élément l'année actuelle
    @annee_academique = AnneeAcademique.order(:annee) 
    
    #respond_to do |format|
    #  format.html # new.html.erb
    #  format.xml  { render :xml => @departement }
    #end
  end
  
  # GET /departements/1/edit
  def edit
    type_professeur =TypePersonne.find(:first, :conditions => "typePersonne='Professeur'")
    type_professeur_id = type_professeur.id
   
    @departement = Departement.find(params[:id])
    # Personne supervisant ce departement
    superviseur=@departement.personne 
    # Liste des professeurs pouvant superviser un département 
    @personnes = Personne.find(:all, :conditions => ["type_personne_id = ?", type_professeur_id], :order => "initiale") 
    # Liste des personnes à afficher (superviseur + toutes les personnes ne supervisant aucun departement)
    @personnes.insert(0, Personne.new(:id => nil, :initiale => "---"))
    if !(superviseur==nil) 
      @personnes.insert(0, superviseur)
    end
    
  
    # Liste de toutes les annnes académiques avec comme premier élément l'année souhaitée
    @annee_academique = self.liste_annees_choix(@departement.annee_academique_id) 
    
  end
  
  # POST /departements
  # POST /departements.xml
  def create
    @departement = Departement.new(params[:departement])
  
    respond_to do |format|
      if @departement.save
        flash[:notice] = 'Departement was successfully created.'
        format.html { redirect_to(@departement) }
        format.xml  { render :xml => @departement, :status => :created, :location => @departement }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @departement.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # PUT /departements/1
  # PUT /departements/1.xml
  def update
    @departement = Departement.find(params[:id])
  
    respond_to do |format|
      if @departement.update_attributes(params[:departement])
        flash[:notice] = 'Departement was successfully updated.'
        format.html { redirect_to(@departement) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @departement.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  # DELETE /departements/1
  # DELETE /departements/1.xml
  def destroy
    @departement = Departement.find(params[:id])
    @departement.destroy
  
    respond_to do |format|
      format.html { redirect_to(departements_url) }
      format.xml  { head :ok }
    end
  end
  
  
  
end

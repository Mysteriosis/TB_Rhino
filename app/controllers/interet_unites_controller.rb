# -*- encoding : utf-8 -*-
class InteretUnitesController < ApplicationController
  
  load_and_authorize_resource
  
  # GET /interet_unites
  # GET /interet_unites.xml
  def index
    @interet_unites = InteretUnite.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @interet_unites }
    end
  end

  # GET /interet_unites/1
  # GET /interet_unites/1.xml
  def show
    @interet_unite = InteretUnite.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @interet_unite }
    end
  end

  # GET /interet_unites/new
  # GET /interet_unites/new.xml
  def new
    @interet_unite = InteretUnite.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @interet_unite }
    end
  end

  # GET /interet_unites/1/edit
  def edit
    @interet_unite = InteretUnite.find(params[:id])
  end

  # POST /interet_unites
  # POST /interet_unites.xml
  def create
    @interet_unite = InteretUnite.new(params[:interet_unite])

    respond_to do |format|
      if @interet_unite.save
        flash[:notice] = 'InteretUnite was successfully created.'
        format.html { redirect_to(@interet_unite) }
        format.xml  { render :xml => @interet_unite, :status => :created, :location => @interet_unite }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @interet_unite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /interet_unites/1
  # PUT /interet_unites/1.xml
  def update
    @interet_unite = InteretUnite.find(params[:id])

    respond_to do |format|
      if @interet_unite.update_attributes(params[:interet_unite])
        flash[:notice] = 'InteretUnite was successfully updated.'
        format.html { redirect_to(@interet_unite) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @interet_unite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /interet_unites/1
  # DELETE /interet_unites/1.xml
  def destroy
    @interet_unite = InteretUnite.find(params[:id])
    @interet_unite.destroy

    respond_to do |format|
      format.html { redirect_to(interet_unites_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class BudgetsController < ApplicationController

  load_and_authorize_resource :projet
  load_and_authorize_resource :budget, :through => :projet
  
  # GET /budgets
  # GET /budgets.xml
  def index
    @budgets = Budget.all
    respond_with(@budgets)
  end

  # GET /budgets/1
  # GET /budgets/1.xml
  def show
    @budget = Budget.find(params[:id])
    respond_with(@budget)
  end

  # GET /budgets/new
  # GET /budgets/new.xml
  def new
    @budget = Budget.new
    respond_with(@budget)
  end

  # GET /budgets/1/edit
  def edit
    @budget = Budget.find(params[:id])
  end

  # POST /budgets
  # POST /budgets.xml
  def create
    @budget = Budget.new(params[:budget])
    @budget.save
    respond_with(@budget)
  end

  # PUT /budgets/1
  # PUT /budgets/1.xml
  def update
    @budget = Budget.find(params[:id])
    @budget.update_attributes(params[:budget])
    respond_with(@budget)
  end

  # DELETE /budgets/1
  # DELETE /budgets/1.xml
  def destroy
    @budget = Budget.find(params[:id])
    @budget.destroy
    respond_with(@budget)
  end
end

# -*- encoding : utf-8 -*-
class TypeEngagementsController < ApplicationController
  
  load_and_authorize_resource
  

  def index
    @type_engagements = TypeEngagement.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_engagements }
    end
  end


  def show
    @type_engagement = TypeEngagement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_engagement }
    end
  end


  def new
    @type_engagement = TypeEngagement.new
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_engagement }
    end
  end


  def edit
    @type_engagement = TypeEngagement.find(params[:id])
  end


  def create
    @type_engagement = TypeEngagement.new(params[:type_engagement])

    respond_to do |format|
      if @type_engagement.save
        flash[:notice] = 'Création réalisée avec succès'
        format.html { redirect_to(type_engagements_path) }
        format.xml  { render :xml => @type_engagement, :status => :created, :location => @type_engagement }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_engagement.errors, :status => :unprocessable_entity }
      end
    end
  end

 
  def update
    @type_engagement = TypeEngagement.find(params[:id])

    respond_to do |format|
      if @type_engagement.update_attributes(params[:type_engagement])
        flash[:notice] = 'Mise à jour réalisée avec succès'
        format.html { redirect_to(type_engagements_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_engagement.errors, :status => :unprocessable_entity }
      end
    end
  end


  def destroy
    @type_engagement = TypeEngagement.find(params[:id])
    if !(@type_engagement.suppression_empechements.nil?)
      flash[:alert] = "Impossible de supprimer ce type d'engagement: " + @type_engagement.suppression_empechements
    else
      @type_engagement.destroy
      flash[:notice] = 'Suppression réalisée avec succès'
    end
    respond_to do |format|
      format.html { redirect_to(type_engagements_path) }
      format.xml  { head :ok }
    end
  end
end

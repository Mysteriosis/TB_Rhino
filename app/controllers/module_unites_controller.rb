# -*- encoding : utf-8 -*-
class ModuleUnitesController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /module_unites
  # GET /module_unites.xml
  def index
    
    # Liste des modules
    @module_unites = ModuleUnite.find(:all)
    # Année actuelle
    @annee_academique = AnneeAcademique.courante
    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @module_unites }
    end
  end

  # GET /module_unites/1
  # GET /module_unites/1.xml
  def show
    @module_unite = ModuleUnite.find(params[:id])
    
    # Liste des orientations
    orientations = Orientation.find(:all, :order => :abreviation)
    # Liste des liens entre les modules et les orientations
    modules_orientations = ModuleOrientation.find(:all)
    # Liste des orientations liées au module
    @orientations_concernees = []
    
    i=0
    # Pour toutes les orientations liées aux modules
    for module_orientation in modules_orientations
      if module_orientation.module_id == @module_unite.id
        # Pour toutes les orientations
        for orientation in orientations
          if module_orientation.orientation_id == orientation.id
            @orientations_concernees[i] = orientation
            i=i+1
          end
        end
      end
    end
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @module_unite }
    end
  end

  # GET /module_unites/new
  # GET /module_unites/new.xml
  def new
    
    @module_unite = ModuleUnite.new
    
    # Liste de toutes les années académiques avec comme premier élément
    # l'année courante
    @annees_academiques = AnneeAcademique.order(:annee)
    
    # Liste des orientations concernant l'année actuelle
    #orientations = Orientation.find_all_by_annee_academique_id(AnneeAcademique.courante.id, :order=>'abreviation')
    orientations = Orientation.where(annee_academique_id: AnneeAcademique.courante.id).order('abreviation ASC')
    
    @orientations_concernees=[]
    @orientations_non_concernees=[]
    i=0
    
    # Formatage du nom de l'orientation
    for orientation in orientations
        @orientations_non_concernees[i]=orientation.abreviation+" ("+orientation.texte+")"
        i+=1
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @module_unite }
    end
  end

  # GET /module_unites/1/edit
  def edit
    @module_unite = ModuleUnite.find(params[:id])
    
    # Année académique du module
    annee_academique_souhaitee = AnneeAcademique.find(@module_unite.annee_academique_id)
    
    # Liste de toutes les années académiques avec comme premier élément l'année choisie
    @annees_academiques = self.liste_annees_choix(annee_academique_souhaitee.id)
    
    # Liste des orientations
    @orientations = Orientation.find(:all)
    # Liste des liaisons entre des modules et des orientations
    module_orientations = ModuleOrientation.find(:all)
    
    @orientations_concernees = []
    orientations_concernees = []
    @orientations_non_concernees = []
    i = 0
    
    # Cherche les orientations concernées
    # Pour toutes les orientations
    for orientation in @orientations
      # Pour toutes les liaisons entre des modules et des orientations
      for module_orientation in module_orientations
          # L'orientation concerne ce module et cette orientation et si elle est de cette année
          if module_orientation.orientation_id == orientation.id && @module_unite.id == module_orientation.module_id && orientation.annee_academique_id == @module_unite.annee_academique_id
            @orientations_concernees[i]=orientation.abreviation + " (" + orientation.texte + ")"
            orientations_concernees[i]=orientation
            i+=1
          end
      end
    end
    i=0
    
    # Cherche les orientations non concérnées
    # Pour toutes les orientations
    for orientation in @orientations
      non_concernee = true
      # Pour toutes les orientations concernées
      for orientation_concernee in orientations_concernees
        # Si l'orientation est concernée
        if orientation.id == orientation_concernee.id
          non_concernee = false
        end
      end
      if non_concernee && orientation.annee_academique_id == @module_unite.annee_academique_id
        @orientations_non_concernees[i]= orientation.abreviation + " (" + orientation.texte + ")"
        i+=1
      end   
    end
  end

  # POST /module_unites
  # POST /module_unites.xml
  def create
    
    @module_unite = ModuleUnite.new(params[:module_unite])
    
    respond_to do |format|
      if @module_unite.save
        flash[:notice] = 'ModuleUnite was successfully created.'
        format.html { redirect_to(@module_unite) }
        format.xml  { render :xml => @module_unite, :status => :created, :location => @module_unite }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @module_unite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /module_unites/1
  # PUT /module_unites/1.xml
  def update
    @module_unite = ModuleUnite.find(params[:id])

    respond_to do |format|
      if @module_unite.update_attributes(params[:module_unite])
        flash[:notice] = 'ModuleUnite was successfully updated.'
        format.html { redirect_to(@module_unite) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @module_unite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /module_unites/1
  # DELETE /module_unites/1.xml
  def destroy
    @module_unite = ModuleUnite.find(params[:id])

    # Liste des liens entre le module et les orientations
    #modules_orientations = ModuleOrientation.find_all_by_orientation_id(@module_unite.id)
    modules_orientations = ModuleOrientation.where(orientation_id: @module_unite.id)
    # Liste des liens entre le module et les unités d'enseignement
    #modules_unites = ModuleUniteEnseignement.find_all_by_unite_enseignement_id(@module_unite.id)
    modules_unites = ModuleUniteEnseignement.where(unite_enseignement_id: @module_unite.id)
    
    # Supprime tous les liens entre ce module et des orientations
    for module_orientation in modules_orientations
       module_orientation.destroy
    end
    # Supprime tous les liens entre ce module et des unités d'enseignement
    for module_unite in modules_unites
       module_unite.destroy
    end
      
    @module_unite.delete

    respond_to do |format|
      format.html { redirect_to(module_unites_url) }
      format.xml  { head :ok }
    end
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: creation_orientation_draggable
  # But: Permet de rendre draggable les orientations
  #--------------------------------------------------------------------------------------------------------
  def creation_orientation_draggable 
    
    # Liste des orientations
    orientations = Orientation.find(:all)
    
    # Si l'on crée un nouveau module, on affiche les orientations de l'année actuelle
    if params[:module_unite]==""
      @orientations= orientation_annee(AnneeAcademique.courante.id)
    # Si l'on modifie un module, on affiche les orientations de l'année du module
    else
      module_unite = ModuleUnite.find(params[:module_unite])
      @orientations= orientation_annee(module_unite.annee_academique_id)
    end
    
    render :action => "creation_orientation_draggable" 
  end 
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: enregistrer_module
  # But: Enregistre le module et ses liens vers les orientations dans la base de données
  #--------------------------------------------------------------------------------------------------------
  def enregistrer_module 
    
    # Il s'agit de créer un module
    if params[:creer] == "true"
      # Création d'un nouveau module
      module_unite = ModuleUnite.new()
    # Il s'agit de modifier un module
    else
      # Charge le module
      module_unite = ModuleUnite.find(:all, :conditions=>["id = :id",{:id => params[:module_unite]}])[0]
    end
    
    # Insertion des données dans le  module
    module_unite.abreviation = params[:abreviation]
    module_unite.texte = params[:texte]
    module_unite.annee_academique_id = params[:annee_academique]
    
    # Enregistrement du module
    module_unite.save
    
    # Liste des orientations concernées
    orientations_concernees = params[:orientations].split(',')
    orientation_abreviation = []
    
    # Liste des orientations
    orientations = Orientation.find(:all)
    
    # Il s'agit de modifier un module, on supprime les liens
    if params[:creer] == "false"
      # Liste des liens entre un module et ses orientations
      #modules_orientations = ModuleOrientation.find_all_by_module_id(params[:module_unite].to_i)
      modules_orientations = ModuleOrientation.where(module_id: params[:module_unite].to_i)
      for module_orientation in modules_orientations
        module_orientation.destroy
      end
    end
    
    
    # Enregistre tous les liens entre un module et des orientations
    for orientation_concernee in orientations_concernees
      
        # Création d'un nouveau lien
        new_orientation = ModuleOrientation.new
        new_orientation.module_id = module_unite.id
        
        # Trouve l'abreviation de l'orientation
        orientation_abreviation = orientation_concernee.split(' (').first
        # Trouve l'id de l'orientation
        for orientation in orientations
          if orientation.abreviation == orientation_abreviation && orientation.annee_academique_id.to_i == params[:annee_academique].to_i
            new_orientation.orientation_id = orientation.id
          end
        end
        
        # Enregistre le lien
        new_orientation.save

    end
    
  end 
  #--------------------------------------------------------------------------------------------------------
  # Nom: annee_mise_a_jour
  # But: Modification du contenu des orientations à afficher
  #--------------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    
    
    # Liste de tous les modules
    @modules = ModuleUnite.find(:all)
    # Liste de tous les modules concernant l'année souhaitée
    #@modules_annee = ModuleUnite.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @modules_annee = ModuleUnite.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')
    # Est-ce que l'appel de la fonction vient de la vue d'index
    @vue_index = params['index']  
    
    # Récupération de l'id de l'année académique
    id_annee = params['id_annee_academique'].to_i
    
    
    
    orientations = Orientation.find(:all)
    @orientations_non_concernees_annee=[]
    @orientations_concernees_annee=[]
    i=0
    
    # Si l'on créé un nouveau module
    if params['id_module'] == "" || params['id_module'] == nil

      @orientations_non_concernees_annee=orientation_annee(id_annee)
      
    # Si l'on modifie le module
    else
    
      module_unite = ModuleUnite.find(params['id_module'])
      liens = ModuleOrientation.find(:all,:conditions=>["module_id = :id",{:id => params['id_module']}])
      orientations_concernees_annee=[]
      
      # Trouve toutes les orientations en lien avec le module
      for lien in liens
        for orientation in orientations
          if orientation.id == lien.orientation_id && orientation.annee_academique_id == id_annee
            @orientations_concernees_annee[i]=orientation.abreviation+" ("+orientation.texte+")"
            orientations_concernees_annee[i]=orientation
            i+=1
          end
        end
      end

      i=0
      # Cherche les orientations non concérnées
      for orientation in orientations
        non_concernee = true
        # Pour toutes les orientations concernées
        for orientation_concernee in orientations_concernees_annee
          # Si l'orientation est concernée
          if orientation.id == orientation_concernee.id
            non_concernee = false
          end
        end
        if non_concernee && orientation.annee_academique_id == id_annee
          @orientations_non_concernees_annee[i]= orientation.abreviation + " (" + orientation.texte + ")"
          i+=1
        end   
      end
    end
    
    # Si l'appel de la fonction ne vient pas de la page d'index
    if @vue_index == "false"
      # Liste des anciennes orientations
      @orientations_anciennement_non_concernees = params[:orientations_non_concernees].split(',')
      @orientations_anciennement_concernees = params[:orientations_concernees].split(',')
    end
    
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : orientation_annee
  # But : Retourne un tableau contenant toutes les orientations (formatées) de l'année souhaitée
  #---------------------------------------------------------------------------------------------------
  def orientation_annee (id_annee)
    # Orientations de l'années souhaitée
    #orientations = Orientation.find_all_by_annee_academique_id(id_annee.to_i, :order => 'abreviation')
    orientations = Orientation.where(annee_academique_id: id_annee.to_i).order('abreviation ASC')
    orientation_retour = []
    i=0
    # Formatage de l'affichage
    for orientation in orientations
      orientation_retour[i]=orientation.abreviation+" ("+orientation.texte+")"
      i+=1
    end
    return orientation_retour
  end
  
end

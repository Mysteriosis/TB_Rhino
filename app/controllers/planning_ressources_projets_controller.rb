class PlanningRessourcesProjetsController < ApplicationController
  # GET /planning_ressources_projets
  # GET /planning_ressources_projets.xml
  def index
    @planning_ressources_projets = PlanningRessourcesProjet.all
    respond_with(@planning_ressources_projets)
  end

  # GET /planning_ressources_projets/1
  # GET /planning_ressources_projets/1.xml
  def show
    @planning_ressources_projet = PlanningRessourcesProjet.find(params[:id])
    respond_with(@planning_ressources_projet)
  end

  # GET /planning_ressources_projets/new
  # GET /planning_ressources_projets/new.xml
  def new
    @planning_ressources_projet = PlanningRessourcesProjet.new
    respond_with(@planning_ressources_projet)
  end

  # GET /planning_ressources_projets/1/edit
  def edit
    @planning_ressources_projet = PlanningRessourcesProjet.find(params[:id])
  end

  # POST /planning_ressources_projets
  # POST /planning_ressources_projets.xml
  def create
    @planning_ressources_projet = PlanningRessourcesProjet.new(params[:planning_ressources_projet])
    flash[:notice] = 'PlanningRessourcesProjet was successfully created.' if @planning_ressources_projet.save
    respond_with(@planning_ressources_projet)
  end

  # PUT /planning_ressources_projets/1
  # PUT /planning_ressources_projets/1.xml
  def update
    @planning_ressources_projet = PlanningRessourcesProjet.find(params[:id])
    flash[:notice] = 'PlanningRessourcesProjet was successfully updated.' if @planning_ressources_projet.update_attributes(params[:planning_ressources_projet])
    respond_with(@planning_ressources_projet)
  end

  # DELETE /planning_ressources_projets/1
  # DELETE /planning_ressources_projets/1.xml
  def destroy
    @planning_ressources_projet = PlanningRessourcesProjet.find(params[:id])
    @planning_ressources_projet.destroy
    respond_with(@planning_ressources_projet)
  end
end

# -*- encoding : utf-8 -*-
class ContratsController < ApplicationController  

  load_and_authorize_resource :contrat

  before_filter :set_form_data, :only => [:new, :create, :edit, :update]

  helper_method :sort_column, :sort_direction

  def index_global
    #@search = Contrat.en_cours.search(params[:search])
    @search = Contrat.en_cours.search(params[:q])
    parametre_present= !(params[:q]==nil)
  
    @contrats=Contrat.joins('join engagements on engagements.id=contrats.engagement_id')
    @contrats=@contrats.joins('join type_engagements on type_engagements.id=engagements.type_engagement_id')
    @contrats=@contrats.joins('join personnes on engagements.personne_id=personnes.id')
    today = Date.today
    # Contrats en cours
    @contrats=@contrats.where{(start_date.lteq today) & ((end_date.gteq today) | (end_date.eq nil))}

    @contrats_ransack=@contrats.search(params[:q])
    @contrats_ransack.sorts = (sort_column + " " + sort_direction) if @contrats_ransack.sorts.empty?
    @contrats=@contrats_ransack.result
    ##@contrats=@search.join(:engagements).where({:end_date.not_eq => nil} & {:end_date.lt => @end_date_date})
  end
  
  def index
        
    @personne = Personne.find(params[:personne_id])
    if cannot? :read, @personne.contrats.build
      raise CanCan::AccessDenied
    end
  
    @contrats_en_cours = @personne.contrats.en_cours
    @contrats_futurs = @personne.contrats.futurs
    @taux_activite_actuel = @personne.contrats.taux_activite_actuel
    @engagements = @personne.engagements
    @contrats = @personne.contrats.order("start_date")
    @structure_contrats=Hash.new
    @engagements.each do |eng|
      @structure_contrats[eng.type_engagement.sigle]={:en_cours => Array.new, :futurs => Array.new,:tous => Array.new}
    end
    @contrats_en_cours.each do |en_cours|
      @structure_contrats[en_cours.engagement.type_engagement.sigle][:en_cours]<<en_cours
    end
    @contrats_futurs.each do |en_cours|
      @structure_contrats[en_cours.engagement.type_engagement.sigle][:futurs]<<en_cours
    end
    @contrats.each do |contrat|
      @structure_contrats[contrat.engagement.type_engagement.sigle][:tous]<<contrat
    end
    
    @annee_id = AnneeCivile.courante
    @cahier = @personne.cahier_charge_assistants.pour_annee_civile(AnneeCivile.courante).first

    ## !! Provisoire 
    #@annee_civile = Configuration.annee_civile_initiale
    #while @annee_civile < AnneeCivile.annee_prochaine do
    #  if !InfoHeure.plages_OK?(@personne, @annee_civile)
    #    PlageContractuelle.update_heures(@personne, @annee_civile)
    #    @annee_civile += 1
    #    break 
    #  end
    #  @annee_civile += 1
    #end
    #@annee_civile -= 1
    @annee_civile= Date.today.year
    @contrats_de_lannee = @personne.contrats.en_cours_dannee(Date.today.year)
    @plages_contractuelles = PlageContractuelle.entetes_plages_contractuelles(@personne, Date.today.year)
  end

  ##
  # Création d'un contrat pour un engagement donné en paramètre
  ##
  def new
    @contrat = Contrat.new
    @contrat.engagement = Engagement.find(params[:engagement])
  end
  
  ##
  # Edition d'un contrat
  ##
  def edit
    #Sélection du contrat à éditer
    @contrat = Contrat.find(params[:id])
    #Listage des personnes
    @personnes = Personne.order("initiale")
  end

  ##
  # Création du contrat
  ##
  def create
    
    @contrat = Contrat.new(params[:contrat])
    if !@contrat.save
      render :action => "new"
      return
    end

    personne=@contrat.engagement.personne
    PlageContractuelle.update_avec_nouveau_contrat(personne, @contrat)
    redirect_to contrats_path(:personne_id =>@contrat.engagement.personne.id)


    # Mise à jour des chaiers de charge de la personne
    #today = Date.today
    #id_personne = @contrat.engagement.personne_id
    #
    ##Récupération des contrat en cours (déterminé et indéterminé)
    #@contrats= @contrat.engagement.personne.contrats.en_cours
    #
    ##Nombre d'heure pour un 100%
    #@nbr_heure_100 = 1870
    ##Comptage du nombre d'heure pour l'enseignement
    #@heure_enseignement = 0
    ##Comptage du nombre d'heure pour la R&D
    #@heure_rd = 0
    ##Pour chacun des contrats actuellement en cours
    #for contrat in @contrats
    #  #Calcul du nombre d'heure de R&D que représente ce contrat
    #  heure_RD = @nbr_heure_100 / (100 / contrat.type_engagement.prc_RD)
    #  heure_RD = (heure_RD / 100) * contrat.pourcentage
    #  @heure_rd += heure_RD
    #  #Calcul du nombre d'enseignement que représente ce contrat
    #  heure_ENS = @nbr_heure_100 / (100 / (100-contrat.type_engagement.prc_RD))
    #  heure_ENS = (heure_ENS / 100) * contrat.pourcentage
    #  @heure_enseignement += heure_ENS
    #end
    #  
    #@AnneeID = AnneeCivile.find(:first, :conditions => {:annee => Time.now.year})
    #cahier = CahierChargeAssistant.find(:first, :conditions =>{:personne_id => @contrat.personne_id,:annee_civile_id => @AnneeID.id })
    #
    #if cahier
    #  cahier.update_attribute(:heures_RD,   @heure_rd)
    #  cahier.update_attribute(:heures_ens,  @heure_enseignement)  
    #else
    #  cahier_charge_assistant = CahierChargeAssistant.new
    #  cahier_charge_assistant.heures_RD = @heure_rd
    #  cahier_charge_assistant.heures_ens = @heure_enseignement
    #  cahier_charge_assistant.heures_infra = 0
    #  cahier_charge_assistant.definition_poste = ''
    #  cahier_charge_assistant.remarque = ''
    #  cahier_charge_assistant.charge_assistant_id = -1
    #  cahier_charge_assistant.personne_id = @contrat.personne_id
    #  cahier_charge_assistant.annee_civile_id = @AnneeID.id
    #  cahier_charge_assistant.save
    #end
    
  end

  ##
  #Mise à jour d'un contrat
  ##
  def update
    @contrat = Contrat.find(params[:id])
    @contrat.update_attributes(params[:contrat])

    personne=@contrat.engagement.personne
    PlageContractuelle.update_apres_mise_a_jour_contrat(personne, @contrat)
    redirect_to contrats_path(:personne_id =>personne.id)
  end

  # DELETE /contrats/1
  # DELETE /contrats/1.xml
  def destroy
    @contrat = Contrat.find(params[:id])
    @contrat.destroy

    personne=@contrat.engagement.personne
    PlageContractuelle.update_apres_suppression_contrat(personne, @contrat)
    redirect_to contrats_path(:personne_id =>personne.id)

    # Destruction des cahiers des charges
    #personne = @contrat.personne 
    #cahiers_de_charge = CahierChargeAssistant.find(:all, :conditions => {:personne_id => personne.id})
    #cahiers_de_charge.each do |cch|
    #  year = cch.annee_civile.annee
    #  start_date_of_year = year + "-01-01"
    #  end_date_of_year = year + "-12-31"
    #  contrats = Contrat.find_by_sql ["SELECT * FROM contrats WHERE (end_date IS ? OR end_date <= ?) AND personne_id = ? ORDER BY start_date", nil, end_date_of_year, personne.id]
    #  if contrats.length==0
    #    cch.destroy
    #  end
    #end
  end
  
  def nouvel_engagement
    @engagement = Engagement.new
    @personne = Personne.find(params[:personne])
    @types_engagements_possibles=Contrat.types_engagements_possibles(@personne)
  end
  
  def create_engagement
    @engagement = Engagement.new(params[:engagement])
    personne = @engagement.personne
    if @engagement.type_engagement==nil
      redirect_to contrats_path(:personne_id =>personne.id)
      return
    end
    if !@engagement.save
      render :action => "nouvel_engagement"
      return
    end
    redirect_to contrats_path(:personne_id =>personne.id)
  end
  
  def update_engagement
    @engagement = Engagement.find(params[:id])
    @engagement.update_attributes(params[:engagement])

    personne=@engagement.personne
    redirect_to contrats_path(:personne_id =>personne.id)
  end
  
  def destroy_engagement
    engagement = Engagement.find(params[:engagement])
    personne = engagement.personne

    # Contrôle si destruction possible
    # Destruction possible uniquement si
    # -  aucun contrat associé
    # -  aucune affectation de projet associée
    
    deletable = true
    error = ""
    
    affectations = engagement.affectation_projets
    if affectations.length!=0
      deletable = false
      error += " - projet(s) affecté(s)"
    end
    
    contrats = engagement.contrats
    if contrats.length!=0
      deletable = false
      error += " - contrat(s) associé(s)"
    end

    if !deletable 
      flash[:alert] = "Impossible de supprimer cet engagement, elle a encore des éléments associés : " + error
    else
      flash[:notice] = "Suppression réalisée avec succès!"    
      engagement.destroy
    end
    
    redirect_to contrats_path(:personne_id =>personne.id)
  end

  ##
  # Affichage des statistiques concernant les contrats
  ##
  def stats
    #Récupération de la date du jour
    today = Date.today
    #Récupération des axes
    @axes = AxeStrategique.all
    ####
    #Tableau des statistiques
    #####
    # [AXE_ID (0 = all)], [NBR_COL], [[][]], [HEURE_ENS], [HEURE_RD]]
    ####
    @statistique = []
    @statistique[0] = []
    
    #Récupération des contrat en cours (déterminé et indéterminé)
    @contrats = Contrat.find_by_sql ["SELECT * FROM contrats WHERE start_date <= ? AND (end_date IS ? OR end_date >= ?)", today, nil, today]
    #Récupération des types d'engagements
    @types_engagements = TypeEngagement.all
    #Comptage du nombre de collaborateur sous contrat
    @statistique[0][0] = Contrat.count_by_sql ["SELECT count(DISTINCT engagements.personne_id) FROM contrats,engagements WHERE contrats.engagement_id = engagements.id AND start_date <= ? AND (end_date IS ? OR end_date >= ?)", today, nil, today]
    @statistique[0][1] = []
    @i = 0
    #On traite les types de contrats séparément
    for typeEngagement in @types_engagements
      @statistique[0][1][@i] = []
      #Type de contrat
      @statistique[0][1][@i][0] = typeEngagement.libelle
      #Nombre de contrat de ce type
      @statistique[0][1][@i][1] = Contrat.count_by_sql ["SELECT count(contrats.id) FROM contrats,engagements WHERE contrats.engagement_id = engagements.id AND start_date <= ? AND (end_date IS ? OR end_date >= ?) AND type_engagement_id = ?", today, nil, today, typeEngagement.id]
      @i = @i + 1
    end
    
    #Nombre d'heure pour un 100%
    @nbr_heure_100 = 1870
    #Comptage du nombre d'heure pour l'enseignement
    @statistique[0][2] = 0
    #Comptage du nombre d'heure pour la R&D
    @statistique[0][3] = 0
    for contrat in @contrats
      #Calcul du nombre d'heure de R&D que représente ce contrat
      heure_RD = @nbr_heure_100 / (100 / contrat.engagement.type_engagement.prc_RD)
      heure_RD = (heure_RD / 100) * contrat.pourcentage
      @statistique[0][3] += heure_RD
      #Calcul du nombre d'enseignement que représente ce contrat
      heure_ENS = @nbr_heure_100 / (100 / (100-contrat.engagement.type_engagement.prc_RD))
      heure_ENS = (heure_ENS / 100) * contrat.pourcentage
      @statistique[0][2] += heure_ENS
    end
    
    #Creation du graph total
    @statistique[0][4] = []
    @statistique[0][4][0] = []
    @statistique[0][4][1] = []
    @statistique[0][4][0][0] = "Enseignement"
    @statistique[0][4][1][0] = "Recherche"
    if @statistique[0][2] == 0 and @statistique[0][3] == 0
      @statistique[0][4][0][1] = 0
      @statistique[0][4][1][1] = 0
    else
      @statistique[0][4][0][1] = @statistique[0][2] * 100 / (@statistique[0][3] + @statistique[0][2])
      @statistique[0][4][1][1] = @statistique[0][3] * 100 / (@statistique[0][3] + @statistique[0][2])
      @statistique[0][4][0][1] = @statistique[0][4][0][1].round(1)
      @statistique[0][4][1][1] = @statistique[0][4][1][1].round(1)
    end

    for axe in @axes
      @statistique[axe.id] = []
      
      #Nombre d'heure pour un 100%
      @nbr_heure_100 = 1870
      #Comptage du nombre d'heure pour l'enseignement
      @statistique[axe.id][2] = 0
      #Comptage du nombre d'heure pour la R&D
      @statistique[axe.id][3] = 0
      
      ###
      # Comptage du nombre de collaborateur sous contrat pour l'axe
      ###
      @statistique[axe.id][0] = 0
      boucle_personne_id  = 0
      for contrat in @contrats
        if contrat.engagement.personne.axe_strategique_id == axe.id and  boucle_personne_id  != contrat.engagement.personne_id
          @statistique[axe.id][0] += 1
          #Calcul du nombre d'heure de R&D que représente ce contrat
          heure_RD = @nbr_heure_100 / (100 / contrat.engagement.type_engagement.prc_RD)
          heure_RD = (heure_RD / 100) * contrat.pourcentage
          @statistique[axe.id][3] += heure_RD
          #Calcul du nombre d'enseignement que représente ce contrat
          heure_ENS = @nbr_heure_100 / (100 / (100-contrat.engagement.type_engagement.prc_RD))
          heure_ENS = (heure_ENS / 100) * contrat.pourcentage
          @statistique[axe.id][2] += heure_ENS
        end
        if contrat.engagement.personne.axe_strategique_id == axe.id and  boucle_personne_id  == contrat.engagement.personne_id
          #Calcul du nombre d'heure de R&D que représente ce contrat
          heure_RD = @nbr_heure_100 / (100 / contrat.engagement.type_engagement.prc_RD)
          heure_RD = (heure_RD / 100) * contrat.pourcentage
          @statistique[axe.id][3] += heure_RD
          #Calcul du nombre d'enseignement que représente ce contrat
          heure_ENS = @nbr_heure_100 / (100 / (100-contrat.engagement.type_engagement.prc_RD))
          heure_ENS = (heure_ENS / 100) * contrat.pourcentage
          @statistique[axe.id][2] += heure_ENS
        end
         boucle_personne_id  = contrat.engagement.personne_id
      end
      
      ###
      # Comptage des types d'engagements pour les collaborateurs sous contrat pour l'axe
      ###
      @statistique[axe.id][1] = []
      @i = 0
      #On traite les types d'engagements séparément
      for typeEngagement in @types_engagements
        @statistique[axe.id][1][@i] = []
        #Type d'engagement
        @statistique[axe.id][1][@i][0] = typeEngagement.libelle
        #Nombre de contrat de ce type
        @Contrat_TC = Contrat.find_by_sql ["SELECT * FROM contrats,engagements WHERE contrats.engagement_id = engagements.id AND start_date <= ? AND (end_date IS ? OR end_date >= ?) AND type_engagement_id = ? ORDER BY engagements.personne_id", today, nil, today, typeEngagement.id]
        @statistique[axe.id][1][@i][1] = 0
        boucle_personne_id = 0
        for contrat_TC in @Contrat_TC
          if contrat_TC.engagement.personne.axe_strategique_id == axe.id and boucle_personne_id != contrat_TC.engagement.personne_id
            @statistique[axe.id][1][@i][1] += 1
          end
          boucle_personne_id = contrat_TC.engagement.personne_id
        end
        @i = @i + 1
      end
      
      #Creation du graph total
      @statistique[axe.id][4] = []
      @statistique[axe.id][4][0] = []
      @statistique[axe.id][4][1] = []
      @statistique[axe.id][4][0][0] = CGI.escapeHTML "Enseignement"
      @statistique[axe.id][4][1][0] = CGI.escapeHTML "Recherche"
      if @statistique[axe.id][2] == 0 and @statistique[axe.id][3] == 0
        @statistique[axe.id][4][0][1] = 0
        @statistique[axe.id][4][1][1] = 0
      else
        @statistique[axe.id][4][0][1] = @statistique[axe.id][2] * 100 / (@statistique[axe.id][3] + @statistique[axe.id][2])
        @statistique[axe.id][4][1][1] = @statistique[axe.id][3] * 100 / (@statistique[axe.id][3] + @statistique[axe.id][2])
        @statistique[axe.id][4][0][1] = @statistique[axe.id][4][0][1].round(1)
        @statistique[axe.id][4][1][1] = @statistique[axe.id][4][1][1].round(1)
      end
    end
  end
  
  private
   
  def set_form_data
    #Création de la liste des personnes (toutes les personnes actives de l'institut)
    @personnes = Personne.professeurs_et_collaborateurs_actifs_de_linstitut.order(:nom)
  end
  
  def sort_column
    params[:sort] || "personnes.nom"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  

end

# -*- encoding : utf-8 -*-
class UniteEnseignementsController < ApplicationController
  require "fonctions"
  
  #before_fileter :activate_authlogic
  # GET /unite_enseignements
  # GET /unite_enseignements.xml

  load_and_authorize_resource
  
  def index
    # D'où on vient
    @comes_from = params['comes_from']

    # Année actuelle
    @annee_academique = AnneeAcademique.courante
    
    
    # Liste des unités d'enseignement de l'année actuelle
    unite_enseignements = UniteEnseignement.unites_annee_souhaitee(@annee_academique.id)
    @unite_enseignements_actives = unite_enseignements.select{|ue| ue.reconductible}.sort_by(&:abreviation)
    @unite_enseignements_inactives = unite_enseignements.select{|ue| !ue.reconductible}.sort_by(&:abreviation)

    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    #@annees_academiques = AnneeAcademique.liste_annees_academiques
    @annees_academiques=[@annee_academique]

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @unite_enseignements }
    end
  end

  # GET /unite_enseignements/1
  # GET /unite_enseignements/1.xml
  #---------------------------------------------------------------------------------------------------
  # Nom : show
  # But : Affiche toutes les informations de l'unité d'enseignement
  #---------------------------------------------------------------------------------------------------
  def show
    @unite_enseignement = UniteEnseignement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @unite_enseignement }
    end
  end

  # GET /unite_enseignements/new
  # GET /unite_enseignements/new.xml
  #---------------------------------------------------------------------------------------------------
  # Nom : new
  # But : Page de création d'une nouvelle unité d'enseignement
  #---------------------------------------------------------------------------------------------------
  def new
    
    @unite_enseignement = UniteEnseignement.new(:coursBloc => false, :reconductible => true, :chargeExamen => 0, :personne => nil, :nbGroupesTh=>1,:nbGroupesLab=>1, :nbPeriodesTheoriquesSemaine => 2, :nbPeriodesPratiquesSemaine => 2 )
    
    
    data_prepare_for_new

    p "NEW NEW"
    p "*******"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @unite_enseignement }
    end
  end

  # GET /unite_enseignements/1/edit
  #---------------------------------------------------------------------------------------------------
  # Nom : edit
  # But : Modifie l'unité d'enseignement ainsi que ces liens vers des modules
  #---------------------------------------------------------------------------------------------------
  def edit
    @unite_enseignement = UniteEnseignement.find(params[:id])
    
    data_prepare_for_edit

  end

  # POST /unite_enseignements
  # POST /unite_enseignements.xml
  #---------------------------------------------------------------------------------------------------
  # Nom : create
  # But : Enregistre l'unité d'enseignement ainsi que ces liens vers des modules
  #---------------------------------------------------------------------------------------------------
  def create
    
    # Unite à enregistrer
    @unite_enseignement = UniteEnseignement.new(params[:unite_enseignement])
    
    # Définition de l'extension et du nouveau chemin du pdf
    #if params[:unite_enseignement]['upload_datafile'] != "" && params[:unite_enseignement]['upload_datafile'] != nil
    #  if params[:unite_enseignement]['upload_datafile'].content_type == "application/pdf"
    #    extension = "pdf"
    #    @unite_enseignement.pdf = "public/pdf/fiches_unites/"+ @unite_enseignement.abreviation + "." + extension
    #  end
    #end

    respond_to do |format|
      if @unite_enseignement.save
        
        # Enregistrement de tous les liens entre cette unité d'enseignement et les orientations
        liens = UniteEnseignementOrientation.find(:all)
        for lien in liens
          # S'approprie tous les liens entre les orientations et des unités d'enseignement qui n'ont pas d'id d'unité
          # Il s'agit des liens spécifiés pendant l'opération de mise à jour de cette unité d'enseignement.
          controle_lien(lien,@unite_enseignement)
        end
        
        # Si l'untié possède un fichier pdf
        if params[:unite_enseignement]['upload_datafile']!="" && params[:unite_enseignement]['upload_datafile'] != nil
          # Enregistre le pdf sur le serveur
          post = DataFile.save(params[:unite_enseignement]['upload_datafile'], extension, @unite_enseignement.abreviation)
        end
        
        flash[:notice] = 'Unité d\'enseignement créée correctement'
        format.html { redirect_to(unite_enseignements_url) }
        format.xml  { render :xml => @unite_enseignement, :status => :created, :location => @unite_enseignement }
      else
        flash[:error] = 'Saisie incorrecte'
        data_prepare_for_new
        format.html { render :action => "new" }
        format.xml  { render :xml => @unite_enseignement.errors, :status => :unprocessable_entity }
      end
    end
  end  
  
  #---------------------------------------------------------------------------------------------------
  # Nom : controle_lien
  # But : Controle l'orientation qui est enregistre dans le lien (Est-ce celui de l'annee correspondant à
  #       l'annee de l'unite
  # Parametre : lien => lien a controler et a enregistrer
  #             unite => unite a enregistrer
  #---------------------------------------------------------------------------------------------------
  def controle_lien (lien, unite)
    
    orientations = Orientation.find(:all)
    # Si le lien n'est pas attribué à une unité on contrôle son orientation et on l'enregistre avec l'unité demandée
    if lien.unite_enseignement_id == nil
      # Enregistrement de l'unité
      lien.unite_enseignement_id = unite.id
      # Trouvons l'orientation enregistrée
      orientation_test = Orientation.find(lien.orientation_id) 
      # Contrôle si l'orientation enregistrée est la bonne. Est-ce celle qui correspond à l'année de l'unité?
      if orientation_test.annee_academique_id.to_i != unite.periode.annee_academique_id.to_i
        for orientation in orientations
          # Enregistrement si c'est la bonne orientation
          if orientation.abreviation == orientation_test.abreviation && orientation.annee_academique_id.to_i == unite.periode.annee_academique_id.to_i
            lien.orientation_id = orientation.id
          end
        end
      end
      lien.save
    end
    
  end

  # PUT /unite_enseignements/1
  # PUT /unite_enseignements/1.xml
  #---------------------------------------------------------------------------------------------------
  # Nom : update
  # But : Modifie l'unité d'enseignement ainsi que ces liens vers des modules
  #---------------------------------------------------------------------------------------------------
  def update
    
    # Récupération de l'unité d'enseignement à modifier
    @unite_enseignement = UniteEnseignement.find(params[:id])
    
    
    # Nombre de personnes assistant le laboratoire
    nb_personnes = 2      

    respond_to do |format|
      if @unite_enseignement.update_attributes(params[:unite_enseignement])

        # Enregistrement de tous les liens entre cette unité d'enseignement et les orientations
        liens = UniteEnseignementOrientation.find(:all)
        for lien in liens
          # Supprime tous les "anciens" liens entre les orientations et cette unité d'enseignement
          if lien.unite_enseignement_id ==  @unite_enseignement.id
            lien.delete
          end
          # S'approprie tous les liens entre les orientations et des unités d'enseignement qui n'ont pas d'id d'unité
          # Il s'agit des liens spécifiés pendant l'opération de mise à jour de cette unité d'enseignement.
          controle_lien(lien,@unite_enseignement)
        end
        
        
        # Si le chemin du pdf a été modifié, on l'enregistre
        if params[:unite_enseignement]['pdf'] != "" && params[:unite_enseignement]['pdf'] != nil 
          if params[:unite_enseignement]['pdf'].content_type == "application/pdf"
            extension = "pdf"
            # Enregistre le pdf sur le serveur
            post = DataFile.save(params[:unite_enseignement]['pdf'], extension, @unite_enseignement.abreviation)
            # Enregistre le chemin dans la base de données
            @unite_enseignement.pdf = "public/pdf/fiches_unites/"+ @unite_enseignement.abreviation + "." + extension
            @unite_enseignement.save
          end
        end
        
        flash[:notice] = 'L\'unité d\'enseignement s\'est modifiée correctement'
        format.html { redirect_to unite_enseignement_path(@unite_enseignement) }
        format.xml  { head :ok }
      else
        data_prepare_for_edit
        format.html { render :action => "edit" }
        format.xml  { render :xml => @unite_enseignement.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /unite_enseignements/1
  # DELETE /unite_enseignements/1.xml
  #---------------------------------------------------------------------------------------------------
  # Nom : destroy
  # But : Supprime l'unité d'enseignement
  #---------------------------------------------------------------------------------------------------
  def destroy

    @unite_enseignement = UniteEnseignement.find(params[:id])
    if @unite_enseignement.projets.size!=0
      flash[:alert]="Suppression impossible, cette unité est associée à des projets d'enseignement"
      redirect_to(unite_enseignements_path)
    else    
      # Liste des liens entre des orientations et des unités d'enseignement
      liens = UniteEnseignementOrientation.find(:all)
  
      # Supprime tous les liens entre cette unité d'enseignement et les orientations
      for lien in liens
       if lien.unite_enseignement_id == @unite_enseignement.id
          lien.destroy
       end
      end
 
      # Récupère tous les desideratas en lien avec l'unité d'enseignement
      desideratas=@unite_enseignement.desideratas
  
      # Suppression de tous les desideratas d'assistants associés au desiderata
      for desiderata in desideratas
        assistants_desideratas= desiderata.personne_desideratas
        for un_assistant_desiderata in assistants_desideratas
          un_assistant_desiderata.delete
        end
        # Puis suppression du desiderata
        desiderata.delete
      end

     
      # Puis suppression de l'unité
      @unite_enseignement.delete

      respond_to do |format|
        format.html { redirect_to(unite_enseignements_path) }
        format.xml  { head :ok }
      end
    end
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: creation_orientation_draggable
  # But: Permet de rendre draggable les orientations
  #--------------------------------------------------------------------------------------------------------
  def creation_orientation_draggable
    
    # Si l'on crée une nouvelle unité, on affiche les orientations de l'année actuelle
    if params[:mode]=="new"
      id_annee=AnneeAcademique.courante.id
      
      # Orientations de l'années souhaitée
      orientations = Orientation.find(:all, :conditions => {:annee_academique_id=>id_annee}, :order => 'abreviation')
      @orientations = []
      i=0
      #Formatage de l'affichage
      for orientation in orientations
         @orientations[i]=orientation.abreviation+" ("+orientation.texte+")"
         i+=1
      end
      
    end
        p "**********************"
        p "!!!!!!!!!!!!!!!!!!!!!!"
    render :action => "creation_orientation_draggable" 
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : deplacement_orientation
  # But : Cette fonction permet d'enregistrer les liens entre les orientations et les unités
  #---------------------------------------------------------------------------------------------------
  def deplacement_orientation

    lien=UniteEnseignementOrientation.new
    lien.orientation_id = 100
    lien.unite_enseignement_id = 100
    lien.save!
    param_recu = params[:id]
    p "Valeur du paramètre reçu #{param_recu}"

    # Liste de toutes les orientations
    orientations = Orientation.find(:all) 

    if params[:target]=="orientations_concernees"
      # Création d'un nouveau lien entre une orientation et une unité
      lien=UniteEnseignementOrientation.new
      # On trouve l'orientation correspondante et on l'enregistre
      for orientation in orientations
          if orientation.abreviation==params[:id].split(' (').first
            lien.orientation_id=orientation.id
            lien.save
          end
      end
    else #suppression d'un lien
      # Liste de tous les liens entre orientations et unités
      liens = UniteEnseignementOrientation.find(:all)
      
      # On trouve le lien correspondant et on le supprime
      for lien in liens
          if lien.unite_enseignement_id == nil
            for orientation in orientations
              if orientation.abreviation == params[:id].split(' (').first && lien.orientation_id == orientation.id
                lien.delete
              end
            end
          end
      end
    end
    
    render :action => "deplacement_orientation" 
  end 
  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Modifie l'affichage des périodes et des modules en fonction de l'année académique choisie
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    
    id_annee = params[:id_annee_academique]
    
    # Est-ce que l'appel de la fonction vient de la vue d'index
    @vue_index = params[:index]
    
    # Liste de toutes les unités (éléments draggables)
    @unites=UniteEnseignement.find(:all, :order => :abreviation)
    
    # Liste de toutes les unités concernant l'année souhaitée
    @unites_annee =  UniteEnseignement.unites_annee_souhaitee(id_annee)
    
    #@liste_periodes = Periode.find_all_by_annee_academique_id(id_annee, :order => 'descriptif')
    @liste_periodes = Periode.where(annee_academique_id: id_annee).order('descriptif ASC')
    
    orientations = Orientation.find(:all, :conditions => {:annee_academique_id=>id_annee}, :order => 'abreviation')
    @orientations_non_concernees_annee=[]
    @orientations_concernees_annee=[]

    #Si l'on créé une nouvelle unité
    if params[:id_unite] == "" || params[:id_unite] == nil

           # Orientations de l'années souhaitée
           i=0
           #Formatage de l'affichage
           for orientation in orientations
              @orientations_non_concernees_annee[i]=orientation.abreviation+" ("+orientation.texte+")"
              i+=1
           end

    # Si l'on modifie l'unité
    else
          unite_enseignement = UniteEnseignement.find(params[:id_unite])
          liens = UniteEnseignementOrientation.find(:all,:conditions=> {:unite_enseignement_id => unite_enseignement.id})
          orientations_concernees_annee=[]

          # Trouve toutes les orientations en lien avec l'unité
          i=0
          for lien in liens
            for orientation in orientations
              if orientation.id == lien.orientation_id 
                @orientations_concernees_annee[i]=orientation.abreviation+" ("+orientation.texte+")"
                orientations_concernees_annee[i]=orientation
                i+=1
              end
            end
          end

          
          # Cherche les orientations non concernées
          i=0
          for orientation in orientations
              concernee = false
              # Pour toutes les orientations concernées
              for orientation_concernee in orientations_concernees_annee
                # Si l'orientation est concernée
                if orientation.id == orientation_concernee.id
                  concernee = true
                end
              end
              if !concernee
                @orientations_non_concernees_annee[i]= orientation.abreviation + " (" + orientation.texte + ")"
                i+=1
              end   
          end
      end

      # Si l'appel de la fonction ne vient pas de la page d'index
      if @vue_index == "false"
        # Liste des anciennes orientations
        #@orientations_anciennement_non_concernees = params[:orientations_non_concernees].split(',')
        #@orientations_anciennement_concernees = params[:orientations_concernees].split(',')
      end
    
  end
  

  def charges
    @unite_enseignement = UniteEnseignement.find(params[:id]) 
    tabAllGroupes = ('A'..'Z').to_a
    @tabGroupesTheorie=@unite_enseignement.tableau_des_no_de_groupe('T')
    @tabGroupesLab=@unite_enseignement.tableau_des_no_de_groupe('L')
  end
  
  def up_groupe
    # Upgrade d'un groupe (de C à B par exemple)
    @unite_enseignement = UniteEnseignement.find(params[:id])  
    noGroupe=params[:groupe]
    typeGroupe=params[:type_groupe] #'T' (théorie) ou 'L' (Labo)
    code=noGroupe[0].ord
    noGroupeBefore=(code-1).chr
    charges_assistanat=@unite_enseignement.charges_associees_pour_groupe(noGroupe, 'assistanat', typeGroupe)
    charges_assistanat_to_down=@unite_enseignement.charges_associees_pour_groupe(noGroupeBefore, 'assistanat', typeGroupe)
    charges_assistanat_to_down.each do |charge|
      charges_assistanat<<charge
    end
    charges_assistanat.each do |charge|
      if charge.numGroupe.eql?(noGroupe)
        charge.numGroupe=noGroupeBefore
      else
        charge.numGroupe=noGroupe          
      end
      charge.save
    end
    
    charges_enseignement=@unite_enseignement.charges_associees_pour_groupe(noGroupe, 'enseignement', typeGroupe)
    charges_enseignement_to_down=@unite_enseignement.charges_associees_pour_groupe(noGroupeBefore, 'enseignement', typeGroupe)
    charges_enseignement_to_down.each do |charge|
      charges_enseignement<<charge
    end
    charges_enseignement.each do |charge|
      if charge.numGroupe.eql?(noGroupe)
        charge.numGroupe=noGroupeBefore
      else
        charge.numGroupe=noGroupe          
      end
      charge.save
    end

    flash[:notice] = "Permutation effectuée -  type groupe: #{typeGroupe}  - #{noGroupe} over #{noGroupeBefore} "
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end
  
  def nouvelle_charge
    @unite_enseignement = UniteEnseignement.find(params[:id]) 
    @mode=params[:mode]
    if @mode.eql?("enseignement") 
      @charge = ChargeEnseignement.new
      @personnes_possibles=responsables_fictifs_enseignement + responsables_enseignement
      #@personnes_fictives_possibles=responsables_fictifs_enseignement
    else # assistanat
      @charge = ChargeAssistanat.new
      @charge.priorite=1
      @personnes_possibles = responsables_fictifs_assistanat + responsables_assistanat
      #@personnes_fictives_possibles = responsables_fictifs_assistanat
    end
    @numero_groupe = params[:groupe]
    @type_groupe = params[:type_groupe]    
  end
  
  def create_charge
    @unite_enseignement = UniteEnseignement.find(params[:id]) 
    @mode=params[:mode]
    if @mode.eql?("enseignement") 
      @charge = ChargeEnseignement.new(params[:charge_enseignement])
    else # assistanat
      @charge = ChargeAssistanat.new(params[:charge_assistanat])
    end
    @charge.unite_enseignement=@unite_enseignement
    if !@charge.save
      if @mode.eql?("enseignement") 
        @personnes_possibles = responsables_fictifs_enseignement + responsables_enseignement
        #@personnes_fictives_possibles=responsables_fictifs_enseignement
      else # assistanat
        @personnes_possibles = responsables_fictifs_assistanat + responsables_assistanat
        #@personnes_fictives_possibles=responsables_fictifs_assistanat
      end
      render :nouvelle_charge
      return
    end
    flash[:notice] = "Nouvelle charge créée correctement - #{params[:charge]}"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end
  
  #def edit_charge_enseignement
  #  @unite_enseignement = UniteEnseignement.find(params[:id]) 
  #  @charge_enseignement = ChargeEnseignement.find(params[:charge_id])
  #  @enseignants_possibles = responsables_fictifs_enseignement
  #  @enseignants_fictifs_possibles = responsables_fictifs_enseignement
  #  @numero_groupe = @charge_enseignement.numGroupe
  #  @type_groupe = @charge_enseignement.typeGroupe
  #end
  
  def edit_charge
    @unite_enseignement = UniteEnseignement.find(params[:id]) 
    @mode=params[:mode]
    if @mode.eql?("enseignement") 
      @charge = ChargeEnseignement.find(params[:charge_id])
      @personnes_possibles = responsables_fictifs_enseignement + responsables_enseignement
      #@personnes_fictives_possibles=responsables_fictifs_enseignement
    else # assistanat
      @charge = ChargeAssistanat.find(params[:charge_id])
      @personnes_possibles = responsables_fictifs_assistanat + responsables_assistanat
      #@personnes_fictives_possibles=responsables_fictifs_assistanat
    end
    @numero_groupe = @charge.numGroupe
    @type_groupe = @charge.typeGroupe
  end

  def update_charge
    @unite_enseignement = UniteEnseignement.find(params[:id]) 
    @mode=params[:mode]
    if @mode.eql?("enseignement") 
      @charge = ChargeEnseignement.find(params[:charge_id])
      @charge.update_attributes(params[:charge_enseignement])
    else # assistanat
      @charge = ChargeAssistanat.find(params[:charge_id])
      @charge.update_attributes(params[:charge_assistanat])
    end
    @charge.unite_enseignement=@unite_enseignement
    if !@charge.save
      flash[:error] = "Problème rencontré pour mettre à jour le charge"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    flash[:notice] = "Charge mise à jour correctement"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end
  
  def inc_groupes_th
    unite_enseignement = UniteEnseignement.find(params[:id]) 
    unite_enseignement.nbGroupesTh+=1
    if !unite_enseignement.save
      flash[:error] = "Problème rencontré - Nombre de groupes de théorie inchangé d'une unité"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    flash[:notice] = "Nombre de groupes de théorie augmenté"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end

  def change_assistanat_reconnu
    unite_enseignement = UniteEnseignement.find(params[:id]) 
    unite_enseignement.assistanat_reconnu = !unite_enseignement.assistanat_reconnu
    if !unite_enseignement.save
      flash[:error] = "Problème rencontré - Reconnaissance de l'assistanat inchangée"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    flash[:notice] = "Reconnaissance de l'assistanat modifiée"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end

  def dec_groupes_th
    unite_enseignement = UniteEnseignement.find(params[:id]) 
    if !(unite_enseignement.nbGroupesTh > 0)
      flash[:error] = "Problème rencontré - Nombre de groupes de théorie inchangé"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    unite_enseignement.nbGroupesTh-=1
    if !unite_enseignement.save
      flash[:error] = "Problème rencontré - Nombre de groupes de théorie inchangé"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    flash[:notice] = "Nombre de groupes de théorie diminué d'une unité"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end
  
    def inc_groupes_labo
    unite_enseignement = UniteEnseignement.find(params[:id]) 
    unite_enseignement.nbGroupesLab+=1
    if !unite_enseignement.save
      flash[:error] = "Problème rencontré - Nombre de groupes de labo inchangé d'une unité"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    flash[:notice] = "Nombre de groupes de labo augmenté"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end

  def dec_groupes_labo
    unite_enseignement = UniteEnseignement.find(params[:id]) 
    if !(unite_enseignement.nbGroupesLab > 0)
      flash[:error] = "Problème rencontré - Nombre de groupes de théorie inchangé"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    unite_enseignement.nbGroupesLab-=1
    if !unite_enseignement.save
      flash[:error] = "Problème rencontré - Nombre de groupes de labo inchangé"
      redirect_to charges_unite_enseignement_path(@unite_enseignement)  
      return
    end
    flash[:notice] = "Nombre de groupes de labo diminué d'une unité"
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end

  def destroy_charge
    @unite_enseignement = UniteEnseignement.find(params[:id])
    @mode=params[:mode]
    if @mode.eql?("enseignement") 
      @charge = ChargeEnseignement.find(params[:charge_id])
    else # assistanat
      @charge = ChargeAssistanat.find(params[:charge_id])
    end 
    @charge.destroy
    redirect_to charges_unite_enseignement_path(@unite_enseignement)
  end

  def data_prepare_for_new
    # Liste de tous les type d'unités d'enseignement
    @type_unite_enseignements = TypeUniteEnseignement.find(:all, :order=>'type_unite')
    
    # Liste de toutes les periodes liées à l'année actuelle
    #@periodes = Periode.find_all_by_annee_academique_id(AnneeAcademique.courante, :order=>'descriptif')
    @periodes = Periode.where(annee_academique_id: AnneeAcademique.courante).order('descriptif ASC')
    
    # Liste de toutes les annnes académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.liste_annees_academiques

    # Liste des professeurs susceptibles de superviser l'unité
    @professeurs=Personne.professeurs.sort_by(&:initiale)
    
  
    # Liste des modules à afficher
    @orientations_concernees = []
    @orientations_non_concernees = []
    
    orientations = Orientation.find(:all, :conditions => {:annee_academique_id=>AnneeAcademique.courante}, :order => 'abreviation')
    
    # Formatage du nom de l'orientation
    orientations.each do |orientation|
        @orientations_non_concernees<< orientation.abreviation+" ("+orientation.texte+")"
    end
  end

  def data_prepare_for_edit
    # Liste de tous les type d'unités d'enseignement
     @type_unite_enseignements = TypeUniteEnseignement.find(:all, :order=>'type_unite')

     # Periodes actuelles
     @periodes = Periode.find(:all, :order => 'descriptif', :conditions => ["annee_academique_id = :id",{:id => @unite_enseignement.periode.annee_academique_id}]) 

     # Liste de toutes les annnes académiques avec comme premier élément l'année souhaitée
     @annees_academiques = AnneeAcademique.liste_annees_academiques_choix(@unite_enseignement.periode.annee_academique_id)

     # Liste des professeurs susceptibles de superviser l'unité
     @professeurs=Personne.professeurs.sort_by(&:initiale)

     # Liste des orientations
     orientations = Orientation.find(:all)

     # Liste des liaisons entre des unités et des orientations
     liens = UniteEnseignementOrientation.find(:all)

     @orientations_concernees = []
     orientations_concernees = []
     @orientations_non_concernees = []
     i = 0  
     # Cherche les orientations concernées pour toutes les orientations
     for orientation in orientations
       # Pour toutes les liaisons entre des unitées et des orientations
       for lien in liens
           # L'orientation concerne cette unité si elle est de cette année
           if lien.orientation_id == orientation.id && @unite_enseignement.id == lien.unite_enseignement_id && orientation.annee_academique_id == @unite_enseignement.periode.annee_academique_id
             @orientations_concernees[i]=orientation.abreviation + " (" + orientation.texte + ")"
             orientations_concernees[i]=orientation
             i+=1
           end
       end
     end
     i=0

     # Cherche les orientations non concernées pour toutes les orientations
     for orientation in orientations
       non_concernee = true
       # Pour toutes les orientations concernées
       for orientation_concernee in orientations_concernees
         # Si l'orientation est concernée
         if orientation.id == orientation_concernee.id
           non_concernee = false
         end
       end
       if non_concernee && orientation.annee_academique_id == @unite_enseignement.periode.annee_academique_id
         @orientations_non_concernees[i]= orientation.abreviation + " (" + orientation.texte + ")"
         i+=1
       end   
     end


     # Création de liens temporaire vers des orientations. Ceci a pour but de travailler avec de faux liens tant 
     # que l'on n'a pas modifié l'unité d'enseignement

     liens = UniteEnseignementOrientation.find(:all)

     # Nettoyage: efface tous les liens incomplets existants dans la base de données
     for lien in liens
         if lien.unite_enseignement_id == nil
             lien.delete
         end
     end

     for lien in liens
         if lien.unite_enseignement_id == @unite_enseignement.id
             lien_tmp = UniteEnseignementOrientation.new
             lien_tmp.orientation_id = lien.orientation_id
             lien_tmp.save
         end
     end
  end
  
  private 
  
  def responsables_enseignement
  # Retourne les personnes susceptibles d'obtenir une charge d'enseignement
    return (Personne.professeurs_actifs + Personne.collaborateurs_actifs_de_linstitut).sort_by(&:initiale)
  end
  
  def responsables_assistanat
  # Retourne les personnes susceptibles d'obtenir une charge d'enseignement
    return Personne.professeurs_et_collaborateurs_actifs_de_linstitut.sort_by(&:initiale) 
  end 
  def responsables_fictifs_enseignement
  # Retourne les personnes fictives susceptibles d'obtenir une charge d'enseignement
    return Personne.professeurs_fictifs.sort_by(&:initiale)
  end
  
  def responsables_fictifs_assistanat
  # Retourne les personnes fictives susceptibles d'obtenir une charge d'enseignement
    return Personne.professeurs_et_collaborateurs_fictifs.sort_by(&:initiale)
  end
end

class JalonsController < ApplicationController
  before_action :set_jalon, only: [:show, :edit, :update, :destroy]

  # GET /jalons
  # GET /jalons.json
  def index
    if(params[:affectation_projet_id].nil?) then
      @jalons = Jalon.all
    else
      rDate = Date.parse(params[:date])
      puts(rDate)
      @jalons = Jalon.order(date: :asc).where{(affectation_projet_id.eq my{params[:affectation_projet_id]}) & (date.gteq my{rDate}) & (date.lteq my{rDate+6})}
    end
  end

  # GET /jalons/1
  # GET /jalons/1.json
  def show
  end

  # GET /jalons/new
  def new
    @jalon = Jalon.new(affectation_projet_id: params[:affectation_id], date: params[:date])
  end

  # GET /jalons/1/edit
  def edit
  end

  # POST /jalons
  # POST /jalons.json
  def create
    @jalon = Jalon.new(jalon_params)

    if @jalon.save
      render :json => {:status => "success", :class => "notice_box", :message => "Jalon crée avec succès"}
    else
      @planification.destroy
      render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la création du jalon"}
    end

    # respond_to do |format|
    #   if @jalon.save
    #     format.html { redirect_to @jalon, notice: 'Jalon was successfully created.' }
    #     format.json { render :show, status: :created, location: @jalon }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @jalon.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /jalons/1
  # PATCH/PUT /jalons/1.json
  def update
    respond_to do |format|
      if @jalon.update(jalon_params)
        format.html { redirect_to @jalon, notice: 'Jalon was successfully updated.' }
        format.json { render :show, status: :ok, location: @jalon }
      else
        format.html { render :edit }
        format.json { render json: @jalon.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jalons/1
  # DELETE /jalons/1.json
  def destroy
    #@jalon.destroy
    # respond_to do |format|
    #   format.html { redirect_to jalons_url, notice: 'Jalon was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
    @jalon.destroy
    render :layout => false
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_jalon
      @jalon = Jalon.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def jalon_params
      params.require(:jalon).permit(:affectation_projet_id, :mail_id, :date, :titre, :description)
    end
end

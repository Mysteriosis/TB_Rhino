# -*- encoding : utf-8 -*-
class PeriodesController < ApplicationController
 
  load_and_authorize_resource
 
  # GET /periodes
  # GET /periodes.xml
  def index
    
    if params[:annee_academique_id]
      @annee_academique = AnneeAcademique.find(params[:annee_academique_id])
    else
      @annee_academique = AnneeAcademique.courante
    end
    
    @periodes = @annee_academique.periodes
    @annees_academiques = AnneeAcademique.order(:annee)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @periodes }
    end
  end

  # GET /periodes/1
  # GET /periodes/1.xml
  def show
    @periode = Periode.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @periode }
    end
  end

  # GET /periodes/new
  # GET /periodes/new.xml
  def new
    @periode = Periode.new
    @annee_academique = AnneeAcademique.order("annee")
    @types_periode = TypePeriode.order('typePeriode') 
  
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @periode }
    end
  end

  # GET /periodes/1/edit
  def edit
    @periode = Periode.find(params[:id])
    # Liste de toutes les annnes académique
    @annee_academique = AnneeAcademique.find(:all, :order => 'annee') 
    # Liste de tous les type de périodes
    @types_periode = TypePeriode.find(:all, :order => 'typePeriode') 
  end

  # POST /periodes
  # POST /periodes.xml
  def create
    @periode = Periode.new(params[:periode])

    respond_to do |format|
      if @periode.save
        flash[:notice] = 'Periode was successfully created.'
        format.html { redirect_to(@periode) }
        format.xml  { render :xml => @periode, :status => :created, :location => @periode }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @periode.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /periodes/1
  # PUT /periodes/1.xml
  def update
    @periode = Periode.find(params[:id])

    respond_to do |format|
      if @periode.update_attributes(params[:periode])
        flash[:notice] = 'Periode was successfully updated.'
        format.html { redirect_to(@periode) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @periode.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /periodes/1
  # DELETE /periodes/1.xml
  def destroy
    @periode = Periode.find(params[:id])
    @periode.destroy

    respond_to do |format|
      format.html { redirect_to(periodes_url) }
      format.xml  { head :ok }
    end
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Met a jour les périodes en fonction des années académiques
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    # Liste des périodes
    @periodes = Periode.find(:all)
    # Liste de toutes les périodes concernant l'année souhaitée
    #@periodes_annee = Periode.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'Descriptif')
    @periodes_annee = Periode.where(annee_academique_id: params['id_annee_academique'].to_i).order('Descriptif ASC')
  end
  
end

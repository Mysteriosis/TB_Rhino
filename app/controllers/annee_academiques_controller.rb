# -*- encoding : utf-8 -*-
class AnneeAcademiquesController < ApplicationController
  
  load_and_authorize_resource
  
  # GET /annee_academiques
  # GET /annee_academiques.xml
  def index
    @annee_academiques = AnneeAcademique.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @annee_academiques }
    end
  end

  # GET /annee_academiques/1
  # GET /annee_academiques/1.xml
  def show
    @annee_academique = AnneeAcademique.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @annee_academique }
    end
  end

  # GET /annee_academiques/new
  # GET /annee_academiques/new.xml
  def new
    @annee_academique = AnneeAcademique.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @annee_academique }
    end
  end

  # GET /annee_academiques/1/edit
  def edit
    @annee_academique = AnneeAcademique.find(params[:id])
  end

  # POST /annee_academiques
  # POST /annee_academiques.xml
  def create
    @annee_academique = AnneeAcademique.new(params[:annee_academique])

    respond_to do |format|
      if @annee_academique.save
        flash[:notice] = 'AnneeAcademique was successfully created.'
        format.html { redirect_to(@annee_academique) }
        format.xml  { render :xml => @annee_academique, :status => :created, :location => @annee_academique }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @annee_academique.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /annee_academiques/1
  # PUT /annee_academiques/1.xml
  def update
    @annee_academique = AnneeAcademique.find(params[:id])

    respond_to do |format|
      if @annee_academique.update_attributes(params[:annee_academique])
        flash[:notice] = 'AnneeAcademique was successfully updated.'
        format.html { redirect_to(@annee_academique) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @annee_academique.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /annee_academiques/1
  # DELETE /annee_academiques/1.xml
  def destroy
    @annee_academique = AnneeAcademique.find(params[:id])
    @annee_academique.destroy

    respond_to do |format|
      format.html { redirect_to(annee_academiques_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class TypeIdExternesController < ApplicationController

  load_and_authorize_resource

  def index
    @type_id_externes = TypeIdExterne.all
    respond_with(@type_id_externes)
  end

  def show
    @type_id_externe = TypeIdExterne.find(params[:id])
    respond_with(@type_id_externe)
  end

  def new
    @type_id_externe = TypeIdExterne.new
    respond_with(@type_id_externe)
  end

  def edit
    @type_id_externe = TypeIdExterne.find(params[:id])
  end

  def create
    @type_id_externe = TypeIdExterne.new(params[:type_id_externe])
    @type_id_externe.save
    respond_with(@type_id_externe)
  end

  def update
    @type_id_externe = TypeIdExterne.find(params[:id])
    @type_id_externe.update_attributes(params[:type_id_externe])
    respond_with(@type_id_externe)
  end

  def destroy
    @type_id_externe = TypeIdExterne.find(params[:id])
    @type_id_externe.destroy
    respond_with(@type_id_externe)
  end
end

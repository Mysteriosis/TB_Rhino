class PlanificationsController < ApplicationController
  load_and_authorize_resource

  before_action :set_planification, only: [:show, :edit, :update, :destroy]

  # GET /planifications
  def index
    @ajd = Date.today

    respond_to do |format|
      format.html
      format.json
    end
  end

  # GET /planifications/1
  def show
  end

  # GET /planifications/new
  def new
    @planification = Planification.new
  end

  # GET /planifications/1/edit
  def edit
  end

  # POST /planifications
  def create
    @planification = Planification.new(planification_params)
    @planification.personne_id = current_user.id

    if @planification.save
      render :json => {:status => "success", :class => "notice_box", :message => "Planification crée avec succès"}
    else
      @planification.destroy
      render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la création de la planification"}
    end
  end

  # PATCH/PUT /planifications/1
  def update
    if @planification.update(planification_params)
      render :json => {:status => "success", :class => "notice_box", :message => "Planification modifiée avec succès"}
    else
      @planification.destroy
      render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la modification de la planification"}
    end
  end

  # DELETE /planifications/1
  def destroy
    @planification.destroy
    redirect_to planifications_url, notice: 'Planification was successfully destroyed.'
  end

  # GET /planifications/getRessources
  def getRessources()

    #Traitement des paramètres de requête
    @semaine  = (params[:date].nil?) ? Date.today.beginning_of_week : Date.parse(params[:date]).beginning_of_week
    avant    = params[:avant].nil? ? (ConfigurationsPlanification.where{variable.eq 'semaines_avant'}).first!.valeur.to_i : params[:avant]
    apres    = params[:apres].nil? ? (ConfigurationsPlanification.where{variable.eq 'semaines_apres'}).first!.valeur.to_i : params[:apres]
    @mode    = 'R';
    @appFormat  = params[:appFormat].nil? ? ConfigurationsPlanification.where{variable.eq 'format_planif'}.first!.valeur.to_s : params[:appFormat]
    @deplier = params[:deplier].nil? ? [current_user.initiale] : params[:deplier]
    @filtre  = params[:filtre].nil? ? '' : params[:filtre]
    @edit = false

    @limite_de = (@semaine-(avant*7)).beginning_of_week
    @limite_a  = (@semaine+(apres*7)).beginning_of_week
    @semaines  = Array.new

    @limite_de.step(@limite_a, +7).each do |d|
      @semaines << {:de => d, :a => d+6, :num => d.strftime("%W"), :jours => 5 - (JourAcademique.entre(d, d+7)).length}
    end

    #Requête
    if current_user.has_role? :responsable_rh or current_user.has_role? :admin
      @ressources = Personne.order{initiale.asc}.joins{affectation_projets.outer}.group{personnes.id}.where{
        (
          (initiale =~ my{@filtre+'%'}) &
          ((affectation_projets.date_debut.lteq my{@limite_a}) | (affectation_projets.date_debut.eq nil)) &
          ((affectation_projets.date_fin.gteq my{@limite_de}) | (affectation_projets.date_fin.eq nil)) &
          (affectation_projets.est_active.eq 1) &
          (actif.eq 1) &
          (est_fictive.eq 0)
        ) |
        (initiale =~ my{current_user.initiale})
      }
      @edit = true
    elsif current_user.isChefProjet?
      @ressources = Personne.order{initiale.asc}.joins{projets.outer}.group{personnes.id}.distinct.where{(
      (initiale =~ my{@filtre+'%'}) &
        ((affectation_projets.date_debut.lteq my{@limite_a}) | (affectation_projets.date_debut.eq nil)) &
        ((affectation_projets.date_fin.gteq my{@limite_de}) | (affectation_projets.date_fin.eq nil)) &
        (affectation_projets.est_active.eq 1) &
        (projets.chef_id.eq my{current_user.id})
      (actif.eq 1) &
        (est_fictive.eq 0)) |
        (initiale =~ my{current_user.initiale})
      }
      @edit = true
    # else if current_user.is_professeur?
    # else if current_user.is_collaborateur?
    elsif current_user.is_collaborateur? | current_user.is_professeur?
      @ressources = [current_user]
    end
  end

  # GET /planifications/getProjets
  def getProjets()

    #Renvoie les données de getRessources aux personnes non autorisées
    if !current_user.has_role? :responsable_rh and !current_user.has_role? :admin and !current_user.isChefProjet?
      redirect_to action: 'getRessources', status: 302
    end

    #Traitement des paramètres de requête
    @semaine  = params[:date].nil?  ? Date.today.beginning_of_week : Date.parse(params[:date]).beginning_of_week
    avant    = params[:avant].nil? ? (ConfigurationsPlanification.where{variable.eq 'semaines_avant'}).first!.valeur.to_i : params[:avant]
    apres    = params[:apres].nil? ? (ConfigurationsPlanification.where{variable.eq 'semaines_apres'}).first!.valeur.to_i : params[:apres]
    @mode    = 'P';
    @appFormat  = params[:appFormat].nil? ? ConfigurationsPlanification.where{variable.eq 'format_planif'}.first!.valeur.to_s : params[:appFormat]
    @deplier = params[:deplier].nil? ? [current_user.initiale] : params[:deplier]
    @filtre  = params[:filtre].nil? ? '' : params[:filtre]
    @edit = true

    @limite_de = (@semaine-(avant*7)).beginning_of_week
    @limite_a  = (@semaine+(apres*7)).beginning_of_week
    @semaines  = Array.new

    @limite_de.step(@limite_a, +7).each do |d|
      @semaines << {:de => d, :a => d+6, :num => d.strftime("%W"), :jours => 5 - (JourAcademique.entre(d, d+7)).length}
    end

    #Requête
    if current_user.has_role? :responsable_rh or current_user.has_role? :admin
      @projets = Projet.order(id_externe: :asc).joins{personnes.inner}.group{projets.id}.where{(
        ((projets.date_debut.lteq my{@limite_a}) | (projets.date_debut.eq nil)) &
        ((projets.date_fin.gteq my{@limite_de}) | (projets.date_fin.eq nil)) &
        (projets.est_actif.eq 1) &
        ((affectation_projets.date_debut.lteq my{@limite_a}) | (affectation_projets.date_debut.eq nil)) &
        ((affectation_projets.date_fin.gteq my{@limite_de}) | (affectation_projets.date_fin.eq nil)) &
        (affectation_projets.est_active.eq 1) &
        (personnes.actif.eq 1) &
        (personnes.est_fictive.eq 0) &
        (id_interne =~ my{@filtre+'%'}))
      }
    elsif current_user.isChefProjet?
      @projets = Projet.order(id_externe: :asc).joins{personnes.inner}.group{projets.id}.where{(
      ((projets.date_debut.lteq my{@limite_a}) | (projets.date_debut.eq nil)) &
        ((projets.date_fin.gteq my{@limite_de}) | (projets.date_fin.eq nil)) &
        (projets.est_actif.eq 1) &
        (projets.chef_id.eq my{current_user.id}) &
        ((affectation_projets.date_debut.lteq my{@limite_a}) | (affectation_projets.date_debut.eq nil)) &
        ((affectation_projets.date_fin.gteq my{@limite_de}) | (affectation_projets.date_fin.eq nil)) &
        (affectation_projets.est_active.eq 1) &
        (personnes.actif.eq 1) &
        (personnes.est_fictive.eq 0) &
        (id_interne =~ my{@filtre+'%'}))
      }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_planification
      @planification = Planification.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def planification_params
      params.permit(:id, :affectation_projet_id, :valeur, :date)
    end
end

# -*- encoding : utf-8 -*-
class StatutProjetsController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /statut_projets
  # GET /statut_projets.xml
  def index
    @statut_projets = StatutProjet.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @statut_projets }
    end
  end

  # GET /statut_projets/1
  # GET /statut_projets/1.xml
  def show
    @statut_projet = StatutProjet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @statut_projet }
    end
  end

  # GET /statut_projets/new
  # GET /statut_projets/new.xml
  def new
    @statut_projet = StatutProjet.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @statut_projet }
    end
  end

  # GET /statut_projets/1/edit
  def edit
    @statut_projet = StatutProjet.find(params[:id])
  end

  # POST /statut_projets
  # POST /statut_projets.xml
  def create
    @statut_projet = StatutProjet.new(params[:statut_projet])

    respond_to do |format|
      if @statut_projet.save
        flash[:notice] = 'StatutProjet was successfully created.'
        format.html { redirect_to(@statut_projet) }
        format.xml  { render :xml => @statut_projet, :status => :created, :location => @statut_projet }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @statut_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /statut_projets/1
  # PUT /statut_projets/1.xml
  def update
    @statut_projet = StatutProjet.find(params[:id])

    respond_to do |format|
      if @statut_projet.update_attributes(params[:statut_projet])
        flash[:notice] = 'StatutProjet was successfully updated.'
        format.html { redirect_to(@statut_projet) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @statut_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /statut_projets/1
  # DELETE /statut_projets/1.xml
  def destroy
    @statut_projet = StatutProjet.find(params[:id])
    @statut_projet.destroy

    respond_to do |format|
      format.html { redirect_to(statut_projets_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class AffectationProjetsController < ApplicationController
  
  
  def new
    authorize!(:new, AffectationProjet)
    
    if params[:projet_id]  # On connaît le projet
      @projet = Projet.find(params[:projet_id])
      
      if @projet.kind_of?(ProjetAcademique)
        @affectation_projet = AffectationProjetAcademique.new
      else
        @affectation_projet = AffectationProjet.new
      end
      @affectation_projet.projet=@projet

      # Initialisations complémentaires spécifiques
      @affectation_projet.display_name=@projet.id_interne
      load_project_data_if_enseignement(@projet) 

      

      if params[:personne_id] # Si la personne est connue également
        # Personnes & engagements
        @personne = Personne.find(params[:personne_id])
        @affectation_projet.personne=@personne
        @engagements = @affectation_projet.personne.engagements

      else 
        # Personnes & engagements
        init_liste_personnes_potentielles(@projet)
        @engagements = [] 
      end

      
    else # On connaît la personne
      @personne = Personne.find(params[:personne_id])
      
      @affectation_projet = AffectationProjet.new
      @affectation_projet.personne=@personne
      
      # Projets & engagements
      @projets = Projet.projets_non_archives.sort_by(&:id_interne)
      @engagements = @affectation_projet.personne.engagements
    end
    
    # Initialisations complémentaires générales
    @affectation_projet.taux=100
    
    @vient_de= params[:vient_de]

    respond_with(@affectation_projet)
  end

  def edit
    @affectation_projet = AffectationProjet.find(params[:id])
    authorize!(:edit, @affectation_projet)

    @engagements = @affectation_projet.personne.engagements
    @personne=@affectation_projet.personne
    @projet=@affectation_projet.projet

    @vient_de= params[:vient_de]
  end

  def create
    if params[:affectation_projet][:projet_id] && Projet.find(params[:affectation_projet][:projet_id]).kind_of?(ProjetAcademique)
      @affectation_projet = AffectationProjetAcademique.new
    else
      @affectation_projet = AffectationProjet.new
    end
    @affectation_projet.attributes = params[:affectation_projet]
    if @affectation_projet.display_name
      @affectation_projet.display_name=@affectation_projet.display_name.strip
    end
    @affectation_projet.est_active=true
    if @affectation_projet.personne
      @affectation_projet.init_color
    end
    
    if @affectation_projet.valid?
      authorize!(:create, @affectation_projet)
    end

    if  @affectation_projet.engagement
      hors_limites_contrats_flag=@affectation_projet.hors_limites_contrats
      if hors_limites_contrats_flag
        flash[:alert] = "MAIS ATTENTION!! Cette affectation est en dehors des limites des contrats associés à l'engagement #{@affectation_projet.engagement.display_name}" 
      end
    end
    
    if params[:projet_id] # On vient depuis un projet
      @projet = Projet.find(params[:projet_id])
      @affectation_projet.projet = @projet

    end
    
    if params[:personne_id] # On vient depuis une personne
      @personne = Personne.find(params[:personne_id]) 
      @affectation_projet.personne = @personne
    end

    @vient_de=params[:vient_de]
    if @vient_de=="projet"
      path = @projet.becomes(Projet)
    else
      path = @personne
    end

    if @affectation_projet.save    
      redirect_to path
    else  #  Saisie non complète, on repart dans le formulaire new
      @projets = Projet.projets_non_archives.sort_by(&:id_interne)
      init_liste_personnes_potentielles(@projet)
      if @affectation_projet.personne
        @engagements = @affectation_projet.personne.engagements
      else
        @engagements = []
      end
      render :action => "new"
    end
  end

  def update
    @affectation_projet = AffectationProjet.find(params[:id])
    @affectation_projet.attributes = params[:affectation_projet]
    if @affectation_projet.display_name!=nil
      @affectation_projet.display_name=@affectation_projet.display_name.strip
    end
    if @affectation_projet.valid?
      authorize!(:update, @affectation_projet)
    end
    
    if @affectation_projet.engagement
      hors_limites_contrats_flag=@affectation_projet.hors_limites_contrats
      if hors_limites_contrats_flag
        flash[:alert] = "MAIS ATTENTION!! Cette affectation est en dehors des limites des contrats associés à l'engagement #{@affectation_projet.engagement.display_name}" 
      end
    end
    
    @vient_de=params[:vient_de]
    if @vient_de=="projet"
      path = @affectation_projet.projet.becomes(Projet)
    else
      path = @affectation_projet.personne
    end

    if @affectation_projet.save   
      redirect_to path
    else  #  Saisie non complète, on repart dans le formulaire edit
      @projet=@affectation_projet.projet
      @personne=@affectation_projet.personne
      @engagements = @affectation_projet.personne.engagements
      render :action => "edit"
    end
  end

  def destroy
    @affectation_projet = AffectationProjet.find(params[:id])
    authorize!(:destroy, @affectation_projet)
    
    projet = @affectation_projet.projet.becomes(Projet)
    personne = @affectation_projet.personne

    # Contrôle objets dependants avant suppression
    deletable = true
    errors = ""
    
    if @affectation_projet.plage_horaire_saisies.length != 0
      deletable = false
      errors += " - heures saisies"
    end
    
    if !deletable 
      flash[:alert] = "Impossible de supprimer cette affectation, elle a encore des éléments associés : " + errors
    else
      flash[:notice] = "Suppression réalisée avec succès!"
      @affectation_projet.destroy
    end

    if params[:vient_de].eql?("projet") # On vient depuis un projet 
       redirect_to(projet) 
    else # On vient depuis une personne
       redirect_to(personne) 
    end
  end
  
  def supprimer_heures_saisies
    @affectation_projet = AffectationProjet.find(params[:id])

    heures_saisies=@affectation_projet.plage_horaire_saisies
    heures_saisies.each do |hs|
      hs.destroy
    end
    
    if params[:vient_de].eql?("projet") # On vient depuis un projet 
       redirect_to(projet_path(@affectation_projet.projet.becomes(Projet))) 
    else # On vient depuis une personne
       redirect_to(personne_path(@affectation_projet.personne)) 
    end  
  end
  
  def activer
    @affectation_projet = AffectationProjet.find(params[:id])
    @affectation_projet.est_active=true
    if @affectation_projet.save
      redirect_to(projet_path(@affectation_projet.projet)) 
    end
  end
  
  def desactiver
    @affectation_projet = AffectationProjet.find(params[:id])
    @affectation_projet.est_active=false
    if @affectation_projet.save
      redirect_to(projet_path(@affectation_projet.projet)) 
    end
  end
  
  def get_engagements_select
    # Fonction invoquée par Ajax, quand il s'agit de mettre à jour la liste des engagements à disposition pour une personne
    # dans le formulaire de création, dès qu'une personne est sélectionnée dans le formulaire
    personne = Personne.find(params[:personne_id])
    projet = Projet.find_by_id(params[:projet_id])
    @affectation_projet = AffectationProjet.new
    @affectation_projet.personne=personne 
    @affectation_projet.projet=projet

    load_project_data_if_enseignement(@affectation_projet.projet)   
     
    @engagements = personne.engagements
    respond_to do |format|
      format.js do
        render :partial => "engagements_select", :locals => {:o => @affectation_projet, :engagements => @engagements}
      end
    end
  end
  
  
  def project_specific_fields
    # Fonction invoquée par Ajax, quand il s'agit de mettre à jour les champs de saisie dès qu'un projet est sélectionné 
    # dans le formulaire 
    personne = Personne.find_by_id(params[:personne_id])
    @projet = Projet.find_by_id(params[:projet_id])
    if @projet.kind_of?(ProjetAcademique)
      @affectation_projet = AffectationProjetAcademique.new
    else
      @affectation_projet = AffectationProjet.new
    end
    @affectation_projet.personne=personne 
    @affectation_projet.projet = @projet
    
    # Préparation des données pour listes à choix
    @engagements = personne.engagements 
    
    # Initialisation des données du formulaire
    @affectation_projet.display_name=@projet.id_interne
    load_project_data_if_enseignement(@affectation_projet.projet)

    respond_to do |format|
      format.html {render :partial => "project_specific_fields", :locals => {:o => @affectation_projet, :engagements => @engagements}}
      format.js {render :partial => "project_specific_fields", :locals => {:o => @affectation_projet, :engagements => @engagements}}
    end
  end
        
  def get_details
  
    @affectation_projet = AffectationProjet.find(params[:id])

    respond_to do |format|
      format.html {render :partial => "details", :locals => {:ap => @affectation_projet, :vient_de => params[:vient_de]}}
      format.js {render :partial => "details", :locals => {:ap => @affectation_projet, :vient_de => params[:vient_de]}}
    end
    
  end
 
  def get_consommation_graph_data
    
    res = []
    @affectation_projet = AffectationProjet.find(params[:affectation_projet_id])
    @affectation_projet.plage_horaire_saisies.order(:debut).each do |plage_horaire|
      
      if res.count > 0 && plage_horaire.debut.at_midnight.to_i*1000 == res.last[0]
        res.last[1] += plage_horaire.heures
      else
        res << [plage_horaire.debut.at_midnight.to_i*1000, plage_horaire.heures]
      end
    
    end
    
    
    respond_to do |format|
      format.js do
        render :text => res.to_json
      end
    end
  end
 
private
  
  
  def init_liste_personnes_potentielles(projet)
    collabo_et_profs_actifs_de_linstitut = Personne.professeurs_et_collaborateurs_actifs_de_linstitut

    if projet
      @personnes = (collabo_et_profs_actifs_de_linstitut - [projet.personnes]).sort_by(&:display_name)
    else
      @personnes = collabo_et_profs_actifs_de_linstitut.sort_by(&:display_name)
    end
  end
  
  def load_project_data_if_enseignement(projet) 
    if projet.kind_of?(ProjetEnseignement)
        @affectation_projet.taux = 100
        @affectation_projet.date_debut=projet.date_debut
        @affectation_projet.date_fin=projet.date_fin
        @affectation_projet.export_sagex=false
        if params[:heures_a_charge]
          @affectation_projet.charge_horaire = params[:heures_a_charge]
        else
          begin
            @affectation_projet.charge_horaire=@affectation_projet.charge_enseignement*@affectation_projet.taux/100
            rescue
              @affectation_projet.charge_horaire=0
          end
        end
    end
  end
  

  
end

# -*- encoding : utf-8 -*-
class MoisController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /mois
  # GET /mois.xml
  def index
    @mois = Moi.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @mois }
    end
  end

  # GET /mois/1
  # GET /mois/1.xml
  def show
    @moi = Moi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @moi }
    end
  end

  # GET /mois/new
  # GET /mois/new.xml
  def new
    @moi = Moi.new
    
    # Liste des années civiles
    @annees_civiles=AnneeCivile.find(:all, :order=>'annee')

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @moi }
    end
  end

  # GET /mois/1/edit
  def edit
    @moi = Moi.find(params[:id])
    
    # Liste des années civiles
    @annees_civiles=AnneeCivile.find(:all, :order=>'annee')
  end

  # POST /mois
  # POST /mois.xml
  def create
    @moi = Moi.new(params[:moi])

    respond_to do |format|
      if @moi.save
        flash[:notice] = 'Moi was successfully created.'
        format.html { redirect_to(@moi) }
        format.xml  { render :xml => @moi, :status => :created, :location => @moi }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @moi.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mois/1
  # PUT /mois/1.xml
  def update
    @moi = Moi.find(params[:id])

    respond_to do |format|
      if @moi.update_attributes(params[:moi])
        flash[:notice] = 'Moi was successfully updated.'
        format.html { redirect_to(@moi) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @moi.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mois/1
  # DELETE /mois/1.xml
  def destroy
    @moi = Moi.find(params[:id])
    @moi.destroy

    respond_to do |format|
      format.html { redirect_to(mois_url) }
      format.xml  { head :ok }
    end
  end
end

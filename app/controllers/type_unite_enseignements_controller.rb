# -*- encoding : utf-8 -*-
class TypeUniteEnseignementsController < ApplicationController
  #before_fileter :activate_authlogic
  load_and_authorize_resource
  
  # GET /type_unite_enseignements
  # GET /type_unite_enseignements.xml
  def index
    @type_unite_enseignements = TypeUniteEnseignement.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_unite_enseignements }
    end
  end

  # GET /type_unite_enseignements/1
  # GET /type_unite_enseignements/1.xml
  def show
    @type_unite_enseignement = TypeUniteEnseignement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_unite_enseignement }
    end
  end

  # GET /type_unite_enseignements/new
  # GET /type_unite_enseignements/new.xml
  def new
    @type_unite_enseignement = TypeUniteEnseignement.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_unite_enseignement }
    end
  end

  # GET /type_unite_enseignements/1/edit
  def edit
    @type_unite_enseignement = TypeUniteEnseignement.find(params[:id])
  end

  # POST /type_unite_enseignements
  # POST /type_unite_enseignements.xml
  def create
    @type_unite_enseignement = TypeUniteEnseignement.new(params[:type_unite_enseignement])

    respond_to do |format|
      if @type_unite_enseignement.save
        flash[:notice] = 'TypeUniteEnseignement was successfully created.'
        format.html { redirect_to(@type_unite_enseignement) }
        format.xml  { render :xml => @type_unite_enseignement, :status => :created, :location => @type_unite_enseignement }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_unite_enseignement.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /type_unite_enseignements/1
  # PUT /type_unite_enseignements/1.xml
  def update
    @type_unite_enseignement = TypeUniteEnseignement.find(params[:id])

    respond_to do |format|
      if @type_unite_enseignement.update_attributes(params[:type_unite_enseignement])
        flash[:notice] = 'TypeUniteEnseignement was successfully updated.'
        format.html { redirect_to(@type_unite_enseignement) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_unite_enseignement.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /type_unite_enseignements/1
  # DELETE /type_unite_enseignements/1.xml
  def destroy
    @type_unite_enseignement = TypeUniteEnseignement.find(params[:id])
    @type_unite_enseignement.destroy

    respond_to do |format|
      format.html { redirect_to(type_unite_enseignements_url) }
      format.xml  { head :ok }
    end
  end
end

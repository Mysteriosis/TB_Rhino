# -*- encoding : utf-8 -*-
class EvenementProjetsController < ApplicationController
  
  #before_fileter :activate_authlogic
  #before_filter :authorize
  # GET /evenement_projets
  # GET /evenement_projets.xml
  def index
    @evenement_projets = EvenementProjet.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evenement_projets }
    end
  end

  # GET /evenement_projets/1
  # GET /evenement_projets/1.xml
  def show
    @evenement_projet = EvenementProjet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evenement_projet }
    end
  end

  # GET /evenement_projets/new
  # GET /evenement_projets/new.xml
  def new
    @evenement_projet = EvenementProjet.new
    
    # Liste de tous les types d'événement de projet
    @types_evenement_projet = TypeEvenementProjet.find(:all, :order=>'typeEvenementProjet')

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evenement_projet }
    end
  end

  # GET /evenement_projets/1/edit
  def edit
    @evenement_projet = EvenementProjet.find(params[:id])
    
    # Liste de tous les types d'événement de projet
    @types_evenement_projet = TypeEvenementProjet.find(:all, :order=>'typeEvenementProjet')
  end

  # POST /evenement_projets
  # POST /evenement_projets.xml
  def create
    @evenement_projet = EvenementProjet.new(params[:evenement_projet])

    respond_to do |format|
      if @evenement_projet.save
        flash[:notice] = 'EvenementProjet was successfully created.'
        format.html { redirect_to(@evenement_projet) }
        format.xml  { render :xml => @evenement_projet, :status => :created, :location => @evenement_projet }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evenement_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evenement_projets/1
  # PUT /evenement_projets/1.xml
  def update
    @evenement_projet = EvenementProjet.find(params[:id])

    respond_to do |format|
      if @evenement_projet.update_attributes(params[:evenement_projet])
        flash[:notice] = 'EvenementProjet was successfully updated.'
        format.html { redirect_to(@evenement_projet) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evenement_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evenement_projets/1
  # DELETE /evenement_projets/1.xml
  def destroy
    @evenement_projet = EvenementProjet.find(params[:id])
    @evenement_projet.destroy

    respond_to do |format|
      format.html { redirect_to(evenement_projets_url) }
      format.xml  { head :ok }
    end
  end
end

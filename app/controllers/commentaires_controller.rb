class CommentairesController < ApplicationController
  skip_before_filter  :verify_authenticity_token
  before_action :set_commentaire, only: [:show, :edit, :update, :destroy]

  # GET /commentaires
  # GET /commentaires.json
  def index
    redirect_to "/"
  end

  # GET /commentaires/1
  # GET /commentaires/1.json
  def show
  end

  # GET /commentaires/new
  def new
    @commentaire = Commentaire.new(planification_id: params[:planification_id])
    @affectation_id = params[:affectation_id]
    @date = params[:date]
  end

  # GET /commentaires/1/edit
  def edit
  end

  # POST /commentaires
  # POST /commentaires.json
  def create

    if !params[:comm_affectation_id].nil? then
      @planification = Planification.new(affectation_projet_id: params[:comm_affectation_id],
                                         personne_id: current_user.id,
                                         valeur: 0,
                                         date: params[:comm_date]);

      if !@planification.save then
        render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la création de la planification"}
      end
    end

    @commentaire = Commentaire.new(commentaire_params)
    if(!@planification.nil?)
      @commentaire.planification_id = @planification.id
    end
    @commentaire.personne_id = current_user.id

    if @commentaire.save
      render :json => {:status => "success", :class => "notice_box", :message => "Commentaire crée avec succès"}
    else
      @planification.destroy
      render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la création du commentaire"}
    end

  end

  # PATCH/PUT /commentaires/1
  # PATCH/PUT /commentaires/1.json
  def update
    if @commentaire.update(commentaire_params)
      render :json => {:status => "success", :class => "notice_box", :message => "Commentaire modifié avec succès"}
    else
      render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la modification du commentaire"}
    end
  end

  # DELETE /commentaires/1
  # DELETE /commentaires/1.json
  def destroy
    # @commentaire.destroy
    # respond_to do |format|
    #   format.html { redirect_to commentaires_url, notice: 'Commentaire was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
    if @commentaire.destroy
      render :json => {:status => "success", :class => "notice_box", :message => "Commentaire supprimé avec succès"}
    else
      render :json => {:status => "error", :class => "error_box", :message => "Erreur lors de la suppression du commentaire"}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_commentaire
      @commentaire = Commentaire.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def commentaire_params
      params.require(:commentaire).permit(:texte, :planification_id)#.merge(:affectation_id, :date)
    end
end

# -*- encoding : utf-8 -*-
class DesiderataProjetsController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /desiderata_projets
  # GET /desiderata_projets.xml
  def index
    
    # Liste des desiderata de projet
    @desiderata_projets = DesiderataProjet.find(:all)
    # Année actuelle
    @annee_academique = AnneeAcademique.courante
    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @desiderata_projets }
    end
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Met a jour les desiderata de projet en fonction des annees academiques
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    # Liste de tous les desiderata de projet
    @desiderata_projets = DesiderataProjet.find(:all)
    # Liste de tous les desiderata de projet concernant l'année souhaitée
    @desiderata_projet_annee = DesiderataProjet.where(annee_academique_id: params['id_annee_academique'].to_i)
  end

  # GET /desiderata_projets/1
  # GET /desiderata_projets/1.xml
  def show
    @desiderata_projet = DesiderataProjet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @desiderata_projet }
    end
  end

  # GET /desiderata_projets/new
  # GET /desiderata_projets/new.xml
  def new
    @desiderata_projet = DesiderataProjet.new
    
    # Liste des annees académiques
    @annees_academiques = AnneeAcademique.find(:all,:order=>'annee')
    # Liste des personne
    @personnes = Personne.find(:all,:order=>'initiale')

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @desiderata_projet }
    end
  end

  # GET /desiderata_projets/1/edit
  def edit
    @desiderata_projet = DesiderataProjet.find(params[:id])
    # Liste des annees académiques
    @annees_academiques = AnneeAcademique.find(:all,:order=>'annee')
    # Liste des personne
    @personnes = Personne.find(:all,:order=>'initiale')
  end

  # POST /desiderata_projets
  # POST /desiderata_projets.xml
  def create
    @desiderata_projet = DesiderataProjet.new(params[:desiderata_projet])

    respond_to do |format|
      if @desiderata_projet.save
        flash[:notice] = 'DesiderataProjet was successfully created.'
        format.html { redirect_to(@desiderata_projet) }
        format.xml  { render :xml => @desiderata_projet, :status => :created, :location => @desiderata_projet }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @desiderata_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /desiderata_projets/1
  # PUT /desiderata_projets/1.xml
  def update
    @desiderata_projet = DesiderataProjet.find(params[:id])

    respond_to do |format|
      if @desiderata_projet.update_attributes(params[:desiderata_projet])
        flash[:notice] = 'DesiderataProjet was successfully updated.'
        format.html { redirect_to(@desiderata_projet) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @desiderata_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /desiderata_projets/1
  # DELETE /desiderata_projets/1.xml
  def destroy
    @desiderata_projet = DesiderataProjet.find(params[:id])
    @desiderata_projet.destroy

    respond_to do |format|
      format.html { redirect_to(desiderata_projets_url) }
      format.xml  { head :ok }
    end
  end
end

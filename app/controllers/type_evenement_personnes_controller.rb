# -*- encoding : utf-8 -*-
class TypeEvenementPersonnesController < ApplicationController
  
  load_and_authorize_resource
  #before_fileter :activate_authlogic
  # GET /type_evenement_personnes
  # GET /type_evenement_personnes.xml
  def index
    @type_evenement_personnes = TypeEvenementPersonne.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_evenement_personnes }
    end
  end

  # GET /type_evenement_personnes/1
  # GET /type_evenement_personnes/1.xml
  def show
    @type_evenement_personne = TypeEvenementPersonne.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_evenement_personne }
    end
  end

  # GET /type_evenement_personnes/new
  # GET /type_evenement_personnes/new.xml
  def new
    @type_evenement_personne = TypeEvenementPersonne.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_evenement_personne }
    end
  end

  # GET /type_evenement_personnes/1/edit
  def edit
    @type_evenement_personne = TypeEvenementPersonne.find(params[:id])
  end

  # POST /type_evenement_personnes
  # POST /type_evenement_personnes.xml
  def create
    @type_evenement_personne = TypeEvenementPersonne.new(params[:type_evenement_personne])

    respond_to do |format|
      if @type_evenement_personne.save
        flash[:notice] = 'TypeEvenementPersonne was successfully created.'
        format.html { redirect_to(@type_evenement_personne) }
        format.xml  { render :xml => @type_evenement_personne, :status => :created, :location => @type_evenement_personne }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_evenement_personne.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /type_evenement_personnes/1
  # PUT /type_evenement_personnes/1.xml
  def update
    @type_evenement_personne = TypeEvenementPersonne.find(params[:id])

    respond_to do |format|
      if @type_evenement_personne.update_attributes(params[:type_evenement_personne])
        flash[:notice] = 'TypeEvenementPersonne was successfully updated.'
        format.html { redirect_to(@type_evenement_personne) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_evenement_personne.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /type_evenement_personnes/1
  # DELETE /type_evenement_personnes/1.xml
  def destroy
    @type_evenement_personne = TypeEvenementPersonne.find(params[:id])
    @type_evenement_personne.destroy

    respond_to do |format|
      format.html { redirect_to(type_evenement_personnes_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class FilieresController < ApplicationController
  
  load_and_authorize_resource
  #before_fileter :activate_authlogic
  # GET /filieres
  # GET /filieres.xml
  def index
    
    
    @annee_academique = AnneeAcademique.courante
    
      # Liste de toutes les filieres
      # !!!!! - 4 Juillet 2012
      # @filieres = Filiere.find(:all)  -- Si on veut le choix de l'année académique avec ajax, les prendre tous
      # Sinon:    # Liste de toutes les filières
      #@filieres = Filiere.find_all_by_annee_academique_id(@annee_academique.id, :order => 'abreviation')
      @filieres = Filiere.where(annee_academique_id: @annee_academique.id).order('abreviation ASC')

    
    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @filieres }
    end
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Met a jour les filieres en fonction des annees academiques
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
        # Liste de toutes les filieres
        # !!!!! - 4 Juillet 2012
        # @filieres = Filiere.find(:all)  -- Si on veut le choix de l'année académique avec ajax, les prendre tous
        # Sinon:    # Liste de toutes les filières
    #@filieres = Filiere.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @filieres = Filiere.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')
    # Liste de toutes les filières concernant l'année souhaitée
    #@filieres_annee = Filiere.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @filieres_annee = Filiere.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')
    # Liste de tous les départements concernant l'année souhaitée
    #@departements_annee = Departement.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @departements_annee = Departement.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')
    # Est-ce que l'appel de la fonction vient de la vue d'index
    @vue_index = params['index']
  end

  # GET /filieres/1
  # GET /filieres/1.xml
  def show
    @filiere = Filiere.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @filiere }
    end
  end

  # GET /filieres/new
  # GET /filieres/new.xml
  def new
    @filiere = Filiere.new
    # Liste de tous les départements concernant l'année actuelle
    #@departements_annee = Departement.find_all_by_annee_academique_id(AnneeAcademique.courante, :order => 'abreviation')
    @departements_annee = Departement.where(annee_academique_id: AnneeAcademique.courante).order('abreviation ASC')
    # Liste de toutes les annnes académiques avec comme premier élément l'année actuelle
    @annee_academique = AnneeAcademique.order(:annee) 

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @filiere }
    end
  end

  # GET /filieres/1/edit
  def edit
    @filiere = Filiere.find(params[:id])
    # Liste de tous les départements concernant l'année actuelle
    #@departements_annee = Departement.find_all_by_annee_academique_id(AnneeAcademique.courante, :order => 'abreviation')
    @departements_annee = Departement.where(annee_academique_id: AnneeAcademique.courante).order('abreviation ASC')
    # Liste de toutes les annnes académiques avec comme premier élément l'année souhaitée
    @annee_academique = self.liste_annees_choix(@filiere.annee_academique_id)
  end

  # POST /filieres
  # POST /filieres.xml
  def create
    @filiere = Filiere.new(params[:filiere])

    respond_to do |format|
      if @filiere.save
        flash[:notice] = 'Filiere was successfully created.'
        format.html { redirect_to(@filiere) }
        format.xml  { render :xml => @filiere, :status => :created, :location => @filiere }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @filiere.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /filieres/1
  # PUT /filieres/1.xml
  def update
    @filiere = Filiere.find(params[:id])

    respond_to do |format|
      if @filiere.update_attributes(params[:filiere])
        flash[:notice] = 'Filiere was successfully updated.'
        format.html { redirect_to(@filiere) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @filiere.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /filieres/1
  # DELETE /filieres/1.xml
  def destroy
    @filiere = Filiere.find(params[:id])
    @filiere.destroy

    respond_to do |format|
      format.html { redirect_to(filieres_url) }
      format.xml  { head :ok }
    end
  end
end

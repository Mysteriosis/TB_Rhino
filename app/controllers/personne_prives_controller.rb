# -*- encoding : utf-8 -*-
class PersonnePrivesController < ApplicationController
  
    #load_and_authorize_resource :only => [:show, :edit, :update, :new, :create, :destroy] 
    


  def edit
    @personne_prive = PersonnePrive.find(params[:id])
    @personne = Personne.find(:first, :conditions => "id = '#{@personne_prive.personne_id}'")
    authorize! :edit, @personne_prive
  end
  
  def create
    @personne_prive = PersonnePrive.new(params[:personne_prive])
    
    if @personne_prive.save
      redirect_to :controller => "personnes", :action => "show", :id  => @personne_prive.personne_id
    end
    authorize! :create, @personne_prive
  end

  def update
    @personne_prive = PersonnePrive.find(params[:id])
    if @personne_prive.update_attributes(params[:personne_prive])
      InfoHeure.set_report_KO_des_annees_suivantes(@personne_prive.personne, Configuration.annee_civile_initiale)
      PlageContractuelle.update_heures_partiel(@personne_prive.personne)
      redirect_to :controller => "personnes", :action => "show", :id  => @personne_prive.personne_id
    end
    authorize! :update, @personne_prive
  end


end

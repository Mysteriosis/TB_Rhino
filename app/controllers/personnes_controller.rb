# -*- encoding : utf-8 -*-
class PersonnesController < ApplicationController
  
  load_and_authorize_resource :only => [:resume_heures,:old_resume_heures,:resume_heures_annee_precedente,:resume_conso_graph_data, :resume_conso, :index, :droits, :edit_roles, :update_roles, :show, :edit, :update, :new, :create, :destroy] 
  
  helper_method :sort_column, :sort_direction
  
  def index
    #Recherche Ransack
    @search = Personne.includes(:axe_strategique, :type_personne)
    @search = @search.search(params[:q] || Personne.default_search_options)
    
    #@personnes = @search.order(sort_column + " " + sort_direction)
    @search.sorts = (sort_column + " " + sort_direction) if @search.sorts.empty?
    @personnes = @search.result
    
    # Données pour les filtres de recherche
    @types_personne = TypePersonne.order('typePersonne')
    @axes_strategiques = AxeStrategique.order('nom')
    
    respond_with(@personnes)
  end
  
  def droits
    @personnes = Personne.search(Personne.default_search_options).order("nom, prenom")
    @types_personne = TypePersonne.order('typePersonne')
  end

  def edit_roles
    authorize! :edit_roles, @personne
  end
  
  def update_roles
    authorize! :update_roles, @personne
    @personne.roles = params[:personne][:roles].map(&:to_sym)
    if @personne.save
      flash[:notice] = "Droits mis à jour avec succès"
    else
      flash[:alert] = "Erreur lors de a mise à jour des droits"
    end
    redirect_to(@personne)
  end
  
  def edit_horaire
    # Personne sélectionnée
    @personne = Personne.find(params[:id])
    authorize! :edit_horaire, @personne
    if cannot? :read, @personne.contrats.build
      raise CanCan::AccessDenied
    end
  
    @contrats_en_cours = @personne.contrats.en_cours
    @taux_activite_actuel = @personne.contrats.taux_activite_actuel
  
  end
  
  def update_horaire
    # Personne sélectionnée
    @personne = Personne.find(params[:id])
    horaire ='xxxxxxxxxx'
    horaire[0] = params[:personne][:travaille_lundi_am]
    horaire[1] = params[:personne][:travaille_lundi_pm]
    horaire[2] = params[:personne][:travaille_mardi_am]
    horaire[3] = params[:personne][:travaille_mardi_pm]
    horaire[4] = params[:personne][:travaille_mercredi_am]
    horaire[5] = params[:personne][:travaille_mercredi_pm]
    horaire[6] = params[:personne][:travaille_jeudi_am]
    horaire[7] = params[:personne][:travaille_jeudi_pm] 
    horaire[8] = params[:personne][:travaille_vendredi_am]
    horaire[9] = params[:personne][:travaille_vendredi_pm]   
    @personne.horaire=horaire
    if @personne.save
      flash[:notice] = "Horaire hebdomadaire mis à jour"
      redirect_to(@personne) 
      return
    else
      flash.now[:alert] = "Erreur lors de la mise à jour de l'horaire hebdomaire"
      @contrats_en_cours = @personne.contrats.en_cours
      @taux_activite_actuel = @personne.contrats.taux_activite_actuel
      render :edit_horaire
    end

  end
  
  def resume_conso
    render :partial => "resume_conso", :locals => {:personne => @personne}
  end
  
  def resume_conso_graph_data
    res_proj=[]
    msec_aday=24*60*60*1000
    no_mois_actuel=Time.now.month # no du mois en cours 1..12
    if no_mois_actuel < 3 # Fixer l'intervalle des 3 mois à afficher
      no_mois_debut= 1
      no_mois_fin=3
    else
      no_mois_fin=no_mois_actuel
      no_mois_debut= no_mois_fin-2
    end
    nombre_de_jours=Time.days_in_month(no_mois_debut)+Time.days_in_month(no_mois_debut+1)+Time.days_in_month(no_mois_debut+2)

   
   
   heure_debut_periode=Time.new(Time.now.year, no_mois_debut, 1).at_beginning_of_month
   heure_fin_periode_precedente=heure_debut_periode.months_ago(1).at_end_of_month
   heure_debut_annee=Time.now.at_beginning_of_year
   decalage_annuel=(heure_fin_periode_precedente-heure_debut_annee)*1000 # en msec
   heure_debut_1970=Time.new(1970,1, 1).at_beginning_of_month
   decalage_depuis_1970=(heure_debut_annee-heure_debut_1970)*1000+decalage_annuel
   
   no_du_premier_jour=heure_debut_periode.yday

   @personne.affectation_projets.select{|item| item.saisie_heures_possible?}.each do |ap|
       plages_horaires= ap.plages_horaires_saisies_pendant_les_mois(no_mois_debut, no_mois_fin)
       res_ap = Array.new(nombre_de_jours){|i| [msec_aday*i+decalage_depuis_1970, 0]}   
       plages_horaires.each do |plage_horaire| 
         res_ap[plage_horaire.debut.yday-no_du_premier_jour][1] += plage_horaire.heures
       end
       
       res_proj << {"label" => ap.display_name, "data" => res_ap, "color" => ap.display_color}
   end
   
    respond_to do |format|
      format.js do
        render :text => res_proj.to_json
      end
    end
  end

  def old_resume_heures
    resume_heures_pour_annee_civile(AnneeCivile.courante)
  end
  
  def resume_heures_collaborateurs_sem1
    @personnes= Personne.collaborateurs_actifs_de_linstitut.order("nom")
    resume_heures_semestre :semestre1
  end
  
  def resume_heures_professeurs_sem1
    @personnes= Personne.professeurs_actifs_de_linstitut.order("nom")
    resume_heures_semestre :semestre1
  end  
  
  def resume_heures_collaborateurs_sem2
    @personnes= Personne.collaborateurs_actifs_de_linstitut.order("nom")
    resume_heures_semestre :semestre2
  end
  
  def resume_heures_professeurs_sem2
    @personnes= Personne.professeurs_actifs_de_linstitut.order("nom")
    resume_heures_semestre :semestre2
  end
  

  # GET /personnes/1
  def show
  # GET /personnes/1.xml

    # Personne sélectionnée
    @personne = Personne.find(params[:id])
    #Caractéristique privés
    @personne_prive = PersonnePrive.find(:first, :conditions => "personne_id = '#{@personne.id}'")
    
    @road_maps = RoadMap.find(:all, :conditions => {:type_personne => @personne.type_personne_id})
    
    if @personne.type_personne.typePersonne == "Collaborateur"
      #Date du jour
      today = Date.today
      # Road map concerné
      action = "gestion_competences"
      action_map = RoadMapAction.find(:first, :conditions => {:action => action})
      road_map = RoadMap.find(:first, :conditions => ["start_date <= ? AND end_date >= ? AND road_map_action_id = ?" ,today, today, action_map.id])
      if road_map == nil
        @active = false
      else
        @active = true
      end
    elsif @personne.type_personne.typePersonne == "Professeur"
      #Date du jour
      today = Date.today
      # Road map concerné
      action = "gestion_desiderata"
      action_map = RoadMapAction.find(:first, :conditions => {:action => action})
      road_map = RoadMap.find(:first, :conditions => ["start_date <= ? AND end_date >= ? AND road_map_action_id = ?" ,today, today, action_map.id])
      if road_map == nil
        @active = false
      else
        @active = true
      end
    else
      @active = false
    end
    
    # Affichage des affectations
    @affectations_hors_absences=@personne.affectations_hors_absences

    # Affichage des projets personneles si prof
    @projets_personnels=Projet.find(:all, :conditions => {:chef_id => @personne.id}).sort_by(&:display_name)
    
    #@total_heures_saisies = @personne.affectation_projets.joins(:plage_horaire_saisies).sum(:heures) 
    @total_heures_saisies= @personne.heures_effectuees_pour_lannee_courante
    #@charge_globale = @personne.affectation_projets.sum(:charge_horaire)
    @charge_globale=@personne.heures_a_effectuer_pour_lannee_courante
 
    contrats = @personne.contrats
    
    ### Liste de toutes les unités
    ##liste_unites = UniteEnseignement.find(:all)
    ### Liste de tous les domaines
    ##domaines = CompetenceDomaine.find(:all)
    ### Liste de tous les événements
    ##liste_evenements= EvenementPersonne.find(:all)
##
    ###--- TABLE COMPETENCE UNITE---
    ### Liste de toutes les compétences d'une personne
    ##competences_unites_associees = CompetencePersonneUnite.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    ### Initialisation de la liste des unités dont la personne est compétente
    ##@unites_associees_competence = []
    ##@interet_unite = []
    ### Initialisation de la variable de boucle
    ##i=0
    ### Liste de toutes les unités d'enseignement dont la personne est compétente
    ##for competence in competences_unites_associees
    ##  for unite in liste_unites
    ##    if (competence.unite_enseignement_id == unite.id)
    ##      @unites_associees_competence[i] = unite  
    ##      @interet_unite[i] = competence.interet_unite.interet
    ##    end
    ##  end
    ##  i=i+1
    ##end
    ##
    ###--- TABLE COMPETENCE DOMAINE---
    ### Liste de toutes les compétences d'une personne
    ##@competences_domaines_associes= CompetencePersonneDomaine.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    ### Initialisation de la liste des domaines dont la personne est compétente
    ##@domaines_competents = []
    ### Initialisation de la variable de boucle
    ##i=0
    ### Liste de tous les domaines dont la personne est compétente
    ##for competence in @competences_domaines_associes
    ##  for domaine in domaines
    ##    if (competence.competence_domaine_id == domaine.id)
    ##      @domaines_competents[i] = domaine  
    ##    end
    ##  end
    ##  i=i+1
    ##end
    ##   
    ### Liste de toutes les unités d'enseignement
    ##annee_academique = AnneeAcademique.courante
    ##@liste_unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    ##@desideratas =Desiderata.find(:all, :conditions=>{:personne_id=>@personne.id})
    ##@desideratas_annee = Desiderata.desideratas_annee(@personne, annee_academique)
    ##@liens_assistants_desideratas = PersonneDesiderata.find(:all)
    ##
    ### Liste de tous les assistants
    ##@assistants = Personne.find(:all, :order=>"initiale", :conditions=>["type_personne_id = 1"])
    ##
    ### Calcul le nombre d'heures total pour chaque desiderata
    ##i=0
    ##@nombre_heures = []
    ##@total_periodes=[]
    ##@charge_examen=[]
    ##@charge_examen_heures=[]
    ##
    ##@nombre_heures_total_desiderata = 0.0
    ##@charge_totale_examen_desiderata = 0.0
    ##@charge_totale_examen_desiderata_heures = 0.0
    ##for desideratum in @desideratas_annee 
    ##  #Calcul la charge du desideratum et somme le tout 
    ##  @total_periodes[i]= Desiderata.somme_periodes_desideratum(desideratum)
    ##  @nombre_heures[i] = Desiderata.somme_heures_desideratum(desideratum)
    ##  @charge_examen[i] = Desiderata.charge_examen_desideratum(desideratum)
    ##  @charge_examen_heures[i] = Desiderata.charge_examen_desideratum_heures(desideratum)
    ##  @charge_totale_examen_desiderata += @charge_examen[i]
    ##  @charge_totale_examen_desiderata_heures += @charge_examen_heures[i]
    ##  @nombre_heures_total_desiderata += @nombre_heures[i]
    ##  i+=1
    ##end
    ##@assignements_associees= Charge.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    ##
    ### Somme le nombre d'heures total de chaque projet
    ##@nombre_heures_total = somme_heure_projet(@assignements_associees) + @nombre_heures_total_desiderata
    ##
    ###--- TABLE DESIR ---
    ### Liste de tous les désirs d'une personne
    ##desirs_associees= DesirPersonneUnite.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    ### Initialisation de la liste des unités désirées par la personne
    ##@unites_associees_desir = []
    ### Initialisation de la variable de boucle
    ##i=0
    ### Liste de toutes les unités d'enseignement désirées par la personne
    ##for desir in desirs_associees
    ##  for unite in liste_unites
    ##    if (desir.unite_enseignement_id == unite.id)
    ##      @unites_associees_desir[i] = unite  
    ##    end
    ##  end
    ##  i+=+1
    ##end
    ##
    ##
    ###--- TABLE CHARGE ---
    ### Liste de tous les assignement d'une personne
    ##@assignements_associees= Charge.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
##
    ##
    ###--- TABLE EVENEMENT ---
    ### Liste de toutes les dates d'événement de la personne
    ##@date_associees= DateEvenementPersonne.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    ### Initialisation de la liste des événements associés à la personne
    ##@evenements_associees = []
    ##@date_ev = []
    ### Initialisation de la variable de boucle
    ##i=0
    ### Liste de tous les événements de la personne
    ##for date in @date_associees
    ##  for evenement in liste_evenements
    ##    if (date.evenement_personne_id == evenement.id)
    ##      @evenements_associees[i] = evenement
    ##      @date_ev[i] = date
    ##    end
    ##  end
    ##  i=i+1
    ##end
    ##
    ##@annee_id = AnneeCivile.find_by_sql ["SELECT id FROM annee_civiles WHERE annee = ?", Time.now.year ]
##
    ##@nbr_cahier = CahierChargeAssistant.count_by_sql ["SELECT count(id) FROM cahier_charge_assistants WHERE personne_id = ? AND annee_civile_id = ?", @personne.id, @annee_id]
##
    ##@cahier_id = CahierChargeAssistant.find_by_sql ["SELECT id FROM cahier_charge_assistants WHERE personne_id = ? AND annee_civile_id = ?", @personne.id, @annee_id]
##
    ##@cahier = CahierChargeAssistant.find(:first, :conditions => {:personne_id => @personne.id, :annee_civile_id => @annee_id})
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @personne }
    end
  end

  # GET /personnes/new
  # GET /personnes/new.xml
  def new
    @personne = Personne.new
    @personne.est_fictive=false
    @types_personne = TypePersonne.order(:id)
    @axes_strategiques = AxeStrategique.order(:nom)
    
    respond_with(@personne)
  end
  
  def new_personne_fictive
    @personne = Personne.new
    @personne.est_fictive=true
    @types_personne = TypePersonne.order(:id)

    render :new
  end

  # GET /personnes/1/edit
  def edit
    @personne = Personne.find(params[:id])
    # Liste de tous les type de personne
    @types_personne = TypePersonne.find(:all, :order => 'id')
    #Liste des axes
    @axes = AxeStrategique.find(:all, :order => 'nom')
    @axes.insert(0, AxeStrategique.new(:id => nil, :nom => "---"))
    #Informations privées
    @personne_prive = PersonnePrive.find(:first, :conditions => "personne_id = '#{@personne.id}'")
  end

  # POST /personnes
  # POST /personnes.xml
  def create
    @personne = Personne.new(params[:personne])
    if @personne.username
      @personne.username=@personne.username.strip
    end
    
    @personne.solde_heures_sup_2012=0
    
    if @personne.est_fictive
       @personne.actif=true
       @personne.username=@personne.initiale
    end
    
    if @personne.save
      if !@personne.est_fictive
        @personnePrive = PersonnePrive.create(:personne_id => @personne.id, :mode_saisie_simple => 0)
        flash[:notice] = "Nouvelle personne ajoutée avec succès"
        redirect_to [:edit, @personnePrive]
        return
      else
        redirect_to :controller => "personnes", :action => "show", :id  => @personne.id
        return
      end
    else
      error_text=""
      @personne.errors.each do |erreur|
        error_text += erreur.to_s + " | "
      end
      flash.now[:alert] = "Erreur lors de la création avec: " + error_text
    end
    
    respond_with(@personne)
  end

  # PUT /personnes/1
  # PUT /personnes/1.xml
  def update
    @personne = Personne.find(params[:id])  
    mode_activite_original = @personne.actif
    mode_institut_original = @personne.institut
    @personne.attributes = params[:personne]
    if @personne.username
      @personne.username=@personne.username.strip
    end
    #if mode_activite_original != @personne.actif || mode_institut_original != @personne.institut
    #  PlageContractuelle.reset_plages_contractuelles_pour_toutes_les_annees(@personne)
    #  PlageContractuelle.update_heures_partiel(@personne)
    #end
    
    ## only and admin user can set the roles
    #if current_user.has_role?(:admin)
    #  @personne.roles = params[:personne][:roles]
    #end

    if @personne.save
      flash[:notice] = "Profil mis à jour avec succès"
    else
      flash.now[:error] = "Erreur lors de a mise à jour des informations, vérifiez les champs et réessayez"
    end
    redirect_to(@personne) 
    return
  end

  # DELETE /personnes/1
  # DELETE /personnes/1.xml
  def destroy
    @personne = Personne.find(params[:id])
    @personne_prive = PersonnePrive.find(:first, :conditions => ["personne_id = :id",{:id => @personne.id}])
    
    deletable = true
    error = ""
    

    #cahier_charges = @personne.cahier_charge_assistants
    #if cahier_charges.length!=0
    #  deletable = false
    #  error += " - cahier des charges"
    #end
    contrats = @personne.contrats
    if contrats.length!=0
      deletable = false
      error += "contrat(s)"
    end
    engagements = @personne.engagements
    if engagements.length!=0
      deletable = false
      error += "engagement(s)"
    end
    charges_enseignement = @personne.charge_enseignements
    if charges_enseignement.length!=0
      deletable = false
      error += "charge(s) d'enseignement"
    end
    charges_assistanat = @personne.charge_assistanats
    if charges_assistanat.length!=0
      deletable = false
      error += "charge(s) d'assistanat"
    end
    affectations = @personne.affectation_projets
    if affectations.length!=0
      deletable = false
      error += " - affectation(s) de projet(s)"
    end

    if !deletable 
      flash[:alert] = "Impossible de supprimer cette personne, elle a encore des éléments associés : " + error
    else
      flash[:notice] = "Suppression réalisée avec succès!"
      cahier_charges = @personne.cahier_charge_assistants
      if cahier_charges.length!=0
        cahier_charges.each do |cch|
          cch.destroy
        end
      end

      # Détruire les plage_mensuelles correspondantes
      @personne.plage_contractuelles.each do |plage|
        plage.plage_mensuelles.destroy_all
      end

      # Puis les plages contractuelles
      @personne.plage_contractuelles.destroy_all
  
      # Puis les info_heures
      @personne.info_heures.destroy_all

      @personne.destroy
      if @personne_prive
        @personne_prive.destroy
      end
    end

    respond_to do |format|
      format.html { redirect_to(personnes_url) }
      format.xml  { head :ok }
    end
  end
  
  #def filtres_mis_a_jour
  #  if params['is_actif'] == 'true'
  #    is_actif = '1'
  #  else
  #    is_actif = '0'
  #  end
  #  
  #  if params['is_institut'] == 'true'
  #    is_institut = '1'
  #  else
  #    is_institut = '0'
  #  end
  #  
  #  if (params['id_axe'] == '' and params['id_type_personne'] == '')
  #    @personnes = Personne.find(:all, :conditions => {:institut => is_institut, :actif => is_actif}, :order => "prenom, nom")
  #  elsif (params['id_axe'] == '' and params['id_type_personne'] != '')
  #    @personnes = Personne.find(:all, :conditions => {:type_personne_id => params['id_type_personne'], :institut => is_institut, :actif => is_actif}, :order => "prenom, nom")
  #  elsif (params['id_axe'] != '' and params['id_type_personne'] == '')
  #    @personnes = Personne.find(:all, :conditions => {:axe_strategique_id => params['id_axe'], :institut => is_institut, :actif => is_actif}, :order => "prenom, nom")
  #  elsif (params['id_axe'] != '' and params['id_type_personne'] != '')
  #    @personnes = Personne.find(:all, :conditions => {:axe_strategique_id => params['id_axe'], :type_personne_id => params['id_type_personne'], :institut => is_institut, :actif => is_actif}, :order => "prenom, nom")
  #  end
  #
  #end
  
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #//////////////////////////////////////////////GESTION COMPETENCES////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: gestion_competences
  # But: Permet d'appeler la page permettant d'ajouter des competences sur les unités d'enseignement pour
  #      une personne
  #--------------------------------------------------------------------------------------------------------
  def gestion_competences
    
      #Date du jour
      today = Date.today
      # Road map concerné
      action = "gestion_competences"
      action_map = RoadMapAction.find(:first, :conditions => {:action => action})
      road_map = RoadMap.find(:first, :conditions => ["start_date <= ? AND end_date >= ? AND road_map_action_id = ?" ,today, today, action_map.id])
      if road_map == nil
        @active = false
      else
        @active = true
      end
    
    if @active
   
      # Nouvelle compétence
      @competence = CompetencePersonneDomaine.new 
      # Personne sélectionnée
      @personne = Personne.find(params[:id]) 
      # Année actuelle
      @annee_academique = AnneeAcademique.courante
      # Liste de toutes les années académiques avec comme premier élément l'année actuelle
      @annees_academiques = AnneeAcademique.order(:annee)
      # Liste de tous les domaines
      @liste_domaines = CompetenceDomaine.find(:all, :order => :domaine)
      # Liste de tous les niveaux de compétence
      @niveaux_competence = NiveauCompetence.find(:all, :order => :id) 
      # Initialisation de la liste des unités dont la personne n'est pas compétente
      @unites_non_associees = []
      # Initialisation de la liste des unités dont la personne est compétente
      @unites_associees = []
  
      
      # Liste de tous les domaines
      liste_domaines = CompetenceDomaine.find(:all, :order => :domaine)
      #Tous les domaines
      @tous_domaines = liste_domaines
      # Liste de toutes les compétences d'une personne dans un domaine
      competences_domaines_associes = CompetencePersonneDomaine.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
      # Initialisation de la liste des domaines dont la personne n'est pas compétente
      @domaines_non_associes = []
      # Initialisation de la liste des domaines dont la personne est compétente
      @domaines_associes = []
  
      # Initialisation de la variable de boucle
      non_associe=0
      associe=0
      
      # La personne est compétente dans ce domaine
      est_associee = false
      # Liste de tous les domaines dont la personne est compétente
      for domaine in liste_domaines
        for competence in competences_domaines_associes
          if (domaine.id == competence.competence_domaine_id)
            est_associee = true
            @domaines_associes[associe] = domaine.domaine + " (" + competence.niveau_competence.niveau + ")"
            associe=associe+1
          end
        end
        # Si l'unité n'est pas encore associée on l'ajoute à la liste
        if (!est_associee)
          @domaines_non_associes[non_associe] = domaine.domaine
          non_associe=non_associe+1
        end
        # Réinitialisation
        est_associee = false
      end
    end
  end 
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: annee_mise_a_jour_competence
  # But: Cette action permet de mettre les compétences à jour en fonction de l'année
  #--------------------------------------------------------------------------------------------------------
  def annee_mise_a_jour_competence
    annee_academique=AnneeAcademique.courante
    
    # Liste de toutes les unités concernant l'année souhaitée
    unites_annee = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    
    # Liste de toutes les compétences d'unité
    competences_unites = CompetencePersonneUnite.find(:all)
    
    # Varibales d'affichage
    @unites_non_concernees_annee=[]
    @unites_concernees_annee=[]
    @unites_anciennement_non_concernees=[]
    @unites_anciennement_concernees=[]
    
    # Si l'on appel cette méthode pour l'initialisation
    if params['initialisation'] == "false"
     @unites_anciennement_non_concernees=params[:unites_non_concernes].split(',')
     @unites_anciennement_concernees=params[:unites_concernes].split(',')
    end

    
    # Itérateur
    concernee = 0
    non_concernee = 0
    total = 0
    
    @all_unite_annee=[]
    
    # Gestion des compétences en compétences concernées ou non
    for unite in unites_annee
      competence_actuelle = false
      # Trouve si l'unité est concernée ou non
      for competence in competences_unites
        if unite.id == competence.unite_enseignement_id && params['id_personne'].to_i == competence.personne_id
          competence_actuelle = true
          @unites_concernees_annee[concernee] = unite
          concernee+=1
        end
      end
      
      # Si l'unité n'est pas concernée
      if !competence_actuelle
        @unites_non_concernees_annee[non_concernee] = unite
        non_concernee+=1
      end
      @all_unite_annee[total] = unite
      total+=1
    end
    @personne_id = params['id_personne']

  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: deplacement_unite_competence
  # But: Cette action permet d'ajouter une unité de compétence à la personne
  #--------------------------------------------------------------------------------------------------------
  def deplacement_unite_competence
    annee_academique=AnneeAcademique.courante
    #---------------------------------------
    # Trouve l'id de l'unité à ajouter en fonction de l'année
    #---------------------------------------
    unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
      
    # Lien à ajouter avec insertion de la personne
    lien_competence_unite = CompetencePersonneUnite.new
    lien_competence_unite.personne_id = params[:personne]
    lien_competence_unite.unite_enseignement_id = params[:id]
    lien_competence_unite.interet_unite_id = 1
    
    # Enregistrement dans la base de données
    lien_competence_unite.save 
    
  end
   #--------------------------------------------------------------------------------------------------------
  # Nom: supprime_unite_competence
  # But: Cette action permet de supprimer la compétence déplacée
  #--------------------------------------------------------------------------------------------------------
  def supprime_unite_competence
    annee_academique=AnneeAcademique.courante
    # Liste de toutes les unités de l'année
    unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
   
    # Liste de toutes les compétences
    competence = CompetencePersonneUnite.find(:first, :conditions => {:personne_id => params[:personne], :unite_enseignement_id => params[:id]})
    competence.destroy
    @unite = UniteEnseignement.find(:first, :conditions => {:id => params[:id]})

  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: supprime_domaine_competence
  # But: Cette action permet de supprimer un domaine de compétence à la personne
  #--------------------------------------------------------------------------------------------------------
  def supprime_domaine_competence
    
  end
  
  def deplacement_domaine_competence
    
  end
  
  def modification_niveau_domaine
    personne_id = params[:personne]
    level_id = params['id_niveau_domaine']   
    domaine = CompetenceDomaine.find(:first, :conditions => {:domaine => params[:domaine]})
    
    cmp = CompetencePersonneDomaine.find(:first, :conditions => {:personne_id => personne_id, :competence_domaine_id => domaine.id})
    cmp.update_attribute(:niveau_competence_id, level_id)
  end
  
  def modification_interet_unite
    uniteInteret = CompetencePersonneUnite.find(:first, :conditions => {:personne_id => params[:personne_id], :unite_enseignement_id  => params[:unite]})
    uniteInteret.update_attribute(:interet_unite_id, params['id_interet_unite'])
  end

  #--------------------------------------------------------------------------------------------------------
  # Nom: nouveau_domaine_competence
  # But: Cette action permet de connaître le domaine qui a été ajouté dans les compétences de la personne   
  #--------------------------------------------------------------------------------------------------------
  def nouveau_domaine_competence
    
    # Tableau contenant tous les domaines dont la personne est compétente
    @domaines_acquis = params[:domaines].split(',')
    # Liste de toutes les anciennes compétences de la personne
    anciennes_competences_domaines = CompetencePersonneDomaine.find(:all, :conditions => ["personne_id = :id",{:id => params[:personne]}])
    # Liste de tous les domaines
    domaines = CompetenceDomaine.find(:all)
    # niveau de l'untié déplacée
    niveau = NiveauCompetence.find(:all, :conditions => ["id = :id",{:id => params[:niveau]}]).first
    
    
    #---------------------------------------
    # Donne la liste des domaines acquis
    #---------------------------------------
    liste_domaines_acquis = []
    i=0
    for domaine_acquis in @domaines_acquis
      liste_domaines_acquis[i]=domaine_acquis.split(' (').first
      i+=1
    end
    
    #---------------------------------------
    # Donne tous les domaines
    # dont la personne est compétente
    #---------------------------------------
    nouvelles_competences_domaines = []
    i=0
    # Pour tous les domaines
    for domaine in domaines
      # Pour tous les domaines acquis
      for domaine_acquis in liste_domaines_acquis
        if (domaine.domaine == domaine_acquis)
          nouvelles_competences_domaines[i] = domaine
          i=i+1
        end
      end
    end
    
    #---------------------------------------
    # Donne tous les anciens domaines
    # acquis
    #---------------------------------------
    ancien = 0
    @anciens_domaines_deplaces = []
    for ancienne_competence_domaine in anciennes_competences_domaines
      for domaine in domaines
        if domaine.id == ancienne_competence_domaine.competence_domaine_id
          @anciens_domaines_deplaces[ancien] = domaine.domaine + " (" + ancienne_competence_domaine.niveau_competence.niveau + ")"
          ancien += 1
        end
      end
    end    
   
    #---------------------------------------
    # Donne le domaine qui doit
    # être enregistrée
    #---------------------------------------
    # Pour toutes les nouvelles compétences de la personne
    for nouvelle_competence_domaine in nouvelles_competences_domaines
      domaine_deja_existant = false
      # Pour toutes les anciennes compétences de la personne
      for ancienne_competence_domaine in anciennes_competences_domaines
        # Si la compétence vient de la personne
        if (ancienne_competence_domaine.competence_domaine_id == nouvelle_competence_domaine.id)
          domaine_deja_existant = true
        end 
      end
      # On possède le nouveau domaine
      if (!domaine_deja_existant)
        @domaine_deplace = nouvelle_competence_domaine
        @domaine_deplace_niveau = nouvelle_competence_domaine.domaine + " (" + niveau.niveau + ")"
      end
    end
    
  end

  #--------------------------------------------------------------------------------------------------------
  # Nom: creation_element_competence_draggable
  # But: Permet d'ajouter une compétence sur une unité d'enseignement pour une personne
  #--------------------------------------------------------------------------------------------------------
  def creation_element_competence_draggable 
    
    # Liste de toutes les unités d'enseignement
    @unites = UniteEnseignement.find(:all, :order => :abreviation)
    # Liste de tous les domaines
    domaines = CompetenceDomaine.find(:all, :order => :domaine)
    @tous_domaines = domaines
    # Liste de toutes les compétences d'une personne dans un domaine
    competences_domaines_associes = CompetencePersonneDomaine.find(:all, :conditions => ["personne_id = :id",{:id => params[:personne]}])
    # Initialisation de la liste des domaines dont la personne n'est pas compétente
    @domaines_non_associes = []
    # Initialisation de la liste des domaines dont la personne est compétente
    @domaines_associes = []
    
    # La personne est compétente dans ce domaine
    est_associee = false
    associe = 0
    non_associe = 0
    # Liste de tous les domaines dont la personne est compétente
    for domaine in domaines
      for competence in competences_domaines_associes
        if (domaine.id == competence.competence_domaine_id)
          est_associee = true
          @domaines_associes[associe] = domaine.domaine + " (" + competence.niveau_competence.niveau + ")"
          associe=associe+1
        end
      end
      # Si l'unité n'est pas encore associée on l'ajoute à la liste
      if (!est_associee)
        @domaines_non_associes[non_associe] = domaine
        non_associe=non_associe+1
      end
      # Réinitialisation
      est_associee = false
    end
    
    render :action => "creation_element_competence_draggable" 
  end 
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: Ajoute_competence
  # But: Permet d'ajouter une compétence sur un domaine pour une personne
  #--------------------------------------------------------------------------------------------------------
  def ajoute_competence    

    # Nouvelle compétence
    @competence = CompetencePersonneDomaine.new(params[:competence_personne_domaine]) 
     
    # Association avec la personne
    @personne = Personne.find(params[:id]) 
    @competence.personne_id = params[:id]
    
    # Enregistrement dans la base de données
    @competence.save 

    redirect_to("personnes/personnes/gestion_competences/#{@personne.id}") 
     
  end 
  
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////DESIDERATA ASSISTANTS///////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  #---------------------------------------------------------
  # Ajout_desir : Permet d'appeler la page permettant
  # d'ajouter des désirs en vers une unité
  #---------------------------------------------------------
  def ajout_desir
    annee_academique=AnneeAcademique.courante
    

    # Nouveau désir
    @desir = DesirPersonneUnite.new 
    
    # Personne sélectionnée
    @personne = Personne.find(params[:id])
    
    # Liste de toutes les unités de l'année
   @liste_unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    
    # Liste de tous les désirs d'une personne
    @desirs_associees= DesirPersonneUnite.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    
    # Initialisation de la liste des unités dont la personne n'est pas de désir
    @unites_non_associees = []
    
    # Initialisation de la variable de boucle
    i=0
    # La personne a un désir sur cette unité
    est_associee = false
    # Liste de toutes les unités d'enseignement dont la personne n'a pas de désir
    for unite in @liste_unites
      for desir in @desirs_associees
        if (unite.id == desir.unite_enseignement_id)
          est_associee = true
        end
      end
      # Si l'unité n'est pas encore désirée on l'ajoute à la liste
      if (!est_associee)
        @unites_non_associees[i] = unite 
        i=i+1
      end
      # Réinitialisation
      est_associee = false
    end
  end
  #---------------------------------------------------------
  # Update_desir: Permet d'ajouter un désir dans une unité
  # d'enseignement pour une personne
  #---------------------------------------------------------
  def update_desir
    
    # Nouveau désir
    @desir = DesirPersonneUnite.new(params[:desir]) 
     
    # Association avec la personne
    @personne = Personne.find(params[:id]) 
    @desir.personne_id = params[:id]
    
    # Enregistrement dans la base de données
    @desir.save 
     
    # Pour rediriger vers le "show" du logiciel 
    redirect_to(@personne) 
  end 
  
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #//////////////////////////////////////////////GESTION ASSIGNEMENT////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  #---------------------------------------------------------
  # Ajout_assignement : Permet d'appeler la page permettant
  # d'ajouter une assignement sur un projet
  #---------------------------------------------------------
  def ajout_assignement
    
    # Nouvel assignement
    @assignement = Charge.new 
    # Personne sélectionnée
    @personne = Personne.find(params[:id])
    # Liste de toutes les projets
    @liste_projets = Projet.find(:all, :order => :nomOutlook) 
    # Liste de toutes les annees civiles
    @liste_annnes_civiles = AnneeCivile.find(:all, :order => :annee) 
    # Liste de tous les assignements d'une personne
    @assignements_associees= Charge.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    # Initialisation de la liste des projets dont la personne n'a pas d'assignement
    @projets_non_associees = []
    # Initialisation de la variable de boucle
    i=0
    # La personne est deja assignée pour ce projet
    est_associee = false
    # Liste de tous les projets dont la personne n'est pas assignée
    for projet in @liste_projets
      for assignement in @assignements_associees
        if (projet.id == assignement.projet_id)
          est_associee = true
        end
      end
      # Si l'unité n'est pas encore désirée on l'ajoute à la liste
      if (!est_associee)
        @projets_non_associees[i] = projet 
        i=i+1
      end
      # Réinitialisation
      est_associee = false
    end
  end
   #---------------------------------------------------------
  # Update_assignement: Permet d'ajouter un assignement 
  # entre une personne et un projet
  #---------------------------------------------------------
  def update_assignement
    
    # Nouveau désir
    @assignement = Charge.new(params[:assignement]) 
     
    # Association avec la personne
    @personne = Personne.find(params[:id]) 
    @assignement.personne_id = params[:id]
    
    # Enregistrement dans la base de données
    if @assignement.save 
        # Pour rediriger vers le "show" du logiciel 
        redirect_to(@personne) 
    else
        error = 'Erreur dans le formulaire<br/><br/>'
        @assignement.errors.each_full { |msg| error = error + msg + "<br/>" }
        flash[:top_error] = error
        redirect_to :controller => 'personnes', :action => "ajout_assignement", :id => @personne.id
    end


  end 
    
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #///////////////////////////////////////////////GESTION EVENEMENT/////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #---------------------------------------------------------
  # Ajout_evenement : Permet d'appeler la page permettant
  # d'ajouter des événements à une personne
  #---------------------------------------------------------
  def ajout_evenement
    
    # Nouvelle date d'événement
    @dateEvenement = DateEvenementPersonne.new 
    # Projet sélectionné
    @personne = Personne.find(params[:id])
    # Liste de tous les événements
    evenements = EvenementPersonne.find(:all, :order => :texte)
    # Liste de toutes les dates d'événement associée au projet
    dates_associees = DateEvenementPersonne.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    # Initialisation de la liste des événements pas encore utilisés
    @evenements_non_associes = []
    
    # Initialisation de la variable de boucle
    non_associe=0
    # L'événement est déjà utilisé pour ce projet
    est_associee = false
    # Liste de tous les événement inexistant pour ce projet
    for evenement in evenements
      for date in dates_associees
        if (evenement.id == date.evenement_personne_id)
          est_associee = true
        end
      end
      # Si l'événement n'est pas encore associé on l'ajoute à la liste
      if (!est_associee)
        @evenements_non_associes[non_associe] = evenement
        non_associe=non_associe+1
      end
      # Réinitialisation
      est_associee = false
    end
  end
  
  #---------------------------------------------------------
  # Update_evenement: Permet d'ajouter un événement à la
  # personne
  #---------------------------------------------------------
  def update_evenement
    
    # Nouvelle date d'événement
    @dateEvenement = DateEvenementPersonne.new(params[:dateEvenement]) 
    
    # Association avec le projet
    @personne= Personne.find(params[:id]) 
    @dateEvenement.personne_id = params[:id]
    
    
    # Enregistrement dans la base de données
    @dateEvenement.save 
     
    # Pour rediriger vers le "show" du projet 
    redirect_to(@personne) 
  end
  
  def mod_evenement
    @dateEvenement = DateEvenementPersonne.find(params[:id])
    @dateEvenement.update_attributes(params[:date_evenement_personne])
  
    redirect_to(@dateEvenement.personne) 
  end
  
  def del_evenement
    @dateEvenement = DateEvenementPersonne.find(params[:id])
    personne = @dateEvenement.personne
    @dateEvenement.destroy
    redirect_to(personne) 
  end
  
  def edit_evenement
    @dateEvenement = DateEvenementPersonne.find(params[:id])
    @personne = Personne.find(:first, :conditions => {:id => @dateEvenement.personne_id})
    
    # Projet sélectionné
    # Liste de tous les événements
    evenements = EvenementPersonne.find(:all, :order => :texte)
    # Liste de toutes les dates d'événement associée au projet
    dates_associees = DateEvenementPersonne.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
    # Initialisation de la liste des événements pas encore utilisés
    @evenements_non_associes = []
    
    # Initialisation de la variable de boucle
    non_associe=0
    # L'événement est déjà utilisé pour ce projet
    est_associee = false
    # Liste de tous les événement inexistant pour ce projet
    for evenement in evenements
      for date in dates_associees
        if (evenement.id == date.evenement_personne_id)
          if (date.id == @dateEvenement.id)
            est_associee = false
          else
            est_associee = true
          end
        end
      end
      # Si l'événement n'est pas encore associé on l'ajoute à la liste
      if (!est_associee)
        @evenements_non_associes[non_associe] = evenement
        non_associe=non_associe+1
      end
      # Réinitialisation
      est_associee = false
    end
  end
  
  
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////DESIDERATA PROFESSEUR///////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  #/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: gestion_desideratas
  # But: Permet d'appeler la page de gestion des désidératas
  #--------------------------------------------------------------------------------------------------------
  def gestion_desideratas

#Date du jour
      today = Date.today
      # Road map concerné
      action = "gestion_desiderata"
      action_map = RoadMapAction.find(:first, :conditions => {:action => action})
      road_map = RoadMap.find(:first, :conditions => ["start_date <= ? AND end_date >= ? AND road_map_action_id = ?" ,today, today, action_map.id])
      if road_map == nil
        @active = false
      else
        @active = true
      end
    
    
    if road_map != nil
      annee_academique=AnneeAcademique.courante
      # Nouveau desiderata
      @desiderata = Desiderata.new
      # Personne sélectionnée
      @personne = Personne.find(params[:id]) 
      
      # Liste de toutes les unités d'enseignement liées à l'année actuelle
      @liste_unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id) 
      
      # Liste des desiderata de l'année actuelle
      @desideratas =Desiderata.find(:all, :conditions=>{:personne_id=>@personne.id})
      @desideratas_annee = Desiderata.desideratas_annee(@personne,annee_academique)
      
      # Liste de tous les assistants
      @assistants = Personne.collaborateurs
      
      # Liste de toutes les orientations
      un_IL=Personne.new(:initiale=>"IL")
      un_TR=Personne.new(:initiale=>"TR")
      un_IE=Personne.new(:initiale=>"IE")
      @orientations = [un_IL, un_TR, un_IE]
      
      # Liste des liens entre un desiderata et un assistants
      @liens_assistants_desideratas = PersonneDesiderata.find(:all)
      # Liste de tous les assignement d'une personne
      @assignements_associees= Charge.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])
      # Liste de toutes les initiales des assistants
      @liste_assistants = []
      # Liste contenant le nombre de groupe possible
      @liste_nb_groupes = 0
      
      # Liste des années académiques avec comme premier élément l'année actuelle
      @annees_academiques = [annee_academique]
      
      # Initialisation de la liste de toutes les initiales des assistants
      i=0
      for assistant in @assistants
        @liste_assistants[i] = assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
        i=i+1
      end
  
      @unites = []
      i=0
      for unite in @liste_unites
        ignore = false
        for desiderata in @desideratas_annee
          if unite.id == desiderata.unite_enseignement_id
            ignore = true
          end
        end
        if !ignore
          @unites[i] = unite
          i+=1
        end
      end
      
      # Calcul le nombre d'heures total pour chaque desiderata
      i=0
      @nombre_heures = []
      @nombre_heures_total_desiderata = 0
      for desideratum in @desideratas_annee 
        #Calcul la charge du desideratum et somme le tout 
        @nombre_heures[i] = Desiderata.somme_heures_desideratum(desideratum)
        @nombre_heures_total_desiderata += @nombre_heures[i]
        i+=1
      end
      @assignements_associees= Charge.find(:all, :conditions => ["personne_id = :id",{:id => @personne.id}])

      # Somme le nombre d'heures total de chaque projet
      @nombre_heures_total = somme_heure_projet(@assignements_associees) + @nombre_heures_total_desiderata

    end
  end
 
  
 
  #--------------------------------------------------------------------------------------------------------
  # Nom: somme_heure_desiderata
  # But: Somme le nombre d'heures total de chaque desideratum
  # Parametre : desiderata : liste des desiderata a sommer
  #-------------------------------------------------------------------------------------------------------- 
  def somme_heure_desiderata (desiderata)
    # Somme le nombre d'heure total de chaque desideratum
    nombre_heures_total = 0
    for desideratum in desiderata
      #Calcul la charge du desideratum et somme le tout 
      nombre_heures_total += Desiderata.somme_heures_desideratum(desideratum)
    end
    return nombre_heures_total
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: somme_heure_projet
  # But: Somme le nombre d'heures total de chaque projet
  # Parametre : assignements : liste des assignements entre une personne et des projets
  #-------------------------------------------------------------------------------------------------------- 
  def somme_heure_projet (assignements)
    # Somme le nombre d'heure total de chaque assignement
    nombre_heures_total = 0
    for assignement in assignements
      nombre_heures_total += assignement.nbHeuresAnnuelles
    end
    return nombre_heures_total
  end
  
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: annee_mise_a_jour_desiderata
  # But: Met à jour toutes les infos de la page en fonction de l'année académique choisie
  # FONCTION A SUPPRIMER!!!!!!!!!!:  l'année n'est plus sélectionnable (annee académique actuelel)
  #-------------------------------------------------------------------------------------------------------- 
  def annee_mise_a_jour_desiderata
    personne = Personne.find_by_id(params['id_personne'])
    annee_academique=AnneeAcademique.find_by_id(params['id_annee_academique'])
    
    # Liste des desiderata de la personne
    @desideratas = Desiderata.find(:all, :conditions => ["personne_id = :id",{:id => personne.id}]) 
    
    # Liste de toutes les unités d'enseignement liées à l'année actuelle
    @unites_annee = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    
    # Liste des desiderata de l'année souhaitée
    @desideratas_annee = Desiderata.desideratas_annee(personne, annee_academique)
    
    # Liste des unités à effacer
    @unites_a_effacer = params['unites_desiderata'].split(',') + params['unites_non_desiderata'].split(',')
    
    # Liste de tous les assistants
    @assistants = Personne.find(:all, :order=>"initiale", :conditions=>["type_personne_id = 1"])
    
    # Liste de toutes les orientations
    un_IL=Personne.new(:initiale=>"IL")
    un_TR=Personne.new(:initiale=>"TR")
    un_IE=Personne.new(:initiale=>"IE")
    @orientations = [un_IL, un_TR, un_IE]
    
    # Nombre d'heures total de tous les desiderata sélectionné
    @nombre_heures_total_desiderata = Desiderata.somme_heures_desiderata(@desideratas_annee)
    
    # Liste de tous les assignement d'une personne
    @assignements_associees= Charge.find(:all, :conditions => ["personne_id = :id",{:id => params['id_personne']}])
    
    # Somme le nombre d'heures total de chaque projet
    @nombre_heures_total = somme_heure_projet(@assignements_associees) + @nombre_heures_total_desiderata
    
    @unites = []
    i=0
    for unite in @unites_annee
      ignore = false
      for desiderata in @desideratas_annee
        if unite.id == desiderata.unite_enseignement_id
          ignore = true
        end
      end
      if !ignore
        @unites[i] = unite
        i+=1
      end
    end
    
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: creation_element_draggable
  # But: Permet d'appeler la page qui rend les éléments draggable
  #--------------------------------------------------------------------------------------------------------
  def creation_element_draggable
    # Liste de toutes les orientations
    un_IL=Personne.new(:initiale=>"IL")
    un_TR=Personne.new(:initiale=>"TR")
    un_IE=Personne.new(:initiale=>"IE")
    @orientations = [un_IL, un_TR, un_IE]
    
    personne=Personne.find(params['id_personne'])
    
    annee_academique=AnneeAcademique.courante
    # Liste de toutes les unités d'enseignement liées à l'année actuelle
    @liste_unites=UniteEnseignement.unites_annee_souhaitee(annee_academique.id) 

    # Liste de tous les desiderata liés à l'année actuelle
    @desideratas = Desiderata.find(:all, :conditions => {:personne_id=>personne.id}) 
    @desideratas_annee = Desiderata.desideratas_annee(personne, annee_academique)
    
    @unites = []
    i=0
    for unite in @liste_unites
      ignore = false
      for desiderata in @desideratas_annee
        if unite.id == desiderata.unite_enseignement_id
          ignore = true
        end
      end
      if !ignore
        @unites[i] = unite
        i+=1
      end
    end
    
  end
  
 
  #--------------------------------------------------------------------------------------------------------
  # Nom: enregistrer_desiderata
  # But: Permet d'ajouter un désidérata à un professeur
  #--------------------------------------------------------------------------------------------------------
  def enregistrer_desiderata
    tauxTheorique = params[:tauxTheorique].to_i
    tauxPratique = params[:tauxPratique].to_i
    
    if tauxTheorique > 100 or tauxPratique > 100 or tauxTheorique < 0 or tauxPratique < 0
      redirect_to(:controller => "Personnes", :action => "gestion_desideratas", :id => @personne.id) and return
    end
    
    # Liste de toutes les unités d'enseignement de cette année
    annee_academique=AnneeAcademique.courante
    unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    
    # Liste de toutes les personnes
    personnes = Personne.collaborateurs
    
    if params[:enregistrer] == "true"
      # Nouveau désidérata
      desiderata = Desiderata.create()
    else
      # Charge le desiderata
      desiderata = Desiderata.find(:all, :conditions=>["id = :id",{:id => params[:id_desiderata].to_i}])[0]
    end

    unite_retournes = params[:cours].split(',')

    # Pour retrouver l'unité choisie dans le desiderata
    for unite in unites
      for unite_retourne in unite_retournes
        if unite.abreviation == unite_retourne
         unite_choisie = unite
        end
      end
    end
    
    # Informations sur le desiderata
    desiderata.tauxTheorique = params[:tauxTheorique]
    desiderata.tauxPratique = params[:tauxPratique]
    desiderata.nbGroupes = params[:nbGroupes]
    desiderata.personne_id = params[:personne]
    desiderata.unite_enseignement_id = unite_choisie.id
    
    # Enregistrement du desiderata
    desiderata.save
    
    
    # Pour retrouver et enregistrer tous les assistants souhaités
    assistants_retournes = params[:assistants].split(',')
    assistants_initiales_decomposition = []
    assistants_initiales = []
    
    # Retourne toutes les initiales des assistants désirés
    i=0
    for assistant in assistants_retournes
      if assistant != ""
        assistants_initiales_decomposition[i] = assistant.split(' (')
        assistants_initiales_decomposition[i] = assistants_initiales_decomposition[i][1].split(')')
        assistants_initiales[i] = assistants_initiales_decomposition[i][0]
        i=i+1
      end
    end
    
    # Si l'on modifie un enregistrement
    if params[:enregistrer] == "false"
      # Lien entre un desiderata et des assistants ou des orientations
      liens_assistant_desiderata = PersonneDesiderata.find(:all, :conditions => ["desiderata_id = ?",params[:id_desiderata].to_i])
      # Efface les liens entre un desiderata et des assistants ou des orientations
      for lien in liens_assistant_desiderata
        lien.destroy
      end
    end

    # Enregistre le desiderata de tous les assistants
    priorite = 1
    for assistant_initiale in assistants_initiales
        personne = Personne.get_by_initiale(assistant_initiale)
        if personne!=nil
          # Nouveau lien entre un assistant et un désidérata
          lien_desiderata_assistant = PersonneDesiderata.new()
          lien_desiderata_assistant.personne_id = personne.id
          lien_desiderata_assistant.desiderata_id = desiderata.id
          lien_desiderata_assistant.priorite = priorite
          lien_desiderata_assistant.save
          priorite += 1
        end
    end

#      # Enregistre le desiderata de toutes les orientations
#      for orientation in orientations
#        if(personne.initiale == orientation)
#          # Nouveau lien entre un assistant et un désidérata
#          lien_desiderata_assistant = PersonneDesiderata.new()
#          lien_desiderata_assistant.personne_id = personne.id
#          lien_desiderata_assistant.desiderata_id = desiderata.id
#          lien_desiderata_assistant.priorite = priorite
#          lien_desiderata_assistant.save
#          priorite += 1
#        end
#      end

    
  
  end
  #--------------------------------------------------------------------------------------------------------
  # Nom: deplacement_unite_desiderata_draggable
  # But: Met à jour la liste des assistants
  #--------------------------------------------------------------------------------------------------------
  def deplacement_unite_desiderata_draggable
    annee_academique=AnneeAcademique.courante
    professeur=Personne.find_by_id(params[:personne_id])
    
    # Liste des unités associées à l'année académique
    @liste_unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id) 
    
    # Liste de tous les assistants
    @assistants = Personne.collaborateurs
    
    # Liste de tous les professeur
    @professeurs = Personne.professeurs
    
    # Liste de toutes les orientations
    un_IL=Personne.new(:initiale=>"IL")
    un_TR=Personne.new(:initiale=>"TR")
    un_IE=Personne.new(:initiale=>"IE")
    @orientations = [un_IL, un_TR, un_IE]
    

    # Liste de toutes les compétences
    competences = CompetencePersonneUnite.find(:all)
    
    # Liste de tous les desideratas de l'annee
    desideratas = Desiderata.desideratas(annee_academique)
    
    # Liste des ids des assistants compétents
    assistants_ids = []
    
    # Liste des assistants compétents
    @assistants_competents = []
    
    # Liste des assistants non compétents
    @assistants_non_competents = []
    
    # Liste des unités non sélectionnées
    @unites_non_selectionnees = []
    
    # Liste des desiderata déjà effectués pour cette unité par l'ensemble des professeurs
    @desideratas_unite = []
    
    # Liste des désidératas du professeur concerné
    @desideratas = Desiderata.desideratas_annee(professeur, annee_academique)
    
    # Liste des unités d'enseignement de l'année qui ne font pas partie des desideratas du prof
    @unites = []
    i=0
    for unite in @liste_unites
      ignore = false
      for desiderata in @desideratas
        if unite.id == desiderata.unite_enseignement_id
          ignore = true
        end
      end
      if !ignore
        @unites[i] = unite
        i+=1
      end
    end
    
    # Mise à jour des unités sélectionnée/non sélectionnées
    i=0
    for unite in @liste_unites
        if (unite.abreviation == params[:id])
          @unite_selectionnee = unite
        else
          @unites_non_selectionnees[i]=unite
          i+=1
        end
      #end
    end
    
    # Définit si l'unité possède un pdf
    if @unite_selectionnee.pdf != nil && @unite_selectionnee.pdf != ""
      @pfd = true
    else
      @pfd = false
    end
   
    # Trouve tous les desideratas de l'ensemble des professeurs qui concernant la même unité d'enseignement 
    i=0
    for desiderata in desideratas
      if desiderata.unite_enseignement_id == @unite_selectionnee.id
        @desideratas_unite[i] = desiderata
        i+=1
      end
    end
    
    # Calcul si le desiderata doit afficher un avertissement. C'est-à-dire qu'on vérifie
    # si le total de tous les taux dépasse le maximum compte tenu du nombre de groupes
    total_deja_desire = 0
    # Calcul du poucentage désiré
    for desiderata in @desideratas_unite
      total_deja_desire += (desiderata.tauxTheorique + desiderata.tauxPratique) * desiderata.nbGroupes
    end
    # Calcul du pourcentage possible
    total_possible = 0
    if @unite_selectionnee.nbPeriodesTheoriquesSemaine != 0
      total_possible = 100
    end
    if @unite_selectionnee.nbPeriodesPratiquesSemaine != 0
      total_possible += 100
    end
    if @unite_selectionnee.nbGroupes == "" || @unite_selectionnee.nbGroupes == nil
      @unite_selectionnee.nbGroupes = 0
    end
    total_possible *= @unite_selectionnee.nbGroupes
    # Si le total desiré est plus grand que le total possible cela mérite un avertissement
    if total_deja_desire > total_possible.to_f
      @avertissement = true
    # Si le total desiré est plus petit ou égal au total possible cela mérite pas d'avertissement
    else
      @avertissement = false
    end
    
    
    @competences_unite_selectionnee = CompetencePersonneUnite.find(:all, :order=>"interet_unite_id DESC",:conditions=>{:unite_enseignement_id => @unite_selectionnee.id})
    competent = 0
    for comp_unite_selectionnee in @competences_unite_selectionnee
        assistant=Personne.find_by_id(comp_unite_selectionnee.personne_id)
        @assistants_competents[competent] = comp_unite_selectionnee.interet_unite.interet + " " + assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
        competent += 1
    end 
    
    estCompetent = false
    noncompetent = 0
    for assistant in @assistants
      for comp_unite_selectionnee in @competences_unite_selectionnee
        if assistant.id == comp_unite_selectionnee.personne_id
          estCompetent = true
        end
      end
      if (!estCompetent)
        @assistants_non_competents[noncompetent]=assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
        noncompetent += 1
      end
      estCompetent = false
    end
  end
 
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: suppression_unite_desiderata_draggable
  # But: Rajoute toutes les unités d'enseignement
  #--------------------------------------------------------------------------------------------------------
  def suppression_unite_desiderata_draggable
    
    annee_academique=AnneeAcademique.courante
    
    # Liste de tous les assistants
    @assistants = Personne.collaborateurs
    
    # Liste de toutes les orientations
    un_IL=Personne.new(:initiale=>"IL")
    un_TR=Personne.new(:initiale=>"TR")
    un_IE=Personne.new(:initiale=>"IE")
    @orientations = [un_IL, un_TR, un_IE]
    
    # Liste de toutes les unités d'enseignement liées à l'année
    unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    
    # Liste de toutes les compétences
    competences = CompetencePersonneUnite.find(:all)
    
    # Liste des ids des assistants compétents
    assistants_ids = []
    
    # Liste des assistants
    @liste_assistants = []
    # Liste des assistants compétent
    @assistants_competents = []
    # Liste des assistants non compétent
    @assistants_non_competents = []
    # Liste des unités pas sélectionnées
    @unites_non_selectionnees = []
    # Liste des unités de cette année
    @unite_annee = []
    
    # Trouve toutes les unité qu'il faut effacer
    i=0
    j=0
    for unite in unites
      @unite_annee[j] = unite
      j += 1
      if (unite.abreviation.to_s == params[:id].to_s)
        @unite_selectionnee = unite
      else
        @unites_non_selectionnees[i]=unite
        i=i+1
      end
    end
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: charger_desiderata
  # But: Charge le désidérata dans les boîte de gestion
  #--------------------------------------------------------------------------------------------------------
  def charger_desiderata
    #Le désiderata en question
    @desiderata = Desiderata.find(:first, :conditions => {:id => params[:desiderata].to_i})
    personne=@desiderata.personne
    annee_academique=AnneeAcademique.courante
    
    #Tableau des unités n'appartenant pas au désiderata
    @unites_non_desiderata = []
    
    # Liste de toutes les unités d'enseignement liées à l'année
    unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)

    #Liste de tous les desideratas du professeur
    desideratas_professeur = Desiderata.desideratas_annee(personne, annee_academique)
    
    # Chargement des unités d'enseignement
    @unite_selectionnee = UniteEnseignement.find_by_id(@desiderata.unite_enseignement_id)
    @unites_desiderata = @unite_selectionnee.abreviation

    i=0
    for unite in unites
      if unite.id != @desiderata.unite_enseignement_id.to_i
        ignore = false
        for desideratum in desideratas_professeur
          if unite.id == desideratum.unite_enseignement_id
            ignore = true
          end
        end
        if !ignore
          @unites_non_desiderata[i] = unite.abreviation
          i+=1
        end
      end
    end
    
    # Définit si l'unité possède un pdf
    if @unite_selectionnee.pdf != nil && @unite_selectionnee.pdf != ""
      @pfd = true
    else
      @pfd = false
    end
    
    # Constante concernée par l'unité
    constante_concernee = Fonctions.facteur_actuel("Facteur_#{@unite_selectionnee.type_unite_enseignement.type_unite}",annee_academique.id)
    
    # Nombre de périodes en fonction des taux et du nombre de groupes indiqués
    if @unite_selectionnee.coursBloc
      @nombre_periodes_theoriques = (@unite_selectionnee.nbPeriodesTheoriquesTotal.to_f * @desiderata.tauxTheorique.to_f * @desiderata.nbGroupes.to_f)/100.0
      @charge_theorique = @nombre_periodes_theoriques * constante_concernee
      @nombre_periodes_pratiques = (@unite_selectionnee.nbPeriodesPratiquesTotal.to_f * @desiderata.tauxPratique.to_f * @desiderata.nbGroupes.to_f)/100.0
      @charge_pratique = @nombre_periodes_pratiques * constante_concernee
    else
      @nombre_periodes_theoriques = (@unite_selectionnee.nbPeriodesTheoriquesSemaine.to_f * @desiderata.tauxTheorique.to_f * @unite_selectionnee.periode.dureeSemaine.to_f * @desiderata.nbGroupes.to_f)/100.0
      @charge_theorique = @nombre_periodes_theoriques * constante_concernee
      @nombre_periodes_pratiques = (@unite_selectionnee.nbPeriodesPratiquesSemaine.to_f * @desiderata.tauxPratique.to_f * @unite_selectionnee.periode.dureeSemaine.to_f * @desiderata.nbGroupes.to_f)/100.0
      @charge_pratique = @nombre_periodes_pratiques * constante_concernee
    end
    
    #Lien entre tous les assistants et les desideratas
    tous_liens_assistant_desiderata = PersonneDesiderata.find(:all)
    
    # Recherche tous les liens entre un assistants et ce desiderata
    i=0
    liens_desiderata = []
    for lien in tous_liens_assistant_desiderata
       if (lien.desiderata_id == @desiderata.id)
         liens_desiderata[i] = lien
         i+=1
       end
    end
    
    #Liste des assistants
    assistants = Personne.collaborateurs
    
    #Liste des assistants du désideratas
    @assistants_desiderata = []
    
    # Chargement des assistants en lien avec ce desiderata
    assistant_souhaite=0
    for assistant in assistants
      for lien in liens_desiderata
        if (lien.personne_id == assistant.id)
          lvl_comp = CompetencePersonneUnite.find(:first,:conditions=>{:unite_enseignement_id => @unite_selectionnee.id, :personne_id => assistant.id})
          if lvl_comp == nil
            @assistants_desiderata[assistant_souhaite] = assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
          else
            @assistants_desiderata[assistant_souhaite] = lvl_comp.interet_unite.interet + " " +assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
          end
          assistant_souhaite=assistant_souhaite+1
        end 
      end
    end
    
    #Tableau des assistants compétents
    assistants_competents_ids = []
    
    # Trouve l'id des assistants compétents
    competences = CompetencePersonneUnite.find(:all)
    i = 0
    for competence in competences
      if (competence.unite_enseignement_id == @unite_selectionnee.id)
        assistants_competents_ids[i]=competence.personne_id
        i+=1
      end
    end
    
    # Trouve les assistants non compétents
    i=0
    assistants_non_competents = []
    for assistant in assistants
        est_competent = false
      for assistant_competent_ids in assistants_competents_ids
        if(assistant_competent_ids == assistant.id)
          est_competent = true
        end
      end
      # Si l'assistant n'est pas compétent
      if(!est_competent)
        assistants_non_competents[i] = assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
        i+=1
      end
    end
    
    # Trouve les assistants compétents
    i = 0
    assistants_competents = []
    for assistant in assistants
      for assistant_id in assistants_competents_ids
        if(assistant.id == assistant_id)
          lvl_comp = CompetencePersonneUnite.find(:first,:conditions=>{:unite_enseignement_id => @unite_selectionnee.id, :personne_id => assistant.id})
          assistants_competents[i]= lvl_comp.interet_unite.interet + " " +assistant.prenom + " " + assistant.nom + " (" + assistant.initiale + ")"
          i+=1
        end
      end
    end
    
    #Liste des orientations
    un_IL=Personne.new(:initiale=>"IL")
    un_TR=Personne.new(:initiale=>"TR")
    un_IE=Personne.new(:initiale=>"IE")
    @orientations = [un_IL, un_TR, un_IE]
        
    # Trouve les orientations  faisant partie du desiderata
    i = 0
    @orientations_desiderata = []
    for orientation in @orientations
      for lien in liens_desiderata
        if(orientation.id.to_i == lien.personne_id.to_i)
          @orientations_desiderata[i]= orientation.initiale
          i+=1
        end
      end
    end
    
    # Trouve les orientations qui ne font pas partie du désidérata
    i=0
    @orientations_non_desiderata = []
    for orientation in @orientations
      est_desiderata = false
      for orientation_desiderata in @orientations_desiderata
        if (orientation.initiale.to_s == orientation_desiderata.to_s)
          est_desiderata = true
        end
      end
      if (!est_desiderata)
        @orientations_non_desiderata[i] = orientation.initiale
        i+=1
      end
    end   
    
    # Trouve les assistants compétent mais ne faisant pas partie du désidérata
    @assistants_competents_non_desiderata = []
    i=0
    for assistant_competent in assistants_competents
      est_desiderata = false
      for assistant_desiderata in @assistants_desiderata
        if (assistant_competent == assistant_desiderata)
          est_desiderata = true
        end
      end
      if (!est_desiderata)
        @assistants_competents_non_desiderata[i] = assistant_competent
        i+=1
      end
    end
    
    # Trouve les assistants non compétents mais ne faisant pas partie du désidérata
    @assistants_non_competents_non_desiderata = []
    i=0
    for assistant_non_competent in assistants_non_competents
      est_desiderata = false
      for assistant_desiderata in @assistants_desiderata
        if (assistant_non_competent == assistant_desiderata)
          est_desiderata = true
        end
      end
      if (!est_desiderata)
        @assistants_non_competents_non_desiderata[i] = assistant_non_competent
        i+=1
      end
    end 
  end

  #--------------------------------------------------------------------------------------------------------
  # Nom: modification_nombre_heures
  # But: Modifie le nombre de périodes en fonction des taux et du nombre de groupes indiqués
  #--------------------------------------------------------------------------------------------------------
  def modification_nombre_heures
    annee_academique=AnneeAcademique.courante

    # Liste des unités d'enseignement 
    unites = UniteEnseignement.unites_annee_souhaitee(annee_academique.id)
    
    unite_retournees = params[:unite].split(',')
    if unite_retournees.length!=0
    
      for unite_retournee in unite_retournees
        if unite_retournee != ""
          abr_unite_retournee = unite_retournee
        end
      end
    
      # Recherche de l'unité concernée
      for unite in unites
        if unite.abreviation == abr_unite_retournee
          unite_concernee = unite
        end
      end
    
      # Constante concernée par l'unité
      constante_concernee = Fonctions.facteur_actuel("Facteur_#{unite_concernee.type_unite_enseignement.type_unite}",annee_academique.id)

      # Nombre de périodes en fonction des taux et du nombre de groupes indiqués
      if unite_concernee.coursBloc
        @nombre_periodes_theoriques = (unite_concernee.nbPeriodesTheoriquesTotal.to_f * params[:taux_theorique].to_f  * params[:nombre_Groupes].to_f)/100.0
        @charge_theorique = @nombre_periodes_theoriques * constante_concernee
        @nombre_periodes_pratiques = (unite_concernee.nbPeriodesPratiquesTotal.to_f * params[:taux_pratique].to_f  * params[:nombre_Groupes].to_f)/100.0
        @charge_pratique = @nombre_periodes_pratiques * constante_concernee
      else
        @nombre_periodes_theoriques = (unite_concernee.nbPeriodesTheoriquesSemaine.to_f * params[:taux_theorique].to_f * unite_concernee.periode.dureeSemaine.to_f * params[:nombre_Groupes].to_f)/100.0
        @charge_theorique = @nombre_periodes_theoriques * constante_concernee
        @nombre_periodes_pratiques = (unite_concernee.nbPeriodesPratiquesSemaine.to_f * params[:taux_pratique].to_f * unite_concernee.periode.dureeSemaine.to_f * params[:nombre_Groupes].to_f)/100.0
        @charge_pratique = @nombre_periodes_pratiques * constante_concernee
      end
    end
  
    
  end
 
  private
  
  
  def resume_heures_pour_annee_civile(annee_civile)
    @personne = Personne.find_by_id(params[:id])
    authorize! :resume_heures, @personne  
    
    # Spécial pour Chantal Gabriel - 4 Fev 2013
    @solde_heures_sup_2012=@personne.solde_heures_sup_2012
    
    # Si l'année civile est sélectionnée dans le menu
    if params[:annee_civile]
      if params[:annee_civile][:annee]
        @annee_civile=AnneeCivile.where{id.eq params[:annee_civile][:annee]}.first
        if !@annee_civile
          @annee_civile=AnneeCivile.courante
        end
      end
    else
      @annee_civile=annee_civile
    end
    
    # Liste des années civiles à choix pour le menu
    @annees_civiles=AnneeCivile.where{(annee.gteq '2010') & (annee.lteq AnneeCivile.courante.annee)}
    
    @mois = Moi.where{annee_civile_id.eq @annee_civile.id}.joins(annee_civile).order("annee_civiles.annee, num").all

    # Affectations associées à la personne (toutes, y compris hors date, projets archivés, etc..)
    affectations_projets=@personne.affectations_hors_absences
    affectations_absences=@personne.affectations_absences
    
    # Calcul des éléments à afficher

    @absences = Array.new
    @total_effectue=Hash.new
    @mois.each do |mois|
      @total_effectue[mois.mois]=0
      @total_effectue[:total]=0
    end
    
    # -- 1. Calcul des éléments relatifs aux projets
    @projets = Array.new
    no_projet=0
    affectations_projets.each do |ap|
      total_projet_dans_lannee=0
      projet=Hash.new
      @mois.each do |mois|
        heures = ap.heures_pour_mois_vrapide(mois)
        projet[mois.mois]=heures
        total_projet_dans_lannee += heures
      end
      
      if (total_projet_dans_lannee > 0) or (@annee_civile==AnneeCivile.courante && ap.en_activite?)
        # On garde le projet, il sera affiché
        @projets[no_projet] = projet
        @projets[no_projet][:nom] = ap.display_name
        @projets[no_projet][:total] = total_projet_dans_lannee
        @mois.each do |mois|
          heures_saisies_pendant_mois = projet[mois.mois]
          @total_effectue[mois.mois] += heures_saisies_pendant_mois
          @total_effectue[:total] += heures_saisies_pendant_mois
        end
        no_projet +=1
      end
    end

    
    # -- 2. Calcul des éléments relatifs aux projets "absences"
    @absences = Array.new
    no_projet=0
    affectations_absences.each do |ap|
      total_projet_dans_lannee=0
      projet=Hash.new
      @mois.each do |mois|
        heures = ap.heures_pour_mois_vrapide(mois)
        projet[mois.mois]=heures
        total_projet_dans_lannee += heures
      end
      
      if (total_projet_dans_lannee > 0) or (@annee_civile==AnneeCivile.courante && ap.en_activite?)
        # On garde le projet, il sera affiché
        @absences[no_projet] = projet
        @absences[no_projet][:nom] = ap.display_name
        @absences[no_projet][:total] = total_projet_dans_lannee
        @mois.each do |mois|
          heures_saisies_pendant_mois = projet[mois.mois]
          @total_effectue[mois.mois] += heures_saisies_pendant_mois
          @total_effectue[:total] += heures_saisies_pendant_mois
        end
        no_projet +=1
      end
    end
    
    # -- 3. Calcul des éléments relatifs au bilan
    # Heures à effectuer
    total_a_effectuer = 0
    total_heures_sup = 0    
    # cumul_heures_sup = 0   --- Modification pour Chantal Gabriel le 4 Fevrier 2013 - Tenir compte d'un cumul d'heures sup
    cumul_heures_sup = @solde_heures_sup_2012
    
    @a_effectuer=Hash.new
    @heures_sup=Hash.new
    @cumul_heures_sup=Hash.new
    
    @mois.each do |mois|
      heures_a_effectuer = @personne.heures_a_effectuer_pour_mois(mois)
      @a_effectuer[mois.mois] = heures_a_effectuer
      @heures_sup[mois.mois] = @total_effectue[mois.mois] - heures_a_effectuer
      cumul_heures_sup += @heures_sup[mois.mois]
      @cumul_heures_sup[mois.mois] = cumul_heures_sup
      total_a_effectuer += heures_a_effectuer
      total_heures_sup = total_heures_sup + @heures_sup[mois.mois]   
    end
    
    @a_effectuer[:total] = total_a_effectuer
    @heures_sup[:total] = total_heures_sup
    @cumul_heures_sup[:total] = cumul_heures_sup

  end
  
  def resume_heures_bilan_affectations(affectations)
    projets = Array.new
    no_projet=0
    affectations.each do |ap|
      total_projet_dans_lannee=0
      projet=Hash.new
      @mois.each do |mois|
        heures = ap.heures_pour_mois_vrapide(mois)
        projet[mois.mois]=heures
        total_projet_dans_lannee += heures
      end
      
      if (total_projet_dans_lannee > 0) or (@annee_civile==AnneeCivile.courante && ap.en_activite?)
        # On garde le projet, il sera affiché
        projets[no_projet] = Hash.new
        projets[no_projet][:nom] = ap.display_name
        projets[no_projet][:total] = PlageHoraireSaisie.heure_en_format_heures_minutes(total_projet_dans_lannee)
        @mois.each do |mois|
          heures_saisies_pendant_mois = projet[mois.mois]
          projets[no_projet][mois.mois] = PlageHoraireSaisie.heure_en_format_heures_minutes(heures_saisies_pendant_mois)
          total_projets_par_mois[mois.mois] += heures_saisies_pendant_mois
          total_global += heures_saisies_pendant_mois
        end
        no_projet +=1
      end
    end
    return projets
  end 
  
  def resume_heures_semestre (semestre)
    authorize! :resume_global_heures, Personne
    @annee_civile = AnneeCivile.courante
    if semestre== :semestre2
      @mois_a_afficher = @annee_civile.mois.where("num >= 7 and num <= 12").order("num ASC")
    else
      @mois_a_afficher = @annee_civile.mois.where("id").where("num <= 6").order("num ASC")
    end
    render :resume_global_heures
  end
  

  def sort_column
    params[:sort] || "personnes.nom"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
 
 
end

class ConfigurationsPlanificationsController < ApplicationController
  before_action :set_configurations_planification, only: [:show, :edit, :update, :destroy]

  # GET /configurations_planifications
  def index
    @configurations_planifications = ConfigurationsPlanification.all
  end

  # GET /configurations_planifications/1
  def show
  end

  # GET /configurations_planifications/new
  def new
    @configurations_planification = ConfigurationsPlanification.new
  end

  # GET /configurations_planifications/1/edit
  def edit
  end

  # POST /configurations_planifications
  def create
    @configurations_planification = ConfigurationsPlanification.new(configurations_planification_params)

    if @configurations_planification.save
      redirect_to @configurations_planification, notice: 'Configurations planification was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /configurations_planifications/1
  def update
    if @configurations_planification.update(configurations_planification_params)
      redirect_to @configurations_planification, notice: 'Configurations planification was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /configurations_planifications/1
  def destroy
    @configurations_planification.destroy
    redirect_to configurations_planifications_url, notice: 'Configurations planification was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_configurations_planification
      @configurations_planification = ConfigurationsPlanification.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def configurations_planification_params
      params.require(:configurations_planification).permit(:description, :valeur)
    end
end

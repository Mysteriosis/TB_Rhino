# -*- encoding : utf-8 -*-
class AnneeCivilesController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /annee_civiles
  # GET /annee_civiles.xml
  def index
    @annee_civiles = AnneeCivile.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @annee_civiles }
    end
  end

  # GET /annee_civiles/1
  # GET /annee_civiles/1.xml
  def show
    @annee_civile = AnneeCivile.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @annee_civile }
    end
  end

  # GET /annee_civiles/new
  # GET /annee_civiles/new.xml
  def new
    @annee_civile = AnneeCivile.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @annee_civile }
    end
  end

  # GET /annee_civiles/1/edit
  def edit
    @annee_civile = AnneeCivile.find(params[:id])
  end

  # POST /annee_civiles
  # POST /annee_civiles.xml
  def create
    @annee_civile = AnneeCivile.new(params[:annee_civile])

    respond_to do |format|
      if @annee_civile.save
        flash[:notice] = 'AnneeCivile was successfully created.'
        format.html { redirect_to(@annee_civile) }
        format.xml  { render :xml => @annee_civile, :status => :created, :location => @annee_civile }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @annee_civile.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /annee_civiles/1
  # PUT /annee_civiles/1.xml
  def update
    @annee_civile = AnneeCivile.find(params[:id])

    respond_to do |format|
      if @annee_civile.update_attributes(params[:annee_civile])
        flash[:notice] = 'AnneeCivile was successfully updated.'
        format.html { redirect_to(@annee_civile) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @annee_civile.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /annee_civiles/1
  # DELETE /annee_civiles/1.xml
  def destroy
    @annee_civile = AnneeCivile.find(params[:id])
    @annee_civile.destroy

    respond_to do |format|
      format.html { redirect_to(annee_civiles_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class PlageHoraireSaisiesController < ApplicationController
  
    # L'utilisateur a choisi de faire une saisie en mode mois (calendrier)
    def mois
    @personne = Personne.find(params[:personne_id])
    @month = (params[:month] || Time.zone.now.month).to_i
    @year = (params[:year] || Time.zone.now.year).to_i

    @shown_month = Date.civil(@year, @month)

    if current_user.roles.include?(:responsable_rh)
      @event_strips = @personne.plage_horaire_saisies.event_strips_for_month(@shown_month, 1) 
    else 
      @event_strips = @personne.plage_horaire_saisies.event_strips_for_month(@shown_month, 1, :include => :affectation_projet, :conditions => 'affectation_projets.saisie_heures_par_RH_uniquement=false or affectation_projets.saisie_heures_par_RH_uniquement IS NULL')
    end
    
    @heures_par_affectation = {"Total global pour le mois" => 0}
    @plages_du_mois = Hash.new
    @event_strips.flatten.each do |plage|
    
      if plage && plage.debut.month == @month
        @heures_par_affectation["Total global pour le mois"] += plage.heures
        if @heures_par_affectation.has_key? plage.affectation_projet.display_name
          @heures_par_affectation[plage.affectation_projet.display_name] += plage.heures
        else
          @heures_par_affectation[plage.affectation_projet.display_name] = plage.heures
        end
        if @plages_du_mois.has_key? plage.affectation_projet.display_name
          if plage.commentaire and plage.commentaire !=""
              @plages_du_mois[plage.affectation_projet.display_name] << plage
          end
        else
          if plage.commentaire and plage.commentaire !=""
            @plages_du_mois[plage.affectation_projet.display_name] = [plage]
          end
        end
      end
    
    end
    authorize! :consulter_heures_saisies, @personne
  end  
  
  # L'utilisateur a choisi de faire une saisie en mode semaine
  def semaine
    # la date à partir de laquelle on affiche les jours
    # par défaut : le lundi de la semaine en cours
    if params[:date_debut]
      @date_debut = Date.parse(params[:date_debut])
    else
      @date_debut = Date.today - Date.today.wday.days + 1.day
    end
    @date_fin = @date_debut + 6.days
    @personne = Personne.find(params[:personne_id])
    
    authorize! :consulter_heures_saisies, @personne

  end
  
  # retourne les jours entre deux dates (comprises) avec les heures saisies
  def jours
    @personne = Personne.find(params[:personne_id])
    authorize! :consulter_heures_saisies, @personne
    @date_debut = Date.parse(params[:date_debut])
    @date_fin = Date.parse(params[:date_fin])
    # on prends depuis la première minute du jour de début jusqu'à la dernière seconde
    @plage_horaire_saisies = @personne.plage_horaire_saisies.entre(@date_debut,@date_fin.to_time + 1.day - 1.second)
    render :partial => "jours"
  end
  
  # Création en mode semaine d'une nouvelle plage horaire après un drop - Plage horaire vide, qui sera spécifiée (update) par le 
  # biais du partial _plage_horaire_form
  def add
    @personne = Personne.find(params[:personne_id])
    moment = Date.parse(params[:date]).to_time
    @affectation_projet = AffectationProjet.find(params[:affectation_projet_id])
    authorize! :modifier_heures_saisies, @affectation_projet.personne
    @plage_horaire = PlageHoraireSaisie.new(:debut => moment, :fin => moment, :heures => 0.0)
    @plage_horaire.affectation_projet=@affectation_projet
    
    plage_gelee = Hash.new
    if !current_user.roles.include?(:responsable_rh)
      plage_gelee = @plage_horaire.saisie_dans_plage_gelee
    end
    if plage_gelee.count>0 
      error_text="Erreur :  #{plage_gelee.first.last}"
      @plage_horaire.errors.add(:debut, error_text)
    else
       # Enregistrement avec validations diverses
      @plage_horaire.save
      InfoHeure.set_report_KO_des_annees_suivantes(@personne, @plage_horaire.debut.to_date.year)
    end
    render :partial => "plage_horaire_form", :locals => {:plage => @plage_horaire, :mode => "mode_semaine"}
  end

   
  
   # Création en mode mois d'une nouvelle plage horaire une fois l'édition faite dans la fenêtre fugitive
   def add_mois
    avec_commentaire = Configuration.saisie_avec_commentaire?
    moment = Date.parse(params[:date]).to_time
    @affectation_projet = AffectationProjet.find(params[:affectation_projet_id])
    personne = @affectation_projet.personne
    begin
      if params[:heures]=="" 
        heures=0.0
      else
        heures = Float(params[:heures])
      end
      if params[:minutes]==""
        minutes=0.0
      else
        minutes = Float(params[:minutes])
      end

      if avec_commentaire
        commentaire = params[:commentaire]
      end

      heures = heures + minutes/60  + 0.0001
      #if heures % 0.25 != 0.0 
      #  raise "non"
      #end
      if !personne.mode_saisie_simple?
        # Saisie avec sliders - Contrôler un max de 24 heures
        maximum_plage = [Configuration.max_saisie_par_jour, 24.0].min
      else
        maximum_plage = Configuration.max_saisie_par_jour
      end
      if heures > maximum_plage
        raise "non plus"
      end
    rescue
      # render :text => "Choisissez un nombe d'heure valide (multiple de quarts d'heure entre 0 et #{maximum_plage})" and return
      render :text => "Choisissez un nombe d'heure valide (entre 0 et #{maximum_plage})" and return
    end
    authorize! :modifier_heures_saisies, @affectation_projet.personne
    
    #if Configuration.max_saisie_sup_24?
    if heures >= 24.0  # Le slider doit afficher pile 24 heures
      moment_debut = moment
      moment_fin = moment_debut + (23.984).hours
    else
      moment_debut = moment
      moment_fin = moment + (heures).hours
    end
    if avec_commentaire
      @plage_horaire = PlageHoraireSaisie.new(:debut => moment_debut, :fin => moment_fin, :heures => heures, :commentaire => commentaire)
    else
      @plage_horaire = PlageHoraireSaisie.new(:debut => moment_debut, :fin => moment_fin, :heures => heures)
    end

    @plage_horaire.affectation_projet=@affectation_projet

    controle_ok= valider_plage_horaire(@plage_horaire)
    if  controle_ok==""
      @plage_horaire.save
      InfoHeure.set_report_KO_des_annees_suivantes(personne, @plage_horaire.debut.to_date.year)
    end
    render :text => controle_ok
  end
  
  def cloner_semaine_precedente
  
    personne = Personne.find(params[:personne_id])
    authorize! :modifier_heures_saisies, personne
    premier_jour = Date.parse(params[:premier_jour])
    PlageHoraireSaisie.transaction do
      # on efface les plages saisie de la semaine courante
      personne.plage_horaire_saisies.entre(premier_jour.to_time, (premier_jour+6.days).to_time + 1.day - 1.second).map(&:destroy)
      # on clone les plages saisies de la semaine passée
      personne.plage_horaire_saisies.entre(premier_jour-7.days, premier_jour.to_time - 1.second).each do |plage_horaire|
        n = plage_horaire.clone
        n.debut += 7.days
        n.fin += 7.days
        n.save!
      end
    end
  
    render :nothing => true # le code HTTP de la réponse suffit
  end
  
  # GET /plage_horaire_saisies/1/edit
  def edit
    @plage_horaire_saisie = PlageHoraireSaisie.find(params[:id])
    @personne = Personne.find(params[:personne_id])
  end

  # PUT /plage_horaire_saisies/1
  # PUT /plage_horaire_saisies/1.xml
  def update
    avec_commentaire = Configuration.saisie_avec_commentaire?
    @plage_horaire_saisie = PlageHoraireSaisie.find(params[:id])
    personne=@plage_horaire_saisie.affectation_projet.personne
    authorize! :modifier_heures_saisies, personne
    
    # Récupération des valeurs saisies
    if (!params[:debut]) and (!params[:fin])
    # Si le mode s'est effectué en mode simple...
      begin
        if params[:heures]=="" 
          heures=0.0
        else
          heures = Float(params[:heures])
        end
        if params[:minutes]==""
          minutes=0.0
        else
          minutes = Float(params[:minutes])
        end
        heures = heures + minutes/60 + 0.0001
        
        #if heures % 0.25 != 0.0 
        #  raise "non"
        #end
        if heures > Configuration.max_saisie_par_jour
          raise "non plus"
        end
      rescue
        #render :text => "Choisissez un nombe d'heure valide (multiple de quarts d'heure entre 0 et #{Configuration.max_saisie_par_jour})" and return
        render :text => "Choisissez un nombe d'heure valide (entre 0 et #{Configuration.max_saisie_par_jour})" and return
      end
      
      if heures < 24.0  
          @plage_horaire_saisie.debut = @plage_horaire_saisie.debut.to_date
          @plage_horaire_saisie.fin = @plage_horaire_saisie.debut+(heures).hours
      else
          # Le slider doit afficher pile 24 heures
          @plage_horaire_saisie.debut = @plage_horaire_saisie.debut.to_date
          @plage_horaire_saisie.fin = @plage_horaire_saisie.debut+(23.984).hours
      end

      
      @plage_horaire_saisie.heures = heures 
      
          
    else
      @plage_horaire_saisie.debut = @plage_horaire_saisie.debut.to_date + params[:debut].to_i.minutes
      @plage_horaire_saisie.fin = @plage_horaire_saisie.fin.to_date + params[:fin].to_i.minutes
      @plage_horaire_saisie.calculer_heures  #Donner une valeur à l'attribut "heures" de la plage horaire: (fin-debut)/3600
    end
    
   if avec_commentaire
     @plage_horaire_saisie.commentaire = params[:commentaire]
   end
    
    # Contrôle de la validité de la saisie
    plage_gelee = Hash.new
    if !current_user.roles.include?(:responsable_rh)
        plage_gelee = @plage_horaire_saisie.saisie_dans_plage_gelee
    end
    if plage_gelee.count>0 
       render :text => "Erreur :  #{plage_gelee.first.last}"
    else
      # Enregistrement avec validations diverses
      @plage_horaire_saisie.save
      InfoHeure.set_report_KO_des_annees_suivantes(personne, @plage_horaire_saisie.debut.to_date.year)
      render :partial => "status", :locals => {:plage => @plage_horaire_saisie}
    end
  end

  # DELETE /plage_horaire_saisies/1
  # DELETE /plage_horaire_saisies/1.xml
  def destroy
    @plage_horaire_saisie = PlageHoraireSaisie.find(params[:id])
    personne= @plage_horaire_saisie.affectation_projet.personne
    authorize! :modifier_heures_saisies, personne
    # Contrôle de la validité de la saisie
    plage_gelee = Hash.new
    if !current_user.roles.include?(:responsable_rh)
        plage_gelee = @plage_horaire_saisie.saisie_dans_plage_gelee
    end
    if plage_gelee.count>0 
      # La suppression n'est pas opérée
    else
      # Suppression
      @plage_horaire_saisie.destroy
      InfoHeure.set_report_KO_des_annees_suivantes(personne, @plage_horaire_saisie.debut.to_date.year)
      render :text => "ok"
    end
  end
  
  def valider_plage_horaire(plage_horaire)
    # Contrôle divers ---------------------
    # Contrôle no1, sauf si RH: saisie en dehors des plages gelées 
    plage_gelee = Hash.new
    if !current_user.roles.include?(:responsable_rh)
        plage_gelee = plage_horaire.saisie_dans_plage_gelee
    end
  
    # Autres contrôle, aussi pour RH: Saisie dans les limites de l'affectation, max par jour, etc..
    res = ""
    if !plage_horaire.valid? || (plage_gelee.count>0)  
      if plage_gelee.count>0  # Des erreurs de plage gelée
          res = "Erreur : #{plage_gelee.first.last}"
      else
          res = "Erreur : #{plage_horaire.errors.first.last}"
      end
    end
    return res
  end
  
end

# -*- encoding : utf-8 -*-
class NiveauCompetencesController < ApplicationController
  
  load_and_authorize_resource
  #before_fileter :activate_authlogic
  # GET /niveau_competences
  # GET /niveau_competences.xml
  def index
    @niveau_competences = NiveauCompetence.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @niveau_competences }
    end
  end

  # GET /niveau_competences/1
  # GET /niveau_competences/1.xml
  def show
    @niveau_competence = NiveauCompetence.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @niveau_competence }
    end
  end

  # GET /niveau_competences/new
  # GET /niveau_competences/new.xml
  def new
    @niveau_competence = NiveauCompetence.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @niveau_competence }
    end
  end

  # GET /niveau_competences/1/edit
  def edit
    @niveau_competence = NiveauCompetence.find(params[:id])
  end

  # POST /niveau_competences
  # POST /niveau_competences.xml
  def create
    @niveau_competence = NiveauCompetence.new(params[:niveau_competence])

    respond_to do |format|
      if @niveau_competence.save
        flash[:notice] = 'NiveauCompetence was successfully created.'
        format.html { redirect_to(@niveau_competence) }
        format.xml  { render :xml => @niveau_competence, :status => :created, :location => @niveau_competence }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @niveau_competence.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /niveau_competences/1
  # PUT /niveau_competences/1.xml
  def update
    @niveau_competence = NiveauCompetence.find(params[:id])

    respond_to do |format|
      if @niveau_competence.update_attributes(params[:niveau_competence])
        flash[:notice] = 'NiveauCompetence was successfully updated.'
        format.html { redirect_to(@niveau_competence) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @niveau_competence.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /niveau_competences/1
  # DELETE /niveau_competences/1.xml
  def destroy
    @niveau_competence = NiveauCompetence.find(params[:id])
    @niveau_competence.destroy

    respond_to do |format|
      format.html { redirect_to(niveau_competences_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
require "application_responder"
# "application_responder" est un module placé dans le dossier "lib"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Mis en commentaire pour obtenir des erreurs lisibles sinon, "template manquant".
  # rescue_from Exception, :with => :internal_server_error

  protect_from_forgery
  
  layout proc { |controller| controller.request.xhr? ? false : 'application' } 
  
  #before_filter :authenticate_personne!
  def personne_signed_in?
    return true
  end
  
  def current_personne
    return Personne.where(:initiale => 'PAC').first #DBS
  end

  def current_user
    current_personne
    #User.current_user = @current_user  # Pour que   cattr_accessor : current_user fonctionne dans les modèles
  end
  
  helper_method :current_user
  
  def store_location
    session[:return_to] = request.request_uri
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def internal_server_error(exception)
    
    # search backtrace for the line in the app that caused the error
    guilty_line = exception.backtrace[0]
    
    exception.backtrace.each do |line|
      if line.include?("app")
        guilty_line = line
        break
      end
    end
    
    if request.xhr?
      render :text => "#{exception.message}\n\n#{guilty_line}}", :status => :internal_server_error
    else
      render "pages/rescue", :locals => {:exception => exception, :guilty_line => guilty_line}, :layout => nil
    end
    
  end

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
   # :secret => 'b0b30483238db3bf8a2df46407904855'
  
  #helper_method :current_user, :logged_in?
  #helper_method :current_personne_session, :current_user
  
  #before_filter { |c| Authorization.current_user = c.current_user }
 
  #before_filter :set_current_user

  #def permission_denied
  #  flash[:error] = "Désolé! Vous n'avez pas le droit d'accèder à ces pages"
  #  redirect_to (:back) rescue redirect_to(@current_user)
  #end
  #
  #def current_personne_session
  #  return @current_personne_session if defined?(@current_personne_session)
  #  @current_personne_session = PersonneSession.find
  #end
  #  
  #def current_user
  #  return @current_user if defined?(@current_user)
  #  @current_user = current_personne_session && current_personne_session.record
  #end
  
  #def require_user
  #  unless current_user
  #    store_location
  #    flash[:notice] = "Une authentification est requise pour voir cette page !"
  #    redirect_to sign_in_path
  #      return false
  #  end
  #end
  #
  #def require_no_user
  #    if current_user
  #      store_location
  #      flash[:notice] = "Une authentification est requise pour voir cette page !"
  #      redirect_to account_url
  #      return false
  #    end
  #  end

  #def set_current_user
  #  Authorization.current_user = current_user
  #end

  #unless Rails.application.config.consider_all_requests_local
  #  rescue_from Exception,                            :with => :render_error
  #  rescue_from ActiveRecord::RecordNotFound,         :with => :render_not_found
  #  rescue_from ActionController::RoutingError,       :with => :render_not_found
  #  rescue_from ActionController::UnknownController,  :with => :render_not_found
  #  rescue_from ActionController::UnknownAction,      :with => :render_not_found
  #end 

  #def render_not_found(exception)
  #  log_error(exception)
  #  redirect_to :controller => "Errors", :action => 'default'
  #end
  #def render_error(exception)
  #  log_error(exception)
  #  redirect_to :controller => "Errors", :action => 'default'
  #end
  
  ## See ActionController::Base for details 
  ## Uncomment this to filter the contents of submitted sensitive data parameters
  ## from your application log (in this case, all fields with names like "password"). 
  ## filter_parameter_logging :password
  #
  # Liste de toutes les années académiques
  $annees_academiques = AnneeAcademique.all
  # Liste de toutes les années civiles
  $annees_civiles = AnneeCivile.all
  #
  ##--------------------------------------------------------------------------------------------------------
  ## Nom: AnneeAcademique.courante
  ## But: retourne l'année académique actuelle
  ## NOTE REFACTORING: Supprimer cette méthode à terme: Existe une méthode equivalente annee_academique_actuelle 
  ## dans le modele AnneeAcademique 
  ##--------------------------------------------------------------------------------------------------------
  #def AnneeAcademique.courante
  #  AnneeAcademique.courante
  #end
  #
  #
  ##--------------------------------------------------------------------------------------------------------
  ## Nom: annee_civile_actuelle
  ## But: retourne l'année civile actuelle
  ## NOTE REFACTORING: Supprimer cette méthode à terme: Existe une méthode equivalente dans le modele AnneeAcademique 
  ##--------------------------------------------------------------------------------------------------------
  #def annee_civile_actuelle
  #  
  #  # Date d'aujourd'hui
  #  annee = Date.today.year
  #
  #  # Recherche de l'unité concernée
  #  for annee_civile in $annees_civiles
  #    if annee_civile.annee == "#{annee}"
  #      tmp = annee_civile
  #    end
  #  end
  #  return tmp
  #end
  #
  ##--------------------------------------------------------------------------------------------------------
  ## Nom: liste_annees_actuelle
  ## But: retourne la liste des années académiques avec comme premier élément l'année courante
  ## NOTE REFACTORING: Supprimer cette méthode à terme: Existe une méthode equivalente dans le modele AnneeAcademique 
  ##--------------------------------------------------------------------------------------------------------
  #def AnneeAcademique.order(:annee)
  #  return AnneeAcademique.liste_annees_academiques
  #end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: liste_annees_civiles_actuelles
  # But: retourne la liste des années civiles avec comme premier élément l'année courante
  # NOTE REFACTORING: Supprimer cette méthode à terme: Existe une méthode equivalente dans le modele AnneeAcademique 
  #--------------------------------------------------------------------------------------------------------
  def liste_annees_civiles_actuelles
  
    annee_ajd = annee_civile_actuelle
    # Ajoute l'année actuelle à la première position
    tab_annees_civiles = []
    tab_annees_civiles[0]=annee_ajd
    i=1
    for annee in $annees_civiles
      if annee.id != annee_ajd.id
        tab_annees_civiles[i]= annee
        i+=1
      end
    end
    
    return tab_annees_civiles
    
  end
  #
  #--------------------------------------------------------------------------------------------------------
  # Nom: liste_annees_choix
  # But: retourne la liste des années avec comme premier élément l'année souhaitée
  # NOTE REFACTORING: Supprimer cette méthode à terme: Existe une méthode equivalente dans le modele AnneeAcademique 
  #--------------------------------------------------------------------------------------------------------
  def liste_annees_choix(id_annee)
    
    annee_ajd = AnneeAcademique.courante
    # Ajoute l'année choisie en première position
    tab_annees_academiques = []
    i=1
    for annee in $annees_academiques
      if annee.id != id_annee.to_i
        tab_annees_academiques[i]= annee
        i+=1
      else
        tab_annees_academiques[0]= annee
      end
    end
    
    return tab_annees_academiques
    
  end
  ##--------------------------------------------------------------------------------------------------------
  ## Nom: triage
  ## But: Trie le tableau dans l'ordre alphabétique
  ##--------------------------------------------------------------------------------------------------------
  def triage(liste)
    
    tab_en_ordre = false # A t'on fini de le trier ?
    taille = liste.size # Taille du tableau
    i=0
    
    # Tant que le triage est en cours
    while(!tab_en_ordre)
        tab_en_ordre = true
        # On remonte l'élément le plus grand
        while(i < taille-1)
            if(liste[i] > liste[i+1] )
                # Inversion de l'élément
                tmp = liste[i]
                liste[i] = liste[i+1]
                liste[i+1] = tmp
                tab_en_ordre = false
            end
            i+=1
        end
        taille-=1
        i=0
    end
    return liste
  
  end
  ##--------------------------------------------------------------------------------------------------------
  ## Nom: enleve_doublons
  ## But: Enlève les éléments doubles d'un tableau
  ##--------------------------------------------------------------------------------------------------------
  def enleve_doublons(ancienne_liste)
    nouvelle_liste = []
    i=0
    # Pour chaque élément
    for ancien in ancienne_liste
      deja_presente = false
      # Contrôle si l'élément est déjà dans la nouvelle liste
      for nouveau in nouvelle_liste
        if nouveau.id == ancien.id
          deja_presente = true
        end
      end
      # On l'insère ou non
      if !deja_presente
        nouvelle_liste[i] = ancien
        i+=1
      end
    end
    return nouvelle_liste
  end
  #
  #
  #
  #  #--------------------------------------------------------------------------------------------------------
  ## Nom: triage_chemin
  ## But: Trie le tableau dans l'ordre alphabetique
  ## Parametre : liste => Liste que l'on veut trier
  ##--------------------------------------------------------------------------------------------------------
  def triage_chemin(liste)
    
    tab_en_ordre = false # A t'on fini de le trier ?
    taille = liste[0].size # Taille du tableau
    i=0
    
    # Tant que le triage est en cours
    while(!tab_en_ordre)
        tab_en_ordre = true
        # On remonte l'élément le plus grand
        while(i < taille-1)
            if(liste[0][i] > liste[0][i+1] )
                # Inversion de l'élément
                tmp_chemin = liste[0][i]
                tmp_id = liste[1][i]
                liste[0][i] = liste[0][i+1]
                liste[1][i] = liste[1][i+1]
                liste[0][i+1] = tmp_chemin
                liste[1][i+1] = tmp_id
                tab_en_ordre = false
            end
            i+=1
        end
        taille-=1
        i=0
    end
    return liste
  
  end
  ## -------------------------------------------------------------------------------------------------------------
  ## Nom : liste_chemin
  ## But : Fonction permettant de creer le chemin depuis la racine jusqu'au type passe en paramètre
  ## Parametre : type => type de depart du chemin
  ## -------------------------------------------------------------------------------------------------------------
  def liste_chemin(type)
    
    # Id du type hiérarchiquement supérieur
    id_du_superieur = type.type_projet_id
    
    # Varibale contenant le chemin
    chemins = []
    
    # Liste de tous les types
    types = TypeProjet.find(:all)
    
    i=0
    # Tant qu'on n'est pas à un type racine
    while id_du_superieur != nil && id_du_superieur != ""
      for type_superieur in types
        # On connaît le père du type
        if type_superieur.id == id_du_superieur
          # Mise à jour du type hiérarchiquement supérieur
          id_du_superieur = type_superieur.type_projet_id
          # Laisse un trace de notre passage
          chemins[i] = type_superieur
          i+=1
        end
      end
    end
   
    # Création du chemin du type jusqu'à la racine
    lieu = ""
    for chemin in chemins.reverse
      lieu = lieu + chemin.typeProjet + "/"
    end
  
    # On retourne le chemin
    return lieu + type.typeProjet + "/."
    
  end
  

  
 
  
end




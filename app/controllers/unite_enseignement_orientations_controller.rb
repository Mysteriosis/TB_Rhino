# -*- encoding : utf-8 -*-
class UniteEnseignementOrientationsController < ApplicationController
  # GET /unite_enseignement_orientations
  # GET /unite_enseignement_orientations.xml
  def index
    @unite_enseignement_orientations = UniteEnseignementOrientation.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @unite_enseignement_orientations }
    end
  end

  # GET /unite_enseignement_orientations/1
  # GET /unite_enseignement_orientations/1.xml
  def show
    @unite_enseignement_orientation = UniteEnseignementOrientation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @unite_enseignement_orientation }
    end
  end

  # GET /unite_enseignement_orientations/new
  # GET /unite_enseignement_orientations/new.xml
  def new
    @unite_enseignement_orientation = UniteEnseignementOrientation.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @unite_enseignement_orientation }
    end
  end

  # GET /unite_enseignement_orientations/1/edit
  def edit
    @unite_enseignement_orientation = UniteEnseignementOrientation.find(params[:id])
  end

  # POST /unite_enseignement_orientations
  # POST /unite_enseignement_orientations.xml
  def create
    @unite_enseignement_orientation = UniteEnseignementOrientation.new(params[:unite_enseignement_orientation])

    respond_to do |format|
      if @unite_enseignement_orientation.save
        flash[:notice] = 'UniteEnseignementOrientation was successfully created.'
        format.html { redirect_to(@unite_enseignement_orientation) }
        format.xml  { render :xml => @unite_enseignement_orientation, :status => :created, :location => @unite_enseignement_orientation }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @unite_enseignement_orientation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /unite_enseignement_orientations/1
  # PUT /unite_enseignement_orientations/1.xml
  def update
    @unite_enseignement_orientation = UniteEnseignementOrientation.find(params[:id])

    respond_to do |format|
      if @unite_enseignement_orientation.update_attributes(params[:unite_enseignement_orientation])
        flash[:notice] = 'UniteEnseignementOrientation was successfully updated.'
        format.html { redirect_to(@unite_enseignement_orientation) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @unite_enseignement_orientation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /unite_enseignement_orientations/1
  # DELETE /unite_enseignement_orientations/1.xml
  def destroy
    @unite_enseignement_orientation = UniteEnseignementOrientation.find(params[:id])
    @unite_enseignement_orientation.destroy

    respond_to do |format|
      format.html { redirect_to(unite_enseignement_orientations_url) }
      format.xml  { head :ok }
    end
  end
end

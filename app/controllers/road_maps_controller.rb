# -*- encoding : utf-8 -*-
class RoadMapsController < ApplicationController
  
  # before_filter :require_user
  load_and_authorize_resource    #Rajoute des before filters sur les actions de maniere à contrôler les autorisations
  
  
  # GET /road_maps
  # GET /road_maps.xml
  def index
    @road_maps = RoadMap.all
    @annees_academiques = AnneeAcademique.find(:all, :order=>'annee')
    @ancienne_annee_academique = AnneeAcademique.find(:all,:order=>'annee').last
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @road_maps }
    end
  end


  # GET /road_maps/new
  # GET /road_maps/new.xml
  def new
    @road_map = RoadMap.new
    @actions = RoadMapAction.find(:all, :order => :id)
    @type_personne = TypePersonne.find(:all, :order => :id)
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @road_map }
    end
  end

  # GET /road_maps/1/edit
  def edit
    @road_map = RoadMap.find(params[:id])
    @actions = RoadMapAction.find(:all, :order => :id)
    @type_personne = TypePersonne.find(:all, :order => :id)
  end

  # POST /road_maps
  # POST /road_maps.xml
  def create
    @road_map = RoadMap.new(params[:road_map])

    respond_to do |format|
      if @road_map.save
        flash[:notice] = 'RoadMap was successfully created.'
        format.html { redirect_to(road_maps_path) }
        format.xml  { render :xml => @road_map, :status => :created, :location => @road_map }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @road_map.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /road_maps/1
  # PUT /road_maps/1.xml
  def update
    @road_map = RoadMap.find(params[:id])
    respond_to do |format|
      if @road_map.update_attributes(params[:road_map])
        flash[:notice] = "Sauvegarde réussie du Road Map"
        format.html { redirect_to(road_maps_path) }
        format.xml  { head :ok }
      else
        flash[:notice] = "Problème avec sauvegarde du Road Map"
        format.html { render :action => "edit" }
        format.xml  { render :xml => @road_map.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /road_maps/1
  # DELETE /road_maps/1.xml
  def destroy
    @road_map = RoadMap.find(params[:id])
    @road_map.destroy

    respond_to do |format|
      format.html { redirect_to(road_maps_url) }
      format.xml  { head :ok }
    end
  end
  
  #-------------------------------------------------------------------------------------------
  # Nom : new_tableau_2_dimensions
  # But : Initialisation d'un tableau à deux dimensions
  #-------------------------------------------------------------------------------------------
  def new_tableau_2_dimensions
    tableau = []
    tableau[0] = []
    tableau[1] = []
    return tableau
  end
  
  #-------------------------------------------------------------------------------------------
  # Nom : enregistrement_reference
  # But : Enregistre dans le tableau les nouvelles references entre un ancien element
  #       et un nouveau
  # Parametres : tableau => tableau de reference
  #              posiion => itérateur, position de l'enregistrement
  #              id_nouveau => id du nouvel element
  #              id_ancien => id de l'ancien element
  #-------------------------------------------------------------------------------------------
  def enregistrement_reference(tableau, position, id_nouveau, id_ancien)
    tableau[0][position] = id_nouveau.to_i
    tableau[1][position] = id_ancien.to_i
    return tableau
  end
  
  #----------------------------------------------------------------------------------  
  #----------------------------------------------------------------------------------
  #--- ACTIONS PONCTUELLES - Invoquées par "Doit" dans le ROAD MAP via la méthode doit
  #----------------------------------------------------------------------------------
  #----------------------------------------------------------------------------------
    
  def doit
  # ACTION GENERIQUE 
  #-------------------------------------------------------------------------------------------
  # But - Lancer une procédure d'initialisation (pour l'administrateur)
  # Pour ce faire: commenter la ligne "rien_faire", et la remplacer par la commande désirée
  #-------------------------------------------------------------------------------------------
    rien_faire
    redirect_to(road_maps_url)
  end
  
  def rien_faire
    flash[:notice]= "Aucune action entreprise"
  end
  
  def affecter_aux_projets_absence
    # Affecter tous les collaborateurs actifs de l'institut aux projets absence

    projet_vacances = Projet.where(:id_interne => 'Vacances 2015').first
    projet_maladie = Projet.where(:id_interne => 'Maladie 2015').first
    projet_conges = Projet.where(:id_interne => 'Congé 2015').first

    done = affecter_a_un_projet_absence(projet_vacances)
    if done 
      done = affecter_a_un_projet_absence(projet_maladie)
    end
    if done 
      done = affecter_a_un_projet_absence(projet_conges)
    end

    if done
      flash[:notice]= "Opération réalisée"  
    end
  end

  def affecter_a_un_projet_absence(projet)
      # Affecter tous les collaborateurs actifs de l'institut à un projet absence
      done = true
      collaborateurs= Personne.collaborateurs_actifs_de_linstitut
      collaborateurs.each do |collaborateur|
        affectation_projet = AffectationProjet.new
        affectation_projet.projet=projet
        affectation_projet.personne=collaborateur

        affectation_projet.date_debut=projet.date_debut
        affectation_projet.date_fin=projet.date_fin

        affectation_projet.engagement = Engagement.premier_engagement_avec_contrats_actuels_ou_futurs(collaborateur)


        affectation_projet.saisie_heures_par_RH_uniquement=false
        affectation_projet.export_sagex=false
        affectation_projet.est_active=true
        affectation_projet.init_color
        affectation_projet.set_display_label(projet.display_name)
        if affectation_projet.engagement
          if !affectation_projet.save
            done=false
            flash[:alert]= "Création stoppée - Problème rencontré avec l'affectation #{affectation_projet.display_name} - #{affectation_projet.errors}"
          end
        else
          done = false
          flash[:alert]= "Création stoppée - Problème rencontré avec l'engagement de la personne #{collaborateur.sigle} - #{affectation_projet.errors}"
        end  
      end
      return done
  end
  
  def creer_charges_from_desideratas
    UniteEnseignement.all.each do |ue|
      ue.nbGroupesLab=ue.nbGroupes
      ue.nbGroupesTh=ue.nbGroupes
      ue.save
    end
    desideratas=Desiderata.desideratas(AnneeAcademique.courante)
    desideratas.each do |desiderata|
      if desiderata.tauxTheorique > 0
        (1..desiderata.nbGroupes).each do |i|
          chargeEnseignement=ChargeEnseignement.new
          chargeEnseignement.unite_enseignement=desiderata.unite_enseignement
          chargeEnseignement.taux=desiderata.tauxTheorique
          chargeEnseignement.numGroupe=desiderata.unite_enseignement.prochainGroupeLibre('T')
          chargeEnseignement.typeGroupe='T'
          chargeEnseignement.personne=desiderata.personne
          chargeEnseignement.save
        end
      end
       if desiderata.tauxPratique > 0 
        (1..desiderata.nbGroupes).each do |i|    
          chargeEnseignement=ChargeEnseignement.new
          chargeEnseignement.unite_enseignement=desiderata.unite_enseignement
          chargeEnseignement.taux=desiderata.tauxPratique
          chargeEnseignement.numGroupe=desiderata.unite_enseignement.prochainGroupeLibre('L')
          chargeEnseignement.typeGroupe='L'
          chargeEnseignement.personne=desiderata.personne
          chargeEnseignement.save
          
          assistant=desiderata.personne_desideratas.where(:priorite=>1).first
          if assistant
            chargeAssistanat=ChargeAssistanat.new
            chargeAssistanat.unite_enseignement=desiderata.unite_enseignement
            chargeAssistanat.taux=desiderata.tauxPratique
            chargeAssistanat.priorite=1
            chargeAssistanat.numGroupe=chargeEnseignement.numGroupe
            chargeAssistanat.typeGroupe='L'
            chargeAssistanat.personne=assistant.personne
            chargeAssistanat.save  
          end
        end
      end
    end
    flash[:notice]= "C'est fait!!!!!!!  "
  end
  #-----------------------------------------------------------
  # A partir des contrats
  # 1/ Créer automatiquement les engagements d'une personne
  # 2/ Associer les contrats aux engagements
  # Méthode utilisée pour passer de l'ancienne version des contrats à la nouvelle (notion d'engagament)
  # Aout 2013
  def creer_les_engagements_des_personnes
    engagements_des_personnes=Hash.new
    Personne.all.each do |personne|
      engagements_des_personnes[personne.initiale]=Hash.new
    end
    Contrat.all.each do |contrat|
      if engagements_des_personnes[contrat.personne.initiale][contrat.type_contrat.typeContrat].nil?
        engagements_des_personnes[contrat.personne.initiale][contrat.type_contrat.typeContrat]=""
        nouvel_engagement=Engagement.new
        nouvel_engagement.personne=contrat.personne
        nouvel_engagement.type_engagement=TypeEngagement.where(:sigle=>contrat.type_contrat.typeContrat).first
        nouvel_engagement.save
      end
        engagement=contrat.personne.engagements.where(:type_engagement_id=>TypeEngagement.where(:sigle=>contrat.type_contrat.typeContrat).first.id).first
        contrat.engagement=engagement
        contrat.save
    end
  end
  
  #------------------------------------------------------------------------
  # Pour toutes les affectations de projets ayant aucun engagement associé,
  # lui associer l'engagement associé au contrat
  # Méthode utilisée pour passer de l'ancienne version des contrats à la nouvelle (notion d'engagament)
  # Aout 2013
  def associer_projets_avec_leur_engagement
    AffectationProjet.all.each do |ap|
      if ap.engagement==nil
        if ap.contrat!=nil
          ap.engagement=ap.contrat.engagement
          ap.save
        end
      end
    end
  end
     
  #-----------------------------------------------------------
  # Spécifier la valeur du champ "FIN" aux plages saisies en mode Mois
  # qui se retrouvaient jusqu'à 13 Février 2013 avec un champ fin égal au champ début
  # Utilisation: 13 Février 2013
  def initialiser_fin_des_plages_sans_fin
    k=0
    id=0
    PlageHoraireSaisie.all.select{|item| item.fin==item.debut && item.heures!=0 }.each do |plage|
      if plage.heures >= 24.0 
        plage.fin= plage.fin + (23.984).hours
      else
        heures_saisie=plage.heures.to_i
        minutes_saisie=(plage.heures-heures_saisie)*60.to_i
        plage.fin= plage.fin + (heures_saisie).hours + (minutes_saisie).minutes
      end
      plage.save
      k+=1
      id=plage.id
    end
    flash[:notice]= "C'est fait!!!!!!! #{k} - id #{id}"
 end


  #-----------------------------------------------------------
  # Couleur des affectations 
  #-----------------------------------------------------------
  def initialiser_a_nil_toutes_les_couleurs_de_toutes_les_affectations
    AffectationProjet.all.each do |ap|
      ap.display_color=nil
      ap.save
    end
    
    flash[:notice]= "Couleurs des affectations ré-initialisées à nil"
  end
  
  def initialiser_couleurs_de_toutes_les_affectations_sans_couleur
    Personne.all.each do |une_personne|
      une_personne.affectation_projets.order(:id).each do |ap|
        if !ap.display_color
          ap.init_color
          ap.save
        end
      end
    end
    
    flash[:notice]= "Les couleurs des affectations sont toutes ré-initilaisées "
  end

  
  def creer_projets_labos(semestre)
    # Création des labos à partir des charges 
    annee_academique_id=AnneeAcademique.courante.id
    type_prj_labo_bachelor=TypeProjet.where(:nom=>'Labo-Bachelor').first
    type_prj_labo_master=TypeProjet.where(:nom=>'Labo-Master').first
    done=true
    charges=ChargeAssistanat.where(:unite_enseignement_id.in => UniteEnseignement.where(:periode_id.in => Periode.where(:annee_academique_id => annee_academique_id).select(:id)))
    labos_en_plus=0
    affectations_non_creees=Array.new
    aff_en_plus=0
    charges.each do |charge|
      if (semestre.eql?("hiver") and charge.unite_enseignement.periode.is_en_hiver?) or
        (semestre.eql?("ete") and charge.unite_enseignement.periode.is_en_ete?)
         if charge.priorite==1
             groupe = charge.numGroupe+"*"
             projet=ProjetEnseignement.new
             projet.unite_enseignement=charge.unite_enseignement
             if projet.unite_enseignement.type_unite_enseignement.type_unite.eql?("Master")
               projet.type_projet=type_prj_labo_master
             else
               projet.type_projet=type_prj_labo_bachelor
             end
             projet.chef_id=charge.unite_enseignement.personne
             projet.id_interne=projet.identifiant_interne(groupe)
             projet.id_externe=projet.identifiant_externe(groupe)
             projet.date_debut=projet.unite_enseignement.periode.date_debut-30
             projet.date_fin=projet.unite_enseignement.periode.date_fin+15
             projet.est_actif=true
             
             if !projet.save
               done=false
               flash[:alert]= "Création stoppée - Problème rencontré avec le projet #{projet.display_name} - #{projet.errors}"
             else
               labos_en_plus+=1
             end
             
             affectation_projet = AffectationProjetAcademique.new
             affectation_projet.projet=projet
             affectation_projet.taux = charge.taux.to_i
             affectation_projet.date_debut=projet.date_debut
             affectation_projet.date_fin=projet.date_fin
             begin
               affectation_projet.charge_horaire=affectation_projet.charge_enseignement*affectation_projet.taux/100
               rescue
                 affectation_projet.charge_horaire=0
             end
             affectation_projet.personne=charge.personne
             affectation_projet.engagement = Engagement.premier_engagement_avec_contrats_actuels_ou_futurs(charge.personne)
             affectation_projet.avec_saisie_heures= true
             affectation_projet.saisie_heures_par_RH_uniquement=false
             affectation_projet.export_sagex=true
             affectation_projet.est_active=true
             affectation_projet.init_color
             affectation_projet.set_display_label(projet.display_name)
             if affectation_projet.engagement
               if !affectation_projet.save
                 done=false
                 flash[:alert]= "Création stoppée - Problème rencontré avec l'affectation #{affectation_projet.display_name} - #{affectation_projet.errors}"
               end
               aff_en_plus+=1
             else
               affectations_non_creees<<affectation_projet.display_label
             end  
         end
       end
    end
    if done
      flash[:notice]= "Création OK - Labos #{labos_en_plus} - Affectations #{aff_en_plus} - #{affectations_non_creees} "
    end
  end
  
  # But : Création des affectations academique aux projets type labo bachelor ou master à partir des projets correspondants de l'année ac. en cours  
   #-------------------------------------------------------------------------------------------
   def creer_affectations_labos_semestre_1
       ues = UniteEnseignement.unites_annee_souhaitee(AnneeAcademique.courante.id)
       type_prj_labo_bachelor=TypeProjet.where(:nom=>'Labo-Bachelor').first
       done=true
       labos = PersonneDesiderata.all
       labos_crees={}
       labos_actuels=0
       labos.each do |lb|
           if lb.desiderata!=nil
             if (lb.desiderata.unite_enseignement.periode.annee_academique==AnneeAcademique.courante) and lb.priorite==1
               #### placer dans la variable "projet" le projet correspondant (déjà créé)
               #### A mettre en ouevre!!!!!!!!!!!!
               affectation_projet = AffectationProjetAcademique.new
               affectation_projet.projet=projet
               affectation_projet.taux = lb.desiderata.tauxPratique
               affectation_projet.date_debut=projet.date_debut
               affectation_projet.date_fin=projet.date_fin
               affectation_projet.export_sagex=true
               begin
                 affectation_projet.charge_horaire=affectation_projet.charge_enseignement*affectation_projet.taux/100
                 rescue
                   affectation_projet.charge_horaire=0
               end
               affectation_projet.personne=lb.personne
               affectation_projet.contrat = affectation_projet.personne.contrats.actuels_ou_futurs.first
               affectation_projet.est_active=true
               if !affectation_projet.save
                done=false
                flash[:alert]= "Création stoppée - Problème rencontré avec l'affectation - Projet id_interne #{projet.id_interne} Personne #{affectation_projet.personne.initiale}- #{affectation_projet.errors[:date_fin]}"
               end 
             end
           end
       end
       if done
         flash[:notice]= "Création OK - Nombre d'affectations créées:  #{labos_actuels} "
       end
   end
  
  #-------------------------------------------------------------------------------------------
  #-------------------------------------------------------------------------------------------
  # FIN DES ACTION PONCTUELLES INVOQUEES par doit
  #------------------------------------------------------------------------------------------- 
  #-------------------------------------------------------------------------------------------
   
  #-------------------------------------------------------------------------------------------
  # Nom : duplicata_table
  # But : Duplique tous les valeurs d'une table  
  # Parametres : liste => liste des enregistrements de la table
  #              tableau_references_superieur => tableau des references superieurs
  #              valeur => valeur spéciale à re-enregistrer en fonction de la table
  #-------------------------------------------------------------------------------------------
  def duplicata_table(liste, tableau_references_superieur, valeur)
    
      # Création du tableau de reference
      tableau_references = new_tableau_2_dimensions
      i=0
      
      # Liste des classes
      charge_enseignement = ChargeEnseignement.new
      charge_assistanat = ChargeAssistanat.new
      competence = CompetencePersonneUnite.new
      unite = UniteEnseignement.new
      orientation = Orientation.new
      filiere = Filiere.new
      departement = Departement.new
      projet = Projet.new
      periode = Periode.new
      desiderata_projet = DesiderataProjet.new
      
      # Pour tous les éléments de la liste
      for element in liste
        
        nouvel_element= element.clone
        
        # S'il s'agit de dupliquer la table "Departement" ou "Periode" ou "DesiderataProjet", qui possèdent un lien sur l'année académique
        if liste.first.class ==  departement.class || liste.first.class ==  periode.class || liste.first.class == desiderata_projet.class
          
          nouvel_element.annee_academique_id = valeur

          # S'il s'agit de la table "périodes", incrémenter les dates de début et de fin d'une année
          if liste.first.class ==  periode.class
            nouvel_element.date_debut = nouvel_element.date_debut + 1.year
            nouvel_element.date_fin = nouvel_element.date_fin + 1.year
          end

          # Sauvegarde de la nouvelle valeur
          nouvel_element.save
          # Enregistrement de la nouvelle référence
          tableau_references = enregistrement_reference(tableau_references,i,nouvel_element.id,element.id)
          i+=1
          
        # S'il s'agit de dupliquer la table "Projet"
        elsif liste.first.class == projet.class
          enseignement_concerne=element.unite_enseignement
          if enseignement_concerne != nil
            if enseignement_concerne.periode.annee_academique_id == valeur
              nouvel_element.save
              # Enregistrement de la nouvelle référence
              tableau_references = enregistrement_reference(tableau_references,i,nouvel_element.id,element.id)
              i+=1
            end
          end
          
        end
        
        
        # S'il y a un tableau de référence supérieur
        if tableau_references_superieur != nil
          nombre_references = tableau_references_superieur.first.size - 1
          # Pour toutes les références dupliquées
          while nombre_references>=0
          
            # S'il s'agit de dupliquer la table "ChargeEnseignement", "ChargeAssistanat" ou "CompetencePesonneUnite"
            if liste.first.class == charge_enseignement.class || liste.first.class == charge_assistanat.class || liste.first.class == competence.class
              
              # Si l'élément correspond à une référence qui a été dupliquée, on modifie l'élément et on le sauve
              if element.unite_enseignement_id.to_i == tableau_references_superieur[1][nombre_references]
                nouvel_element.unite_enseignement_id = tableau_references_superieur[0][nombre_references]
                nouvel_element.save
                tableau_references = enregistrement_reference(tableau_references,i,nouvel_element.id,element.id)
                i+=1    
              end
              nombre_references-=1
              

            # S'il s'agit de dupliquer la table "UniteEnseignement"
            elsif liste.first.class ==  unite.class
            
              if element.periode_id.to_i == tableau_references_superieur[1][nombre_references]
                nouvel_element.periode_id = tableau_references_superieur[0][nombre_references]
                nouvel_element.save
                # Enregistrement de la nouvelle référence
                tableau_references = enregistrement_reference(tableau_references,i,nouvel_element.id,element.id)
                i+=1
              end
              nombre_references-=1
             
            # S'il s'agit de dupliquer la table "Orientation"
            elsif liste.first.class ==  orientation.class
            
              if element.filiere_id.to_i == tableau_references_superieur[1][nombre_references]
                nouvel_element.filiere_id = tableau_references_superieur[0][nombre_references]
                nouvel_element.annee_academique_id = valeur
                nouvel_element.save
                # Enregistrement de la nouvelle référence
                tableau_references = enregistrement_reference(tableau_references,i,nouvel_element.id,element.id)
                i+=1
              end
              nombre_references-=1
            
            # S'il s'agit de dupliquer la table "Filiere"
            elsif liste.first.class ==  filiere.class
            
              if element.departement_id.to_i == tableau_references_superieur[1][nombre_references]
                nouvel_element.departement_id = tableau_references_superieur[0][nombre_references]
                nouvel_element.annee_academique_id = valeur
                nouvel_element.save
                # Enregistrement de la nouvelle référence
                tableau_references = enregistrement_reference(tableau_references,i,nouvel_element.id,element.id)
                i+=1
              end
              
              nombre_references-=1
            end 
          end
        end      
      end
        
      # Si l'élément doit retourner un tableau de référence
      return tableau_references
      
    end

  #-------------------------------------------------------------------------------------------
  # Nom : Dupliquer
  # But : Dupliquer toutes les données liées aux années afin d'obtenir un historique
  #       Ce duplicata intervient sur les données suivantes :
  #       01) Annee academique
  #       02) Periodes
  #       03) Constantes
  #       04) Departements
  #       05) Filieres
  #       06) Orientations
  #       07) Unites d'enseignement
  #       08) Competences sur les unites d'enseignement
  #       09) Charges d'enseignement des professeurs
  #       10) Charges d'assistanat des collaborateurs
  #-------------------------------------------------------------------------------------------
  def dupliquer
    
    p "Ca commence!!!!!!!!!!!!!!!!!!!!!!"
    #--------------------------------------------------------------------------------------
    # (01) Duplication de l'année académique
    #--------------------------------------------------------------------------------------
    ancienne_annee_academique = AnneeAcademique.find(:all,:order=>'annee').last
    nouvelle_annee_academique = AnneeAcademique.new
    nouvelle_annee_academique.annee = "#{ancienne_annee_academique.annee.split('-').last}-#{ancienne_annee_academique.annee.split('-').last.to_i + 1}"
    nouvelle_annee_academique.debut = ancienne_annee_academique.debut+1
    nouvelle_annee_academique.save
    
    #--------------------------------------------------------------------------------------
    # (02) Duplication des périodes
    #--------------------------------------------------------------------------------------
    #anciennes_periodes = Periode.find_all_by_annee_academique_id(ancienne_annee_academique.id, :order=>"descriptif")
    anciennes_periodes = Periode.where(annee_academique_id: ancienne_annee_academique.id).order("descriptif ASC")
    tableau_references_periodes = duplicata_table(anciennes_periodes, nil , nouvelle_annee_academique.id)
    
    #--------------------------------------------------------------------------------------
    # (03) Duplication des constantes liées à l'année académique
    #--------------------------------------------------------------------------------------
    #anciennes_constantes = Constante.find_all_by_annee_academique_id(ancienne_annee_academique.id, :order=>"description")
    anciennes_constantes = Constante.where(annee_academique_id: ancienne_annee_academique.id).order("description ASC")
    for ancienne_constante in anciennes_constantes
      nouvelle_constante = ancienne_constante.clone
      nouvelle_constante.annee_academique_id = nouvelle_annee_academique.id
      nouvelle_constante.save
    end

    #--------------------------------------------------------------------------------------
    # (04) Duplication des départements
    #--------------------------------------------------------------------------------------
    #anciens_departements = Departement.find_all_by_annee_academique_id(ancienne_annee_academique.id, :order=>"abreviation")
    anciens_departements = Departement.where(annee_academique_id: ancienne_annee_academique.id).order("abreviation ASC")
    tableau_references_departements = duplicata_table(anciens_departements, nil , nouvelle_annee_academique.id)
    
    #--------------------------------------------------------------------------------------
    # (05) Duplication des filières
    #--------------------------------------------------------------------------------------
    #anciennes_filieres = Filiere.find_all_by_annee_academique_id(ancienne_annee_academique.id, :order=>"abreviation")
    anciennes_filieres = Filiere.where(annee_academique_id: ancienne_annee_academique.id).order("abreviation ASC")
    tableau_references_filieres = duplicata_table(anciennes_filieres, tableau_references_departements , nouvelle_annee_academique.id)
    
    #--------------------------------------------------------------------------------------
    # (06) Duplication des orientations
    #--------------------------------------------------------------------------------------
    #anciennes_orientations = Orientation.find_all_by_annee_academique_id(ancienne_annee_academique.id, :order=>"abreviation")
    anciennes_orientations = Orientation.where(annee_academique_id: ancienne_annee_academique.id).order("abreviation ASC")
    tableau_references_orientations = duplicata_table(anciennes_orientations, tableau_references_filieres , nouvelle_annee_academique.id)

    #--------------------------------------------------------------------------------------
    # (07) Duplication des unités d'enseignement
    #--------------------------------------------------------------------------------------
    anciennes_unites = UniteEnseignement.unites_annee_souhaitee(ancienne_annee_academique.id)
    tableau_references_unites = duplicata_table(anciennes_unites, tableau_references_periodes, nil)
    
    #--------------------------------------------------------------------------------------
    # (08) Duplication des liens entre des unités et les orientations
    #--------------------------------------------------------------------------------------
    liens_orientations_unites = UniteEnseignementOrientation.find(:all)
    for lien in liens_orientations_unites
      nombre_unites = tableau_references_unites.first.size - 1
      while nombre_unites>=0
        # Si l'unité est une unité de la table de reference, cela veut signifie
        # qu'il faut la copier et qu'il faut indiquer son nouvel unite_enseignement_id
        if lien.unite_enseignement_id.to_i == tableau_references_unites[1][nombre_unites]
          nouveau_lien = UniteEnseignementOrientation.new
          nouveau_lien.unite_enseignement_id = tableau_references_unites[0][nombre_unites]
          nombre_orientations = tableau_references_orientations.first.size - 1
          while nombre_orientations>=0
            # Si l'orientation est une orientation de la table de reference, cela veut signifie
            # qu'il faut la copier et qu'il faut indiquer son nouvel orientation_id
            if lien.orientation_id.to_i == tableau_references_orientations[1][nombre_orientations]
              nouveau_lien.orientation_id = tableau_references_orientations[0][nombre_orientations]
            end
            nombre_orientations-=1
          end
          nouveau_lien.save
        end
        nombre_unites-=1
      end
    end
    
    #--------------------------------------------------------------------------------------
    # (09) Duplicata des compétences sur les unités d'enseignement
    #--------------------------------------------------------------------------------------
    competences_personnes = CompetencePersonneUnite.find(:all)
    tableau_references_competences_personnes = duplicata_table(competences_personnes, tableau_references_unites, nil)
    
    #--------------------------------------------------------------------------------------
    # (10) Duplicata des charges d'enseignement des professeurs
    #--------------------------------------------------------------------------------------
    charges_enseignement = ChargeEnseignement.find(:all)
    tableau_references_charges_enseignement = duplicata_table(charges_enseignement, tableau_references_unites, nil)
    
    #--------------------------------------------------------------------------------------
    # (11) Duplicata des charges d'assistanat des collaborateurs
    #--------------------------------------------------------------------------------------
    charges_assistanat = ChargeAssistanat.find(:all)
    tableau_references_charges_assistanat = duplicata_table(charges_assistanat, tableau_references_unites, nil)
    
    
    flash[:notice]= 'Duplicata OK'
    redirect_to(road_maps_url)
    
  end

  #-------------------------------------------------------------------------------------------
  # Nom : supprimer
  # But : Supprime tous les enregistrement d'une année souhaitée
  #-------------------------------------------------------------------------------------------
  def supprimer
    
    # Année académique à supprimer
    id_annee_academique = params[:id_annee_academique].to_i
    
    
    #-------------------------------------------------------
    # (02) Suppression des périodes
    #-------------------------------------------------------
    # anciennes_periodes = Periode.find_all_by_annee_academique_id(id_annee_academique)
    anciennes_periodes = Periode.where(annee_academique_id: id_annee_academique)
    for ancienne_periode in anciennes_periodes
      #-------------------------------------------------------
      # (07) Suppression des unités
      #-------------------------------------------------------
      # anciennes_unites = UniteEnseignement.find_all_by_periode_id(ancienne_periode.id)
      anciennes_unites = UniteEnseignement.where(periode_id: ancienne_periode.id)
      for ancienne_unite in anciennes_unites
        #-------------------------------------------------------
        # (08) Suppression des liens entre unité et orientations
        #-------------------------------------------------------
        # anciens_liens = UniteEnseignementOrientation.find_all_by_unite_enseignement_id(ancienne_unite.id)
        anciens_liens = UniteEnseignementOrientation.where(unite_enseignement_id: ancienne_unite.id)
        for ancien_lien in anciens_liens
          ancien_lien.delete
        end
        #-------------------------------------------------------
        # (09) Suppression des compétences sur les unités d'enseignement
        #-------------------------------------------------------
        # anciennes_competences = CompetencePersonneUnite.find_all_by_unite_enseignement_id(ancienne_unite.id)
        anciennes_competences = CompetencePersonneUnite.where(unite_enseignement_id: ancienne_unite.id)
        for ancienne_competence in anciennes_competences
          ancienne_competence.delete
        end
        #-------------------------------------------------------
        # (10) Suppression des charges d'enseignement des professeurs
        #-------------------------------------------------------
        # anciennes_charges_enseignement = ChargeEnseignement.find_all_by_unite_enseignement_id(ancienne_unite.id)
        anciennes_charges_enseignement = ChargeEnseignement.where(unite_enseignement_id: ancienne_unite.id)
        for ancienne_charge_enseignement in anciennes_charges_enseignement
          ancienne_charge_enseignement.delete
        end
        #--------------------------------------------------------------
        # (11) Suppression des charges d'assistanant des collaborateurs
        #--------------------------------------------------------------
        # anciennes_charges_assistanat = ChargeAssistanat.find_all_by_unite_enseignement_id(ancienne_unite.id)
        anciennes_charges_assistanat = ChargeAssistanat.where(unite_enseignement_id: ancienne_unite.id)
        for ancienne_charge_assistanat in anciennes_charges_assistanat
          ancienne_charge_assistanat.delete
        end
        #--------------------------------------------------------------
        # (12) Suppression des unités d'enseignement
        #--------------------------------------------------------------
        ancienne_unite.delete
      end
      #--------------------------------------------------------------
      # (13) Suppression des périodes d'enseignement
      #--------------------------------------------------------------
      ancienne_periode.delete
    end
    
    #-------------------------------------------------------
    # (03) Suppression des constantes dliées à l'année académique
    #-------------------------------------------------------
    # anciennes_constantes = Constante.find_all_by_annee_academique_id(id_annee_academique)
    anciennes_constantes = Constante.where(annee_academique_id: id_annee_academique)
    for ancienne_constante in anciennes_constantes
      ancienne_constante.delete
    end

    #-------------------------------------------------------
    # (04) Suppression des départements
    #-------------------------------------------------------
    # anciens_departements = Departement.find_all_by_annee_academique_id(id_annee_academique)
    anciens_departements = Departement.where(annee_academique_id: id_annee_academique)
    for ancien_departement in anciens_departements
      ancien_departement.delete
    end
    
    #-------------------------------------------------------
    # (05) Suppression des filières
    #-------------------------------------------------------
    # anciennes_filieres = Filiere.find_all_by_annee_academique_id(id_annee_academique)
    anciennes_filieres = Filiere.where(annee_academique_id: id_annee_academique)
    for ancienne_filiere in anciennes_filieres
      ancienne_filiere.delete
    end
    
    #-------------------------------------------------------
    # (06) Suppression des orientations
    #-------------------------------------------------------
    # anciennes_oriecntations = Orientation.find_all_by_annee_academique_id(id_annee_academique)
    anciennes_oriecntations = Orientation.where(annee_academique_id: id_annee_academique)
    for ancienne_orientation in anciennes_orientations
      ancienne_orientation.delete
    end


    
    #-------------------------------------------------------
    # (01) Suppression de l'année académique
    #-------------------------------------------------------
    ancienne_annee = AnneeAcademique.find_by_id(id_annee_academique)
    ancienne_annee.delete

    flash[:notice] = 'Suppression OK'
    redirect_to(road_maps_url)   
    
  end

  def reset_heures
    @personnes=Personne.personnes_actives_de_linstitut
    if params[:last_personne_id]
      last_personne=Personne.find_by_id(params[:last_personne_id])
      @next_personne=@personnes.first
      index_last_personne=@personnes.index(last_personne)
      if index_last_personne >= @personnes.size - 1
        @next_personne=@personnes.first
      else
        @next_personne= @personnes[@personnes.index(last_personne)+1]
      end
    else
      @next_personne=nil
    end 
  end

  def reset_heures_dune_personne
  # Reset des heures d'une personnes (Plages contractuelles, info_heures, calcul des heures des premières années)
    if !(params[:reset_heures][:personne_id]).eql?("")
      personne=Personne.find_by_id(params[:reset_heures][:personne_id])
      PlageContractuelle.reset_plages_contractuelles_pour_toutes_les_annees(personne)
      PlageContractuelle.update_heures_partiel(personne)
      flash[:notice] = "Update des heures opéré pour #{personne.initiale} avec succès!"
    end
    redirect_to reset_heures_road_maps_path(:last_personne_id => personne.id)
  end 

  def reset_global_des_heures
  # Reset des heures pour toutes les personnes (Plages contractuelles, info_heures, calcul des heures des premières années)
    Personne.personnes_actives_de_linstitut.each do |personne|
      PlageContractuelle.reset_plages_contractuelles_pour_toutes_les_annees(personne)
      PlageContractuelle.update_heures_partiel(personne)  
    end  
    flash[:notice] = "Update des heures opéré pour toutes les personnes actives de l'institut avec succès!"
    redirect_to reset_heures_road_maps_path
  end 
end

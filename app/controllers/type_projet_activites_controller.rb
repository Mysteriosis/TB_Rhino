# -*- encoding : utf-8 -*-
class TypeProjetActivitesController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /type_projet_activites
  # GET /type_projet_activites.xml
  def index
    @type_projet_activites = TypeProjetActivite.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_projet_activites }
    end
  end

  # GET /type_projet_activites/1
  # GET /type_projet_activites/1.xml
  def show
    @type_projet_activite = TypeProjetActivite.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_projet_activite }
    end
  end

  # GET /type_projet_activites/new
  # GET /type_projet_activites/new.xml
  def new
    @type_projet_activite = TypeProjetActivite.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_projet_activite }
    end
  end

  # GET /type_projet_activites/1/edit
  def edit
    @type_projet_activite = TypeProjetActivite.find(params[:id])
  end

  # POST /type_projet_activites
  # POST /type_projet_activites.xml
  def create
    @type_projet_activite = TypeProjetActivite.new(params[:type_projet_activite])

    respond_to do |format|
      if @type_projet_activite.save
        flash[:notice] = 'TypeProjetActivite was successfully created.'
        format.html { redirect_to(@type_projet_activite) }
        format.xml  { render :xml => @type_projet_activite, :status => :created, :location => @type_projet_activite }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_projet_activite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /type_projet_activites/1
  # PUT /type_projet_activites/1.xml
  def update
    @type_projet_activite = TypeProjetActivite.find(params[:id])

    respond_to do |format|
      if @type_projet_activite.update_attributes(params[:type_projet_activite])
        flash[:notice] = 'TypeProjetActivite was successfully updated.'
        format.html { redirect_to(@type_projet_activite) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_projet_activite.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /type_projet_activites/1
  # DELETE /type_projet_activites/1.xml
  def destroy
    @type_projet_activite = TypeProjetActivite.find(params[:id])
    @type_projet_activite.destroy

    respond_to do |format|
      format.html { redirect_to(type_projet_activites_url) }
      format.xml  { head :ok }
    end
  end
end

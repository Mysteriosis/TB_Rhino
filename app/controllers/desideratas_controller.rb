# -*- encoding : utf-8 -*-
class DesideratasController < ApplicationController

  # GET /desideratas
  # GET /desideratas.xml
  def index
    
    # Liste de tous les desiderata
    @desideratas = Desiderata.find(:all)
    # Liste de toutes les personnes
    @personnes = Personne.find(:all, :order=>"initiale", :conditions=>["type_personne_id != 5"])
    # Liste de tous les liens entre un desiderata et un assistant
    @lien_assistanat = PersonneDesiderata.find(:all)
    # Liste de toutes les années académiques
    annees_academiques =AnneeAcademique.find(:all, :order=>'annee')
    # Une année académique nulle
    annee_academique_nulle = AnneeAcademique.new(:annee=>'Toutes')
    # Liste de toutes les années académiques (y.c nul)
    @annees_academiques = []
    # Liste de toutes les périodes
    periodes = Periode.find(:all, :order=>'descriptif')
    # Une période nulle
    periode_nulle = Periode.new(:descriptif=>'Tous')
    # Liste de toutes périodes (y.c nul)
    @periodes = []
    # Liste de tous les professeurs
    professeurs = Personne.professeurs
    # Liste de tous les professeurs (y.c nul)
    @professeurs = []
    # Liste de tous les assistants
    assistants = Personne.collaborateurs
    # Liste de tous les assistants (y.c nul)
    @assistants = []
    # Personne nulle
    personne_nulle = Personne.new(:initiale=>'Tous')
    # Liste de toutes les unités d'enseignement
    @liste_unites = UniteEnseignement.find(:all, :conditions=>{:reconductible=>1},:order=>'abreviation')
    # Liste de toutes les unités d'enseignement (y.c nul)
    @unites = []
    # Unité nulle
    unite_nulle = UniteEnseignement.new(:abreviation=>'Toutes')
    # Liste de tous les départements
    departements = Departement.find(:all, :order=>'abreviation')
    # Liste de tous les départements (y.c nul)
    @departements = []
    # Département nul
    departement_nul = Departement.new(:abreviation=>'Tous')
    # Liste de tous les types d'unité d'enseignement
    types_unites = TypeUniteEnseignement.find(:all, :order=>'type_unite')
    # Liste de tous les type d'unité d'enseignement (y.c nul)
    @types_unites = []
    # Unité nulle
    type_unite_nulle = TypeUniteEnseignement.new(:type_unite=>'Tous')
    # Liste de toutes les filères
    filieres = Filiere.find(:all, :order=>'abreviation')
    # Liste de toutes les filières (y.c nul)
    @filieres = []
    # Filière nulle
    filiere_nulle = Filiere.new(:abreviation=>'Toutes')
    # Liste de toutes les orientations
    orientations = Orientation.find(:all, :order=>'abreviation')
    # Liste de toutes les orientations (y.c nul)
    @orientations = []
    # Orientation nulle
    orientation_nulle = Orientation.new(:abreviation=>'Toutes')

    
    # Calcul pour chaque unité quelle avertissement elle doit avoir
    
    @unites_avertissement = []
    @unites_possedent_desiderata = []
    nombre_unite= 0
    for unite in @liste_unites
      total_desire = 0
      a_desiderata = false
      # On calcul le nombre de pourcentage desiré pour cette unité
      for desiderata in @desideratas
        if unite.id == desiderata.unite_enseignement_id
          total_desire += (desiderata.tauxTheorique + desiderata.tauxPratique) * desiderata.nbGroupes
          a_desiderata = true
        end      
      end 
      # Calcul du pourcentage possible
      total_possible = 0
      if unite.coursBloc
        if unite.nbPeriodesTheoriquesTotal != 0
          total_possible = 100
        end
        if unite.nbPeriodesPratiquesTotal != 0
          total_possible += 100
        end
      else
        if unite.nbPeriodesTheoriquesSemaine != 0
          total_possible = 100
        end
        if unite.nbPeriodesPratiquesSemaine != 0
          total_possible += 100
        end
      end
      
      if unite.nbGroupes == nil 
        unite.nbGroupes=0 
      end
      total_possible *= unite.nbGroupes
      # Si le total desiré est plus grand que le total possible cela mérite un avertissement pour dire que cette
      # unite possède trop de desiderata
      if total_desire > total_possible.to_f
        @unites_avertissement[nombre_unite] = "Trop"
      # Si le total desiré est plus petit que le total possible cela mérite un avertissement pour dire que cette
      # unite ne possède pas assez de desiderata
      elsif total_desire < total_possible.to_f
        @unites_avertissement[nombre_unite] = "PasAssez"
      # Si le total desiré est plus petit ou égal au total possible cela ne mérite pas d'avertissement
      else
        @unites_avertissement[nombre_unite] = "Aucun"
      end
      # Définit si l'unité possède au minimum un desiderata
      @unites_possedent_desiderata[nombre_unite] = a_desiderata
      nombre_unite+=1
    end


    # Retourne toutes les années académiques (y.c nul)
    i=1
    @annees_academiques[0]=annee_academique_nulle
    for annee in annees_academiques
      @annees_academiques[i]=annee
      i+=1
    end
    
    # Retourne toutes les périodes (y.c nul)
    i=1
    @periodes[0]=periode_nulle
    for periode in periodes
      @periodes[i]=periode
      i+=1
    end
    
    # Retourne tous les professeurs (y.c nul)
    i=1
    @professeurs[0]=personne_nulle
    for professeur in professeurs
      @professeurs[i]=professeur
      i+=1
    end
    
    # Retourne tous les assistants (y.c nul)
    i=1
    @assistants[0]=personne_nulle
    for assistant in assistants
      @assistants[i]=assistant
      i+=1
    end
    
    # Retourne toutes les unités d'enseignement (y.c nul)
    i=1
    @unites[0]=unite_nulle
    for unite in @liste_unites
      @unites[i]=unite
      i+=1
    end
    
    # Retourne tous les départements (y.c nul)
    i=1
    @departements[0]=departement_nul
    for departement in departements
      @departements[i]=departement
      i+=1
    end

    # Retourne toutes les filières (y.c nul)
    i=1
    @filieres[0]= filiere_nulle
    for filiere in filieres
      @filieres[i]=filiere
      i+=1
    end
    
    # Retourne toutes les orientations (y.c nul)
    i=1
    @orientations[0]= orientation_nulle
    for orientation in orientations
      @orientations[i]=orientation
      i+=1
    end
    
#    # Retourne tous les modules (y.c nul)
#    i=1
#    @modules[0]=module_nul
#    for module_unite in modules
#      @modules[i]=module_unite
#      i+=1
#    end
    
    # Retourne tous les types d'unité d'enseignement (y.c nul)
    i=1
    @types_unites[0]=type_unite_nulle
    for type_unite in types_unites
      @types_unites[i]=type_unite
      i+=1
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @desideratas }
    end
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: initialisation_filtre
  # But: Initialisation de la page
  #--------------------------------------------------------------------------------------------------------
  def initialisation_filtre
    @liste_unites = UniteEnseignement.find(:all, :conditions=>{:reconductible=>1},:order=>'abreviation')
    annees = AnneeAcademique.find(:all,:order=>'annee') # Liste de toutes les années académiques
    @annee_ajd = AnneeAcademique.courante # Année académique au jour d'aujourd'hui
    
    
    # Liste des unités à afficher
    @unites=[]
    i=0
    for unite in @liste_unites
      if unite.periode.annee_academique_id = @annee_ajd.id
        @unites[i]=unite
        i+=1
      end
    end
    
    # Liste des années à afficher
    @annees = []
    @annees[0] = @annee_ajd
    @annees[1] = AnneeAcademique.new(:annee=>'Toutes')
    i=2
    for annee in annees
      if annee.id != @annee_ajd.id
        @annees[i]=annee
        i+=1
      end
    end
    
    annee_mise_a_jour
 
  end
  
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: annee_mise_a_jour
  # But: Modification du contenu de la liste déroulante des périodes et des unités d'enseignement en  
  #      fonction de l'année académique choisie
  #--------------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    
    # Département nul
    @departement_nul = Departement.new(:abreviation=>'Tous')    
    
    # Filiere nulle
    @filiere_nulle = Filiere.new(:abreviation=>'Toutes')
    
    # Orientation nulle
    @orientation_nulle = Orientation.new(:abreviation=>'Toutes')
    


    # Unité nulle
    @unite_nulle = UniteEnseignement.new(:abreviation=>'Toutes')
    
    # Période nulle
    @periode_nulle = Periode.new(:descriptif=>'Tous')
    
    # Si l'on appelle cette fonction depuis la page des desiderata
    if params['id_annee_academique'] != nil && params['id_annee_academique'] != ""
      @id_annee = params['id_annee_academique']
   
    else  # Si l'on appelle cette fonction comme initialisation
      @id_annee = @annee_ajd.id
    end
    
    # Si aucune années n'est choisie
    if @id_annee == "" || @id_annee == "Toutes"
      
      # Liste de toutes les périodes
      @liste_periodes = Periode.find(:all, :order => 'descriptif')
      # Liste de tous les départements
      @departements = Departement.find(:all,:order=>'abreviation')
      # Liste de toutes les filières
      @filieres = Filiere.find(:all,:order=>'abreviation')
      # Liste de toutes les orientations
      @orientations = Orientation.find(:all,:order=>'abreviation')
      


      # Liste de toutes les unités d'enseignement
      @unites = UniteEnseignement.find(:all,:conditions=>{:reconductible=>1},:order=>'abreviation')
    
   
  else  # Si une année est choisie
    
      # Liste des périodes correspondant à l'année académique choisie
      #@liste_periodes = Periode.find_all_by_annee_academique_id(@id_annee, :order => 'descriptif')
      @liste_periodes = Periode.where(annee_academique_id: @id_annee).order('descriptif ASC')
      # Liste de tous les départements
      departements = Departement.find(:all,:order=>'abreviation')
      # Liste de toutes les filières
      filieres = Filiere.find(:all,:order=>'abreviation')
      # Liste de toutes les orientations
      orientations = Orientation.find(:all,:order=>'abreviation')
      
#      # Liste de tous les modules
#      modules = ModuleUnite.find(:all,:order=>'abreviation')

      # Liste de toutes les unités d'enseignement
      unites = UniteEnseignement.find(:all, :conditions=>{:reconductible=>'1'},:order=>'abreviation')
      
      # Liste des départements correspondant à l'année académique choisie
      i=0
      @departements = []
      for departement in departements
        if departement.annee_academique_id.to_i == @id_annee.to_i
          @departements[i] = departement
          i+=1
        end
      end
      
      # Liste des filière correspondant à l'année académique choisie
      i=0
      @filieres = []
      for filiere in filieres
        if filiere.annee_academique_id.to_i == @id_annee.to_i
          @filieres[i] = filiere
          i+=1
        end
      end
      
      # Liste des orientations correspondant à l'année académique choisie
      i=0
      @orientations = []
      for orientation in orientations
        if orientation.annee_academique_id.to_i == @id_annee.to_i
          @orientations[i] = orientation
          i+=1
        end
      end
      
#      # Liste des module correspondant à l'année académique choisie
#      i=0
#      @modules = []
#      for module_unite in modules
#        if module_unite.annee_academique_id.to_i == @id_annee.to_i
#          @modules[i] = module_unite
#          i+=1
#        end
#      end

      # Liste des unités correspondant à l'année académique choisie
      i=0
      @unites = []
      for unite in unites
        if unite.periode.annee_academique_id.to_i == @id_annee.to_i
          @unites[i] = unite
          i+=1
        end
      end
    end  
    
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: chargement
  # But: Affiche toutes les unités d'enseignement correspondantes à tous les filtres
  #--------------------------------------------------------------------------------------------------------
  def chargement
    
    # Liste de toutes les unités d'enseignement
    @unites = UniteEnseignement.find(:all, :conditions=>{:reconductible=>1},:order=>'abreviation')
    
    # Liste de tous les desiderata
    desideratas = Desiderata.find(:all)
    
    # Liste de tous lien entre un desiderata et des assistants
    lien_assistants_desideratas = PersonneDesiderata.find(:all)

    
    #-------------------------------------------------------------------------------
    # Trouve les unités liées aux avertissements
    #-------------------------------------------------------------------------------
    unite_sans_avertissement = []
    sans_avertissement = 0
    unite_avertissement_trop = []
    avertissement_trop = 0
    unite_avertissement_pas_assez = []
    avertissement_pas_assez = 0
    
    # Calcul pour chaque unité l'avertissement qu'elle doit avoir
    for unite in @unites
      total_desire = 0
      # Calcul du pourcentage desiré pour cette unité
      for desiderata in desideratas
        if unite.id == desiderata.unite_enseignement_id
          total_desire += (desiderata.tauxTheorique + desiderata.tauxPratique) * desiderata.nbGroupes
        end      
      end 
      
      # Calcul du pourcentage possible
      total_possible = 0
      if unite.nbPeriodesTheoriquesSemaine != 0
        total_possible = 100
      end
      if unite.nbPeriodesPratiquesSemaine != 0
        total_possible += 100
      end
      if unite.nbGroupes == "" || unite.nbGroupes == nil
        unite.nbGroupes = 0
      end
      total_possible *= unite.nbGroupes
      
      # Si le total desiré est plus grand que le total possible cela mérite un avertissement pour dire que cette
      # unite possède trop de desiderata
      if total_desire > total_possible.to_f
        unite_avertissement_trop[avertissement_trop] = unite
        avertissement_trop+=1
      # Si le total desiré est plus petit que le total possible cela mérite un avertissement pour dire que cette
      # unite ne possède pas assez de desiderata
      elsif total_desire < total_possible.to_f
        unite_avertissement_pas_assez[avertissement_pas_assez] = unite
        avertissement_pas_assez+=1
      # Si le total desiré est plus petit ou égal au total possible cela mérite pas d'avertissement
      else
        unite_sans_avertissement[sans_avertissement] = unite
        sans_avertissement+=1
      end
    end
    
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondant à l'année choisie
    #-------------------------------------------------------------------------------
    # Si aucune année n'est spécifiée
    if params['filtre_annee'] == "" || params['filtre_annee'] == "Toutes"
      unites_affiche_annee = @unites
    else
      # Liste toutes les unités qui doivent être affichées par le choix dans la liste de sélection "annee"
      i=0
      unites_affiche_annee = []
      for unite in @unites
        if unite.periode.annee_academique_id == params['filtre_annee'].to_i
          unites_affiche_annee[i] = unite
          i+=1
        end
      end
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondant à la période choisie
    #-------------------------------------------------------------------------------
    # Si aucune période n'est spécifiée
    if params['filtre_periode'] == "Tous" || params['filtre_periode'] == ""
      unites_affiche_periode = @unites
    else
      # Liste toutes les unités qui doivent être affichées par le choix dans la liste de sélection "période"
      i=0
      unites_affiche_periode = []
      for unite in @unites
        if unite.periode_id == params['filtre_periode'].to_i
          unites_affiche_periode[i] = unite
          i+=1
        end
      end
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondant au professeur choisi
    #-------------------------------------------------------------------------------
    # Si aucun professeur n'est spécifié
    if params['filtre_professeur'] == ""
      unites_affiche_professeur = @unites
    else
      # Liste toutes les unités qui doivent être affichées par le choix dans la liste de sélection "professeur"
      i=0
      unites_affiche_professeur = []
      for unite in @unites
        fait_partie = false
        for desiderata in desideratas
          if unite.id == desiderata.unite_enseignement_id && desiderata.personne_id == params['filtre_professeur'].to_i && !fait_partie
            unites_affiche_professeur[i] = unite
            i+=1
            fait_partie = true
          end
        end
      end
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondant à l'assistant choisi
    #-------------------------------------------------------------------------------
    # Si aucun assistant n'est spécifié
    if params['filtre_assistant'] == ""
      unites_affiche_assistant = @unites
    else
      # Liste toutes les unités qui doivent être affichées par le choix dans la liste de sélection "assistant"
      i=0
      unites_affiche_assistant = []
      for unite in @unites # Pour toutes les unités d'enseignement
        fait_partie = false
        for desiderata in desideratas # Pour tous les desiderata
          if unite.id == desiderata.unite_enseignement_id # Si le desiderata concerne cette unité d'enseignement
            for lien in lien_assistants_desideratas # Pour tous les liens entre un desiderata et des assistants
              # Si l'assistant fait partie d'un desiderata de l'unité et qu'il n'a encore jamais été marqué
              if lien.desiderata_id == desiderata.id && lien.personne_id == params['filtre_assistant'].to_i && !fait_partie
                  unites_affiche_assistant[i] = unite
                  i+=1
                  fait_partie = true
              end
            end
          end
        end
      end
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondant à l'unité choisie
    #-------------------------------------------------------------------------------
    # Si aucune unité n'est spécifiée
    if params['filtre_unite'] == "" || params['filtre_unite'] == "Toutes"
      unites_affiche_unite = @unites
    else
      # Liste de l'unités qui doit être affichée par le choix dans la liste de sélection "unité"
      unites_affiche_unite = []
      for unite in @unites # Pour toutes les unités d'enseignement
        if unite.id == params['filtre_unite'].to_i
          unites_affiche_unite[0] = unite
        end
      end
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes au departement choisi
    #-------------------------------------------------------------------------------
    # Si aucun departement n'est spécifié
    if params['filtre_departement'] == "" || params['filtre_departement'] == "Tous"
      unites_affiche_departement = @unites
    else 
      unites_affiche_departement = departement_selection(params['filtre_departement'],"unite")
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes à la filière choisie
    #-------------------------------------------------------------------------------
    # Si aucune filière n'est spécifiée
    if params['filtre_filiere'] == "" || params['filtre_filiere'] == "Toutes"
      unites_affiche_filiere = @unites
    else 
      unites_affiche_filiere = filiere_selection(params['filtre_filiere'],"unite")
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes à l'orientation choisie
    #-------------------------------------------------------------------------------
    # Si aucune orientation n'est spécifiée
    if params['filtre_orientation'] == "" || params['filtre_orientation'] == "Toutes"
      unites_affiche_orientation = @unites
    else 
      unites_affiche_orientation = orientation_selection(params['filtre_orientation'],"unite")
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes au module choisi
    #-------------------------------------------------------------------------------
    # Si aucun module n'est spécifié
    #if params['filtre_module'] == "" || params['filtre_module'] == "Tous"
    #  unites_affiche_module = @unites
    #else 
    #  unites_affiche_module = module_selection(params['filtre_module'])
    #end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes à l'avertissement choisi
    #-------------------------------------------------------------------------------
    # Si le choix de la liste de sélection est d'afficher toutes les unités aillant
    # trop de desiderata
    if params['filtre_conflit'] == "Avertissement : Trop de desiderata"
      unites_affiche_avertissement = unite_avertissement_trop
    # Si le choix de la liste de sélection est d'afficher toutes les unités aillant
    # pas assez de desiderata
    elsif params['filtre_conflit'] == "Avertissement : Pas assez de desiderata"
      unites_affiche_avertissement = unite_avertissement_pas_assez
    # Si le choix de la liste de sélection est d'afficher toutes les unités aillant
    # un bon nombre de desiderata
    elsif params['filtre_conflit'] == "Aucun avertissement"
      unites_affiche_avertissement = unite_sans_avertissement
    # Si le choix de la liste de sélection est d'afficher toutes les unités peut
    # importe leur avertissement
    else
      unites_affiche_avertissement = @unites
    end
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes au type d'unité choisi
    #-------------------------------------------------------------------------------
    # Si aucun type d'unité n'est spécifié
    if params['filtre_type_unite'] == "" || params['filtre_type_unite'] == "Tous"
      unites_affiche_type_unite = @unites
    else
      # Liste des unités qui doivent être affichée par le choix dans la liste de sélection "type_unité"
      unites_affiche_type_unite = []
      i=0
      for unite in @unites # Pour toutes les unités d'enseignement
        temp = params['filtre_type_unite']
        if unite.type_unite_enseignement_id == temp.to_i
          unites_affiche_type_unite[i] = unite
          i+=1
        end
      end
    end
    
    
    #-------------------------------------------------------------------------------
    # Trouve les unités correspondantes au filtre de la page
    #-------------------------------------------------------------------------------
    @unites_affiche = []
    nombre_unite = 0
    # Pour toutes les unités
    for unite in @unites
      
      # Mise à zéro des variables
      unite_annee_ok = false
      unite_periode_ok = false
      unite_professeur_ok = false
      unite_assistant_ok = false
      unite_unite_ok = false
      unite_departement_ok = false
      unite_filiere_ok = false
      unite_orientation_ok = false
#      unite_module_ok = false
      unite_avertissement_ok = false
      unites_type_unite_ok = false
      
      # Compare si cette unité correspond à l'année choisie
      for unite_affiche_annee in unites_affiche_annee
        if unite.id == unite_affiche_annee.id
          unite_annee_ok = true
        end
      end
      # Compare si cette unité correspond à la période choisie
      for unite_affiche_periode in unites_affiche_periode
        if unite.id == unite_affiche_periode.id
          unite_periode_ok = true
        end
      end
      # Compare si cette unité correspond au professeur choisi
      for unite_affiche_professeur in unites_affiche_professeur
        if unite.id == unite_affiche_professeur.id
          unite_professeur_ok = true
        end
      end
      # Compare si cette unité correspond à l'assistant choisi
      for unite_affiche_assistant in unites_affiche_assistant
        if unite.id == unite_affiche_assistant.id
          unite_assistant_ok = true
        end
      end
      # Compare si cette unité correspond à l'unité choisie
      for unite_affiche_unite in unites_affiche_unite
        if unite.id == unite_affiche_unite.id
          unite_unite_ok = true
        end
      end
      # Compare si cette unité correspond au département choisi
      for unite_affiche_departement in unites_affiche_departement
        if unite.id == unite_affiche_departement.id
          unite_departement_ok = true
        end
      end
      # Compare si cette unité correspond à la filière choisie
      for unite_affiche_filiere in unites_affiche_filiere
        if unite.id == unite_affiche_filiere.id
          unite_filiere_ok = true
        end
      end
      # Compare si cette unité correspond à l'orientation choisie
      for unite_affiche_orientation in unites_affiche_orientation
        if unite.id == unite_affiche_orientation.id
          unite_orientation_ok = true
        end
      end
#      # Compare si cette unité correspond au module choisi
#      for unite_affiche_module in unites_affiche_module
#        if unite.id == unite_affiche_module.id
#          unite_module_ok = true
#        end
#      end
      # Compare si cette unité correspond à l'avertissement choisi
      for unite_affiche_avertissement in unites_affiche_avertissement
        if unite.id == unite_affiche_avertissement.id
          unite_avertissement_ok = true
        end
      end 
      # Compare si cette unité correspond au type d'unité choisi
      for unite_affiche_type_unite in unites_affiche_type_unite
        if unite.id == unite_affiche_type_unite.id
          unites_type_unite_ok = true
        end
      end 

      # Si l'unité est commune à tous les champs du filtre
      if unite_annee_ok && unite_periode_ok && unite_professeur_ok && unite_assistant_ok && unite_unite_ok && unite_avertissement_ok && unites_type_unite_ok && unite_departement_ok && unite_filiere_ok && unite_orientation_ok 
        @unites_affiche[nombre_unite] = unite
        nombre_unite += 1
      end  
      
    end
    
  end

  # GET /desideratas/1
  # GET /desideratas/1.xml
  def show
    
    @desiderata = Desiderata.find(params[:id])
    #@personne = Personne.find(params[:personne])
    @personne = Personne.find(@desiderata.personne_id)
    
    # Liste de tous les assistants
    @assistants = Personne.find(:all, :order=>"initiale", :conditions=>["type_personne_id = 2"])
    # Liste de toutes les orientations
    @orientations = Personne.find(:all, :order=>"initiale", :conditions=>["type_personne_id = 5"])
    # Liste des liens entre un desiderata et un domaine
    @liens_assistants_desideratas = PersonneDesiderata.find(:all)
    
    # Recherche de l'unité concernée par le desiderata
    unite_concernee = @desiderata.unite_enseignement 
      
    # Constante concernée par l'unité
    constante_concernee = self.facteur_actuel("Facteur_#{unite_concernee.type_unite_enseignement.type_unite}",unite_concernee.periode.annee_academique_id)

    # Nombre de périodes en fonction du taux
    @nombre_periodes_pratiques = (unite_concernee.nbPeriodesPratiquesSemaine.to_f * @desiderata.tauxPratique.to_f * unite_concernee.periode.dureeSemaine.to_f * @desiderata.nbGroupes.to_f)/100.0
    @charge_pratique = @nombre_periodes_pratiques * constante_concernee.valeur.to_f
    @nombre_periodes_theoriques = (unite_concernee.nbPeriodesTheoriquesSemaine.to_f * @desiderata.tauxTheorique.to_f * unite_concernee.periode.dureeSemaine.to_f * @desiderata.nbGroupes.to_f)/100.0
    @charge_theorique = @nombre_periodes_theoriques * constante_concernee.valeur.to_f

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @desiderata }
    end
  end

  # GET /desideratas/new
  # GET /desideratas/new.xml
  def new
    @desiderata = Desiderata.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @desiderata }
    end
  end

  # GET /desideratas/1/edit
  def edit
    @desiderata = Desiderata.find(params[:id])
  end

  # POST /desideratas
  # POST /desideratas.xml
  def create
    @desiderata = Desiderata.new(params[:desiderata])

    respond_to do |format|
      if @desiderata.save
        flash[:notice] = 'Desiderata was successfully created.'
        format.html { redirect_to(@desiderata) }
        format.xml  { render :xml => @desiderata, :status => :created, :location => @desiderata }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @desiderata.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /desideratas/1
  # PUT /desideratas/1.xml
  def update
    @desiderata = Desiderata.find(params[:id])

    respond_to do |format|
      if @desiderata.update_attributes(params[:desiderata])
        flash[:notice] = 'Desiderata was successfully updated.'
        format.html { redirect_to(@desiderata) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @desiderata.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /desideratas/1
  # DELETE /desideratas/1.xml
  def destroy
    
    @desiderata = Desiderata.find(params[:id])
    # Liste de toutes les personnes
    personnes = Personne.find(:all)
    # Propriétaire du désidérata
    @personne = Personne.new
    # Lien entre un desiderata et des assistants ou des orientations
    liens_assistant_desiderata = PersonneDesiderata.find(:all, :conditions => ["desiderata_id = :id",{:id => params[:id]}])
    
    # Trouve le propriétaire du désidérata
    for personne in personnes
      if (personne.id == @desiderata.personne_id)
        @personne = personne
      end
    end
    
    # Efface les liens entre un desiderata et des assistants ou des orientations
    for lien in liens_assistant_desiderata
      lien.destroy
    end
    
    @desiderata.destroy
    

    respond_to do |format|
      format.html { redirect_to("personnes/personnes/gestion_desideratas/#{@personne.id}") }
      format.xml  { head :ok }
    end
    
  end
  #--------------------------------------------------------------------------------------------------------
  # Nom: departement_mise_a_jour
  # But: Modification du contenu des listes déroulantes en fonction du département choisi
  #--------------------------------------------------------------------------------------------------------
  def departement_mise_a_jour
    
    # Unité nulle
    @unite_nulle = UniteEnseignement.new(:abreviation=>'Toutes')
    # Filière nulle
    @filiere_nulle = Filiere.new(:abreviation=>'Toutes')

   
    # Orientation nulle
    @orientation_nulle = Orientation.new(:abreviation=>'Toutes')
    
    # Id du département choisi
    id_departement = params['id_departement'].to_i
    
    # Si aucun département n'est souhaité
    if id_departement == "" || id_departement == "Tous" || id_departement == 0
      # Liste de toutes les filières
      @liste_filieres = Filiere.find(:all,:order=>'abreviation')
      # Liste de toutes les orientations
      @liste_orientations = Orientation.find(:all,:order=>'abreviation')
  

      # Liste de toutes les unités d'enseignement
      @liste_unites = UniteEnseignement.find(:all,:conditions=>{:reconductible=>1},:order=>'abreviation')
    # Si un département est choisi
    else
      # Liste des filières liées au département
      @liste_filieres = departement_selection(id_departement,"filiere")
      # Liste des orientations liées au département
      @liste_orientations = departement_selection(id_departement,"orientation")
      

      # Liste des unités liées au département
      @liste_unites = departement_selection(id_departement,"unite")
    end  
    
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: departement_selection
  # But: Création d'un tableau contenant soit toutes les filières, les orientations, les modules ou les 
  #      unités d'enseignement liés à l'id du département donné en paramètre
  # Pramètres : id_departement => id du département souhaité
  #             souhait => Variable donnant l'indication de quel tableau il faut retourner
  #--------------------------------------------------------------------------------------------------------
  def departement_selection(id_departement, souhait)
    
    # Liste des filieres liées au département souhaité
    filieres_departement = []
    # Liste des orientations liées au département souhaité
    orientations_departement = []
    


    # Liste des unités liées au département souhaité
    unites_departement = []
    # Itérateur
    i_filiere=0
    i_orientation=0
    i_unite=0

    # Liste de toutes les filieres
    filieres = Filiere.find(:all, :order=>'abreviation')
    # Liste de toutes les orientations
    orientations = Orientation.find(:all, :order=>'abreviation')
   
    # Liste de toutes les liaisons entre des orientations et des unites 
     unites_orientations=UniteEnseignementOrientation.find(:all)
    


    # Liste de toutes les unités
    unites = UniteEnseignement.find(:all,:conditions=>{:reconductible=>1},:order=>'abreviation')
    

    
    # Trouve toutes les filières, les orientations et les unités liées au département
    for filiere in filieres # Pour toutes les filieres
        # Trouve toutes les filières liées au département
        if filiere.departement_id== id_departement.to_i 
        
          # Cette filière fait partie du département
          filieres_departement[i_filiere]=filiere
          i_filiere+=1
          # On s'occupe du reste seulement si nécessaire
          if souhait != "filiere"
        
              for orientation in filiere.orientations  # Pour toutes les orientations de la filiere
                  # Cette orientation fait partie du département
                  orientations_departement[i_orientation]=orientation
                  i_orientation+=1
                  # On s'occupe du reste seulement si nécessaire
                  if souhait != "orientation"        
                    for unite in orientation.unite_enseignements # Pour toutes les liaisons entre une orientation et des modules
                      if unite.reconductible
                        unites_departement[i_unite]=unite
                        i_unite+=1
                      end
                    end
                  end
               end
            end
        end
    end

    # Enlève les doubles et trie le tableau avant de le retourner
    case(souhait)
      when "filiere"
        return triage(self.enleve_doublons(filieres_departement))
      when "orientation"
        return triage(self.enleve_doublons(orientations_departement))
      when "unite"
        return triage(self.enleve_doublons(unites_departement))
    end

  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: filiere_mise_a_jour
  # But: Modification du contenu des listes déroulantes en fonction de la filière choisie 
  #--------------------------------------------------------------------------------------------------------
  def filiere_mise_a_jour
    
    # Orientation nulle
    @orientation_nulle = Orientation.new(:abreviation=>'Toutes')
    
    # Unité nulle
    @unite_nulle = UniteEnseignement.new(:abreviation=>'Toutes')
    
    # Id de la filière choisie
    id_filiere = params['id_filiere'].to_i
    
    # Si aucune filière n'est souhaitée
    if id_filiere == "" || id_filiere == "Toutes" || id_filiere == 0
        # Liste de toutes les orientations
        @liste_orientations = Orientation.find(:all,:order=>'abreviation')

        # Liste de toutes les unités d'enseignement
        @liste_unites = UniteEnseignement.find(:all,:conditions=>{:reconductible=>1},:order=>'abreviation')
        
    
    else # Si une filière est choisie
      # Liste des orientations liées à la filière
      @liste_orientations = filiere_selection(id_filiere,"orientation")
      # Liste des unités liées à la filière
      @liste_unites = filiere_selection(id_filiere,"unite")
    end  
    
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: filiere_selection
  # But: Création d'un tableau contenant soit toutes les orientations, les 
  #      unités d'enseignement liés à l'id de la filière donnée en paramètre
  # Pramètres : id_filiere => id de la filière souhaitée
  #             souhait => Variable donnant l'indication de quel tableau il faut retourner
  #--------------------------------------------------------------------------------------------------------
  def filiere_selection(id_filiere, souhait)

    # Liste des orientations liées à la filière souhaitée
    orientations_filiere = []
    
    # Liste des unités liées à la filière souhaitée
    unites_filiere = []
    
    # Itérateurs
    i_orientation=0
    i_unite=0

    # Liste de toutes les orientations
    orientations = Orientation.find(:all, :order=>'abreviation')

    # Liste de toutes les unités
    unites = UniteEnseignement.find(:all,:conditions=>{:reconductible=>1},:order=>'abreviation')
    

    # Trouve toutes les orientations et les unités liées à la filière
    for orientation in orientations # Pour toutes les orientations
        # Trouve toutes les orientations liées à la filière
        if orientation.filiere_id.to_i == id_filiere.to_i

            # Cette orientation fait partie du département
            orientations_filiere[i_orientation]=orientation
            i_orientation+=1
            # On s'occupe du reste seulement si nécessaire
            if souhait != "orientation"
                for unite in orientation.unite_enseignements # Pour toutes les liaisons entre une orientation et des modules
                  if unite.reconductible
                    unites_filiere[i_unite]=unite
                    i_unite+=1
                  end
                end
          end
       end
    end
        
  
    # Enlève les doubles et trie le tableau avant de le retourner
    case(souhait)
      when "orientation"
        return triage(self.enleve_doublons(orientations_filiere))
      when "unite"
        return triage(self.enleve_doublons(unites_filiere))
    end

  end

  #--------------------------------------------------------------------------------------------------------
  # Nom: orientation_mise_a_jour
  # But: Modification du contenu des listes déroulantes en fonction de l'orientation choisie 
  #--------------------------------------------------------------------------------------------------------
  def orientation_mise_a_jour
    
    # Module nul
    @module_nul = ModuleUnite.new(:abreviation=>'Tous')
    # Unité nulle
    @unite_nulle = UniteEnseignement.new(:abreviation=>'Toutes')
    
    # Id de l'orientation choisie
    id_orientation = params['id_orientation'].to_i
    
    # Si aucune orientation n'est souhaitée
    if id_orientation == "" || id_orientation == "Toutes" || id_orientation == 0
      

      # Liste de toutes les unités d'enseignement
      @liste_unites = UniteEnseignement.find(:all,:conditions=>{:reconductible=>1},:order=>'abreviation')
      
    # Si une orientation est choisie
    else

      # Liste des unités liées à la filière
      @liste_unites = orientation_selection(id_orientation,"unite")
    end  
    
  end
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: orientation_selection
  # But: Création d'un tableau contenant soit tous les modules ou soit toutes les 
  #      unités d'enseignement liés à l'id de l'orientation donnée en paramètre
  # Pramètres : id_orientation => id de l'orientation souhaitée
  #             souhait => Variable donnant l'indication de quel tableau il faut retourner
  #--------------------------------------------------------------------------------------------------------
  def orientation_selection(id_orientation, souhait)

    # Liste des unités liées à l'orientation souhaitée
    unites_orientation = []
    
    # Itérateur
     i_unite=0

     orientation=Orientation.find(:first, :conditions=>{:id=>id_orientation})
     for unite in orientation.unite_enseignements
       if unite.reconductible
         unites_orientation[i_unite]=unite
         i_unite+=1
       end
     end


    # Enlève les doubles et trie le tableau avant de le retourner
    case(souhait)
      when "unite"
        return triage(self.enleve_doublons(unites_orientation))
    end

  end
  
  
  #--------------------------------------------------------------------------------------------------------
  # Nom: triage
  # But: Trie le tableau dans l'ordre alphabétique
  #--------------------------------------------------------------------------------------------------------
  def triage(liste)
    
    tab_en_ordre = false # A t'on fini de le trier ?
    taille = liste.size # Taille du tableau
    i=0
    
    # Tant que le triage est en cours
    while(!tab_en_ordre)
        tab_en_ordre = true
        # On remonte l'élément le plus grand
        while(i < taille-1)
            if(liste[i].abreviation > liste[i+1].abreviation )
                # Inversion de l'élément
                tmp = liste[i]
                liste[i] = liste[i+1]
                liste[i+1] = tmp
                tab_en_ordre = false
            end
            i+=1
        end
        taille-=1
        i=0
    end
    return liste

  end
  
end

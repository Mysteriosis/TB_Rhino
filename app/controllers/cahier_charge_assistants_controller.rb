# -*- encoding : utf-8 -*-
class CahierChargeAssistantsController < ApplicationController

  def show
    @cahier = CahierChargeAssistant.find(params[:id])
    @personne = @cahier.personne
    year = @cahier.annee_civile.annee
    start_date_of_year = year + "-01-01"
    end_date_of_year = year + "-12-31"
    @contrats = Contrat.find_by_sql ["SELECT * FROM contrats WHERE ((end_date IS ? AND start_date <= ?) OR (end_date >= ? AND start_date <= ?)) AND personne_id = ? ORDER BY start_date", nil, end_date_of_year, start_date_of_year, end_date_of_year, @personne.id]
    
    @nb_contrats= @contrats.length
    if @nb_contrats > 0
      # 100% = 1870
      pourcentage_total = 0
      for contrat in @contrats
        pourcentage_total += contrat.pourcentage
      end
    
      @heures_total = (1870 * pourcentage_total) / 100
      @heures_rab = @heures_total - @cahier.heures_ens - @cahier.heures_infra - @cahier.heures_RD
      @heures_totales_attribuees = @cahier.heures_ens + @cahier.heures_infra + @cahier.heures_RD
    
      @boite_total = 600
    
    
      @boite_ens    = (@boite_total * @cahier.heures_ens) / @heures_total
      @boite_rd     = (@boite_total * @cahier.heures_RD) / @heures_total
      @boite_infra  = (@boite_total * @cahier.heures_infra) / @heures_total
      @boite_rab    = (@boite_total * @heures_rab) / @heures_total
    end
  end

  def history
    @personne = Personne.find(params[:id])
    @cahiers = CahierChargeAssistant.find(:all, :conditions=>{:personne_id => params[:id]}, :order => "annee_civile_id")
  end
  
  def edit
    @cahier = CahierChargeAssistant.find(params[:id])
    @personne = @cahier.personne
    
    year = @cahier.annee_civile.annee
    start_date_of_year = year + "-01-01"
    end_date_of_year = year + "-12-31"
    @contrats = Contrat.find_by_sql ["SELECT * FROM contrats WHERE (end_date IS ? OR end_date <= ?) AND personne_id = ? ORDER BY start_date", nil, end_date_of_year, @personne.id]
    
    # 100% = 1870
    pourcentage_total = 0
    for contrat in @contrats
      pourcentage_total += contrat.pourcentage
    end
    
    @heures_total = (1870 * pourcentage_total) / 100
    @heures_rab = @heures_total - @cahier.heures_ens - @cahier.heures_infra - @cahier.heures_RD
    
  end
  
  def update
    @personne = Personne.find(params[:personne_id])
    @cahier = CahierChargeAssistant.find(params[:id])

    if @cahier.update_attributes(params[:cahier_charge_assistant])
      redirect_to([@personne, @cahier])
    else
      error = 'Erreur dans le formulaire<br/><br/>'
      @cahier.errors.each_full { |msg| error = error + msg + "<br/>" }
      flash[:notice2] = error
      redirect_to :controller => "cahier_charge_assistants", :action => :edit, :id => @cahier.id, :personne_id => @personne.id
    end
  end

  def new
    @cahier = CahierChargeAssistant.new
    @personne = Personne.find(params[:id])
    @annee = AnneeCivile.find(:all)
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cahier }
    end
  end
  
  def heures_modifiees
  end
    
  def create
   @cahier = CahierChargeAssistant.new(params[:cahier_charge_assistant])
    respond_to do |format|
      if @cahier.save
        flash[:notice] = 'Cahier des charges was successfully created.'
        format.html { redirect_to(@cahier) }
        format.xml  { render :xml => @cahier, :status => :created, :location => @cahier }
      else
        error = 'Erreur dans le formulaire<br/><br/>'
        @cahier.errors.each_full { |msg| error = error + msg + "<br/>" }
        flash[:notice2] = error
        redirect_to :controller => "cahier_charge_assistants", :action => "new", :id => @cahier.personne.id
      end
    end
  end
  
end



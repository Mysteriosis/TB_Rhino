# -*- encoding : utf-8 -*-
class ChargeEffectivesController < ApplicationController
  
  # Activation de l'authentification
  #before_fileter :activate_authlogic 
  
  ####
  # Affichage de l'ensemble des charges pour affectation professeur - assistant
  ####
  def index
    # Liste de toutes les unités d'enseignement pour les années en cours
    @liste_unites = UniteEnseignement.unites_annee_souhaitee(AnneeAcademique.courante.id)
    # Sélection des charges effectives
    @charge_effectives = ChargeEnseignement.find(:all, :conditions => ["unite_enseignement_id IN (?)", @liste_unites])
    #Desiderata pour les unités de l'année
    @desideratas = Desiderata.find(:all, :conditions => ["unite_enseignement_id IN (?)", @liste_unites])

    # Controle du nombre d'unité déjà attribuée
    if @charge_effectives.size() >= @desideratas.size()
      # Calcul des états des charges
      @unites_avertissement = []
      nombre_unite= 0
      # Parcours de la liste des unités
      for unite in @liste_unites
        # Nombre de groupe attribué pour l'unité
        total_attribue = 0
        # Nombre de groupe total de l'unité
        total_unite = unite.nbGroupes
        
        # On regarde s'il y a une charge pour cette unite
        @charges = ChargeEnseignement.find(:all, :conditions => {:unite_enseignement_id => unite.id})
        #Pour chaque charge on calcul le nombre de groupe attribué en fonction du nombre de groupe de l'unité
        for charge in @charges
          assistanat = PersonneChargeEnseignement.find(:first, :conditions=>{:charge_enseignement_id => charge.id})
          if assistanat == nil
            total_attribue += 0
          else
            total_attribue += charge.nbGroupes
          end
        end
        # S'il n'y a pas de charge pour ce cours, alors lui attribuer une class d'affichage spécifique
        if total_attribue == 0
          @unites_avertissement[nombre_unite] = "Aucun"
        # Si le cours a des charges mais pas pour 100%, lui attribuer une class d'affichage spécifique
        elsif total_attribue < total_unite
          @unites_avertissement[nombre_unite] = "PasAssez"
        # SI le cours a toutes ces charges, lui attribuer une class d'affichage spécifique
        else
          @unites_avertissement[nombre_unite] = "Complet"
        end
        nombre_unite += 1
      end
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @charge_effectives }
    end
  end
  
  ####
  # Chargement des unités et des charges
  ####
  def load
    # Liste de toutes les unités d'enseignement pour les années en cours
    @liste_unites = UniteEnseignement.unites_annee_souhaitee(AnneeAcademique.annee_civile_actuelle.id)
  end
  
  ####
  # Transformation des désideratas en charge d'enseignement
  ####
  def transformer_en_charge
    # Liste de toutes les unités d'enseignement pour les années en cours
    @liste_unites = AnneeAcademique.order(:annee).unites_annee_souhaitee(AnneeAcademique.courante.id)
    # Selctionner les desideratas correspondant
    @desideratas = Desiderata.find(:all, :conditions=>["unite_enseignement_id IN (?)", @liste_unites], :order => :unite_enseignement_id)
    
    # id unite précédente afin d'identifier les unités ayant plusieurs groupes
    id_unite_precedente = -1
    groupe = 1
    
    # Pour tous les désideratas
    for desiderata in @desideratas
      #Liste des collaborateurs associés à chaque désiderata
      collaborateurs = PersonneDesiderata.find(:all, :conditions=>{:desiderata_id => desiderata.id})
      
      # Création de la charge d'enseignement pour l'unité
      chargeEnseignement = ChargeEnseignement.create( :tauxTheorique  => desiderata.tauxTheorique,
                                                      :tauxPratique   => desiderata.tauxPratique,
                                                      :nbGroupes      => desiderata.nbGroupes,
                                                      :personne_id    => desiderata.personne_id,
                                                      :unite_enseignement_id => desiderata.unite_enseignement.id,
                                                      :numGroupe => 1,
                                                      :desiderata_id  => desiderata.id)

      # Si l'unité à déjà une charge d'enseignement
      if desiderata.unite_enseignement_id == id_unite_precedente
        # Incrémenter le nombre de groupe
        groupe += 1
      else
        # Sauvegarde la nouvelle unité de référence
        id_unite_precedente = desiderata.unite_enseignement.id
        # Réinitialiser la nombre de groupe
        groupe = 1
      end
      chargeEnseignement.numGroupe = groupe
      # Enregistrer la charge d'enseignement
      chargeEnseignement.save
    end
    redirect_to :action => "index"
  end
  
  ####
  # Mise à jour de la liste des collaborateurs en fonction du cours et du groupe sélectionné
  ####
  def maj_coll

    # Liste des assistants
    @collaborateurs = Personne.find(:all, :conditions=>{:type_personne_id => "1"}, :order => :initiale)
    # Cahier des charges des assistants
    @cahier_des_charges = CahierChargeAssistant.find(:all, :conditions=>{:annee_civile_id => AnneeAcademique.order(:annee).annee_civile_actuelle.id})
    
    @heure_affectee = []
    @largeur_heure = []
    
    #Desiderata en question s'il existe
    @desiderata = Desiderata.find(:first, :conditions => {:unite_enseignement_id => params[:id], :personne_id => params["prof_id"]})
    
    #Tableau des assistants
    @assistants_gris_ardent     = []
    @assistants_gris_moyen      = []
    @assistants_gris_pale       = []
    @assistants_jaune_ardent    = []
    @assistants_jaune_moyen     = []
    @assistants_jaune_pale      = []
    @assistants_orange_ardent   = []
    @assistants_orange_moyen    = []
    @assistants_orange_pale     = []
    @assistants_vert_ardent     = []
    @assistants_vert_moyen      = []
    @assistants_vert_pale       = []

    # Parcours de l'ensemble des collaborateurs
    for collaborateur in @collaborateurs
      #Liste des projets affecte pour le collaborateur
      @projets_affecte = Projet.find(:all, :conditions=>{:personne_id => collaborateur.id})
      @heure_affectee[collaborateur.id] = 0
      for projet in @projets_affecte
        @heure_affectee[collaborateur.id] += projet.budgetHorairePlanifie.to_i
      end
      cahier = CahierChargeAssistant.find(:first, :conditions=>{:personne_id => collaborateur.id,:annee_civile_id => AnneeAcademique.order(:annee).annee_civile_actuelle.id})
      if cahier != nil
        @largeur_heure[collaborateur.id] = (@heure_affectee[collaborateur.id] * 285) / cahier.heures_ens
      else
        @largeur_heure[collaborateur.id] = 0
      end
      
      # Liste des intérets des collaborateurs pour l'unité en question
      @interet = CompetencePersonneUnite.find(:first, :conditions => {:unite_enseignement_id => params[:id], :personne_id => collaborateur.id})
      # Si l'unité n'a pas de désiderata 
      if @desiderata != nil
        # Liste des assistants désiré
        @prioriteAssistant = PersonneDesiderata.find(:first, :conditions =>{:desiderata_id => @desiderata.id, :personne_id => collaborateur.id})
      end
      
      # Si l'assistant n'a pas d'intéret pour l'unité  -> gris
      if @interet == nil
        # Si l'assistant ne fait pas partie du desiderata du prof -> pale
        if @desiderata == nil
          @assistants_gris_pale[@assistants_gris_pale.size()] = collaborateur
        else
          # Si l'assistant n'est pas de priorité 1 ou 2 ou 3 -> pale
          if @prioriteAssistant == nil
            @assistants_gris_pale[@assistants_gris_pale.size()] = collaborateur
          else
            # Si l'assistant est de priorité 1 -> ardent
            if @prioriteAssistant.priorite == 1
              @assistants_gris_ardent[@assistants_gris_ardent.size()] = collaborateur
            # Si l'assistant est de priorité 2 ou 3 -> moyen  
            elsif @prioriteAssistant.priorite == 2 or @prioriteAssistant.priorite == 3
              @assistants_gris_moyen[@assistants_gris_moyen.size()] = collaborateur
            else
              # Si l'assistant ne fait pas partie du desiderata du prof -> pale
              @assistants_gris_pale[@assistants_gris_pale.size()] = collaborateur
            end
          end
        end
      else
        # Si l'assistant a un intéret de * pour l'unité  -> jaune
        if @interet.interet_unite.interet == "*"
          # Si l'assistant ne fait pas partie du desiderata du prof -> pale
          if @desiderata == nil
            @assistants_jaune_pale[@assistants_jaune_pale.size()] = collaborateur
          else
            # Si l'assistant n'est pas de priorité 1 ou 2 ou 3 -> pale
            if @prioriteAssistant == nil
              @assistants_jaune_pale[@assistants_jaune_pale.size()] = collaborateur
            else
              # Si l'assistant est de priorité 1 -> ardent
              if @prioriteAssistant.priorite == 1
                @assistants_jaune_ardent[@assistants_jaune_ardent.size()] = collaborateur
              # Si l'assistant est de priorité 2 ou 3 -> moyen  
              elsif @prioriteAssistant.priorite == 2 or @prioriteAssistant.priorite == 3
                @assistants_jaune_moyen[@assistants_jaune_moyen.size()] = collaborateur
              else
                # Si l'assistant ne fait pas partie du desiderata du prof -> pale
                @assistants_jaune_pale[@assistants_jaune_pale.size()] = collaborateur
              end
            end
          end
        # Si l'assistant a un intéret de ** pour l'unité  -> orange
        elsif @interet.interet_unite.interet == "**"
          # Si l'assistant ne fait pas partie du desiderata du prof -> pale
          if @desiderata == nil
            @assistants_orange_pale[@assistants_orange_pale.size()] = collaborateur
          else
            # Si l'assistant n'est pas de priorité 1 ou 2 ou 3 -> pale
            if @prioriteAssistant == nil
              @assistants_orange_pale[@assistants_orange_pale.size()] = collaborateur
            else
              # Si l'assistant est de priorité 1 -> ardent
              if @prioriteAssistant.priorite == 1
                @assistants_orange_ardent[@assistants_orange_ardent.size()] = collaborateur
              # Si l'assistant est de priorité 2 ou 3 -> moyen  
              elsif @prioriteAssistant.priorite == 2 or @prioriteAssistant.priorite == 3
                @assistants_orange_moyen[@assistants_orange_moyen.size()] = collaborateur
              else
                # Si l'assistant ne fait pas partie du desiderata du prof -> pale
                @assistants_orange_pale[@assistants_orange_pale.size()] = collaborateur
              end
            end
          end
        # Si l'assistant a un intéret de *** pour l'unité  -> vert
        elsif @interet.interet_unite.interet == "***"
          # Si l'assistant ne fait pas partie du desiderata du prof -> pale
          if @desiderata == nil
            @assistants_vert_pale[@assistants_vert_pale.size()] = collaborateur
          else
            # Si l'assistant n'est pas de priorité 1 ou 2 ou 3 -> pale
            if @prioriteAssistant == nil
              @assistants_vert_pale[@assistants_vert_pale.size()] = collaborateur
            else
              # Si l'assistant est de priorité 1 -> ardent
              if @prioriteAssistant.priorite == 1
                @assistants_vert_ardent[@assistants_vert_ardent.size()] = collaborateur
              # Si l'assistant est de priorité 2 ou 3 -> moyen  
              elsif @prioriteAssistant.priorite == 2 or @prioriteAssistant.priorite == 3
                @assistants_vert_moyen[@assistants_vert_moyen.size()] = collaborateur
              else
                # Si l'assistant ne fait pas partie du desiderata du prof -> pale
                @assistants_vert_pale[@assistants_vert_pale.size()] = collaborateur
              end
            end
          end
        end
      end
    end
  end

  ####
  # Déposer un collaborateur dans un cours
  ####
  def drop_collaborateur
    # l'assistant déposé dans la boite
    @collaborateur = Personne.find(:first, :conditions => { :id => params["coll"]})
    # Numero du groupe
    num_groupe = params[:groupe]
    unite = UniteEnseignement.find(:first, :conditions=>{:id => params[:unite]})
    # Retrouver la charge correspondante
    charge = ChargeEnseignement.find(:first, :conditions =>{:unite_enseignement_id => params[:unite], :personne_id => params['prof'], :numGroupe => num_groupe})
    # Ajouter la charge
    if charge == nil
      charge = ChargeEnseignement.new
      charge.tauxTheorique = 100
      charge.tauxPratique = 100
      charge.nbGroupes = 1
      charge.numGroupe = num_groupe
      charge.personne_id = params['prof']
      charge.unite_enseignement_id = params[:unite]
      charge.desiderata_id = 0
      charge.save
    end
    @charge_id = charge.id
    
    ##########################################################
    # Création ou mise à jour de l'enregistrement assistanat #
    ##########################################################
    assistanat = PersonneChargeEnseignement.find(:first, :conditions => {:charge_enseignement_id => charge.id})
    if assistanat == nil
      assistanat = PersonneChargeEnseignement.new
      assistanat.personne_id = @collaborateur.id
      assistanat.charge_enseignement_id = charge.id
      assistanat.save
    else
      assistanat.update_attribute(:personne_id, @collaborateur.id)
    end
    
    ##
    # Supprimer les projets concernant l'unité
    ##
    projets = Projet.find(:all, :conditions => {:unite_enseignement_id => params[:unite]})
    for projet in projets
      projet.destroy
    end

    ##
    # Gestion au niveau du projet du professeur
    ##
    projet = Projet.new
    projet.personne_id = params['prof']
    projet.unite_enseignement_id = params[:unite]
    projet.nom = unite.abreviation
    # A MODIFIER
    projet.type_projet_id = 4
    projet.statut_projet_id = 1
    projet.budgetHorairePlanifie = 0
    projet.save

    ##
    # Création du projet pour le collaborateur
    ##
    projet_coll = Projet.new
    projet_coll.personne_id = @collaborateur.id
    projet_coll.unite_enseignement_id = params[:unite]
    projet_coll.nom = unite.abreviation
    # A MODIFIER
    projet_coll.type_projet_id = 4
    projet_coll.statut_projet_id = 1
    projet_coll.budgetHorairePlanifie = charge.tauxPratique * charge.nbGroupes * unite.nbPeriodesPratiquesSemaine * 1.5 * 2.2
    projet_coll.save

    ##
    # Modification de la classe pour affichage
    ##
    # Nombre de groupe attribué pour l'unité
    total_attribue = 0
    # Nombre de groupe total de l'unité
    total_unite = unite.nbGroupes
    # On regarde s'il y a une charge pour cette unite
    @charges = ChargeEnseignement.find(:all, :conditions => {:unite_enseignement_id => unite.id})
    for charge in @charges
      assistanat = PersonneChargeEnseignement.find(:first, :conditions=>{:charge_enseignement_id => charge.id})
      if assistanat == nil
        total_attribue += 0
      else
        total_attribue += charge.nbGroupes
      end
    end
    # S'il n'y a pas de charge pour ce cours, alors lui attribuer une class d'affichage spécifique
    if total_attribue == 0
      @unites_avertissement = "Aucun"
    # Si le cours a des charges mais pas pour 100%, lui attribuer une class d'affichage spécifique
    elsif total_attribue < total_unite
      @unites_avertissement = "PasAssez"
    # SI le cours a toutes ces charges, lui attribuer une class d'affichage spécifique
    else
      @unites_avertissement = "Complet"
    end
    
  end
  
  def modification_prof
    # Numero du groupe
    num_groupe = params[:groupe]
    # Retrouver la charge correspondante
    charge = ChargeEnseignement.find(:first, :conditions =>{:unite_enseignement_id => params[:unite], :numGroupe => num_groupe})
    # Ajouter la charge s'il n'en n'existe aucune
    if charge == nil
      charge = ChargeEnseignement.new
      charge.tauxTheorique = 100
      charge.tauxPratique = 100
      charge.nbGroupes = 1
      charge.numGroupe = num_groupe
      charge.personne_id = params['id_prof']
      charge.unite_enseignement_id = params[:unite]
      charge.desiderata_id = 0
      charge.save
    else
      #mise à jour de la charge si elle existe
      charge.update_attribute(:personne_id, params['id_prof'])
    end
    
    ##
    # Gestion au niveau du projet du professeur
    ##
    projet = Projet.find(:first, :conditions =>{:budgetHorairePlanifie => 0, :unite_enseignement_id => params[:unite]})
    if projet == nil
      projet = Projet.new
      projet.personne_id = params['id_prof']
      projet.unite_enseignement_id = params[:unite]
      projet.nom = unite.abreviation
      # A MODIFIER
      projet.type_projet_id = 4
      projet.statut_projet_id = 1
      projet.budgetHorairePlanifie = 0
      projet.save
    else
      projet.update_attribute(:personne_id, params['id_prof'])
    end
  end
  
  ##
  # Effacer un collaborateur
  ##
  def del_coll
    # Récuperer la charge d'enseignemnt du collaborateur
    chargeAssistanat = PersonneChargeEnseignement.find(:first, :conditions => {:personne_id => params[:personne_id], :charge_enseignement_id => params[:charge_id]})
    # Détruire la charge du collaborateur
    chargeAssistanat.destroy
    
    # Récupérer la charge d'enseignement de l'unité
    charge = ChargeEnseignement.find(:first, :conditions=>{:id => params[:charge_id]})
    # Récupérer l'unité concernée
    @unite = UniteEnseignement.find(:first, :conditions=>{:id =>charge.unite_enseignement_id})
    
    # Nombre de groupe attribué pour l'unité
    total_attribue = 0
    # Nombre de groupe total de l'unité
    total_unite = @unite.nbGroupes
    # On regarde s'il y a une charge pour cette unite
    @charges = ChargeEnseignement.find(:all, :conditions => {:unite_enseignement_id => @unite.id})
    for charge in @charges
      assistanat = PersonneChargeEnseignement.find(:first, :conditions=>{:charge_enseignement_id => charge.id})
      if assistanat == nil
        total_attribue += 0
      else
        total_attribue += charge.nbGroupes
      end
    end
    # S'il n'y a pas de charge pour ce cours, alors lui attribuer une class d'affichage spécifique
    if total_attribue == 0
      @unites_avertissement = "Aucun"
    # Si le cours a des charges mais pas pour 100%, lui attribuer une class d'affichage spécifique
    elsif total_attribue < total_unite
      @unites_avertissement = "PasAssez"
    # SI le cours a toutes ces charges, lui attribuer une class d'affichage spécifique
    else
      @unites_avertissement = "Complet"
    end
    
    ##
    # Gestion au niveau du projet du collaborateur
    ##
    projets = Projet.find(:all, :conditions =>{:unite_enseignement_id => @unite.id})
    for projet in projets
      if projet.budgetHorairePlanifie != '0'
        projet.destroy
      end
    end
  end
  
  ##
  # Afficher le box de l'unité
  ## 
  def show_unite
    # Liste de toutes les unités d'enseignement pour les années en cours
    @liste_unites = AnneeAcademique.order(:annee).unites_annee_souhaitee(AnneeAcademique.courante.id)
    # Récupérer l'id de l'année courante
    unite_courante_id = params[:id]
    # Récupérer l'unité
    @unite = UniteEnseignement.find(:first, :conditions => {:id => unite_courante_id})
    # Récupérer les charges pour cette unité
    @charges = ChargeEnseignement.find(:all, :conditions => {:unite_enseignement_id => unite_courante_id})
    # Liste des professeurs
    @professeurs = Personne.find(:all, :conditions=>{:type_personne_id => "2"}, :order => :initiale)
    # Nombre de charge
    @nbr_charge = 0
    # Tableau des assistants chargé de cette unité
    @assistant_charge = []
    # S'il existe une charge pour cette unité
    if @charges != []
      # professeurs chargé de cette unité
      @professeurs_charge  = []
      # Stocker les id des charges
      @charge_id = []
      # Stocker le assistant
      @assistants       = []      
      i=1
      # Pour chaque charge
      for charge in @charges
        # S'il y a un desiderata pour cette charge
        if charge.desiderata_id != 0
          # on affiche le professeur ayant émis ce desiderata
          @professeurs_charge[i] = Personne.find(:first, :conditions=>{:id => charge.personne_id})
          @nbr_charge += 1
        end
          # Afficher les assitants chargé de cette charge s'il y en a
          chargeAssistanat = PersonneChargeEnseignement.find(:first, :conditions =>{:charge_enseignement_id => charge.id})
          if chargeAssistanat != nil
            @assistant_charge[i] = Personne.find(:first, :conditions=>{:id => chargeAssistanat.personne_id})
          else
            @assistant_charge[i] = nil
          end
          @charge_id[i] = charge.id
          i+=1
      end
    end
  end
  
end



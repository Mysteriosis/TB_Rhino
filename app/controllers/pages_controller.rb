# -*- encoding : utf-8 -*-
class PagesController < ApplicationController

  def index
     redirect_to(personne_path(current_user))
  end

  def enseignement
    
  end

  def configuration
    authorize! :manage, :configuration
    # Toutes les années académiques
    @annees_academiques = AnneeAcademique.find(:all, :order=>'annee')
  end
  
  def exportation_heures_sagex
    @date_debut = Date.today.at_beginning_of_month.strftime('%d.%m.%Y')
    @date_fin = Date.today.strftime('%d.%m.%Y')
  end
  
  def do_exportation_heures_sagex
    @date_debut = params[:exportation][:date_debut]
    @date_fin = params[:exportation][:date_fin]
    @avec_commentaires = params[:exportation][:avec_commentaires][1]=="Oui"
    begin
      d1 = Date.parse(@date_debut).to_time
      d2 = (Date.parse(@date_fin)+1.day-1.second).to_time
    rescue
      flash.now[:alert] = "Erreur : veuillez saisir des dates valides pour le début et la fin"
      render :action => "exportation_heures_sagex"
      return
    end
    
    respond_to do |format|
      format.html do
        tmpfilename = "tmp/xls_sagex_#{Time.now.to_i}.xls"
        PlageHoraireSaisie.fichier_excel_sagex(d1,d2, @avec_commentaires).write(tmpfilename)
        send_file tmpfilename, :filename => "rh_ash_sagex_#{d1.strftime('%d-%m-%Y')}_au_#{d2.strftime('%d-%m-%Y')}.xls"
      end
    end
    
  end

end

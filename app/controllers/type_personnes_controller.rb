# -*- encoding : utf-8 -*-
class TypePersonnesController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /type_personnes
  # GET /type_personnes.xml
  def index
    @type_personnes = TypePersonne.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_personnes }
    end
  end

  # GET /type_personnes/1
  # GET /type_personnes/1.xml
  def show
    @type_personne = TypePersonne.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_personne }
    end
  end

  # GET /type_personnes/new
  # GET /type_personnes/new.xml
  def new
    @type_personne = TypePersonne.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_personne }
    end
  end

  # GET /type_personnes/1/edit
  def edit
    @type_personne = TypePersonne.find(params[:id])
  end

  # POST /type_personnes
  # POST /type_personnes.xml
  def create
    @type_personne = TypePersonne.new(params[:type_personne])

    respond_to do |format|
      if @type_personne.save
        flash[:notice] = 'TypePersonne was successfully created.'
        format.html { redirect_to(@type_personne) }
        format.xml  { render :xml => @type_personne, :status => :created, :location => @type_personne }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_personne.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /type_personnes/1
  # PUT /type_personnes/1.xml
  def update
    @type_personne = TypePersonne.find(params[:id])

    respond_to do |format|
      if @type_personne.update_attributes(params[:type_personne])
        flash[:notice] = 'TypePersonne was successfully updated.'
        format.html { redirect_to(@type_personne) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_personne.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /type_personnes/1
  # DELETE /type_personnes/1.xml
  def destroy
    @type_personne = TypePersonne.find(params[:id])
    @type_personne.destroy

    respond_to do |format|
      format.html { redirect_to(type_personnes_url) }
      format.xml  { head :ok }
    end
  end
  
  # Retourne l'id du type enseignant
  def id_enseignant
    return first(:first, :conditions => {:typePersonnes => "Professeur"})
  end
  
end

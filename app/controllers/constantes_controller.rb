# -*- encoding : utf-8 -*-
class ConstantesController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /constantes
  # GET /constantes.xml
  def index
    
    # Liste de toutes les constantes
    @constantes= Constante.find(:all, :order=>'description')    
    
    # Liste des constantes des années académiques et civiles
    @constantes_annee_academique=retourne_constantes_annee(nil,true)
    @constantes_annee_civile=retourne_constantes_annee(nil,false)
    
    # Année académique actuelle
    @annee_academique = AnneeAcademique.courante
    # Année civile actuelle
    @annee_civile = AnneeCivile.courante
    # Liste de toutes les années civiles avec comme premier élément l'année actuelle
    @annees_civiles = AnneeCivile.order(:annee)
    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee)
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @constantes }
    end
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : retourne_constantes_annee
  # But : Retourne les constantes liées à l'année donnée en paramètre
  # Parametre : id_annee => id de l'année académique
  #             academique => True si l'on veut les années académiques
  #---------------------------------------------------------------------------------------------------
  def retourne_constantes_annee (id_annee, academique)
    # Liste des constantes
    if academique
      constantes = Constante.where(annee_civile_id: nil)
    else
      constantes = Constante.where(annee_academique_id: nil)
    end
    # Liste des constantes de l'année
    constantes_annee=[]
    i=0
    for constante in constantes
      if id_annee == nil
          constantes_annee[i]=constante
          i+=1
      else
        if academique
          if constante.annee_academique_id.to_i == id_annee.to_i
            constantes_annee[i]=constante
            i+=1
          end
        else
          if constante.annee_civile_id.to_i == id_annee.to_i
            constantes_annee[i]=constante
            i+=1
          end
        end
      end
    end
    return constantes_annee
  end

  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Met a jour les constantes en fonction des années
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    # Liste de toutes les constantes
    @constantes= Constante.find(:all, :order=>'description')
    # Liste des constantes des années académiques et civiles
    @constantes_annee_academique=retourne_constantes_annee(params[:id_annee_academique],true)
    @constantes_annee_civile=retourne_constantes_annee(params[:id_annee_civile],false)
  end

  # GET /constantes/1
  # GET /constantes/1.xml
  def show
    @constante = Constante.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @constante }
    end
  end

  # GET /constantes/new
  # GET /constantes/new.xml
  def new
    @constante = Constante.new
    
    # Liste de toutes les annnes académique
    @liste_annees_academiques = AnneeAcademique.find(:all, :order => 'annee') 
    # Création d'une liste de toutes les années académiques avec une possibilité de valeur nulle
    @annees_academiques = []
    annee_academique = AnneeAcademique.new(:annee=>'---')
    @annees_academiques[0] = annee_academique
    annees_academiques = 1
    for annees in @liste_annees_academiques
      @annees_academiques[annees_academiques] = annees
      annees_academiques=annees_academiques+1
    end
    
    # Liste de toutes les annnes civile
    @liste_annees_civiles = AnneeCivile.find(:all, :order => 'annee') 
    # Création d'une liste de toutes les années civiles avec une possibilité de valeur nulle
    @annees_civiles = []
    annee_civile = AnneeCivile.new(:annee=>'---')
    @annees_civiles[0] = annee_civile
    annees_civiles = 1
    for annees in @liste_annees_civiles
      @annees_civiles[annees_civiles] = annees
      annees_civiles=annees_civiles+1
    end

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @constante }
    end
  end

  # GET /constantes/1/edit
  def edit
    @constante = Constante.find(params[:id])
    
    # Liste de toutes les annnes académique
    @liste_annees_academiques = AnneeAcademique.find(:all, :order => 'annee') 
    # Création d'une liste de toutes les années académiques avec une possibilité de valeur nulle
    @annees_academiques = []
    annee_academique = AnneeAcademique.new(:annee=>'---')
    @annees_academiques[0] = annee_academique
    annees_academiques = 1
    for annees in @liste_annees_academiques
      @annees_academiques[annees_academiques] = annees
      annees_academiques=annees_academiques+1
    end
    
    # Liste de toutes les annnes civile
    @liste_annees_civiles = AnneeCivile.find(:all, :order => 'annee') 
    # Création d'une liste de toutes les années civiles avec une possibilité de valeur nulle
    @annees_civiles = []
    annee_civile = AnneeCivile.new(:annee=>'---')
    @annees_civiles[0] = annee_civile
    annees_civiles = 1
    for annees in @liste_annees_civiles
      @annees_civiles[annees_civiles] = annees
      annees_civiles=annees_civiles+1
    end 
  end

  # POST /constantes
  # POST /constantes.xml
  def create
    @constante = Constante.new(params[:constante])

    respond_to do |format|
      if @constante.save
        flash[:notice] = 'Constante was successfully created.'
        format.html { redirect_to(@constante) }
        format.xml  { render :xml => @constante, :status => :created, :location => @constante }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @constante.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /constantes/1
  # PUT /constantes/1.xml
  def update
    @constante = Constante.find(params[:id])

    respond_to do |format|
      if @constante.update_attributes(params[:constante])
        flash[:notice] = 'Constante was successfully updated.'
        format.html { redirect_to(@constante) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @constante.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /constantes/1
  # DELETE /constantes/1.xml
  def destroy
    @constante = Constante.find(params[:id])
    @constante.destroy

    respond_to do |format|
      format.html { redirect_to(constantes_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class SousTypeProjetsController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /sous_type_projets
  # GET /sous_type_projets.xml
  def index
    @sous_type_projets = SousTypeProjet.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @sous_type_projets }
    end
  end

  # GET /sous_type_projets/1
  # GET /sous_type_projets/1.xml
  def show
    @sous_type_projet = SousTypeProjet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @sous_type_projet }
    end
  end

  # GET /sous_type_projets/new
  # GET /sous_type_projets/new.xml
  def new
    @sous_type_projet = SousTypeProjet.new
    
    # Liste tous les types de projet
    @types_projet = TypeProjet.find(:all,:order=>'typeProjet')

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @sous_type_projet }
    end
  end

  # GET /sous_type_projets/1/edit
  def edit
    @sous_type_projet = SousTypeProjet.find(params[:id])
    
    # Liste tous les types de projet
    @types_projet = TypeProjet.find(:all,:order=>'typeProjet')
  end

  # POST /sous_type_projets
  # POST /sous_type_projets.xml
  def create
    @sous_type_projet = SousTypeProjet.new(params[:sous_type_projet])

    respond_to do |format|
      if @sous_type_projet.save
        flash[:notice] = 'SousTypeProjet was successfully created.'
        format.html { redirect_to(@sous_type_projet) }
        format.xml  { render :xml => @sous_type_projet, :status => :created, :location => @sous_type_projet }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @sous_type_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /sous_type_projets/1
  # PUT /sous_type_projets/1.xml
  def update
    @sous_type_projet = SousTypeProjet.find(params[:id])

    respond_to do |format|
      if @sous_type_projet.update_attributes(params[:sous_type_projet])
        flash[:notice] = 'SousTypeProjet was successfully updated.'
        format.html { redirect_to(@sous_type_projet) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @sous_type_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /sous_type_projets/1
  # DELETE /sous_type_projets/1.xml
  def destroy
    @sous_type_projet = SousTypeProjet.find(params[:id])
    @sous_type_projet.destroy

    respond_to do |format|
      format.html { redirect_to(sous_type_projets_url) }
      format.xml  { head :ok }
    end
  end
end

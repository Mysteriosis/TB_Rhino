# -*- encoding : utf-8 -*-
class OrientationsController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /orientations
  # GET /orientations.xml
  def index
    

    # Année actuelle
    @annee_academique = AnneeAcademique.courante
    
    # Liste des orientations
    #@orientations = Orientation.find_all_by_annee_academique_id(AnneeAcademique.courante, :order => 'abreviation')
    @orientations = Orientation.where(annee_academique_id: AnneeAcademique.courante).order('abreviation ASC')
    
    # Liste de toutes les années académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @orientations }
    end
  end
  
  #---------------------------------------------------------------------------------------------------
  # Nom : annee_mise_a_jour
  # But : Met a jour les orientations en fonction des annees academiques
  #---------------------------------------------------------------------------------------------------
  def annee_mise_a_jour
    # Liste de toutes les orientations concernant l'année souhaitée
    #@orientations = Orientation.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation')
    @orientations = Orientation.where(annee_academique_id: params['id_annee_academique'].to_i).order('abreviation ASC')
    # Liste de toutes les filières concernant l'année souhaitée
    #@filieres_annee = Filiere.find_all_by_annee_academique_id(params['id_annee_academique'].to_i, :order => 'abreviation') 
  end

  # GET /orientations/1
  # GET /orientations/1.xml
  def show
    @orientation = Orientation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @orientation }
    end
  end

  # GET /orientations/new
  # GET /orientations/new.xml
  def new
    @orientation = Orientation.new
    # Liste de toutes les filières concernant l'année actuelle
    #@filieres_annee = Filiere.find_all_by_annee_academique_id(AnneeAcademique.courante, :order => 'abreviation')
    @filieres_annee = Filiere.where(annee_academique_id: AnneeAcademique.courante).order('abreviation ASC')
    # Liste de toutes les annnes académiques avec comme premier élément l'année actuelle
    @annees_academiques = AnneeAcademique.order(:annee) 

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @orientation }
    end
  end

  # GET /orientations/1/edit
  def edit
    @orientation = Orientation.find(params[:id])
    # Liste de toutes les filières concernant l'année actuelle
    #@filieres_annee = Filiere.find_all_by_annee_academique_id(AnneeAcademique.courante, :order => 'abreviation')
    @filieres_annee = Filiere.where(annee_academique_id: AnneeAcademique.courante).order('abreviation ASC')   
    # Liste de toutes les annnes académiques avec comme premier élément l'année souhaitée
    @annees_academiques = self.liste_annees_choix(@orientation.annee_academique_id) 
  end

  # POST /orientations
  # POST /orientations.xml
  def create
    @orientation = Orientation.new(params[:orientation])

    respond_to do |format|
      if @orientation.save
        flash[:notice] = 'Orientation was successfully created.'
        format.html { redirect_to(@orientation) }
        format.xml  { render :xml => @orientation, :status => :created, :location => @orientation }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @orientation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /orientations/1
  # PUT /orientations/1.xml
  def update
    @orientation = Orientation.find(params[:id])

    respond_to do |format|
      if @orientation.update_attributes(params[:orientation])
        flash[:notice] = 'Orientation was successfully updated.'
        format.html { redirect_to(@orientation) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @orientation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /orientations/1
  # DELETE /orientations/1.xml
  def destroy
    @orientation = Orientation.find(params[:id])
      
    # Suppression de tous les liens
    #for lien in liens
    #  lien.delete
    #end
    
    @orientation.delete

    respond_to do |format|
      format.html { redirect_to(orientations_url) }
      format.xml  { head :ok }
    end
  end
end

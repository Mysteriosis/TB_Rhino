# -*- encoding : utf-8 -*-
class TypeProjetsController < ApplicationController

  load_and_authorize_resource
  
  before_filter :set_form_data, :only => [:new, :create, :edit, :update]

  # GET /type_projets
  # GET /type_projets.xml
  def index
    @type_projets_racines = TypeProjet.racines.order(:nom)
    respond_with(@type_projets_racines)
  end

  # GET /type_projets/1
  # GET /type_projets/1.xml
  def show
    @type_projet = TypeProjet.find(params[:id])
    respond_with(@type_projet)
  end

  # GET /type_projets/new
  # GET /type_projets/new.xml
  def new
    @type_projet = TypeProjet.new
    respond_with(@type_projet)
  end
  

  # GET /type_projets/1/edit
  def edit
    @type_projet = TypeProjet.find(params[:id])
  end

  # POST /type_projets
  # POST /type_projets.xml
  def create
    @type_projet = TypeProjet.new(params[:type_projet])
    @type_projet.save
    respond_with(@type_projet)
  end

  # PUT /type_projets/1
  # PUT /type_projets/1.xml
  def update
    @type_projet = TypeProjet.find(params[:id])
    @type_projet.update_attributes(params[:type_projet])
    respond_with(@type_projet)
  end

  # DELETE /type_projets/1
  # DELETE /type_projets/1.xml
  def destroy
    @type_projet = TypeProjet.find(params[:id])
    @type_projet.destroy
    respond_with(@type_projet)
  end
  
  private
  
  def set_form_data
    @modes_calcul_charge = TypeProjet.modes_calcul_charge
  end
  
end

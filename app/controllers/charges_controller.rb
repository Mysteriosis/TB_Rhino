# -*- encoding : utf-8 -*-
class ChargesController < ApplicationController
 
  load_and_authorize_resource

  before_filter :set_form_data, :only => [:index, :historique, :detail]
  
  # GET /charges
  # GET /charges.xml
  def index
    # Vue globale des charges 
    redirect_to charge_des_assistants_charges_path
  end

  def charge_des_assistants
  # Charges effectives et provisionnelles des assistants
    @personnes=Personne.collaborateurs_actifs_de_linstitut.sort_by(&:nom) 
    @personnes_fictives=Personne.collaborateurs_fictifs_institut.sort_by(&:initiale)
    @type_personnes='Collaborateurs'
    render :charges_des_personnes
  end

  def charge_des_professeurs
  # Charges effectives et provisionnelles des assistants
    @personnes=Personne.professeurs_actifs_de_linstitut.sort_by(&:nom) 
    @personnes_fictives=Personne.professeurs_fictifs_institut.sort_by(&:initiale) 
    @type_personnes='Professeurs'
    render :charges_des_personnes
  end

  def liste_eduardo
  # Affectation des UE (Unités d'enseignement)
    annee_academique = AnneeAcademique.courante
    departement = Departement.where(:abreviation => params[:departement], :annee_academique_id => annee_academique.id).first
    pour_semestre = params[:semestre]

    # Liste des unités d'enseignement de l'année actuelle
    if pour_semestre
      @semestre = pour_semestre
      liste_periodes_denseignement=Periode.where(:annee_academique_id => annee_academique.id, :descriptif => pour_semestre)
      unite_enseignements = UniteEnseignement.where({:periode_id => liste_periodes_denseignement.first.id, :reconductible => true})
    else
      @semestre = nil
      liste_periodes_denseignement = Periode.where({:annee_academique_id => annee_academique.id})
      unite_enseignements = UniteEnseignement.where({:periode_id.in => liste_periodes_denseignement.select(:id), :reconductible => true})
    end
    unite_enseignements_du_departement = unite_enseignements.select{|item| item.appartient_a_departement?(departement)}

    # Organisation par type d'enseignement (par semestre, masters, etc..)
    @liste_unites = Hash.new
    @liste_unites[departement.abreviation]=Hash.new
    liste_periodes_denseignement = liste_periodes_denseignement.select(:descriptif)
    liste_periodes_denseignement.each do |periode|
      unite_enseignements_du_departement_de_la_periode = unite_enseignements_du_departement.select{|item| item.periode.descriptif.eql?(periode.descriptif)}
      if unite_enseignements_du_departement_de_la_periode.size > 0
        @liste_unites[departement.abreviation][periode.descriptif]=Array.new
        unite_enseignements_du_departement_de_la_periode.sort_by(&:abreviation).each do |ue|
          @liste_unites[departement.abreviation][periode.descriptif] << ue
        end
      end
    end


  end

  def charge_professeurs_global
    @professeurs=Personne.professeurs_actifs_de_linstitut
  end

  # GET /charges/historique
  def historique
    # Vue de l'historique
    @professeurs = Personne.professeurs.order ("initiale")  
  end
  
  def get_prev_details
  # Pour obtenir le détail des charges provisionnelles d'une personne (assistant ou professeur)
    @personne = Personne.find(params[:personne_id])

    respond_to do |format|
      format.js do
        render :partial => "charges/charges_prev_dune_personne", :locals => {:personne => @personne, :avec_entete => true, :avec_nom_personne => false, :mode=> :read_only}
      end
    end  
  end

  def get_personne_details
  # Pour obtenir le détail des charges provisionnelles d'une personne (assistant ou professeur) à l'occasion de l'affichage des affectations
    @personne = Personne.find(params[:personne_id])

    respond_to do |format|
      format.js do
        render :partial => "charges/charges_prev_resumees_dune_personne", :locals => {:personne => @personne}
      end
    end  
  end

  def get_details
  # Pour obtenir le détail des charges d'une personne (assistant ou professeur) - Charges effectives et prévisionnelles
    @personne = Personne.find(params[:personne_id])

    respond_to do |format|
      format.js do
        render :partial => "charges/charges_dune_personne", :locals => {:personne => @personne, :avec_entete => true, :avec_nom_personne => false, :mode => :read_only}
      end
    end  
  end
  
  def update_effectiv_flag
    # Flip-flop du flag "est_effective" d'une charge d'assistanant ou d'enseignement
    charge_id = params[:charge_id]
    if params[:mode].eql?('Enseignement')
      charge=ChargeEnseignement.find_by_id(charge_id)
    else
      charge=ChargeAssistanat.find_by_id(charge_id)
    end
    charge.est_effective=!charge.est_effective?
    charge.save

    render :index
  end

def create_charge_effective
    # Crétion du projet et de la charge effective correspondant à une charge prévisionnelle
    # Le projet est créé si et seulement si n'existe pas déjà sous le même nom (id_interne)
    charge_id = params[:charge_id]
    if params[:mode].eql?('Enseignement')
      charge=ChargeEnseignement.find_by_id(charge_id)
      type_prj_bachelor=TypeProjet.where(:nom=>'Cours-Bachelor').first
      type_prj_master=TypeProjet.where(:nom=>'Cours-Master').first
    else
      charge=ChargeAssistanat.find_by_id(charge_id)
      type_prj_bachelor=TypeProjet.where(:nom=>'Labo-Bachelor').first
      type_prj_master=TypeProjet.where(:nom=>'Labo-Master').first
    end

    done = true
    groupe = charge.numGroupe
    projet=ProjetEnseignement.new
    projet.unite_enseignement=charge.unite_enseignement
    if projet.unite_enseignement.type_unite_enseignement.type_unite.eql?("Master")
      projet.type_projet=type_prj_master
    else
      projet.type_projet=type_prj_bachelor
    end
    projet.chef_id=charge.unite_enseignement.personne
    projet.id_interne=projet.identifiant_interne(groupe)
    projet.id_externe=projet.identifiant_externe(groupe)
    projet.date_debut=projet.unite_enseignement.periode.date_debut-30
    projet.date_fin=projet.unite_enseignement.periode.date_fin+15
    projet.annee_academique_realisation=AnneeAcademique.courante
    projet.est_actif=true
    
    heures_a_charge = (charge.heures_a_charge * 10).round.to_f / 10
    if !projet.save
      done=false
      flash[:notice]= "Note: Le projet #{projet.display_name} n'a pas été créé - #{projet.errors.first.last}"
      projet=Projet.where(:id_interne => projet.id_interne).first
      redirect_to new_personne_affectation_projet_path(charge.personne.id, {:projet_id => projet.id, :vient_de => :personne, :heures_a_charge => heures_a_charge})
    else
      redirect_to new_personne_affectation_projet_path(charge.personne.id, {:projet_id => projet.id, :vient_de => :personne, :heures_a_charge => heures_a_charge})
    end


  end

  def change_assistanat_reconnu
    unite_enseignement = UniteEnseignement.find(params[:ue]) 
    unite_enseignement.assistanat_reconnu = !unite_enseignement.assistanat_reconnu
    if !unite_enseignement.save
      flash[:error] = "Problème rencontré - Reconnaissance de l'assistanat inchangée"
      redirect_to liste_eduardo_charges_path(:departement => params[:departement])  
      return
    end
    flash[:notice] = "Reconnaissance de l'assistanat modifiée"
    redirect_to liste_eduardo_charges_path(:departement => params[:departement])  
  end

  def graphe_affectations
    render :partial => "graphe_affectations"
  end
  
  def graphe_affectations_data
    res_proj=[]
    annee_academique = AnneeAcademique.courante
    @personnes=Personne.collaborateurs_actifs_de_linstitut.sort_by(&:nom) 
    @personnes_fictives=Personne.collaborateurs_fictifs_institut.sort_by(&:initiale)

    @personnes_fictives.each do |personne|
        total_charge_previsionnelle=ChargeAssistanat.total_heures(personne, annee_academique.id)
        res_proj << {"label" => personne.initiale, "data" => total_charge_previsionnelle, "color" => "#FF5ADC"}
    end
   
    respond_to do |format|
      format.js do
        render :text => res_proj.to_json
      end
    end
  end


  # GET /charges/detail
  def detail
    # Vue détaillée des charges d'un professeur    
  end

  # GET /charges/1
  # GET /charges/1.xml
  def show
  end

  # GET /charges/new
  # GET /charges/new.xml
  def new
  end

  # GET /charges/1/edit
  def edit

  end

  # POST /charges
  # POST /charges.xml
  def create

  end

  # PUT /charges/1
  # PUT /charges/1.xml
  def update

  end

  # DELETE /charges/1
  # DELETE /charges/1.xml
  def destroy

  end
  
private

  def set_form_data
      @professeurs = Personne.professeurs.order ("initiale")
      @annee_civile_courante = AnneeCivile.courante
      @annee_academique_courante = AnneeAcademique.courante
      @charge_examen={}
      @professeurs.each do |p|
         @charge_examen[p]=Charge.charge_examen(p, @annee_civile_courante)
       end
  end
  
end

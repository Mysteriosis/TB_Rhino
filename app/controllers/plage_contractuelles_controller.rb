# -*- encoding : utf-8 -*-
class PlageContractuellesController < ApplicationController

load_and_authorize_resource 

def get_calendrier_details
# Pour obtenir le détail du calendrier académique
  @annee_civile = params[:annee_civile].to_i

  respond_to do |format|
    format.js do
      render :partial => "plage_contractuelles/calendrier_academique", :locals => {:annee_civile => @annee_civile}
    end
  end  
 end

def resume_heures
# Affiche le résumé des heures d'une personne pendant une certaine année civile
# Si l'année civile est sélectionnée dans le menu
    if params[:annee_civile]
      if params[:annee_civile][:annee]
        @annee_civile=AnneeCivile.where({:id => params[:annee_civile][:annee]}).first
        if !@annee_civile
          @annee_civile=AnneeCivile.courante
        end
      end
    else
      @annee_civile=AnneeCivile.courante
    end
    
    # Liste des années civiles à choix pour le menu
    @annees_civiles=AnneeCivile.liste_des_annees_civiles


  	@personne = Personne.find(params[:personne_id])

  	# Contrôler la mise à jour des plages contractuelles et des reports sur l'année suivante
  	@tableau_des_heures=PlageContractuelle.update_heures(@personne, @annee_civile.year) 

  	if !@tableau_des_heures
    	@tableau_des_heures=PlageContractuelle.tableau_des_heures(@personne, @annee_civile.year)
    end

    @info_heure=@personne.info_heures.where({:annee_civile => @annee_civile.year}).first
    @info_heure.report_heures_sup_impose=nil
    @info_heure.report_heures_vacances_impose=nil
end

def heures_supplementaires
# Le RH agit sur le report des heures supplémentaires pour l'ensemble des personnes (actives, non actives...)
    # Liste des années civiles à choix pour le menu
    # Liste des années civiles, sauf l'année courante (dernière année enlevée de la liste)
    @annees_civiles=AnneeCivile.liste_des_annees_civiles
    @annees_civiles.pop

end 

def annuler_heures_supplementaires
# Annulation du report des heures supplémentaires pour l'ensemble des personnes (actives, non actives...)
	if !(params[:annulation_heures_sup][:annee_civile]).eql?("")
		annee_civile=AnneeCivile.where({:id => params[:annulation_heures_sup][:annee_civile]}).first.annee.to_i + 1
		InfoHeure.where(:annee_civile => annee_civile).each do |info_heure|
			info_heure.report_heures_sup_impose=0
			info_heure.save!
		end
		InfoHeure.set_report_KO_des_annees_suivantes_pour_tous(annee_civile)
		flash[:notice] = "Annulation du report des heures supplémentaires de #{annee_civile-1} sur #{annee_civile} réalisée avec succès!"
	end
	redirect_to personnes_path
end 

def ajuster_report_heures_sup
# Le RH ajuste le report des heures supplémentaires  
	personne = Personne.find(params[:personne_id])
	annee_civile = AnneeCivile.where({:annee => params[:annee_civile]}).first
	info_heure= personne.info_heures.where({:annee_civile => annee_civile.year}).first

	if params[:info_heure]
		report_heures_sup_impose = params[:info_heure][:report_heures_sup_impose]
	end

	if report_heures_sup_impose.eql?("")
		info_heure.report_heures_sup_impose= nil
	else
		info_heure.report_heures_sup_impose= report_heures_sup_impose.to_d
	end
	info_heure.save!
	InfoHeure.set_report_KO_des_annees_suivantes(personne, annee_civile.annee.to_i)
	flash[:notice] = "Modification du report réalisé avec succès!"

	redirect_to resume_heures_plage_contractuelles_path({:personne_id => personne.id, :annee_civile => {:annee => annee_civile.id}})
end

def ajuster_report_heures_vacances
# Le RH ajuste le solde des vacances 
	personne = Personne.find(params[:personne_id])
	annee_civile = AnneeCivile.where({:annee => params[:annee_civile]}).first
	info_heure= personne.info_heures.where({:annee_civile => annee_civile.year}).first

	if params[:info_heure]
		report_heures_vacances_impose = params[:info_heure][:report_heures_vacances_impose]
	end

	if report_heures_vacances_impose.eql?("")
		info_heure.report_heures_vacances_impose= nil
	else
		info_heure.report_heures_vacances_impose= report_heures_vacances_impose.to_d
	end
	info_heure.save!
	InfoHeure.set_report_KO_des_annees_suivantes(personne, annee_civile.annee.to_i)
	flash[:notice] = "Modification du report réalisé avec succès!"
	redirect_to resume_heures_plage_contractuelles_path({:personne_id => personne.id, :annee_civile => {:annee => annee_civile.id}})
end


end
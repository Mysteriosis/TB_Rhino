# -*- encoding : utf-8 -*-
class AxeStrategiquesController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /axe_strategiques
  # GET /axe_strategiques.xml
  def index
    @axe_strategiques = AxeStrategique.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @axe_strategiques }
    end
  end

  # GET /axe_strategiques/1
  # GET /axe_strategiques/1.xml
  def show
    @axe_strategique = AxeStrategique.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @axe_strategique }
    end
  end

  # GET /axe_strategiques/new
  # GET /axe_strategiques/new.xml
  def new
    @axe_strategique = AxeStrategique.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @axe_strategique }
    end
  end

  # GET /axe_strategiques/1/edit
  def edit
    @axe_strategique = AxeStrategique.find(params[:id])
  end

  # POST /axe_strategiques
  # POST /axe_strategiques.xml
  def create
    @axe_strategique = AxeStrategique.new(params[:axe_strategique])

    respond_to do |format|
      if @axe_strategique.save
        flash[:notice] = 'Création réalisée avec succès'
        format.html { redirect_to(axe_strategiques_path) }
        format.xml  { render :xml => @axe_strategique, :status => :created, :location => @axe_strategique }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @axe_strategique.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /axe_strategiques/1
  # PUT /axe_strategiques/1.xml
  def update
    @axe_strategique = AxeStrategique.find(params[:id])

    respond_to do |format|
      if @axe_strategique.update_attributes(params[:axe_strategique])
        flash[:notice] = 'Mise à jour réalisée avec succès'
        format.html { redirect_to(axe_strategiques_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @axe_strategique.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /axe_strategiques/1
  # DELETE /axe_strategiques/1.xml
  def destroy
    @axe_strategique = AxeStrategique.find(params[:id])
    if !(@axe_strategique.suppression_empechements.nil?)
      flash[:alert] = "Impossible de supprimer cet axe: " + @axe_strategique.suppression_empechements
    else
      @axe_strategique.destroy
      flash[:notice] = 'Suppression réalisée avec succès'
    end
    respond_to do |format|
      format.html { redirect_to(axe_strategiques_url) }
      format.xml  { head :ok }
    end
  end
end

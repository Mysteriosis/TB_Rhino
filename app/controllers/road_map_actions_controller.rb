# -*- encoding : utf-8 -*-
class RoadMapActionsController < ApplicationController
  # GET /road_map_actions
  # GET /road_map_actions.xml
  def index
    @road_map_actions = RoadMapAction.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @road_map_actions }
    end
  end

  # GET /road_map_actions/1
  # GET /road_map_actions/1.xml
  def show
    @road_map_action = RoadMapAction.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @road_map_action }
    end
  end

  # GET /road_map_actions/new
  # GET /road_map_actions/new.xml
  def new
    @road_map_action = RoadMapAction.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @road_map_action }
    end
  end

  # GET /road_map_actions/1/edit
  def edit
    @road_map_action = RoadMapAction.find(params[:id])
  end

  # POST /road_map_actions
  # POST /road_map_actions.xml
  def create
    @road_map_action = RoadMapAction.new(params[:road_map_action])

    respond_to do |format|
      if @road_map_action.save
        flash[:notice] = 'RoadMapAction was successfully created.'
        format.html { redirect_to(@road_map_action) }
        format.xml  { render :xml => @road_map_action, :status => :created, :location => @road_map_action }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @road_map_action.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /road_map_actions/1
  # PUT /road_map_actions/1.xml
  def update
    @road_map_action = RoadMapAction.find(params[:id])

    respond_to do |format|
      if @road_map_action.update_attributes(params[:road_map_action])
        flash[:notice] = "Sauvegarde réussie de l'action Road Map"
        format.html { redirect_to(@road_map_action) }
        format.xml  { head :ok }
      else
        flash[:notice] = "Problème avec sauvegarde de l'action Road Map"
        format.html { render :action => "edit" }
        format.xml  { render :xml => @road_map_action.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /road_map_actions/1
  # DELETE /road_map_actions/1.xml
  def destroy
    @road_map_action = RoadMapAction.find(params[:id])
    @road_map_action.destroy

    respond_to do |format|
      format.html { redirect_to(road_map_actions_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class CompetenceDomainesController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /competence_domaines
  # GET /competence_domaines.xml
  def index
    @competence_domaines = CompetenceDomaine.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @competence_domaines }
    end
  end

  # GET /competence_domaines/1
  # GET /competence_domaines/1.xml
  def show
    @competence_domaine = CompetenceDomaine.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @competence_domaine }
    end
  end

  # GET /competence_domaines/new
  # GET /competence_domaines/new.xml
  def new
    @competence_domaine = CompetenceDomaine.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @competence_domaine }
    end
  end

  # GET /competence_domaines/1/edit
  def edit
    @competence_domaine = CompetenceDomaine.find(params[:id])
  end

  # POST /competence_domaines
  # POST /competence_domaines.xml
  def create
    @competence_domaine = CompetenceDomaine.new(params[:competence_domaine])

    respond_to do |format|
      if @competence_domaine.save
        flash[:notice] = 'CompetenceDomaine was successfully created.'
        format.html { redirect_to(@competence_domaine) }
        format.xml  { render :xml => @competence_domaine, :status => :created, :location => @competence_domaine }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @competence_domaine.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /competence_domaines/1
  # PUT /competence_domaines/1.xml
  def update
    @competence_domaine = CompetenceDomaine.find(params[:id])

    respond_to do |format|
      if @competence_domaine.update_attributes(params[:competence_domaine])
        flash[:notice] = 'CompetenceDomaine was successfully updated.'
        format.html { redirect_to(@competence_domaine) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @competence_domaine.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /competence_domaines/1
  # DELETE /competence_domaines/1.xml
  def destroy
    @competence_domaine = CompetenceDomaine.find(params[:id])
    @competence_domaine.destroy

    respond_to do |format|
      format.html { redirect_to(competence_domaines_url) }
      format.xml  { head :ok }
    end
  end
end

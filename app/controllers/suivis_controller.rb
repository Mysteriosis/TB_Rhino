# -*- encoding : utf-8 -*-
class SuivisController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /suivis
  # GET /suivis.xml
  def index
    @suivis = Suivi.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @suivis }
    end
  end

  # GET /suivis/1
  # GET /suivis/1.xml
  def show
    @suivi = Suivi.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @suivi }
    end
  end

  # GET /suivis/new
  # GET /suivis/new.xml
  def new
    @suivi = Suivi.new
    
    # Liste de tous les projets
    @projets = Projet.find(:all, :order=>'nomOutlook')
    # Liste de toutes les personnes
    @personnes = Personne.find(:all, :order=>'initiale')
    # Liste de toutes les catégories
    @activites = TypeActivite.find(:all, :order=>'typeActivite')
    
    # Liste des mois
    @mois = Moi.find(:all,:order=>'id')
    # Ajoute l'année au mois
    i=0
    for mois in @mois
      @mois[i].mois = @mois[i].mois+" - "+@mois[i].annee_civile.annee
      i=i+1
      mois=mois
    end

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @suivi }
    end
  end

  # GET /suivis/1/edit
  def edit
    @suivi = Suivi.find(params[:id])
    # Liste de tous les projets
    @projets = Projet.find(:all, :order=>'nomOutlook')
    # Liste de toutes les personnes
    @personnes = Personne.find(:all, :order=>'initiale')
    # Liste de toutes les catégories
    @activites = TypeActivite.find(:all, :order=>'typeActivite')
    
    # Liste des mois
    @mois = Moi.find(:all,:order=>'id')
    # Ajoute l'année au mois
    i=0
    for mois in @mois
      @mois[i].mois = @mois[i].mois+" - "+@mois[i].annee_civile.annee
      i=i+1
      mois=mois
    end
  end

  # POST /suivis
  # POST /suivis.xml
  def create
    @suivi = Suivi.new(params[:suivi])

    respond_to do |format|
      if @suivi.save
        flash[:notice] = 'Suivi was successfully created.'
        format.html { redirect_to(@suivi) }
        format.xml  { render :xml => @suivi, :status => :created, :location => @suivi }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @suivi.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /suivis/1
  # PUT /suivis/1.xml
  def update
    @suivi = Suivi.find(params[:id])

    respond_to do |format|
      if @suivi.update_attributes(params[:suivi])
        flash[:notice] = 'Suivi was successfully updated.'
        format.html { redirect_to(@suivi) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @suivi.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /suivis/1
  # DELETE /suivis/1.xml
  def destroy
    @suivi = Suivi.find(params[:id])
    @suivi.destroy

    respond_to do |format|
      format.html { redirect_to(suivis_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class EvenementPersonnesController < ApplicationController
  #before_filter :authorize
  #before_fileter :activate_authlogic
  # GET /evenement_personnes
  # GET /evenement_personnes.xml
  def index
    @evenement_personnes = EvenementPersonne.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evenement_personnes }
    end
  end

  # GET /evenement_personnes/1
  # GET /evenement_personnes/1.xml
  def show
    @evenement_personne = EvenementPersonne.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evenement_personne }
    end
  end

  # GET /evenement_personnes/new
  # GET /evenement_personnes/new.xml
  def new
    @evenement_personne = EvenementPersonne.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evenement_personne }
    end
  end

  # GET /evenement_personnes/1/edit
  def edit
    @evenement_personne = EvenementPersonne.find(params[:id])
  end

  # POST /evenement_personnes
  # POST /evenement_personnes.xml
  def create
    @evenement_personne = EvenementPersonne.new(params[:evenement_personne])

    respond_to do |format|
      if @evenement_personne.save
        flash[:notice] = 'EvenementPersonne was successfully created.'
        format.html { redirect_to(@evenement_personne) }
        format.xml  { render :xml => @evenement_personne, :status => :created, :location => @evenement_personne }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evenement_personne.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evenement_personnes/1
  # PUT /evenement_personnes/1.xml
  def update
    @evenement_personne = EvenementPersonne.find(params[:id])

    respond_to do |format|
      if @evenement_personne.update_attributes(params[:evenement_personne])
        flash[:notice] = 'EvenementPersonne was successfully updated.'
        format.html { redirect_to(@evenement_personne) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evenement_personne.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evenement_personnes/1
  # DELETE /evenement_personnes/1.xml
  def destroy
    @evenement_personne = EvenementPersonne.find(params[:id])
    @evenement_personne.destroy

    respond_to do |format|
      format.html { redirect_to(evenement_personnes_url) }
      format.xml  { head :ok }
    end
  end
end

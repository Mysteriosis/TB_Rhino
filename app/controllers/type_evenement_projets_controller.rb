# -*- encoding : utf-8 -*-
class TypeEvenementProjetsController < ApplicationController
  #before_fileter :activate_authlogic
  # GET /type_evenement_projets
  # GET /type_evenement_projets.xml
  def index
    @type_evenement_projets = TypeEvenementProjet.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_evenement_projets }
    end
  end

  # GET /type_evenement_projets/1
  # GET /type_evenement_projets/1.xml
  def show
    @type_evenement_projet = TypeEvenementProjet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_evenement_projet }
    end
  end

  # GET /type_evenement_projets/new
  # GET /type_evenement_projets/new.xml
  def new
    @type_evenement_projet = TypeEvenementProjet.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_evenement_projet }
    end
  end

  # GET /type_evenement_projets/1/edit
  def edit
    @type_evenement_projet = TypeEvenementProjet.find(params[:id])
  end

  # POST /type_evenement_projets
  # POST /type_evenement_projets.xml
  def create
    @type_evenement_projet = TypeEvenementProjet.new(params[:type_evenement_projet])

    respond_to do |format|
      if @type_evenement_projet.save
        flash[:notice] = 'TypeEvenementProjet was successfully created.'
        format.html { redirect_to(@type_evenement_projet) }
        format.xml  { render :xml => @type_evenement_projet, :status => :created, :location => @type_evenement_projet }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_evenement_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /type_evenement_projets/1
  # PUT /type_evenement_projets/1.xml
  def update
    @type_evenement_projet = TypeEvenementProjet.find(params[:id])

    respond_to do |format|
      if @type_evenement_projet.update_attributes(params[:type_evenement_projet])
        flash[:notice] = 'TypeEvenementProjet was successfully updated.'
        format.html { redirect_to(@type_evenement_projet) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_evenement_projet.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /type_evenement_projets/1
  # DELETE /type_evenement_projets/1.xml
  def destroy
    @type_evenement_projet = TypeEvenementProjet.find(params[:id])
    @type_evenement_projet.destroy

    respond_to do |format|
      format.html { redirect_to(type_evenement_projets_url) }
      format.xml  { head :ok }
    end
  end
end

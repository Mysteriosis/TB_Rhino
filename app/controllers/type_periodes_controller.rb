# -*- encoding : utf-8 -*-
class TypePeriodesController < ApplicationController
  #before_fileter :activate_authlogic
  load_and_authorize_resource
  # GET /type_periodes
  # GET /type_periodes.xml
  def index
    @type_periodes = TypePeriode.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @type_periodes }
    end
  end

  # GET /type_periodes/1
  # GET /type_periodes/1.xml
  def show
    @type_periode = TypePeriode.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_periode }
    end
  end

  # GET /type_periodes/new
  # GET /type_periodes/new.xml
  def new
    @type_periode = TypePeriode.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_periode }
    end
  end

  # GET /type_periodes/1/edit
  def edit
    @type_periode = TypePeriode.find(params[:id])
  end

  # POST /type_periodes
  # POST /type_periodes.xml
  def create
    @type_periode = TypePeriode.new(params[:type_periode])

    respond_to do |format|
      if @type_periode.save
        flash[:notice] = 'TypePeriode was successfully created.'
        format.html { redirect_to(@type_periode) }
        format.xml  { render :xml => @type_periode, :status => :created, :location => @type_periode }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_periode.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /type_periodes/1
  # PUT /type_periodes/1.xml
  def update
    @type_periode = TypePeriode.find(params[:id])

    respond_to do |format|
      if @type_periode.update_attributes(params[:type_periode])
        flash[:notice] = 'TypePeriode was successfully updated.'
        format.html { redirect_to(@type_periode) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_periode.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /type_periodes/1
  # DELETE /type_periodes/1.xml
  def destroy
    @type_periode = TypePeriode.find(params[:id])
    @type_periode.destroy

    respond_to do |format|
      format.html { redirect_to(type_periodes_url) }
      format.xml  { head :ok }
    end
  end
end

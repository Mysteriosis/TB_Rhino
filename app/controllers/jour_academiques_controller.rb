# -*- encoding : utf-8 -*-
class JourAcademiquesController < ApplicationController
    
  load_and_authorize_resource

  # GET /jour_academiques
  # GET /jour_academiques.xml
  def index
    redirect_to index_pour_annee_jour_academiques_path
  end

  def index_pour_annee

    # Si l'année civile est sélectionnée dans le menu
    if params[:annee_civile]
      if params[:annee_civile][:annee]
        @annee_civile=AnneeCivile.where{id.eq params[:annee_civile][:annee]}.first
        if !@annee_civile
          @annee_civile=AnneeCivile.courante
        end
      end
    else
      @annee_civile=AnneeCivile.courante
    end
    
    # Liste des années civiles à choix pour le menu, avec l'année civile prochaine en plus
    @annees_civiles=AnneeCivile.liste_des_annees_civiles
    @annees_civiles.push(AnneeCivile.prochaine)

    premier_jour_annee=Date.new(@annee_civile.annee.to_i, 1, 1)
    dernier_jour_annee=Date.new(@annee_civile.annee.to_i, 12, 31)
    @jour_academiques = JourAcademique.where{(date.gteq premier_jour_annee) & (date.lteq dernier_jour_annee)}.sort_by(&:date)
    respond_with(@jour_academiques)
  end


  # GET /jour_academiques/1
  # GET /jour_academiques/1.xml
  def show
    @jour_academique = JourAcademique.find(params[:id])
    respond_with(@jour_academique)
  end

  # GET /jour_academiques/new
  # GET /jour_academiques/new.xml
  def new
    @jour_academique = JourAcademique.new
    respond_with(@jour_academique)
  end

  # GET /jour_academiques/1/edit
  def edit
    @jour_academique = JourAcademique.find(params[:id])
    if @jour_academique.descriptif
      @jour_academique.descriptif=@jour_academique.descriptif.force_encoding("UTF-8")
    end
  end

  # POST /jour_academiques
  # POST /jour_academiques.xml
  def create
    @jour_academique = JourAcademique.new(params[:jour_academique])
    @jour_academique.save
    InfoHeure.set_plages_KO_pour_tous(@jour_academique.date.year)
    respond_with(@jour_academique)
  end

  # PUT /jour_academiques/1
  # PUT /jour_academiques/1.xml
  def update
    @jour_academique = JourAcademique.find(params[:id])
    @jour_academique.update_attributes(params[:jour_academique])
    InfoHeure.set_plages_KO_pour_tous(@jour_academique.date.year)
    respond_with(@jour_academique)
  end

  # DELETE /jour_academiques/1
  # DELETE /jour_academiques/1.xml
  def destroy
    @jour_academique = JourAcademique.find(params[:id])
    @jour_academique.destroy
    InfoHeure.set_plages_KO_pour_tous(@jour_academique.date.year)
    respond_with(@jour_academique)
  end
end

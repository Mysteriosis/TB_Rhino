# -*- encoding : utf-8 -*-
class CstHeuresController < ApplicationController
  
  load_and_authorize_resource
  
  #before_fileter :activate_authlogic
  # GET /constantes
  # GET /constantes.xml
  def index
    # Si l'année civile est sélectionnée
    if params[:annee_civile]
      if params[:annee_civile][:annee]
        @annee_civile=AnneeCivile.where({:id => params[:annee_civile][:annee]}).first
        if !@annee_civile
          @annee_civile=AnneeCivile.courante
        end
      end
    else
      @annee_civile=AnneeCivile.courante
    end

    # Liste de toutes les constantes de l'année en cours
    @constantes= CstHeure.where(:annee_civile => @annee_civile.annee).sort_by(&:descriptif)
   
    # Liste des années civiles, à partir de l'année civile initiale jusqu'à et y compris l'année prochaine
    @annees_civiles = AnneeCivile.liste_des_annees_civiles
    @annees_civiles.push(AnneeCivile.prochaine)
  end
  

  def new
  	@annee_civile=AnneeCivile.where({:id => params[:annee_civile_id]}).first
    @constante = CstHeure.new(:annee_civile => @annee_civile.annee)
    respond_with(@constante)
  end


  def edit
    @constante = CstHeure.find(params[:id])
    @annee_civile = AnneeCivile.where({:annee => @constante.annee_civile}).first
    if @constante.descriptif
      @constante.descriptif=@constante.descriptif.force_encoding("UTF-8")
    end
  end


  def create
    @constante = CstHeure.new(params[:cst_heure])
    annee_civile = AnneeCivile.where({:annee => @constante.annee_civile}).first
    @constante.save
    InfoHeure.set_report_KO_des_annees_suivantes_pour_tous(@constante.annee_civile)
    redirect_to cst_heures_path(:annee_civile => {:annee => annee_civile.id})
  end


  def update
    @constante = CstHeure.find(params[:id])
    annee_civile = AnneeCivile.where({:annee => @constante.annee_civile}).first
    @constante.update_attributes(params[:cst_heure])
    InfoHeure.set_report_KO_des_annees_suivantes_pour_tous(@constante.annee_civile)
    redirect_to cst_heures_path(:annee_civile => {:annee => annee_civile.id})
  end


  def destroy
    @constante = CstHeure.find(params[:id])
    annee_civile = AnneeCivile.where({:annee => @constante.annee_civile}).first
    @constante.destroy
    InfoHeure.set_report_KO_des_annees_suivantes_pour_tous(@constante.annee_civile)
    redirect_to cst_heures_path(:annee_civile => {:annee => annee_civile.id})
  end
end
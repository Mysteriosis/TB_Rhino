# -*- encoding : utf-8 -*-
class PersonneChargeEnseignementsController < ApplicationController
  # GET /personne_charge_enseignements
  # GET /personne_charge_enseignements.xml
  def index
    @personne_charge_enseignements = PersonneChargeEnseignement.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @personne_charge_enseignements }
    end
  end

  # GET /personne_charge_enseignements/1
  # GET /personne_charge_enseignements/1.xml
  def show
    @personne_charge_enseignement = PersonneChargeEnseignement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @personne_charge_enseignement }
    end
  end

  # GET /personne_charge_enseignements/new
  # GET /personne_charge_enseignements/new.xml
  def new
    @personne_charge_enseignement = PersonneChargeEnseignement.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @personne_charge_enseignement }
    end
  end

  # GET /personne_charge_enseignements/1/edit
  def edit
    @personne_charge_enseignement = PersonneChargeEnseignement.find(params[:id])
  end

  # POST /personne_charge_enseignements
  # POST /personne_charge_enseignements.xml
  def create
    @personne_charge_enseignement = PersonneChargeEnseignement.new(params[:personne_charge_enseignement])

    respond_to do |format|
      if @personne_charge_enseignement.save
        flash[:notice] = 'PersonneChargeEnseignement was successfully created.'
        format.html { redirect_to(@personne_charge_enseignement) }
        format.xml  { render :xml => @personne_charge_enseignement, :status => :created, :location => @personne_charge_enseignement }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @personne_charge_enseignement.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /personne_charge_enseignements/1
  # PUT /personne_charge_enseignements/1.xml
  def update
    @personne_charge_enseignement = PersonneChargeEnseignement.find(params[:id])

    respond_to do |format|
      if @personne_charge_enseignement.update_attributes(params[:personne_charge_enseignement])
        flash[:notice] = 'PersonneChargeEnseignement was successfully updated.'
        format.html { redirect_to(@personne_charge_enseignement) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @personne_charge_enseignement.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /personne_charge_enseignements/1
  # DELETE /personne_charge_enseignements/1.xml
  def destroy
    @personne_charge_enseignement = PersonneChargeEnseignement.find(params[:id])
    @personne_charge_enseignement.destroy

    respond_to do |format|
      format.html { redirect_to(personne_charge_enseignements_url) }
      format.xml  { head :ok }
    end
  end
end

# -*- encoding : utf-8 -*-
class ProjetsController < ApplicationController

  load_and_authorize_resource

  before_filter :set_form_data, :only => [:new, :create, :update, :edit]

  def index
    #@types_projets = TypeProjet.select
    @projets = Projet.all
    respond_with(@projets)
  end

  def show
    @projet = Projet.find(params[:id])
    @affectations=@projet.affectation_projets
    @total_heures_saisies =@affectations.joins(:plage_horaire_saisies).sum(:heures) 
    @charge_globale = @affectations.sum(:charge_horaire) 

# puts
# puts @projet.inspect
# puts

    respond_with(@projet)
  end

  def new
    
    # si le type de projet n'est pas renseigné, on retourne un formulaire de choix
    if params[:classe_projet].nil? || 
       params[:type_projet_id].nil? ||
       params[:classe_projet].empty? ||
       params[:type_projet_id].empty?
      render :choix_type
      return
    end
    
    case params[:classe_projet]
      when "Projet"
        @projet = Projet.new
      when "ProjetAcademique" 
        if Variable.phase_planification_enseignement?
          raise "Impossible en ce moment: phase de planification des enseignements en cours "
        else
          @projet = ProjetAcademique.new
          @projet.annee_academique_realisation=AnneeAcademique.courante
          @annee_academique_id = AnneeAcademique.courante.id
          @unites_potentielles = UniteEnseignement.unites_annee_souhaitee(@annee_academique_id)
        end
      when "ProjetEnseignement" 
        if Variable.phase_planification_enseignement?
          raise "Impossible en ce moment: phase de planification des enseignements en cours "
        else
          @projet = ProjetEnseignement.new
          @projet.annee_academique_realisation=AnneeAcademique.courante
          @projet.id_interne=""
          @annee_academique_id = AnneeAcademique.courante.id
          @unites_potentielles = UniteEnseignement.unites_annee_souhaitee(@annee_academique_id)
        end
      when "These"
        @projet = These.new
        @annee_academique_id = AnneeAcademique.courante.id
        @unites_potentielles = UniteEnseignement.unites_annee_souhaitee(@annee_academique_id)
      else
        raise "classe de projet inconnue !"
    end
    @projet.type_projet_id = params[:type_projet_id]
    @projet.id_sagex=@projet.type_projet.compte_sagex_par_defaut
    
    respond_with(@projet)
  end

  def edit
    @projet = Projet.find(params[:id])
    if @projet.kind_of? ProjetAcademique
      @projet.id_interne=@projet.identifiant_interne_to_update
      @annee_academique_id = @projet.annee_academique_realisation.id
      if @projet.kind_of? ProjetEnseignement 
        @unites_potentielles = UniteEnseignement.unites_annee_souhaitee(@annee_academique_id)
      end
    end
  end

  def create
    
    case params[:classe_projet]
      when "Projet"
        @projet = Projet.new
      when "ProjetAcademique"
        @projet = ProjetAcademique.new
      when "ProjetEnseignement"
        @projet = ProjetEnseignement.new
      when "These"
        @projet = These.new
      else
        raise "classe de projet inconnue !"
    end

#    @projet.attributes = params[:projet]
    @projet.attributes = projet_params
    @projet.est_actif=true
    
    if @projet.kind_of? ProjetEnseignement 
      no_groupe=@projet.id_interne  # Groupe de l'unité d'enseignement: A, B, etc..
      @projet.id_interne=@projet.identifiant_interne(no_groupe)
      @projet.id_externe=@projet.identifiant_externe(no_groupe)
      @projet.date_debut=@projet.unite_enseignement.periode.date_debut-30
      @projet.date_fin=@projet.unite_enseignement.periode.date_fin+15
    elsif @projet.kind_of? ProjetAcademique
      @projet.id_interne=@projet.id_interne+ " "+ @projet.annee_academique_realisation.annee 
      @projet.id_externe=""
    end
      
    if @projet.save
      redirect_to(projet_path(@projet))
    else
      render :action => "new"
    end
  end

  def update
    @projet = Projet.find(params[:id])
    @projet.update_attributes(params[:projet])
    
    if @projet.kind_of? ProjetEnseignement 
      no_groupe=@projet.id_interne  # Groupe de l'unité d'enseignement: A, B, etc..
      @projet.id_interne=@projet.identifiant_interne(no_groupe)
      @projet.id_externe=@projet.identifiant_externe(no_groupe)
    elsif @projet.kind_of? ProjetAcademique
      @projet.id_interne=@projet.id_interne+ " "+ @projet.annee_academique_realisation.annee 
      @projet.id_externe=""
    end

    @projet.save
    respond_with(@projet, :location => @projet.becomes(Projet))
  end

  def destroy
    @projet = Projet.find(params[:id])
    if @projet.affectation_projets.empty?
      @projet.destroy
      flash[:notice]="Suppression réalisée avec succès!"
      respond_with(@projet, :location => @projet.becomes(Projet))
    else 
      flash[:alert]="Suppression impossible - Le projet possède des affectations"
      respond_with(@projet, :location => @projet.becomes(Projet))
    end
  end
  
  def activer
    @projet = Projet.find(params[:id])
    @projet.est_actif=true
    if @projet.save
      redirect_to(projet_path(@projet)) 
    end
  end
  
  def desactiver
    @projet = Projet.find(params[:id])
    @projet.est_actif=false
    if @projet.save
      redirect_to(projet_path(@projet))
    end
  end
  
  def archiver
    @projet = Projet.find(params[:id])
    @projet.est_archive=true
    if @projet.save
      redirect_to(projet_path(@projet))
    end
  end
  
  def desarchiver
    @projet = Projet.find(params[:id])
    @projet.est_archive=false
    if @projet.save
      redirect_to(projet_path(@projet))
    end
  end
  
  def get_type_fields
  
    render :partial => "type_projet_field", :locals => {:genre => params[:genre]}
  
    require 'unite_enseignements_controller'
  end
  
  def resume_conso
    render :partial => "resume_conso", :locals => {:projet => @projet}
  end
  
  def resume_conso_graph_data
    res_proj = []
    @projet.affectation_projets.each do |ap|
      res_ap = []
      ap.plage_horaire_saisies.order(:debut).each do |plage_horaire| 
        if res_ap.count > 0 && plage_horaire.debut.at_midnight.to_i*1000 == res_ap.last[0]
          res_ap.last[1] += plage_horaire.heures
        else
          res_ap << [plage_horaire.debut.at_midnight.to_i*1000, plage_horaire.heures]
        end
      end
      res_proj << {"label" => ap.personne.display_name, "data" => res_ap}
    end
    
    respond_to do |format|
      format.js do
        render :text => res_proj.to_json
      end
    end
  end
  
  def essai
    redirect_to(personnes_path)
    flash[:notice]="Le routage a fonctionné !!"
  end

  def isChef?(personne)
    self.chef_id == personne.id
  end
  
private

  def set_form_data
    @type_projets = TypeProjet.feuilles(TypeProjet.order(:nom))
    @chefs_potentiels = Personne.personnes_actives_de_linstitut.sort_by(&:display_name)
  end

  # strong parameters de Rails >4.0
  def projet_params
    params.require(:projet).permit(:classe_projet, :type_projet_id)
  end

end

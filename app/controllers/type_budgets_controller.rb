# -*- encoding : utf-8 -*-
class TypeBudgetsController < ApplicationController

  load_and_authorize_resource

  # GET /type_budgets
  # GET /type_budgets.xml
  def index
    @type_budgets = TypeBudget.all
    respond_with(@type_budgets)
  end

  # GET /type_budgets/1
  # GET /type_budgets/1.xml
  def show
    @type_budget = TypeBudget.find(params[:id])
    respond_with(@type_budget)
  end

  # GET /type_budgets/new
  # GET /type_budgets/new.xml
  def new
    @type_budget = TypeBudget.new
    respond_with(@type_budget)
  end

  # GET /type_budgets/1/edit
  def edit
    @type_budget = TypeBudget.find(params[:id])
  end

  # POST /type_budgets
  # POST /type_budgets.xml
  def create
    @type_budget = TypeBudget.new(params[:type_budget])
    @type_budget.save
    respond_with(@type_budget)
  end

  # PUT /type_budgets/1
  # PUT /type_budgets/1.xml
  def update
    @type_budget = TypeBudget.find(params[:id])
    @type_budget.update_attributes(params[:type_budget])
    respond_with(@type_budget)
  end

  # DELETE /type_budgets/1
  # DELETE /type_budgets/1.xml
  def destroy
    @type_budget = TypeBudget.find(params[:id])
    @type_budget.destroy
    respond_with(@type_budget)
  end
end

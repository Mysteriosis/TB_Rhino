$j(function(){
  
  $j(".affectation_button").button();
  
  $j(".affectation_progressbar").each(function(i,e){
    var charge = parseFloat($j(e).attr("data-charge"));
    var heures_saisies = parseFloat($j(e).attr("data-heures_saisies"));
    if (charge == 0)
      if (heures_saisies == 0)
        val = 0;
      else
        val = 100;
    else
      val = (heures_saisies*100.0 / charge);
      
    $j(e).progressbar({value: val});
  });
  
  //$j(".charge_globale_progressbar").each(function(i,e){
  //  $j(e).progressbar({value: parseInt($j(e).attr("data-pourcentage"))});
  //});
  //
  //$j(".budget_global_progressbar").each(function(i,e){
  //  $j(e).progressbar({value: parseInt($j(e).attr("data-pourcentage"))});
  //});
  
  $j("button.affectation_button").click(function(){
    
    var div = $j(this).siblings(".affectation_details");
    
    if (div.html().trim() == "") {
      spinner(div);
      $j.ajax({
        url: "/affectation_projets/"+div.attr("data-ap_id")+"/get_details",
        data: {"projet_id": div.attr("data-projet_id"), "vient_de": div.attr("vient_de")},
        context: div,
        success: function(data){
          $j(this).html(data);
          $j(this).hide();
          $j(this).slideToggle();
          afficher_consommation_graph($j(this).find(".affectation_consommation_graph"));
        }
      })
    }
    else{
      div.slideToggle();
    }
    
  });
  
});

function afficher_consommation_graph(e) {
  $j.ajax({
    url: "/affectation_projets/get_consommation_graph_data",
    data: {"affectation_projet_id": e.attr("data-ap_id")},
    context: e,
    success: function(data){
      serie = JSON.parse(data);
      $j.plot($j(this), [{ label: "Heures saisies",  data: serie}], {
              series: {
                  threshold: {
                    above: {
                      limit: 9,
                      color: "rgb(255,0,0)"
                    }, 
                  },
                  bars: { 
                    show: true ,
                    barWidth: 86400000,
                    align: "center"
                  },
                  points: { show: false }
              },
              xaxis: {
                  mode: "time",
                  timeformat: "%d %b",
                  monthNames: ["Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Aoû", "Sep", "Oct", "Nov", "Déc"],
                  minTickSize: [1, "day"]
              },
              yaxis: {min:0},
              grid: {
                  backgroundColor: { colors: ["#fff", "#eee"] }
              },
              
          });
      //$j(this).closest(".affectation_details").toggle();
      
    }
  });
}


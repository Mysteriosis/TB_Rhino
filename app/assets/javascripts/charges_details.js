$j(function(){
  
  $j("button.charges_button").button();
  
  
  $j("button.charges_button").click(function(){
    
    var div = $j(this).siblings(".charges_details");
    
    if (div.html().trim() == "") {
      spinner(div);
      $j.ajax({
        url: "/charges/get_details",
        data: {"personne_id": div.attr("data-personne-id")},
        context: div,
        success: function(data){
          $j(this).html(data);
          $j(this).hide();
          $j(this).slideToggle();
        }
      })
    }
    else{
      div.slideToggle();
    }
    
  });
  
});

function remove_fields(link) {
  $j(link).previous("input[type=hidden]").value = "1";
  $j(link).up(".fields").hide();
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $j(link).insert({
    before: content.replace(regexp, new_id)
  });
}

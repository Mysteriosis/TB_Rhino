/*
 * Namespace de l'application et variable globales.
 */
var AppPlanif = {};
AppPlanif.destination = null;
AppPlanif.config = {};
AppPlanif.editors = {};
AppPlanif.renderer = {};
AppPlanif.menu = {};
AppPlanif.donnees = null;
AppPlanif.hot = null;

/*
 * Structure de stockage cellulaire des informations dans le tableau de données AppPlanif.donnees
 * element: Elément principal auquel cette cellule fait référence. Peut être une initiale de ressource
 *          ou un id interne de projet.
 * type:    Type de cellule. "cell" pour les cellules de planification, "cell-header" et autre pour les cellules en
 *          lecture seule.
 * data:    Données de la ressource ou du projet concerné par la cellule.
 */
AppPlanif.contenuCellule = function(elem, type, data) {
  this.element = elem;
  this.type = type;
  this.data = data;

  this.has_data = function() {
    return ((this.data !== undefined) && (this.data !== null));
  };

  this.has_planif = function() {
    return (this.has_data() && (this.data.planif !== null));
  };

  this.has_heures = function() {
    return (this.has_data() && (this.data.hSaisies !== undefined) && (this.data.hSaisies !== null) && (this.data.hSaisies.length > 0));
  };

  this.has_comment = function() {
    return (this.has_planif() && (this.data.commentaires != null));
  }

  this.has_jalon = function() {
    return (this.has_data() && (this.data.jalons !== undefined) && (this.data.jalons !== null) && (this.data.jalons.length > 0));
  };

  this.totalPourcentJournalier = function() {
    var total = 0;
    this.data.hSaisies.forEach(function (saisie) {
      total += saisie.pourcentage_journalier;
    });
    return total;
  };

  this.totalPourcentHebdomadaire = function() {
    var total = 0;
    this.data.hSaisies.forEach(function (saisie) {
      total += saisie.pourcentage_hebdomadaire;
    });
    return total;
  };

  this.pourcentEnHeures = function(valeur, precision, ref) {
    ref = (ref !== undefined) ? ref : parseFloat(AppPlanif.config.heuresCalcul);
    precision = (precision !== undefined) ? precision : 0.5;
    return Math.round(((valeur * (ref / 100)) / precision) * precision).toFixed(1);
  };

  this.heuresEnPourcent = function(valeur, precision, ref) {
    //Arrondi au demi pourcent par défaut
    ref = (ref !== undefined) ? ref : parseFloat(AppPlanif.config.heuresCalcul);
    precision = (precision !== undefined) ? precision : 0.5;
    return Math.round(((valeur * (100 / ref)) / precision) * precision).toFixed(0);
  };

  this.getPresence = function() {

  };
};

/*
 * Chargement des données.
 * Effectue la requête ajax permettant de récupérer le nouvel état des planifications.
 * params:   Paramètres ajoutés à la requête GET. Lors du retour, ces paramètres seront ajoutés à la configuration de
 *           l'application.
 * callback: fonction de callback éxécutée en cas de succès.
 */
AppPlanif.loadPlanif = function(params, callback) {
  var adresse = (AppPlanif.config.mode == 'P') ? "planifications/getProjets" : "planifications/getRessources";
  if(typeof params == 'function') {
    callback = params;
    params = {};
  }

  //Création du tableau de params GET
  AppPlanif.config = $j.extend({}, AppPlanif.config, params);

  $j.get(adresse, AppPlanif.config, function(res) {
    //Màj de la configuration de l'application
    AppPlanif.config = res.appInfos.config;
    AppPlanif.donnees = creerContenu(res, AppPlanif.config.mode);
    callback();
  }).fail(function() {
    alert("Erreur lors du chargement des planifications");
  });
}

/*
 * Mise à jour des données de planification.
 * Cette fonction est responsable des POST et PATCH des planifications modifiées par l'utilisateur.
 * infos:    tableau crée par le callback "afterChange" d'handsontable.
 *           [0] & [1]: no de ligne et colonne
 *           [2] & [3]: contenu de la cellule avant et après.
 * callback: fonctionde callback éxécuté après un POST / PATCH réussi.
 */
AppPlanif.majPlanif = function(infos, callback) {
  var contenuCellule = AppPlanif.donnees.tableau[infos[0]][infos[1]],
      valeur = (infos[3] == '') ? 0 : infos[3];

  valeur = (AppPlanif.config.appFormat == 'N') ? contenuCellule.heuresEnPourcent(valeur) : valeur;

  if(!contenuCellule.has_planif()) {
    var url = "planifications/",
        data = {
          affectation_projet_id: contenuCellule.data.affectation,
          valeur: valeur,
          date: contenuCellule.data.date.toString("yyyy-MM-dd")
        };

    //Nouvelle planification
    $j.post(url, data, function (result) {
        if(result.status == 'success')
          console.log("cellule("+infos[0]+","+infos[1]+") sauvegardée.");
        else
          console.log("cellule("+infos[0]+","+infos[1]+") non sauvegardée.");
    });
  }
  else {
    var id = contenuCellule.data.planif.id,
        url = "planifications/" + id,
        data = {
          id: id,
          valeur: valeur
        };

    //Modification de la planification
    $j.ajax({
      url: url,
      data: data,
      type: 'PATCH',
      success: function (result) {
        if(result.status == 'success') {
          console.log("cellule(" + infos[0] + "," + infos[1] + ") sauvegardée.");
          callback();
        }
        else
          console.log("cellule("+infos[0]+","+infos[1]+") non sauvegardée.");
      }
    });
  }
}

/*
 * Traitement des données lors de la réception.
 * Crée le contenu du tableau de données stocké sous AppPlanif.donnees
 * contenu:       JSON reçu du serveur
 * modeAffichage: Option facultative permettant de forcer le mode utilisé pour traiter les données. Une mauvaise
 *                utilisation entrainera une erreur de parsing.
 */
function creerContenu(contenu, modeAffichage) {
  var mode = (mode === undefined) ? AppPlanif.config.mode : modeAffichage;
      ligne = 2, mois = 0, nbSem = 0, lastCol = 2,
      fourchette = contenu.dates.semaines.length;

  // HEADERS (2ère lignes)
  var resultat = {
    tableau: [new Array(fourchette+2), new Array(fourchette+2)],
    mergeCells: [{row: 0, col: 0, rowspan: 2, colspan: 2}]
  };

  resultat.tableau[0][0] = '';

  contenu.dates.semaines.forEach(function(semaine, i){
    de = new Date(semaine.de);
    a = new Date(semaine.a);

    if(mois == 0) {
      resultat.tableau[0][lastCol] = de.toLocaleString("fr-FR", { month: "long" }) + " " + de.getFullYear();
      mois = de.getMonth();
      nbSem++;
    }
    else if(mois == de.getMonth()) nbSem++;
    else {
      resultat.mergeCells.push({row:0, col:lastCol, rowspan:1, colspan:nbSem});
      lastCol = lastCol + nbSem;
      resultat.tableau[0][lastCol] = de.toLocaleString("fr-FR", { month: "long" }) + " " + de.getFullYear();
      mois = de.getMonth();
      nbSem = 1;
    }

    var today = (semaine.num == parseInt(contenu.dates.now)) ? " class='today'" : "";
    var jours = ' ';
    for(var j = 0; j<semaine.jours; j++) jours += "* ";
    resultat.tableau[1][i+2] = "<div" + today + "> Semaine " + semaine.num + "<br>" + jours + "<br>" + semaine.de.split('-')[2] + " - " + semaine.a.split('-')[2] + "</div>";

  });

  resultat.mergeCells.push({row:0, col:lastCol, rowspan:1, colspan:nbSem});

  // CONTENU (lignes suivantes)

  // Surchage de Array dans application_jquery.js:185
  if(mode == 'R') {
    if((indexOf = contenu.ressources.IndexOfObject('initiale', contenu.appInfos.user)) > 0)
      contenu.ressources.move(indexOf,0);
  }

  var niveau1 = (mode == 'P') ? contenu.projets : contenu.ressources;

  niveau1.forEach(function(element1) {

    //En têtes grises
    var identifiant = (mode == 'P') ? element1.id : element1.initiale;
    var arrayRessource = [new AppPlanif.contenuCellule(identifiant, "header", element1), new AppPlanif.contenuCellule(identifiant, "header")];
    for (var i = 0; i < (fourchette); i++)
      arrayRessource.push(new AppPlanif.contenuCellule(identifiant, "header"));

    resultat.tableau.push(arrayRessource);
    resultat.mergeCells.push({row: ligne++, col: 0, rowspan: 1, colspan: 2});

    //Affectations des ressources (si déplié)
    element1.affectations.forEach(function (affectation) {
      var contenuLigne = [new AppPlanif.contenuCellule(identifiant, "double-header")];
      contenuLigne.push(new AppPlanif.contenuCellule(identifiant, "cell-header", affectation));

      //Pour chaque semaine d'une affectation
      for (var i = 0; i < fourchette; i++) {
        var semaineCourante = contenu.dates.semaines[i],
          commentaires = null,
          niveau2 = (mode == 'P') ? affectation.ressource : affectation.projet;

        var infoCellule = {
          affectation: affectation.id,
          name: niveau2.name,
          date: Date.parse(semaineCourante.de),
          planif: null,
          commentaires: null,
          jalons: [],
          hSaisies: []
        };

        //Récupération de la planif
        niveau2.planif.forEach(function (plan) {
          if (plan.semaine == semaineCourante.num) {
            infoCellule.planif = plan;
            if (plan.commentaire !== null)
              infoCellule.commentaires = plan.commentaire.texte + "\n";
          }
        });

        //Récupération des jalons
        niveau2.jalons.forEach(function (jalon) {
          if (jalon.semaine == semaineCourante.num)
            infoCellule.jalons.push(jalon)
        });

        //Récupération des heures
        niveau2.hSaisies.forEach(function (saisie) {
          if (saisie.semaine == semaineCourante.num) {
            infoCellule.hSaisies.push(saisie);
            if (saisie.commentaire != '') {
              var texte = "\n" + Date.parse(saisie.de).toString("dd MMM (") + AppPlanif.contenuCellule.pourcentEnHeures(saisie.pourcentage_journalier) + " h): " + saisie.commentaire;
              if (commentaires === null) infoCellule.commentaires = texte;
              else infoCellule.commentaires += texte;
            }
          }
        });

        //Création de la cellule
        contenuLigne[i + 2] = new AppPlanif.contenuCellule(niveau2.id, "cell", infoCellule);
      }

      resultat.tableau.push(contenuLigne);
      ligne++;
    });

    if (mode == 'R') {
      //Absences des ressources (si déplié)
      element1.affectations_absences.forEach(function (affectation) {
        var contenuLigne = [new AppPlanif.contenuCellule(identifiant, "double-header-vac")];
        contenuLigne.push(new AppPlanif.contenuCellule(identifiant, "cell-header-vac", affectation));

        //Pour chaque semaine d'une affectation
        for (var i = 0; i < fourchette; i++) {
          var semaineCourante = contenu.dates.semaines[i],
            niveau2 = affectation.projet;

          var infoCellule = {
            affectation: affectation.id,
            name: niveau2.name,
            date: Date.parse(semaineCourante.de),
            planif: null,
            commentaires: null,
            jalons: [],
            hSaisies: []
          };

          //Récupération des heures
          niveau2.absences.forEach(function (saisie) {
            if(saisie.semaine == semaineCourante.num)
              infoCellule.hSaisies.push(saisie);
          });


          //Création de la cellule
          contenuLigne[i + 2] = new AppPlanif.contenuCellule(niveau2.id, "cell-vac", infoCellule);
        }

        resultat.tableau.push(contenuLigne);
        ligne++;
      });
    }
  });

  return resultat;
}
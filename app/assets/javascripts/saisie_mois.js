$j(function(){  
  var readonly = !!get_value('readonly');
  var personne_id = get_value('personne_id');
  var avec_commentaire = (get_value('avec_commentaire')=='true')
  var fenetre_largeur=0
  var fenetre_hauteur=0
  if (avec_commentaire){
	fenetre_largeur=600;  //530    530
	fenetre_hauteur=180;  //160    200
  }
  else {
 	fenetre_largeur=520;
	fenetre_hauteur=150;  //110
  }
  
  if (readonly){
    $j('#projets_hint').hide();
  }
  else{
    $j('.projet_drag').draggable({cursor: 'pointer', helper: 'clone', scroll: true, scrollSensitivity: 40, zIndex: 2700});
  }
  
  $j('.ec-day-bg').droppable({activeClass: "moisdropactive", hoverClass: "moisdrophover", accept: '.projet_drag', drop: add_plage_horaire});
  
  // Click de capsule dans le calendrier >> Invocation de l'action edit dans une fenêtre fugitive
  $j('div.ec-event').each(function(i,e){
    if (!($j(e).parent().hasClass('ec-no-event-cell')))
    $j(e).fancybox({
    transitionIn: 'fade',
    transitionOut: 'fade',
    speedIn: 600, 
    speedOut: 200, 
    overlayShow:true,
    type: 'ajax',
    autoDimensions: false,
    height: fenetre_hauteur,    
    width: fenetre_largeur,		
    content:'/personnes/'+personne_id+'/plage_horaire_saisies/'+$j(e).attr('data-event-id')+"/edit",
    onStart: function(){$j.fancybox.showActivity()},
    onComplete : function(){plage_horaire($j('#fancybox-outer .plage_horaire'))},
    onClosed : function(){location.reload();}
    });
    
  })
  
  
});

// Ajout d'une plage horaire en mode calendrier --> Création d'une fenêtre fugitive avec les champs de saisie
function add_plage_horaire(event, ui){
  var charge_journaliere_heures = get_value('charge_journaliere_heures')
  var charge_journaliere_minutes = get_value('charge_journaliere_minutes')
  var avec_commentaire = (get_value('avec_commentaire')=='true')

  var html_input =''
  var fenetre_largeur=0
  var fenetre_hauteur=0
  if (avec_commentaire){
	html_input= '<h2>Nombre d\'heures hh:mm</h2><input type="text" size="4" id="nb_heures" value="'+ charge_journaliere_heures +'"/><input type="text" size="4" id="nb_mn" value="'+charge_journaliere_minutes+'"/>&nbsp;<h2>Commentaire</h2><input type="text" size="80" maxlength="80" id="commentaire" value=""/><br/><br/><button id="ajouter_heures">Ajouter</button>';
	fenetre_largeur=400; 
	fenetre_hauteur=150; 
  }
  else {
	html_input= '<h2>Nombre d\'heures hh:mm</h2><input type="text" size="4" id="nb_heures" value="'+ charge_journaliere_heures +'"/><input type="text" size="4" id="nb_mn" value="'+charge_journaliere_minutes+'"/>&nbsp;<button id="ajouter_heures">Ajouter</button>';
 	fenetre_largeur=160;
	fenetre_hauteur=60;
  }
	 
  date = $j(this).attr("data-date");
  $j.fancybox(
    html_input,
  		{
        'autoDimensions'	: false,
  			'width'         		: fenetre_largeur, 
  			'height'        		: fenetre_hauteur, 
  			'transitionIn'		: 'none',
  			'transitionOut'		: 'none',
  			'onComplete': function(){
  			  $j('input#nb_heures').keypress(function(event){
  			    if (event.keyCode == '13')
  			      $j('button#ajouter_heures').trigger('click');
  			  });
  			  $j('input#nb_mn').keypress(function(event){
  			    if (event.keyCode == '13')
  			      $j('button#ajouter_heures').trigger('click');
  			  });
  			  $j('button#ajouter_heures').click(function(){
				if (avec_commentaire)
  			    	ajouter_heures(ui.draggable.attr("data-ap_id"),date,$j('input#nb_heures').val(), $j('input#nb_mn').val(), $j('input#commentaire').val());
  			    else
					ajouter_heures(ui.draggable.attr("data-ap_id"),date,$j('input#nb_heures').val(), $j('input#nb_mn').val());
  			  });
  			  $j('input#nb_heures').select();
  			}
  		})
  
  
  //$j.fancybox.showActivity();
  //var personne_id = get_value('personne_id');
  //$j.ajax({
  //  url: "/personnes/"+personne_id+"/plage_horaire_saisies/add_mois",
  //  type: 'GET',
  //  context: $j(this),
  //  data: {"affectation_projet_id": ui.draggable.attr("data-ap_id"), "date": $j(this).attr("data-date")},
  //  success: function(data){
  //    if (data != ""){
  //      alert(data);
  //      $j.fancybox.hideActivity();
  //    }
  //    else
  //      location.reload(); // the construction of the calendar is too complex, we have to reload the whole page
  //  }
  //});

}

function ajouter_heures(affectation_id,date,heures, minutes, commentaire){ 
  $j.fancybox.showActivity();
  var personne_id = get_value('personne_id');
  var avec_commentaire = (get_value('avec_commentaire')=='true')
  var donnees={}
  if (avec_commentaire)
	donnees = {"affectation_projet_id": affectation_id,"date": date, "heures": heures, "minutes": minutes, "commentaire": commentaire};
  else
    donnees = {"affectation_projet_id": affectation_id,"date": date, "heures": heures, "minutes": minutes};

  $j.ajax({
    url: "/personnes/"+personne_id+"/plage_horaire_saisies/add_mois",
    type: 'GET',
	//! Anciennement autre - Ajout du commentaire

    data: donnees,
    success: function(data){
      if (data != ""){
        alert(data);
        $j.fancybox.hideActivity();
      }
      else
        location.reload(); // the construction of the calendar is too complex, we have to reload the whole page
    }
  }); 
}


function update_total_journalier(jour){
}

function update_total_semaine(){
}

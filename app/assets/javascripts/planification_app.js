////////////////
// MAIN
////////////////

$j(function(){
  /*
   * Création des listeners de l'application
   */

  // LOADER
  $body = $j("body");
  $j(document).on({
    ajaxStart: function() {$body.addClass("plan-loading");},
    ajaxStop: function() {$body.removeClass("plan-loading");}
  });

  // DATE
  $j("#input_date").change(function(){
    rechargerPlanif({date: Date.parseExact($j(this).val(), "dd.MM.yyyy").toString("yyyy-MM-dd")});
  });

  $j('#preDate').click(function() {
    var new_dateb = $j("#input_date").datepicker('getDate');
    new_dateb.setDate(new_dateb.getDate() - 7);
    $j("#input_date").datepicker('setDate', new_dateb).change();
  });

  $j('#postDate').click(function() {
    var new_datef = $j("#input_date").datepicker('getDate');
    new_datef.setDate(new_datef.getDate() + 7);
    $j("#input_date").datepicker('setDate', new_datef).change();
  });

  // FORMAT
  $j('.input_format').change(function(){
    switch(this.value) {
      case '%':
        AppPlanif.config.appFormat = '%';
        break;
      case 'N':
        AppPlanif.config.appFormat = 'N';
        break;
      default:
        AppPlanif.config.appFormat = '%';
        break;
    }

    //AppPlanif.hot.loadData(makeData(AppPlanif.donnees));
    //AppPlanif.hot.render();
    rechargerPlanif();
  });

  // FILTRE
  $j('#champs_filtre').keyup(function(e) {
    if (e.keyCode == 13) {
      rechargerPlanif({filtre: $j(this).val()});
    }
  });

  // MODE
  $j('.input_mode').change(function(){
    $j('#champs_filtre').val('');
    switch($j(this).val()) {
      case 'R':
        AppPlanif.config.mode = 'R';
        break;
      case 'P':
        AppPlanif.config.mode = 'P';
        break;
      default:
        AppPlanif.config.mode = 'R';
        break;
    }
    //rechargerPlanif({deplier: [], filtre: '', mode: $j(this).val()});
    rechargerPlanif({deplier: [], filtre: ''});
  });

  /*
   * Création des fenêtres modales de l'application
   */

  // MODALS
  $j('#div-commentaire').dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: {
      "Sauvegarder": function(){
        var form = $j("#form-commentaire");
        $j.post(form.attr('action'), form.serialize(), function(result){
          $j('#div-commentaire').dialog("close");
          $j("<div class='" + result.class +"'>" + result.message + "</div>").dialog({
            buttons: {
              Ok: function() { $j(this).dialog("close"); }
            }
          });
          rechargerPlanif();
        });
      },
      "Annuler": function() {
        $j('#div-commentaire').dialog("close");
      }
    },
    close: function() {
      $j("#form-commentaire").get(0).reset();
    }
  });

  $j('#div-jalon').dialog({
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    buttons: {
      "Sauvegarder": function(){
        var form = $j("#form-jalon");
        $j.post(form.attr('action'), form.serialize(), function(result) {
          $j('#div-jalon').dialog("close");
          $j("<div class='" + result.class + "'>" + result.message + "</div>").dialog({
            buttons: {
              Ok: function () {
                $j(this).dialog("close");
              }
            }
          });
          rechargerPlanif();
        });
      },
      "Annuler": function() {
        $j('#div-jalon').dialog("close");
      }
    },
    close: function() {
      $j("#form-jalon").get(0).reset();
    }
  });

  $j('#div-voirJalon').dialog({
    autoOpen: false,
    height: 400,
    width: 300,
    modal: true,
    buttons: {
      "Fermer": function() {
        $j('#div-voirJalon').dialog("close");
      }
    },
    close: function() {
      $j('#div-voirJalon').html('');
      rechargerPlanif();
    }
  });

  /*
   * Premier chargement de l'application, le tableur est crée lors du callback,
   * il est uniquement mis à jour par la suite (futures requêtes)
   */

  // LANCEMENT DU MODULE
  AppPlanif.loadPlanif(function(){
    AppPlanif.hot = new Handsontable($j('#tableur')[0], {
      data: makeData(AppPlanif.donnees),
      mergeCells: AppPlanif.donnees.mergeCells,
      //cell: AppPlanif.donnees.cell,
      colWidths: [20, 80],
      stretchH: 'all',
      fillHandle: false,
      className: "htCenter",
      fillHandle: true,
      fixedRowsTop: 2,
      afterInit: function() {
        /*
         * Création des listeners sur les bandes grises du contenu
         */
        $j('#tableur').on('click', 'tr td.head-cell', function(){
          var identifiant = $j(this).data('identifiant'),
              index = AppPlanif.config.deplier.indexOf(identifiant.toString());

          if(index != -1) AppPlanif.config.deplier.splice(index, 1);
          else AppPlanif.config.deplier.push(identifiant);

          rechargerPlanif();
        });

      },
      afterChange: function (change, source) {
        if (source === 'loadData') {
          return; //Pas de sauvegarde au premier chargement
        }

        // Sauvegarde à la volée
        if(source == 'edit' || source == 'autofill' || source == 'paste') {
          for(var i=0; i<change.length; i++)
            if((change[i][2] != change[i][3]) && $j.isNumeric(parseInt(change[i][3]))) {
              AppPlanif.majPlanif(change[i], function(){
                rechargerPlanif();
              });
            }
        }
      },
      cells: function (row, col, prop) {
        // Configuration des moteurs de rendu / édition (voir plus bas)
        var cellProperties = {
          renderer: PlanningRender,
          editor: PlanningEditor
        };

        // Verrouille en édition les trois première lignes et les deux première colonne du tableur
        // ainsi que les cellules qui ne font pas partie de la planification
        if(row < 3 || col < 2 || AppPlanif.donnees.tableau[row][col].type != 'cell') {
          cellProperties.readOnly = true;
          cellProperties.editor = false;
        }
        else cellProperties.type = 'numeric';

        return cellProperties;
      },
      contextMenu: { //Menu clic droit
        items: {
          'customUndo': {
            name: 'Undo',
            disabled: true
          },
          'customRedo': {
            name: 'Redo',
            disabled: true
          },
          'hsep1': "---------",
          'customComment': {
            name: 'Voir commentaire',
            disabled: function() {
              var cell = AppPlanif.hot.getSelectedRange().from;
              if (AppPlanif.donnees.tableau[cell.row][cell.col].has_comment()) return false;
              else return true;
            },
            callback: function() {
              var cell = AppPlanif.hot.getSelectedRange().from,
                texte = AppPlanif.donnees.tableau[cell.row][cell.col].data.commentaires;

              $j("<div>" + texte + "</div>").dialog({
                modal: true,
                buttons: {
                  Ok: function () {
                    $j(this).dialog("close");
                  }
                }
              });
            }
          },
          'customModComments': {
            name: function() {
              var cell = AppPlanif.hot.getSelectedRange().from;
              return (AppPlanif.donnees.tableau[cell.row][cell.col].has_comment()) ? 'Modifier commentaire' : 'Ajouter commentaire';
            },
            disabled: function() {
              var cell = AppPlanif.hot.getSelectedRange().from,
                  contenuCellule = AppPlanif.donnees.tableau[cell.row][cell.col];

              if(contenuCellule === undefined || contenuCellule.type != 'cell' || !AppPlanif.config.edit) return true;
              else return false;
            },
            callback: function(){
              var cell = AppPlanif.hot.getSelectedRange().from,
                  contenuCellule = AppPlanif.donnees.tableau[cell.row][cell.col];
              if(contenuCellule.has_comment()) {
                var url = "commentaires/" + contenuCellule.data.planif.commentaire.id + "/edit";
                $j.get(url, function(result) {
                  $j('#div-commentaire').html(result);
                }, 'html').fail(function(error){
                  console.log(error);
                });
              }
              else {
                var params;

                //Infos planification
                if (contenuCellule.has_planif()) {
                  params = {
                    planification_id: contenuCellule.data.planif.id
                  };
                }
                else {
                  params = {
                    affectation_id: contenuCellule.data.affectation,
                    date: contenuCellule.data.date.toString("yyyy-MM-dd")
                  };
                }

                $j.get("commentaires/new", params, function(result) {
                  $j('#div-commentaire').html(result);
                }, 'html').fail(function(error){
                  console.log(error);
                });
              }
              $j('#div-commentaire').dialog("open");
            }
          },
          'customDelComments': {
            name: 'Supprimer commentaire',
            disabled: function() {
              var cell = AppPlanif.hot.getSelectedRange().from;
              return (AppPlanif.donnees.tableau[cell.row][cell.col].has_comment() && AppPlanif.config.edit) ? false : true;
            },
            callback: function() {
              var cell = AppPlanif.hot.getSelectedRange().from;
              if(AppPlanif.donnees.tableau[cell.row][cell.col].has_comment()) {
                if(window.confirm("Supprimer le commentaire ?")) {
                  var url = "commentaires/" + AppPlanif.donnees.tableau[cell.row][cell.col].data.planif.commentaire.id;
                  $j.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function (result) {
                      $j("<div class='" + result.class + "'>" + result.message + "</div>").dialog({
                        buttons: {
                          Ok: function () {
                            $j(this).dialog("close");
                          }
                        }
                      });
                      rechargerPlanif();
                    }
                  });
                }
              }
            }
          },
          'hsep2': "---------",
          'customVoirJalons': {
            name: 'Voir les jalons',
            disabled: function() {
              var cell = AppPlanif.hot.getSelectedRange().from;
              if(AppPlanif.donnees.tableau[cell.row][cell.col].has_jalon() && AppPlanif.config.edit) return false;
              else return true;
            },
            callback: function() {
              var cell = AppPlanif.hot.getSelectedRange().from,
                  contenuCellule = AppPlanif.donnees.tableau[cell.row][cell.col].data;

              $j.get("jalons/", {affectation_projet_id: contenuCellule.affectation, date: contenuCellule.date.toString("yyyy-MM-dd")}, function (result) {
                $j('#div-voirJalon').html(result);
                $j('a.jalon_del').on
              }, 'html').fail(function (error) {
                console.log(error);
              });

              $j('#div-voirJalon').dialog("open");
            }
          },
          'customAddJalons': {
            name: 'Ajouter un jalon',
            disabled: function() {
              var cell = AppPlanif.hot.getSelectedRange().from;
              if(cell.row < 2 || cell.col < 2 || !AppPlanif.config.edit) return true;
              else return false;
            },
            callback: function() {
              var cell = AppPlanif.hot.getSelectedRange().from,
                  contenuCellule = AppPlanif.donnees.tableau[cell.row][cell.col].data,
                  params = {
                    affectation_id: contenuCellule.affectation,
                    date: contenuCellule.date.toString("yyyy-MM-dd")
                  };

              $j.get("jalons/new", params, function (result) {
                $j('#div-jalon').html(result);
              }, 'html').fail(function (error) {
                console.log(error);
              });

              $j('#div-jalon').dialog("open");
            }
          }
        }
      }
    });
  });
}); //Fin $(function())

//Mise à jour du tableur
function rechargerPlanif(options) {
  AppPlanif.loadPlanif(options, function() {
    AppPlanif.hot.mergeCells = new Handsontable.MergeCells(AppPlanif.donnees.mergeCells);
    AppPlanif.hot.loadData(makeData(AppPlanif.donnees));
    AppPlanif.hot.render();
    //console.log(AppPlanif.donnees);
  });
}

/*
 * Extraction des valeurs à afficher pour Handsontable.
 * Afin de conserver les fonction de Handsontable, il est crucial que le tableau de données utilisé ne contienne
 * que des types de base (texte ou chiffres) dans les cellules éditables. Cette fonction va donc extraire les données
 * importantes du tableau de données stocké dans AppPlanif.donnees. Les cellules vérouillées en écriture peuvent
 * contenir de l'HTML.
 */
function makeData(datas) {
  var donnees = (datas !== undefined) ? datas.tableau : AppPlanif.donnees.tableau,
      contenuTableur = [];

  if(donnees === undefined) return [[]];

  for(var i=0; i<donnees.length; i++) {
    var row = donnees[i],
      ligne = [];

    for(var j=0; j<row.length; j++) {
      var col = row[j],
        cellule = '';

      if(typeof col === 'string') cellule = col;
      else if(col !== undefined && col.has_data()) {
        switch (col.type) {
          case "header":
            cellule = (AppPlanif.config.mode == 'P') ? col.data.nom : "<a target='_blank' href='personnes/" + col.data.id + "'>" + col.data.initiale + "</a>";
            break;
          case "header-dispo":
            cellule = '';
            break;
          case "cell-header":
            cellule = (AppPlanif.config.mode == 'P') ? "<a target='_blank' href='personnes/" + col.data.ressource.id + "'><b>" + col.data.ressource.nom + "</b></a><br>(" + col.data.label + ")" : col.data.projet.nom;
            break;
          case "cell-header-vac":
            cellule = "<b>" + col.data.projet.nom + "</b>";
            break;
          case "cell":
            // VALEUR PLANIF
            if(col.has_planif()) {
              if(col.data.planif.valeur == 0) cellule += '';
              else if(AppPlanif.config.appFormat == 'N') cellule += col.pourcentEnHeures(parseFloat(col.data.planif.valeur), 0.25);
              else cellule += col.data.planif.valeur;
            }
            else if(col.has_heures()) cellule += 0;
            break;
          case "cell-vac":
            // VALEUR PLANIF ABSCENCES
            if(col.has_heures()) {
              if (AppPlanif.config.appFormat == 'N') cellule = col.pourcentEnHeures(col.totalPourcentHebdomadaire());
              else cellule = col.totalPourcentHebdomadaire();
            }
            break;
          default:
        }
      }

      ligne.push(cellule);
    }

    contenuTableur.push(ligne);
  }

  return contenuTableur;
}

/*
 * Fonction de rendu des cellules.
 * Cette fonction est appliquée à TOUTES les cellules du tableau. C'est donc dans cette fonction que doivent être
 * codée les modifications de mise en page définies dans les cas d'utilisation du module de planification.
 */
function PlanningRender(instance, td, row, col, prop, value, cellProperties) {
  if(row < 2) {
    Handsontable.renderers.HtmlRenderer.apply(this, arguments);
    return td;
  }

  var contenuCellule = AppPlanif.donnees.tableau[row][col],
    txt = value;

  td.setAttribute('data-identifiant', contenuCellule.element);

  switch(contenuCellule.type) {
    case "header":
      td.className = td.className + " head-cell";
      break;
    case "double-header":
    case "double-header-vac":
      td.className = td.className + " double-head-cell";
      break;
    case "cell-header-vac":
      td.className = td.className + " td-absence";
    case "cell-header":
      td.className = td.className + " head-content-cell";
      break;
    case "header-vac":
      td.className = td.className + " head-cell";
      //...
      break;
    case "cell":
      //Ajout test couleurs notifications (différence heures planification / saisies)
      if(contenuCellule.has_planif()){
        var heures = contenuCellule.has_heures() ? contenuCellule.totalPourcentHebdomadaire() : 0;
        var planif = contenuCellule.data.planif.valeur;

        if(planif == 0 && heures > 0) {
          td.className = td.className + " bg-orange";
        }
        else {
          var diff = ((heures - planif) * 100) / planif;
          if (diff <= -15) td.className = td.className + " bg-dark-green";
          else if (diff > -15 && diff <= -5) td.className = td.className + " bg-light-green";
          else if (diff >= 5 && diff < 15) td.className = td.className + " bg-light-red";
          else if (diff >= 15) td.className = td.className + " bg-dark-red";
        }
      }
      else if(contenuCellule.has_heures() && !contenuCellule.has_planif()){
        td.className = td.className + " bg-orange";
      }

      // HEURES
      if(contenuCellule.has_heures()) {
        if(AppPlanif.config.appFormat == 'N') txt += " (" + contenuCellule.pourcentEnHeures(contenuCellule.totalPourcentHebdomadaire()) + ")";
        else txt += " (" + contenuCellule.totalPourcentHebdomadaire() + ")";
      }

      // COMMENTS
      if(contenuCellule.has_comment()) td.className = td.className + " td-comment";

      // JALONS
      if(contenuCellule.has_jalon()) td.className = td.className + " td-bookmark";
      break;
    case "cell-vac":
      //ABSENCES
      td.className = td.className + " td-absence";
      break;
    default: break;
  }

  if(col < 2)
    Handsontable.renderers.HtmlRenderer.apply(this, arguments);
  else
    Handsontable.renderers.TextRenderer.apply(this, arguments);

  td.innerHTML = txt;
  return td;
};

/*
 * Editeur personalisé.
 * Actuellement non utilisé (simple raccourcis vers l'éditeur de texte par défaut d'Handsontable).
 * Pourrait être implémenté si l'on désire par exemple changer le champ textarea crée lors de l'édition des cellules.
 */
var PlanningEditor = Handsontable.editors.TextEditor.prototype.extend();
//PlanningEditor.prototype.setValue = function() {
//
//};
//PlanningEditor.prototype.getValue = function() {
//
//}
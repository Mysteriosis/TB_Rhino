$j = jQuery.noConflict();

$j(function() {

  $j('.datepicker').datepicker({ dateFormat: 'dd.mm.yy' });
  
  $j(document).ajaxError(function(e, xhr, settings, exception) {
    alert('Erreur lors de l\'opération asynchrone sur :\n' + settings.url + ' \n\n' + xhr.responseText );
  });

  // Pour que les elements de class .follow_scroll suivent le scrolling
  
  if ($j(".follow_scroll").size() > 0){
    var offset = $j(".follow_scroll").offset();
    var topPadding = 15;
    $j(window).scroll(function() {
        if ($j(window).scrollTop() > offset.top - topPadding) {
            $j(".follow_scroll").stop().animate({
                marginTop: $j(window).scrollTop() - offset.top + topPadding
            });
        } else {
            $j("#follow_scroll").stop().animate({
                marginTop: 0
            });
        }
    });
  }
});

// remplace le contenu d'un element par un spinner
function spinner(element){
  element.html('<img src="/images/ajax-loader.gif" />');
}

function get_value(element_id){
  return $j('#'+element_id).html();
}

// Mise en place de la fenêtre fugitive dédiée à l'édition d'une plage horaire (en mode calendrier comme en mode semaine)
function plage_horaire(item){
  
  // Lecture des variables cachées du formulaire
  var readonly = !!get_value('readonly');
  var heure_debut = item.find(".heure_debut").val();
  var heure_fin = item.find(".heure_fin").val();
  var avec_commentaire = (get_value('avec_commentaire')=='true')

  // Mise en place des handlers: toute saisie sera immédiatement sauvegardée, sans attendre la presssion du bouton
  item.find(".slider").slider({range: true, animate: true, disabled: readonly, min: 0, max: 1430, step: 15, slide: update_select_from_slider, stop: slider_stop, values: [heure_debut,heure_fin]});
  item.find(".heure_debut").change(update_slider_from_select);	// Sliders déplacés
  item.find(".heure_fin").change(update_slider_from_select);

  item.find(".nombre_heures").change(update_heures_minutes);			// Champs de saisie des heures accomplies
  item.find(".nombre_minutes").change(update_heures_minutes);
  if (avec_commentaire)											// Saisie éventuelle du commentaire
  	item.find(".commentaire").change(update_commentaire);	

  item.find("button.destroy").click(destroy_plage_horaire);		// Suppression d'une plage horaire

  item.find("button").button();									// Pression du bouton --> La fenêtre se fermera 
  
  // disable controlls if readonly
  if (readonly){
    item.find(".heure_debut").attr('disabled', true);
    item.find(".heure_fin").attr('disabled', true);
	if (avec_commentaire)
    	item.find(".commentaire").attr('disabled', true); 
 	item.find(".nombre_heures").attr('disabled', true);
 	item.find(".nombre_minutes").attr('disabled', true);
    item.find("button.destroy").attr('disabled', true);
  }
  
}


function destroy_plage_horaire(){
  if (confirm("Etes-vous sûr ?  Information: Suppression sans effet si plage gelée")){
    var personne_id = get_value('personne_id');
    var plage_horaire_id = $j(this).closest(".plage_horaire").attr("data-id");
    $j.ajax({
      url: "/personnes/"+personne_id+"/plage_horaire_saisies/"+plage_horaire_id,
      type: 'DELETE',
      context: $j(this),
      success: function(data){
        $j(this).closest(".plage_horaire").fadeOut(1000,function(){
          var jour = $j(this).closest(".jour");
          $j(this).closest(".plage_horaire").remove();
          update_total_journalier(jour);
          update_total_semaine();
          $j.fancybox.close();
        });
       }
    });
  }
}

// Mise à jour dynamique des select lorsqu'on déplace le slider
function update_select_from_slider(event,ui){
  
  var plage = $j(this).closest(".plage_horaire");
  
  $j(this).parent().find('.heure_debut').val(ui.values[0]);
  $j(this).parent().find('.heure_fin').val(ui.values[1]);
  
  var nb_heures = (ui.values[1]-ui.values[0]) / 60.0;
  
  plage.find(".status").html("("+nb_heures.toFixed(2)+" heures)");
  plage.find(".status").attr("data-heures", nb_heures);
  update_total_journalier($j(this).closest(".jour"));
  update_total_semaine();
}

// Mise à jour de la position du slider quand on modifie les select
function update_slider_from_select(event, ui) {
  
  plage = $j(this).closest(".plage_horaire");
  
  if ($j(this).hasClass("heure_debut")){
    heure_debut = $j(this);
    heure_fin = $j(this).siblings('.heure_fin');
    if (parseInt(heure_debut.val()) > parseInt(heure_fin.val()))
      heure_fin.val(heure_debut.val());
  }
  
  if ($j(this).hasClass("heure_fin")){
    heure_debut = $j(this).siblings(".heure_debut");
    heure_fin = $j(this);
    if (parseInt(heure_debut.val()) > parseInt(heure_fin.val()))
      heure_debut.val(heure_fin.val());
  }
  
  plage.find(".slider").slider("option", "values", [heure_debut.val(),heure_fin.val()]);
  update_plage_horaire(plage);
  var nb_heures = (heure_fin.val()-heure_debut.val()) / 60.0;
  plage.find(".status").attr("data-heures", nb_heures);
  update_total_journalier($j(this).closest(".jour"));
  update_total_semaine();
}

function slider_stop(event,ui) {
  update_plage_horaire($j(this).closest('.plage_horaire'));
}

function update_commentaire(event,ui) {
	//! Anciennement autre: ajout de cette fonction
  update_plage_horaire($j(this).closest('.plage_horaire'));
}

function update_heures_minutes(event, ui) {
  update_plage_horaire($j(this).closest('.plage_horaire'));
}

function update_plage_horaire(plage){
  // Lecture des champs cachés du formulaire
  var personne_id = get_value('personne_id');
  var avec_commentaire = (get_value('avec_commentaire')=='true')

  //date = $j(this).attr("data-date");  // valeur venant du calendrier (plug-in)

  var donnees={}
  if (avec_commentaire)
	donnees={"heures": plage.find(".nombre_heures").val(),"minutes": plage.find(".nombre_minutes").val(),
	 		"debut": plage.find(".heure_debut").val(), "fin": plage.find(".heure_fin").val(), "commentaire": plage.find(".commentaire").val()};
  else
  	donnees={"heures": plage.find(".nombre_heures").val(),"minutes": plage.find(".nombre_minutes").val(),
 			"debut": plage.find(".heure_debut").val(), "fin": plage.find(".heure_fin").val()};
	
  spinner(plage.find(".status"));
  $j.ajax({
	// L'attribut "data_id" est spécifié dans le formulaire "_plage_horaire_form" --> identificateur de la plage horaire
    url: "/personnes/"+personne_id+"/plage_horaire_saisies/"+plage.attr("data-id"),
    type: 'PUT',
    data: donnees,
    context: $j(this),
    success: function(data){
      plage.find(".status").html(data);   // Envoie l'html retourné par Ajax dans le champ de classe "status" du formulaire

	  // Ci-dessous, invocation de deux méthodes définies dans "saisie_mois.js", deux méthodes vides en l'état 
      update_total_journalier($j(this).closest(".jour"));
      update_total_semaine();
     }
  });
}

//METHODES UTILITAIRES AJOUTEE PAR LE MODULE DE PLANIFICATION

//Recherche selon propriété d'un objet dans un tableau
Array.prototype.IndexOfObject = function (property, searchTerm) {
  for(var i = 0, len = this.length; i < len; i++) {
    if (this[i][property] === searchTerm) return i;
  }
  return -1;
}

//Déplacer un élément dans un tableau
Array.prototype.move = function (old_index, new_index) {
  if (new_index >= this.length) {
    var k = new_index - this.length;
    while ((k--) + 1) {
      this.push(undefined);
    }
  }
  this.splice(new_index, 0, this.splice(old_index, 1)[0]);
  return this; // for testing purposes
};
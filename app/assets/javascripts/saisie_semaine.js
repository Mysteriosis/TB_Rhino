$j(function() {
  
  var readonly = !!get_value('readonly');
  
  $j('button').button();
  
  if (readonly){
    $j('#projets_hint').hide();
  }
  else{
    $j('.projet_drag').draggable({cursor: 'pointer', helper: 'clone', scroll: true, scrollSensitivity: 40, zIndex: 2700});
  }
    
  
  var personne_id = get_value('personne_id');
  var date_debut = get_value('date_debut');
  var date_fin = get_value('date_fin');
  
  update_jours(personne_id,date_debut,date_fin);
          
  $j('button#jours_precedents').click(function(){
    
    var premier_jour = Date.parse($j('#jours').children('.jour').first().attr('data-date')).add(-7).days();
    var dernier_jour = Date.parse($j('#jours').children('.jour').first().attr('data-date')).add(-1).days();

    update_jours(personne_id, premier_jour.toString("yyyy-MM-dd"), dernier_jour.toString("yyyy-MM-dd"));

  })
  
  $j('button#jours_suivants').click(function(){
    
    var premier_jour = Date.parse($j('#jours').children('.jour').last().attr('data-date')).add(1).days();
    var dernier_jour = Date.parse($j('#jours').children('.jour').last().attr('data-date')).add(7).days();

    update_jours(personne_id, premier_jour.toString("yyyy-MM-dd"), dernier_jour.toString("yyyy-MM-dd"));
    
  })
  
  $j('button#cloner_semaine_precedente').click(function(){
    if (confirm("Ceci effacera les heures saisies pour la semaine affichée actuellement et copiera les données de la semaine passée à la place. \nEtes-vous sûr ?")){
      $j.ajax({
        url: "/personnes/"+personne_id+"/plage_horaire_saisies/cloner_semaine_precedente",
        type: 'GET',
        data: {"premier_jour": $j(".jour").first().attr("data-date")},
        success: function(data){
          update_jours(personne_id,$j('.jour').first().attr("data-date"),$j('.jour').last().attr("data-date"));
         }
      });
    }
  })
  
  //$j.get("/personne/");
  //$j(".slider").slider({ animate: true, range: true  });
  
});

function update_jours(personne_id,j1,j2){
  spinner($j('#jours'));
  spinner($j('#semaine_courante'));
  $j.get("/personnes/"+personne_id+"/plage_horaire_saisies/jours", 
          {"date_debut": j1, "date_fin": j2},
          function(data){
            $j('#jours').html(data);
            $j('.jour').each(function(i,e){
              $j(this).droppable({activeClass: "dropactive", hoverClass: "drophover", accept: '.projet_drag', drop: add_plage_horaire});
              $j(this).find('.plage_horaire').each(function(i,e){
                plage_horaire($j(this));
                
              });
              
              update_total_journalier($j(e));
              update_total_semaine();
            });
          });
}

$j
function add_plage_horaire(event, ui){
  
  var personne_id = get_value('personne_id');
  $j.ajax({
    url: "/personnes/"+personne_id+"/plage_horaire_saisies/add",
    type: 'GET',
    context: $j(this),
    data: {"affectation_projet_id": ui.draggable.attr("data-ap_id"), "date": $j(this).attr("data-date")},
    success: function(data){
      var heures = $j(data);
      heures.hide();
      heures.appendTo($j(this).find('.plages_horaires'));
      plage_horaire(heures);
      heures.fadeIn(1000);
     }
  });
  
}

function update_total_journalier(jour){
  
  var total = 0.0;
  var total_heures =0.0;
  var total_minutes =0.0;
  
  jour.find(".status").each(function(i,e){
    total += parseFloat($j(e).attr("data-heures"));
  });
  total_heures = Math.floor(total);
  total_minutes = Math.floor((total-total_heures)*60);
  if (total_minutes==0)
  	jour.find(".total_journalier").html(" : "+total_heures+"h00");
  else
	if (total_minutes < 10)
		jour.find(".total_journalier").html(" : "+total_heures+"h0"+total_minutes);
	else
		jour.find(".total_journalier").html(" : "+total_heures+"h"+total_minutes);
	
  //jour.find(".total_journalier").html(" : " + total.toFixed(2)+" heures");
}

function update_total_semaine(){
  
  var total = 0.0;
  var total_heures =0.0;
  var total_minutes =0.0;
  var total_string ="";

  $j(".status").each(function(i,e){
    total += parseFloat($j(e).attr("data-heures"));
  });

  total_heures = Math.floor(total);
  total_minutes = Math.floor((total-total_heures)*60);
  if (total_minutes==0)
  	total_string= total_heures+"h00";
  else
	if (total_minutes < 10)
		total_string= total_heures+"h0"+total_minutes;
	else
		total_string= total_heures+"h"+total_minutes;
		
  var premier_jour = Date.parse($j('.jour').first().attr('data-date'));
  var dernier_jour = Date.parse($j('.jour').last().attr('data-date'));
  
  res = "Du "+premier_jour.toString("dd.MM.yyyy")+" au "+dernier_jour.toString("dd.MM.yyyy")+" : "+total_string

  
  $j("#semaine_courante").html(res);
}




$j(function(){
  
  $j("button.affectation_button_global").button();
  
  $j("#charge_globale_progressbar").each(function(i,e){
    var charge = parseFloat($j(e).attr("data-charge"));
    var heures_saisies = parseFloat($j(e).attr("data-heures_saisies"));
    if (charge == 0)
      if (heures_saisies == 0)
        val = 0;
      else
        val = 100;
    else
      val = (heures_saisies*100.0 / charge);
    
    $j(e).progressbar({value: val});
  });
  
  $j("button.affectation_button_global").click(function(){
    var div = $j(this).siblings(".affectation_details");
    if (div.html().trim() == "") {
      spinner(div);
      $j.ajax({
        url: "/projets/"+div.attr("data-projet_id")+"/resume_conso",
        context: div,
        success: function(data){
          $j(this).html(data);
          $j(this).hide();
          $j(this).slideToggle();
          afficher_consommation_graph_global($j(this).find(".affectation_consommation_graph_global"));
        }
      })
    }
    else{
      div.slideToggle();
    }
    
  });
  
});

function afficher_consommation_graph_global(e) {
  $j.ajax({
    url: "/projets/"+e.attr("data-projet_id")+"/resume_conso_graph_data",
    context: e,
    success: function(data){
      series = JSON.parse(data);
      $j.plot($j(this), series, {
              series: {
                  stack: true,
                  bars: { 
                    show: true ,
                    barWidth: 86400000,
                    align: "center"
                  },
                  points: { show: false }
              },
              xaxis: {
                  mode: "time",
                  timeformat: "%d %b",
                  monthNames: ["Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Aoû", "Sep", "Oct", "Nov", "Déc"],
                  minTickSize: [1, "day"]
              },
              yaxis: {min:0},
              grid: {
                  backgroundColor: { colors: ["#fff", "#eee"] }
              },
              
          });
      //$j(this).closest(".affectation_details").toggle();
      
    }
  });
}


$j(function(){
  
  $j("button.plages_calendrier_button").button();
  
  
  $j("button.plages_calendrier_button").click(function(){
    
    var div = $j(this).siblings(".plages_calendrier_details");
    
    if (div.html().trim() == "") {
      spinner(div);
      $j.ajax({
        url: "/plage_contractuelles/get_calendrier_details",
        data: {"annee_civile": div.attr("annee_civile")},
        context: div,
        success: function(data){
          $j(this).html(data);
          $j(this).hide();
          $j(this).slideToggle();
        }
      })
    }
    else{
      div.slideToggle();
    }
    
  });
  
});

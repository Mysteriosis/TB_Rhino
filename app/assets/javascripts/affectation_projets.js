$j(function() {
  
  // Mise à jour de la liste déroulante des contrats lorsque l'on change de personne
  $j("#affectation_projet_personne_id").change(get_engagements_select);
  
  // Mise à jour des champs spécifiques "projet" lorsque l'on change de projet
  $j('#affectation_projet_projet_id').change(get_project_specific_fields)
})

// Mise à jour de la liste des contrats actuels d'un collaborateur depuis 
function get_engagements_select(){
  
  var personne_id = $j('select#affectation_projet_personne_id :selected').val();
  var projet_id = $j('#affectation_projet_projet_id').val();
	// Retourne dans projet_id la valeur sélectionnée de l'élément identifié par #affectation_projet_projet_id
	
  $j.get('/affectation_projets/get_engagements_select/'+personne_id+'/'+projet_id, function(data){
   $j("#affectation_projet_engagement_input").replaceWith(data);
  });
  return false;
}

function get_project_specific_fields(){
  
  var projet_id = $j('select#affectation_projet_projet_id :selected').val();
	// Retourne dans projet_id la valeur sélectionnée de l'élément identifié par #affectation_projet_projet_id
  var personne_id = $j('#affectation_projet_personne_id').val();
	// Retourne dans personne_id la valeur de l'élément identifié par #affectation_projet_personne_id (champ caché)
  
  $j.get('/affectation_projets/project_specific_fields/'+projet_id+'/'+personne_id, function(data){
    $j("#project_specific_fields").replaceWith(data);
  })
  return false;
}
$j(function(){
  
  $j("button.bouton_graphe_affectations").button();
  
  $j("button.bouton_graphe_affectations").click(function(){
    var div = $j(this).siblings(".graphe_des_affectations");
    var data = [   
        [0, 11], //London, UK
        [1, 15], //New York, USA
        [2, 25], //New Delhi, India
        [3, 24], //Taipei, Taiwan
        [4, 13], //Beijing, China
        [5, 18]  //Sydney, AU
    ];
    var data_set = [
        { label: "2012 Average Temperature", data: data, color: "#5482FF" }
    ];
    var ticks = [
        [0, "London"], [1, "New York"], [2, "New Delhi"], [3, "Taipei"], [4, "Beijing"], [5, "Sydney"]
    ];
    var options = {
        series: {
            bars: {
                show: true
            }
        },
        bars: {
            align: "center",
            barWidth: 0.5
        },
        xaxis: {
            axisLabel: "World Cities",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10,
            ticks: ticks
        },
        yaxis: {
            axisLabel: "Average Temperature",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 3,
            tickFormatter: function (v, axis) {
                return v + "°C";
            }
        },
        legend: {
            noColumns: 0,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: true,
            borderWidth: 2,
            backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
        }
    };
    alert("Hello\nHow are you?");
    $j.plot($j(this), data_set, options);
    //if (div.html().trim() == "") {
    //  spinner(div);
    //  $j.plot(div, data_set, options);
    //}
    //else{
    //  div.slideToggle();
    //}
    
  });
  

});

function afficher_graphe_affectations(e) {
      var data = [   
          [0, 11], //London, UK
          [1, 15], //New York, USA
          [2, 25], //New Delhi, India
          [3, 24], //Taipei, Taiwan
          [4, 13], //Beijing, China
          [5, 18]  //Sydney, AU
      ];
      var data_set = [
          { label: "2012 Average Temperature", data: data, color: "#5482FF" }
      ];
      var ticks = [
          [0, "London"], [1, "New York"], [2, "New Delhi"], [3, "Taipei"], [4, "Beijing"], [5, "Sydney"]
      ];
      var options = {
          series: {
              bars: {
                  show: true
              }
          },
          bars: {
              align: "center",
              barWidth: 0.5
          },
          xaxis: {
              axisLabel: "World Cities",
              axisLabelUseCanvas: true,
              axisLabelFontSizePixels: 12,
              axisLabelFontFamily: 'Verdana, Arial',
              axisLabelPadding: 10,
              ticks: ticks
          },
          yaxis: {
              axisLabel: "Average Temperature",
              axisLabelUseCanvas: true,
              axisLabelFontSizePixels: 12,
              axisLabelFontFamily: 'Verdana, Arial',
              axisLabelPadding: 3,
              tickFormatter: function (v, axis) {
                  return v + "°C";
              }
          },
          legend: {
              noColumns: 0,
              labelBoxBorderColor: "#000000",
              position: "nw"
          },
          grid: {
              hoverable: true,
              borderWidth: 2,
              backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
          }
      };
  $j.ajax({
    url: "/charges/graphe_affectations_data",
    context: e,
    success: function(data){
      dataset = data_set;
      $j.plot($j(this), dataset, options);

   }
 });
}




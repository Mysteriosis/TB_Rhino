$j(function(){
  
  $j("#projets_index li").click(toggle_li)
  
})

function toggle_li(event){
  if ($j(this).hasClass("type_projet") && $j(this).hasClass("not_empty")) {
    $j(this).stop();
    $j(this).children("ul").slideToggle();
    $j(this).toggleClass("open");
    $j(this).toggleClass("closed");
  }
  
  //alert($j(this).html());
  event.stopPropagation();
  
}
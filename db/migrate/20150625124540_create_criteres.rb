class CreateCriteres < ActiveRecord::Migration
  def change
    create_table "criteres", force: :cascade do |t|
      t.integer "element1_id", limit: 4
      t.integer "element2_id", limit: 4
      t.integer "type",        limit: 4, default: 1, null: false #Enum déclaré dans le Model
      t.integer "unite",       limit: 1, default: 1, null: false #Enum déclaré dans le Model
      t.integer "valeur",      limit: 4
      t.timestamps null: false
    end

    add_index "criteres", ["element1_id"], name: "fk_criteres_elements1_idx", using: :btree
    add_index "criteres", ["element2_id"], name: "fk_criteres_elements2_idx", using: :btree
  end
end

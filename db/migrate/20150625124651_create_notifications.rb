class CreateNotifications < ActiveRecord::Migration
  def change
    create_table "notifications", force: :cascade do |t|
      t.integer "condition_id", limit: 4,                   null: false
      t.integer "mail_id",      limit: 4
      t.string  "titre",        limit: 255,                 null: false
      t.integer "gravite",      limit: 1,     default: 1,   null: false #Enum déclaré dans le Model
      t.integer "type",         limit: 1,     default: 1,   null: false #Enum déclaré dans le Model
      t.integer "active",       limit: 1,     default: 1,   null: false
      t.text    "description",  limit: 65535
      t.string  "couleur",      limit: 45
      t.timestamps null: false
    end

    add_index "notifications", ["condition_id"], name: "fk_notifications_conditions1_idx", using: :btree
    add_index "notifications", ["mail_id"], name: "fk_notifications_mails1_idx", using: :btree
  end
end

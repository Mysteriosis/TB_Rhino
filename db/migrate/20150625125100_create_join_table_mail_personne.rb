class CreateJoinTableMailPersonne < ActiveRecord::Migration
  def change
    create_table "mails_personnes", force: :cascade do |t|
      t.integer "mail_id",     limit: 4, null: false
      t.integer "personne_id", limit: 4, null: false
      t.timestamps null: false
    end

    add_index "mails_personnes", ["mail_id"], name: "fk_mails_has_personnes_mails1_idx", using: :btree
    add_index "mails_personnes", ["personne_id"], name: "fk_mails_has_personnes_personnes1_idx", using: :btree
  end
end

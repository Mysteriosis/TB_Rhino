class CreatePlanifications < ActiveRecord::Migration
  def change
    create_table "planifications", force: :cascade do |t|
      t.integer "affectation_projet_id", limit: 4, null: false
      t.integer "personne_id",           limit: 4, null: true
      t.integer "valeur",                 limit: 4, null: false
      t.date    "date",                             null: false
      t.timestamps null: false
    end

    add_index "planifications", ["affectation_projet_id"], name: "fk_planifications_affectation_projet_idx", using: :btree
    add_index "planifications", ["personne_id"], name: "auteur_idx", using: :btree
  end
end

class CreateMailsPlanifications < ActiveRecord::Migration
  def change
    create_table "mails_planifications", force: :cascade do |t|
      t.string  "objet",     limit: 255, default: "Notification Rhino", null: false
      t.date    "limite",                                               null: false
      t.integer "frequence", limit: 1,   default: 1,                    null: false
      t.timestamps null: false
    end
  end
end

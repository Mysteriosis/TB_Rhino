class CreateConditions < ActiveRecord::Migration
  def change
    create_table "conditions", force: :cascade do |t|
      t.integer "critere1_id",  limit: 4, null: false
      t.integer "critere2_id",  limit: 4, null: false
      t.integer "condition_id", limit: 4
      t.string  "comparaison",  limit: 2, null: false
      t.string  "operation",    limit: 2
      t.timestamps null: false
    end

    add_index "conditions", ["condition_id"], name: "fk_conditions_conditions1_idx", using: :btree
    add_index "conditions", ["critere1_id"], name: "fk_conditions_critere1_idx", using: :btree
    add_index "conditions", ["critere2_id"], name: "fk_conditions_critere2_idx", using: :btree
  end
end

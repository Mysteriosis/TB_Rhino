class CreateCommentaires < ActiveRecord::Migration
  def change
    create_table "commentaires", force: :cascade do |t|
      t.integer "personne_id", limit: 4
      t.integer "planification_id", limit: 4, null: false
      t.text    "texte",       limit: 65535, null: false
      t.timestamps null: false
    end

    add_index "commentaires", ["personne_id"], name: "auteur_idx", using: :btree
    add_index "commentaires", ["planification_id"], name: "planif_idx", using: :btree

  end
end

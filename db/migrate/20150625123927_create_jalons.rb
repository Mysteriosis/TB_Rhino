class CreateJalons < ActiveRecord::Migration
  def change
    create_table "jalons", force: :cascade do |t|
      t.integer "affectation_projet_id", limit: 4,     null: false
      t.integer "mail_id",               limit: 4
      t.integer "personne_id",           limit: 4
      t.date    "date",                                null: false
      t.string  "titre",                 limit: 255,   null: false
      t.text    "description",           limit: 65535
      t.timestamps null: false
    end

    add_index "jalons", ["mail_id"], name: "mail_idx", using: :btree
    add_index "jalons", ["personne_id"], name: "auteur_idx", using: :btree
    add_index "jalons", ["affectation_projet_id"], name: "affectation_idx", using: :btree
  end
end

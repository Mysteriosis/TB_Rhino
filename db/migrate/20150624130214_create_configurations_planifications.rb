class CreateConfigurationsPlanifications < ActiveRecord::Migration
  def change
    create_table :configurations_planifications do |t|
      t.string :variable, limit: 45, null: false, :unique => true
      t.string :description, null: false
      t.string :valeur, null: false
      t.timestamps null: false
    end
  end
end

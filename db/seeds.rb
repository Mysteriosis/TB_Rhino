# Configuration de base du module de planification
ConfigurationsPlanification.create!([
  {variable: "heures_calcul", description: "Nombre d'heures correspondant à 100%", valeur: 40},
  {variable: "conf_access", description: "Autorise les responsables à configurer le module de planification (en plus de l'Admin)", valeur: true},
  {variable: "jalons_contrats", description: "Poser automatiquement des jalons aux limites des contrats", valeur: true},
  {variable: "jalons_projets", description: "Poser automatiquement des jalons aux limites des projets", valeur: true},
  {variable: "jalons_affectations", description: "Poser automatiquement des jalons aux limites des affectations", valeur: true},
  {variable: "planif_access", description: "Accès au module pour les collaborateurs et les professeurs", valeur: true},
  {variable: "elements_reduis", description: "Contenu réduit par défaut", valeur: true},
  {variable: "semaines_avant", description: "Fourchette temps avant la date affichée", valeur: 2},
  {variable: "semaines_apres", description: "Fourchette temps après la date affichée", valeur: 7},
  {variable: "format_planif", description: "Format par défaut des données affichées", valeur: "%"},
  {variable: "colorisation", description: "Utiliser les couleurs dans la planification", valeur: true},
  {variable: "notif_defaut", description: "Utiliser les notifications par défaut du module", valeur: true},
  {variable: "resume_color", description: "Utiliser les couleurs dans la liste des notifications", valeur: false},
  {variable: "mails_planif", description: "Utiliser les alertes mails", valeur: true},
  {variable: "heure_mails", description: "Heure d'envoi des alertes par mails", valeur: "01:00"},
  {variable: "adresse_mails", description: "Adresse mail de l'envoyeur", valeur: "rhino@heig-vd.ch"}
])

TypePersonne.create!([
  {typePersonne: "Responsable de planifications"}
])
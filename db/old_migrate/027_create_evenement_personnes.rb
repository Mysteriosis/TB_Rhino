# -*- encoding : utf-8 -*-
class CreateEvenementPersonnes < ActiveRecord::Migration
  def self.up
    create_table :evenement_personnes do |t|
      t.string :texte
      t.integer :type_evenement_personne_id

      t.timestamps
    end
  end

  def self.down
    drop_table :evenement_personnes
  end
end

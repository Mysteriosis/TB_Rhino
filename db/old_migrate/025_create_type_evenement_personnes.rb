# -*- encoding : utf-8 -*-
class CreateTypeEvenementPersonnes < ActiveRecord::Migration
  def self.up
    create_table :type_evenement_personnes do |t|
      t.string :type_evenement

      t.timestamps
    end
  end

  def self.down
    drop_table :type_evenement_personnes
  end
end

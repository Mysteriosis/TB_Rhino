# -*- encoding : utf-8 -*-
class AddGenreProjetToTypeProjet < ActiveRecord::Migration
  def self.up
    add_column :type_projets, :genre_projet, :string
  end

  def self.down
    remove_column :type_projets, :genre_projet
  end
end

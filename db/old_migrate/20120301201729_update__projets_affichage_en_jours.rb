class UpdateProjetsAffichageEnJours < ActiveRecord::Migration
  def self.up
      add_column :projets, :affichage_charge_en_jours, :boolean
  end

  def self.down
      remove_column :projets, :affichage_charge_en_jours
  end
end

# -*- encoding : utf-8 -*-
class CreatePeriodes < ActiveRecord::Migration

  def self.up
    create_table :periodes  do |t|
      t.string :descriptif
      t.string :idPeriode
      t.date :dateDebut
      t.integer :dureeSemaine
      t.integer :type_periode_id
      t.integer :annee_academique_id

      t.timestamps
    end
  end

  def self.down
    drop_table :periodes
  end
end

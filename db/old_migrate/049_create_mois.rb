# -*- encoding : utf-8 -*-
class CreateMois < ActiveRecord::Migration
  def self.up
    create_table :mois do |t|
      t.string :mois
      t.integer :nbJoursOuvrables
      t.float :nbHeuresCompensation
      t.integer :annee_civile_id

      t.timestamps
    end
  end

  def self.down
    drop_table :mois
  end
end

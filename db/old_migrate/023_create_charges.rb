# -*- encoding : utf-8 -*-
class CreateCharges < ActiveRecord::Migration
  def self.up
    create_table :charges do |t| 
      t.integer :nbHeuresAnnuelles
      t.integer :coutHoraire
      t.string :texte
      t.integer :personne_id
      t.integer :projet_id
      t.integer :annee_civile_id
      
      t.timestamps 
    end
  end

  def self.down
    drop_table :charges
  end
end

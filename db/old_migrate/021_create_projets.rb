# -*- encoding : utf-8 -*-
class CreateProjets < ActiveRecord::Migration
  def self.up
    create_table :projets do |t|
      t.string :id_Projet
      t.string :nomOutlook
      t.string :nom
      t.string :description
      t.string :budgetFinancierPlanifie
      t.string :budgetHorairePlanifie
      t.string :statut_projet_id
      t.string :type_projet_id
      t.string :personne_id
      t.string :unite_enseignement_id

      t.timestamps
    end
  end

  def self.down
    drop_table :projets
  end
end

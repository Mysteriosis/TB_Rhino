# -*- encoding : utf-8 -*-
class CreateTypeProjetActivites < ActiveRecord::Migration
  def self.up
    create_table :type_projet_activites do |t|
      t.integer :type_projet_id
      t.integer :type_activite_id

      t.timestamps
    end
  end

  def self.down
    drop_table :type_projet_activites
  end
end

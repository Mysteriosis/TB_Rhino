class UpdateProjetsEstActif < ActiveRecord::Migration
  def self.up
        add_column :projets, :est_actif, :boolean
  end

  def self.down
        remove_column :projets, :est_actif
  end
end

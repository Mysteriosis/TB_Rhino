# -*- encoding : utf-8 -*-
class CreateAnneeCiviles < ActiveRecord::Migration
  def self.up
    create_table :annee_civiles do |t|
      t.string :annee

      t.timestamps
    end
  end

  def self.down
    drop_table :annee_civiles
  end
end

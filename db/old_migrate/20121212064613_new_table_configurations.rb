class NewTableConfigurations < ActiveRecord::Migration
  def self.up
    create_table :configurations do |t|
      t.string :description
      t.string :valeur

      t.timestamps
    end
  end

  def self.down
    drop_table :configurations
  end
end

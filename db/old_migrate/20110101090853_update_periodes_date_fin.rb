class UpdatePeriodesDateFin < ActiveRecord::Migration
  def self.up
    add_column :periodes, :date_fin, :date
    rename_column :periodes, :dateDebut, :date_debut
  end

  def self.down
    remove_column :periodes, :date_fin
    rename_column :periodes, :date_debut, :dateDebut
  end
end

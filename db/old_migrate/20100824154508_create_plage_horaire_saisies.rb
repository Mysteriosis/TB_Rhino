# -*- encoding : utf-8 -*-
class CreatePlageHoraireSaisies < ActiveRecord::Migration
  def self.up
    create_table :plage_horaire_saisies do |t|
      t.integer :affectation_projet_id
      t.datetime :debut
      t.datetime :fin
      t.float :heures
      t.text :commentaire

      t.timestamps
    end
  end

  def self.down
    drop_table :plage_horaire_saisies
  end
end

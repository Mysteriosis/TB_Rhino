# -*- encoding : utf-8 -*-
class CreateContratAnnuels < ActiveRecord::Migration
  def self.up
    create_table :contrat_annuels do |t|
      t.integer :personne_id
      t.integer :annee_civile_id
      t.timestamps
    end
  end

  def self.down
    drop_table :contrat_annuels
  end
end

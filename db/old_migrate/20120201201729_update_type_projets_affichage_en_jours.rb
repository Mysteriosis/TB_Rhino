class UpdateTypeProjetsAffichageEnJours < ActiveRecord::Migration
  def self.up
      add_column :type_projets, :affichage_possible_charge_en_jours, :boolean
  end

  def self.down
      remove_column :type_projets, :affichage_possible_charge_en_jours
  end
end

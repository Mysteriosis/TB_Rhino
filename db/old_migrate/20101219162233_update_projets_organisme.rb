class UpdateProjetsOrganisme < ActiveRecord::Migration
  def self.up
        remove_column :projets, :organisme_id
  end

  def self.down
        add_column :projets, :organisme_id, :int
  end
end

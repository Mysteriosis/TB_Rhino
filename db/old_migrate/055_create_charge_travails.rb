# -*- encoding : utf-8 -*-
class CreateChargeTravails < ActiveRecord::Migration
  def self.up
    create_table :charge_travails do |t|
      t.integer :pourcentage
      t.integer :contrat
      t.integer :type_contrat_id
      t.integer :contrat_id
      t.timestamps
    end
  end

  def self.down
    drop_table :charge_travails
  end
end

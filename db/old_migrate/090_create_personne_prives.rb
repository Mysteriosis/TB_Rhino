# -*- encoding : utf-8 -*-
class CreatePersonnePrives < ActiveRecord::Migration
  def self.up
    create_table :personne_prives do |t|
      t.date   :date_naissance
      t.string :rue
      t.integer :codePostal
      t.string :ville
      t.string :pays
      t.string :telExterne
      t.string :telUrgence
      t.string :emailPrive
      t.integer :personne_id
      t.timestamps
    end
  end

  def self.down
    drop_table :personne_prives
  end
end

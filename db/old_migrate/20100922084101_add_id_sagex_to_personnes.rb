# -*- encoding : utf-8 -*-
class AddIdSagexToPersonnes < ActiveRecord::Migration
  def self.up
    add_column :personnes, :id_sagex, :string
    add_column :affectation_projets, :export_sagex, :boolean
  end

  def self.down
    remove_column :personnes, :id_sagex
    remove_column :affectation_projets, :export_sagex
  end
end

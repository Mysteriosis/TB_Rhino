# -*- encoding : utf-8 -*-
class CreateSousTypeProjets < ActiveRecord::Migration
  def self.up
    create_table :sous_type_projets do |t|
      t.string :sousTypeProjet
      t.integer :type_projet_id

      t.timestamps
    end
  end

  def self.down
    drop_table :sous_type_projets
  end
end

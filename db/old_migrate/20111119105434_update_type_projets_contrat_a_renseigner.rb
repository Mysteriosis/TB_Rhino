class UpdateTypeProjetsContratARenseigner < ActiveRecord::Migration
  def self.up
    add_column :type_projets, :contrat_a_renseigner, :boolean
  end

  def self.down
    remove_column :type_projets, :contrat_a_renseigner
  end
end

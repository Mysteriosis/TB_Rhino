class UpdateAffectationEstActive < ActiveRecord::Migration
  def self.up
        add_column :affectation_projets, :est_active, :boolean
  end

  def self.down
        remove_column :affectation_projets, :est_active
  end
end

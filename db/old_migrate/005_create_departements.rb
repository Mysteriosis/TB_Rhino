# -*- encoding : utf-8 -*-
class CreateDepartements < ActiveRecord::Migration
  def self.up
    create_table :departements do |t|
      t.string :abreviation
      t.string :texte
      t.integer :annee_academique_id
      t.integer :personne_id

      t.timestamps
    end
    execute "alter table departements add constraint fk_departements_annee_academiques foreign key(annee_academique_id) references annee_academiques(id)"
  end

  def self.down
    drop_table :departements
  end
end

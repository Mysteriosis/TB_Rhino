# -*- encoding : utf-8 -*-
class CreateStatutProjets < ActiveRecord::Migration
  def self.up
    create_table :statut_projets do |t|
      t.string :statut

      t.timestamps
    end
  end

  def self.down
    drop_table :statut_projets
  end
end

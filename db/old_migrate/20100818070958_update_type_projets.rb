# -*- encoding : utf-8 -*-
class UpdateTypeProjets < ActiveRecord::Migration
  def self.up
    add_column :type_projets, :parent_id, :integer
    add_column :type_projets, :mode_calcul_charge, :string
    rename_column :type_projets, :typeProjet, :nom
  end

  def self.down
    remove_column :type_projets, :parent_id
    remove_column :type_projets, :mode_calcul_charge
    rename_column :type_projets, :nom, :typeProjet
  end
end

class CreatePlanningRessourcesProjets < ActiveRecord::Migration
  def self.up
    create_table :planning_ressources_projets do |t|
      t.date :date, :null => false
      t.integer :pourcentage, :null => false
      t.integer :affectation_projet_id, :null => false
      t.string :commentaire
    end
    
    add_index :planning_ressources_projets, [:date, :affectation_projet_id], :unique => true, :name => "date"
  end

  def self.down
    drop_table :planning_ressources_projets
  end
end

# -*- encoding : utf-8 -*-
class CreateAffectationProjets < ActiveRecord::Migration
  def self.up
    create_table :affectation_projets do |t|
      t.integer :projet_id
      t.integer :personne_id
      t.date :date_debut
      t.date :date_fin
      t.integer :contrat_id
      t.integer :charge_horaire
      t.integer :cout_horaire
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :affectation_projets
  end
end

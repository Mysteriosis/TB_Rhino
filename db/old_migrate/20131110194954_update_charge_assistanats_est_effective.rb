class UpdateChargeAssistanatsEstEffective < ActiveRecord::Migration
  def self.up
    add_column :charge_assistanats, :est_effective, :boolean , :default => false
  end

  def self.down
    remove_column :charge_assistanats, :est_effective
  end
end

class UpdatePlageHoraireSaisiesCommentaire < ActiveRecord::Migration
  def self.up
        change_column_type :plage_horaire_saisies, :commentaire, :string(100)
  end

  def self.down
        change_column_type :plage_horaire_saisies, :commentaire, :text
  end
end

class DeleteTableOrganismes < ActiveRecord::Migration
  def self.up
      drop_table :organismes
  end

  def self.down
    create_table :organismes do |t|
      t.string :nom
      t.text :description

      t.timestamps
    end
  end
end

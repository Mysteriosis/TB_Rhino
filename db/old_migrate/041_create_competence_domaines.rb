# -*- encoding : utf-8 -*-
class CreateCompetenceDomaines < ActiveRecord::Migration
  def self.up
    create_table :competence_domaines do |t|
      t.string :domaine

      t.timestamps
    end
  end

  def self.down
    drop_table :competence_domaines
  end
end

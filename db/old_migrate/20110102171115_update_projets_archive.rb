class UpdateProjetsArchive < ActiveRecord::Migration
  def self.up
    add_column :projets, :est_archive, :boolean
  end

  def self.down
    remove_column :projets, :est_archive
  end
end

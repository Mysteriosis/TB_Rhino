# -*- encoding : utf-8 -*-
class UpdateProjets < ActiveRecord::Migration
  def self.up
    
    add_column :projets, :id_interne, :string
    add_column :projets, :id_externe, :string
    add_column :projets, :type_id_externe_id, :integer
    add_column :projets, :type, :string
    change_column :projets, :type_projet_id, :integer
    change_column :projets, :description, :text
    change_column :projets, :personne_id, :integer
    rename_column :projets, :personne_id, :chef_id
    add_column :projets, :date_debut, :date
    add_column :projets, :date_fin, :date
    
    # update existing projets
    Projet.all.each do |projet|
      projet.id_interne = projet_id_externe = projet.id_Projet
      #projet.type_id_externe_id = TypeIdExterne.find_by_nom("GAPS")
      projet.type = "ProjetEnseignement"
    end
    
    remove_column :projets, :id_Projet
    remove_column :projets, :nomOutlook
    remove_column :projets, :nom
    remove_column :projets, :budgetFinancierPlanifie
    remove_column :projets, :budgetHorairePlanifie
    remove_column :projets, :statut_projet_id

  end

  def self.down

    remove_column :projets, :id_interne
    remove_column :projets, :id_externe
    remove_column :projets, :type
    remove_column :projets, :type_id_externe_id
    change_column :projets, :description, :string
    change_column :projets, :type_projet_id, :string
    rename_column :projets, :chef_id, :personne_id
    change_column :projets, :personne_id, :string
    remove_column :projets, :date_debut
    remove_column :projets, :date_fin
    
    add_column :projets, :id_Projet, :string
    add_column :projets, :nomOutlook, :string
    add_column :projets, :nom, :string
    add_column :projets, :budgetFinancierPlanifie, :string
    add_column :projets, :budgetHorairePlanifie, :string
    add_column :projets, :statut_projet_id, :string
    rename_column :projets, :chef_id, :personne_id
  end
end

# -*- encoding : utf-8 -*-
class CreateContratMensuels < ActiveRecord::Migration
  def self.up
    create_table :contrat_mensuels do |t|
      t.integer :quotient
      t.integer :personne_id
      t.integer :moi_id
      t.date :date
      t.integer :contrat_annuel_id
      t.integer :type_contrat_id
      t.timestamps
    end
  end

  def self.down
    drop_table :contrat_mensuels
  end
end

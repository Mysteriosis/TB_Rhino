class UpdatePersonnesHoraire < ActiveRecord::Migration
  def self.up
    add_column :personnes, :horaire, :string
  end

  def self.down
    remove_column :personnes, :horaire
  end
end

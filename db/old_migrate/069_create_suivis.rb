# -*- encoding : utf-8 -*-
class CreateSuivis < ActiveRecord::Migration
  def self.up
    create_table :suivis do |t|
      t.integer :nbHeures
      t.integer :type_activite_id
      t.integer :moi_id
      t.integer :projet_id
      t.integer :personne_id

      t.timestamps
    end
  end

  def self.down
    drop_table :suivis
  end
end

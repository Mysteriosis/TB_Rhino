# -*- encoding : utf-8 -*-
class CreateUniteEnseignements < ActiveRecord::Migration
  # Création de la table unite_enseignement
  def self.up
    create_table :unite_enseignements do |t|
      t.string :abreviation
      t.string :nom
      t.integer :nbPeriodesTheoriquesSemaine
      t.integer :nbPeriodesPratiquesSemaine
      t.boolean :coursBloc
      t.integer :nbPeriodesTheoriquesTotal
      t.integer :nbPeriodesPratiquesTotal
      t.integer :nbGroupes
      t.float :chargeExamen
      t.string :pdf
      t.string :texte
      t.boolean :reconductible
      t.integer :type_unite_enseignement_id
      t.integer :personne_id, :default => 46
      t.integer :periode_id 

      t.timestamps
    end
  end
  # Suppression de la table unite_enseignement
  def self.down
    drop_table :unite_enseignements
  end
end


# -*- encoding : utf-8 -*-
class CreateTypeIdExternes < ActiveRecord::Migration
  def self.up
    create_table :type_id_externes do |t|
      t.string :nom
      t.string :description
    
      t.timestamps
    end
    
  end

  def self.down
    drop_table :type_id_externes
  end
end

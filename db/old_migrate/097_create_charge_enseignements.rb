# -*- encoding : utf-8 -*-
class CreateChargeEnseignements < ActiveRecord::Migration
  def self.up
    create_table :charge_enseignements do |t|
      t.float :tauxTheorique
      t.float :tauxPratique
      t.integer :nbGroupes
      t.integer :numGroupe
      t.integer :personne_id
      t.integer :unite_enseignement_id
      t.integer :desiderata_id

      t.timestamps
    end
  end

  def self.down
    drop_table :charge_enseignements
  end
end

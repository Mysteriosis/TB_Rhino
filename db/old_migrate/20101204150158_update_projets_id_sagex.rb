class UpdateProjetsIdSagex < ActiveRecord::Migration
  def self.up
    add_column :projets, :id_sagex, :string
  end

  def self.down
    remove_column :projets, :id_sagex
  end
end

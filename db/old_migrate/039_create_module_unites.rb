# -*- encoding : utf-8 -*-
class CreateModuleUnites < ActiveRecord::Migration
  def self.up
    create_table :module_unites do |t|
      t.string :abreviation
      t.string :texte
      t.integer :annee_academique_id

      t.timestamps
    end
  end

  def self.down
    drop_table :module_unites
  end
end

# -*- encoding : utf-8 -*-
class CreateEvenementProjets < ActiveRecord::Migration
  def self.up
    create_table :evenement_projets do |t|
      t.string :texte
      t.integer :type_evenement_projet_id

      t.timestamps
    end
  end

  def self.down
    drop_table :evenement_projets
  end
end

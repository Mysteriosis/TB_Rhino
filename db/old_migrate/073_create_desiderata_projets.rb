# -*- encoding : utf-8 -*-
class CreateDesiderataProjets < ActiveRecord::Migration
  def self.up
    create_table :desiderata_projets do |t|
      t.integer :nbHeuresRetD
      t.integer :nbHeuresEnseignement
      t.integer :personne_id
      t.integer :annee_academique_id

      t.timestamps
    end
  end

  def self.down
    drop_table :desiderata_projets
  end
end

# -*- encoding : utf-8 -*-
class CreateAnneeAcademiques < ActiveRecord::Migration
  def self.up
    create_table :annee_academiques do |t|
      t.string :annee

      t.timestamps
    end
  end

  def self.down
    drop_table :annee_academiques
  end
end

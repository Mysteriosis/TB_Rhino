# -*- encoding : utf-8 -*-
class CreateContrats < ActiveRecord::Migration
  def self.up
    create_table :contrats do |t|
      t.date :start_date
      t.date :end_date
      t.integer :pourcentage
      t.integer :personne_id
      t.integer :type_contrat_id
      t.timestamps
    end
  end

  def self.down
    drop_table :contrats
  end
end

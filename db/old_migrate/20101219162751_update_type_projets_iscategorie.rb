class UpdateTypeProjetsIscategorie < ActiveRecord::Migration
  def self.up
      remove_column :type_projets, :is_categorie
  end

  def self.down
      add_column :type_projets, :is_categorie, :boolean
  end
end

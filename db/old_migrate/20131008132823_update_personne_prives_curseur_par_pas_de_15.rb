class UpdatePersonnePrivesCurseurParPasDe15 < ActiveRecord::Migration

  def self.up
    add_column :personne_prives, :saisie_par_pas_de_15mn, :boolean , :default => true
  end

  def self.down
    remove_column :personne_prives, :saisie_par_pas_de_15mn
  end
end

# -*- encoding : utf-8 -*-
class CreatePersonneDesideratas < ActiveRecord::Migration
  def self.up
    create_table :personne_desideratas do |t|
      t.integer :priorite
      t.integer :personne_id
      t.integer :desiderata_id

      t.timestamps
    end
  end

  def self.down
    drop_table :personne_desideratas
  end
end

class UpdateTypeProjetCompteSagexParDefaut < ActiveRecord::Migration
  def self.up
    add_column :type_projets, :compte_sagex_par_defaut, :string
  end

  def self.down
    remove_column :type_projets, :compte_sagex_par_defaut
  end
end

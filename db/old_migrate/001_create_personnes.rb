# -*- encoding : utf-8 -*-
class CreatePersonnes < ActiveRecord::Migration
  def self.up
    create_table :personnes do |t|
      t.string :initiale
      t.string :prenom
      t.string :nom
      t.string :telInterne
      t.string :emailTravail
      t.string :bureau
      t.string  :prof_responsable
      t.boolean :institut
      t.boolean :actif
      t.integer :type_personne_id
      t.integer :axe_strategique_id
      t.integer :roles_mask
      t.string :login 
      t.string :persistence_token 
      t.integer :login_count 
      t.datetime :last_request_at 
      t.datetime :last_login_at 
      t.datetime :current_login_at 
      t.string :last_login_ip 
      t.string :current_login_ip 
      t.timestamps 

  
    end
    
   
  end

  def self.down
    drop_table :personnes
  end
end

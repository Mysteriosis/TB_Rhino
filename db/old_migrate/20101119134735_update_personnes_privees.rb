# -*- encoding : utf-8 -*-
class UpdatePersonnesPrivees < ActiveRecord::Migration
  def self.up
        add_column :personne_prives, :communication_tel_autorisee, :boolean
  end

  def self.down
        remove_column :personne_prives, :communication_tel_autorisee
  end
end

# -*- encoding : utf-8 -*-
class CreateDesirPersonneUnites < ActiveRecord::Migration
  def self.up
    create_table :desir_personne_unites do |t| 
      t.integer :personne_id 
      t.integer :unite_enseignement_id 
      
      t.timestamps 
    end
  end

  def self.down
    drop_table :desir_personne_unites 
  end
end

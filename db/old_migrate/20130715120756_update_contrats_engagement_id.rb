class UpdateContratsEngagementId < ActiveRecord::Migration
  def self.up
    add_column :contrats, :engagement_id, :int
  end

  def self.down
    remove_column :contrats, :engagement_id
  end
end

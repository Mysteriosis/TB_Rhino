# -*- encoding : utf-8 -*-
class CreateDateEvenementPersonnes < ActiveRecord::Migration
  def self.up
  	create_table :date_evenement_personnes do |t| 
      t.date :date 
      t.integer :personne_id
      t.integer :evenement_personne_id 
      
      t.timestamps 
    end
  end

  def self.down
  	drop_table :date_evenement_personnes
  end
end




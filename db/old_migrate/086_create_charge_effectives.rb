# -*- encoding : utf-8 -*-
class CreateChargeEffectives < ActiveRecord::Migration
  def self.up
    create_table :charge_effectives do |t|
      t.float :tauxTheorique
      t.float :tauxPratique
      t.integer :nbGroupes
      t.integer :personne_id
      t.integer :unite_enseignement_id

      t.timestamps
    end
  end

  def self.down
    drop_table :charge_effectives
  end
end

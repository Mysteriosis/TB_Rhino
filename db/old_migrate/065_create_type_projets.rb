# -*- encoding : utf-8 -*-
class CreateTypeProjets < ActiveRecord::Migration
  def self.up
    create_table :type_projets do |t|
      t.string :typeProjet
      t.string :facteur
      t.integer :type_projet_id

      t.timestamps
    end
  end

  def self.down
    drop_table :type_projets
  end
end

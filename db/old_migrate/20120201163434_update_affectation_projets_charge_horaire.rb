class UpdateAffectationProjetsChargeHoraire < ActiveRecord::Migration
  def self.up
    change_column :affectation_projets, :charge_horaire, :float, :null => true
  end

  def self.down
    change_column :affectation_projets, :charge_horaire, :int, :length => 11, :null => true
  end
end

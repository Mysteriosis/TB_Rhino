# -*- encoding : utf-8 -*-
class CreateDesideratas < ActiveRecord::Migration
  def self.up
    create_table :desideratas do |t|
      t.float :tauxTheorique
      t.float :tauxPratique
      t.integer :nbGroupes
      t.integer :personne_id
      t.integer :unite_enseignement_id

      t.timestamps
    end
  end

  def self.down
    drop_table :desideratas
  end
end

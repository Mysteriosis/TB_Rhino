class UpdateAffectationProjetsEngagementId < ActiveRecord::Migration
  def self.up
    add_column :affectation_projets, :engagement_id, :int
  end

  def self.down
    remove_column :affectation_projets, :engagement_id
  end
end

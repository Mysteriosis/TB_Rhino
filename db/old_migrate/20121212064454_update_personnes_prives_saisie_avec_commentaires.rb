class UpdatePersonnesPrivesSaisieAvecCommentaires < ActiveRecord::Migration
  def self.up
        add_column :personne_prives, :saisie_avec_commentaire, :boolean
  end

  def self.down
        remove_column :personne_prives, :saisie_avec_commentaire
  end
end

# -*- encoding : utf-8 -*-
class CreatePersonneChargeEnseignements < ActiveRecord::Migration
  def self.up
    create_table :personne_charge_enseignements do |t|
      t.integer :personne_id
      t.integer :charge_enseignements_id
      t.timestamps
    end
  end

  def self.down
    drop_table :personne_charge_enseignements
  end
end

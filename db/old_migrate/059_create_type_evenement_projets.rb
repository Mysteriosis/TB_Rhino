# -*- encoding : utf-8 -*-
class CreateTypeEvenementProjets < ActiveRecord::Migration
  def self.up
    create_table :type_evenement_projets do |t|
      t.string :typeEvenementProjet

      t.timestamps
    end
  end

  def self.down
    drop_table :type_evenement_projets
  end
end

# -*- encoding : utf-8 -*-
class CreateCompetencePersonneDomaines < ActiveRecord::Migration
  def self.up
  	create_table :competence_personne_domaines do |t| 
      t.integer :personne_id 
      t.integer :competence_domaine_id
      t.integer :niveau_competence_id
      
      t.timestamps 
    end
  end

  def self.down
  	drop_table :competence_personne_domaines 
  end
end




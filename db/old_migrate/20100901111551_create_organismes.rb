# -*- encoding : utf-8 -*-
class CreateOrganismes < ActiveRecord::Migration
  def self.up
    create_table :organismes do |t|
      t.string :nom
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :organismes
  end
end

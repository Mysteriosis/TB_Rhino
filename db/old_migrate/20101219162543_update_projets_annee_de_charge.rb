class UpdateProjetsAnneeDeCharge < ActiveRecord::Migration
  def self.up
        remove_column :projets, :annee_academique_charge_id
  end

  def self.down
        add_column :projets, :annee_academique_charge_id, :int
  end
end

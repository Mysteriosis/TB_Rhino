# -*- encoding : utf-8 -*-
class UpdateSaisieHeuresPossible < ActiveRecord::Migration
  def self.up
    rename_column :type_projets, :saisie_heures_possible, :saisie_heures_possible_collaborateurs
    add_column :type_projets, :saisie_heures_possible_profs, :boolean
  end

  def self.down
    remove_column :type_projets, :saisie_heures_possible_profs
    rename_column :type_projets, :saisie_heures_possible_collaborateurs, :saisie_heures_possible
  end
end

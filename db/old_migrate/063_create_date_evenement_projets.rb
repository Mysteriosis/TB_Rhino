# -*- encoding : utf-8 -*-
class CreateDateEvenementProjets < ActiveRecord::Migration
  def self.up
    create_table :date_evenement_projets do |t|
      t.date :date
      t.integer :projet_id
      t.integer :evenement_projet_id

      t.timestamps
    end
  end

  def self.down
    drop_table :date_evenement_projets
  end
end

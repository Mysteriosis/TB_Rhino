# -*- encoding : utf-8 -*-
class CreateTypeUniteEnseignements < ActiveRecord::Migration
  def self.up
    create_table :type_unite_enseignements do |t|
      t.string :type_unite

      t.timestamps
    end
  end

  def self.down
    drop_table :type_unite_enseignements
  end
end

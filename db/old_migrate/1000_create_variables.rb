# -*- encoding : utf-8 -*-
class CreateVariables < ActiveRecord::Migration
  def self.up
    create_table :variables do |t|
      t.string :nom
      t.string :valeur

      t.timestamps
    end
  end

  def self.down
    drop_table :variables
  end
end

class UpdateAffectationProjetsDisplayColor < ActiveRecord::Migration
  def self.up
      add_column :affectation_projets, :display_label, :string
      add_column :affectation_projets, :display_couleur, :string
      add_column :affectation_projets, :saisie_heures_par_RH_uniquement, :boolean
  end

  def self.down
      remove_column :affectation_projets, :display_label
      remove_column :affectation_projets, :display_couleur
      remove_column :affectation_projets, :saisie_heures_par_RH_uniquement
  end
end

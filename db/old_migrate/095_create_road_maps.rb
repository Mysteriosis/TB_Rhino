# -*- encoding : utf-8 -*-
class CreateRoadMaps < ActiveRecord::Migration
   def self.up
     create_table :road_maps do |t|
      t.date    :start_date
      t.date    :end_date
      t.string  :description
      t.integer :type_personne
      t.integer :road_map_action_id
      t.timestamps
     end
   end

   def self.down
     drop_table :road_maps
   end
 end



# -*- encoding : utf-8 -*-
class CreateTypePersonnes < ActiveRecord::Migration
  def self.up
    create_table :type_personnes do |t|
      t.string :typePersonne

      t.timestamps
    end
  end

  def self.down
    drop_table :type_personnes
  end
end

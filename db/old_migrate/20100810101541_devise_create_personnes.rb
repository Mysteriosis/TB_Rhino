# -*- encoding : utf-8 -*-
class DeviseCreatePersonnes < ActiveRecord::Migration
  def self.up
    rename_column :personnes, :login, :username
    
    # remove the old columns
    remove_column :personnes, :persistence_token 
    remove_column :personnes, :login_count 
    remove_column :personnes, :last_request_at 
    remove_column :personnes, :last_login_at 
    remove_column :personnes, :current_login_at
    remove_column :personnes, :last_login_ip 
    remove_column :personnes, :current_login_ip    
    
    # rememberable
    add_column :personnes ,:remember_token, :string
    add_column :personnes, :remember_created_at , :datetime
     
    # trackable
    add_column :personnes, :sign_in_count,      :integer, :default => 0
    add_column :personnes, :current_sign_in_at, :datetime
    add_column :personnes, :last_sign_in_at,    :datetime
    add_column :personnes, :current_sign_in_ip, :string
    add_column :personnes, :last_sign_in_ip,    :string
    
    add_index :personnes, :username, :unique => true
  end

  def self.down
    
    remove_index :personnes, :username, :unique => true
    
    rename_column :personnes, :username, :login
    
    # remove the old columns
    add_column :personnes, :persistence_token , :string
    add_column :personnes, :login_count       , :integer
    add_column :personnes, :last_request_at   , :datetime
    add_column :personnes, :last_login_at     , :datetime
    add_column :personnes, :current_login_at  , :datetime
    add_column :personnes, :last_login_ip     , :string
    add_column :personnes, :current_login_ip  , :string  
    
    # rememberable
    remove_column :personnes ,:remember_token
    remove_column :personnes, :remember_created_at
     
    # trackable
    remove_column :personnes, :sign_in_count      
    remove_column :personnes, :current_sign_in_at 
    remove_column :personnes, :last_sign_in_at  
    remove_column :personnes, :current_sign_in_ip 
    remove_column :personnes, :last_sign_in_ip  
    
  end
end

# -*- encoding : utf-8 -*-
class CreateCompetencePersonneUnites < ActiveRecord::Migration
  def self.up
  	create_table :competence_personne_unites do |t| 
      t.integer :personne_id 
      t.integer :unite_enseignement_id
	t.integer :niveau_competence_id 
      
      t.timestamps 
    end
  end

  def self.down
  	drop_table :competence_personne_unites 
  end
end




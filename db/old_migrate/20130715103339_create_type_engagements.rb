class CreateTypeEngagements < ActiveRecord::Migration
  def self.up
    create_table :type_engagements do |t|
      t.string :sigle
      t.float :libelle
      t.timestamps
    end
  end

  def self.down
      drop_table :type_engagements
  end
end

# -*- encoding : utf-8 -*-
class CreateChargeAssistants < ActiveRecord::Migration
  def self.up
    create_table :charge_assistants do |t|

      t.integer :nbr_heure
      t.integer :cahier_charge_assistant_id
      t.integer :projet_id
      
      t.timestamps
    end
  end

  def self.down
    drop_table :charge_assistants
  end
end

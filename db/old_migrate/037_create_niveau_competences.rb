# -*- encoding : utf-8 -*-
class CreateNiveauCompetences < ActiveRecord::Migration
  def self.up
    create_table :niveau_competences do |t|
      t.string :niveau

      t.timestamps
    end
  end

  def self.down
    drop_table :niveau_competences
  end
end

# -*- encoding : utf-8 -*-
class CreateJourAcademiques < ActiveRecord::Migration
  def self.up
    create_table :jour_academiques do |t|
      t.date :date
      t.string :periode
      t.string :type_jour

      t.timestamps
    end
  end

  def self.down
    drop_table :jour_academiques
  end
end

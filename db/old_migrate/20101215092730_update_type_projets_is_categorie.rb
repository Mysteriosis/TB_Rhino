class UpdateTypeProjetsIsCategorie < ActiveRecord::Migration
  def self.up
    add_column :type_projets, :is_categorie, :boolean
  end

  def self.down
    remove_column :type_projets, :is_categorie
  end
end

# -*- encoding : utf-8 -*-
class RefactorProjets < ActiveRecord::Migration
  def self.up
    remove_column :projets, :type_id_externe_id
    add_column :projets, :nom_etudiant, :string
    add_column :projets, :annee_academique_realisation_id, :integer
    add_column :projets, :annee_academique_charge_id, :integer
    add_column :projets, :organisme_id, :integer
    
    add_column :affectation_projets, :avec_saisie_heures, :boolean
    add_column :affectation_projets, :taux, :integer
    add_column :affectation_projets, :type, :string
    
    add_column :type_projets, :type_id_externe_id, :integer
    add_column :type_projets, :est_singleton, :boolean
    add_column :type_projets, :saisie_heures_possible, :boolean
    add_column :type_projets, :charge_financiere_renseignee, :boolean
    add_column :type_projets, :date_debut_indeterminee, :boolean
    add_column :type_projets, :date_fin_indeterminee, :boolean
    
    
  end

  def self.down
    
    add_column :projets, :type_id_externe_id, :integer
    remove_column :projets, :nom_etudiant
    remove_column :projets, :annee_academique_realisation_id
    remove_column :projets, :annee_academique_charge_id
    remove_column :projets, :organisme_id
    
    remove_column :affectation_projets, :avec_saisie_heures
    remove_column :affectation_projets, :taux
    remove_column :affectation_projets, :type
    
    remove_column :type_projets, :type_id_externe_id
    remove_column :type_projets, :est_singleton
    remove_column :type_projets, :saisie_heures_possible
    remove_column :type_projets, :charge_financiere_renseignee
    remove_column :type_projets, :date_debut_indeterminee
    remove_column :type_projets, :date_fin_indeterminee
    
  end
end

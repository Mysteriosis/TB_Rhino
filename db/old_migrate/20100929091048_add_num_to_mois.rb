class AddNumToMois < ActiveRecord::Migration
  def self.up
    add_column :mois, :num, :integer
  end

  def self.down
    remove_column :mois, :num
  end
end

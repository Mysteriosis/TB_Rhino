# -*- encoding : utf-8 -*-
class CreateSuperviationPersonnes < ActiveRecord::Migration
  def self.up
  	# Creation de la table d'association entre Personne et Personne
  	create_table :personnes_personnes do |t| 
    	t.string :departement_id 
    	t.string :logiciel_id 
 
      	t.timestamps 
    end 
  end 
 
  def self.down 
        drop_table :personnes_personnes 
  end 
end

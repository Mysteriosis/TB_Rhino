# -*- encoding : utf-8 -*-
class AddRessourcesHumainesToBudgets < ActiveRecord::Migration
  def self.up
    add_column :type_budgets, :ressources_humaines, :boolean
  end

  def self.down
    remove_column :type_budgets, :ressources_humaines
  end
end

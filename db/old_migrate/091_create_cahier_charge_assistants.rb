# -*- encoding : utf-8 -*-
class CreateCahierChargeAssistants < ActiveRecord::Migration
  def self.up
    create_table :cahier_charge_assistants do |t|
      t.integer :heures_RD
      t.integer :heures_infra
      t.integer :heures_ens
      t.string :definition_poste
      t.string :remarque
      t.integer :personne_id
      t.integer :charge_assistant_id
      t.integer :annee_civile_id
      
      t.timestamps
    end
  end

  def self.down
    drop_table :cahier_charge_assistants
  end
end

# -*- encoding : utf-8 -*-
class CreateAxeStrategiques < ActiveRecord::Migration
  def self.up
    create_table :axe_strategiques do |t|
      t.string :nom
	  t.string :sigle
      t.timestamps
    end
  end

  def self.down
    drop_table :axe_strategiques
  end
end

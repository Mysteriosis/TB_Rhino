# -*- encoding : utf-8 -*-
class CreateOrientations < ActiveRecord::Migration
  def self.up
    create_table :orientations do |t|
      t.string :abreviation
      t.string :texte
      t.integer :filiere_id
      t.integer :annee_academique_id

      t.timestamps
    end
  end

  def self.down
    drop_table :orientations
  end
end

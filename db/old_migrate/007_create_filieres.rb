# -*- encoding : utf-8 -*-
class CreateFilieres < ActiveRecord::Migration
  def self.up
    create_table :filieres do |t|
      t.string :abreviation
      t.integer :departement_id
      t.integer :annee_academique_id

      t.timestamps
    end
  end

  def self.down
    drop_table :filieres
  end
end

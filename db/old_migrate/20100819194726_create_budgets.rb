# -*- encoding : utf-8 -*-
class CreateBudgets < ActiveRecord::Migration
  def self.up
    create_table :budgets do |t|
      t.string :horaire
      t.string :financier
      t.string :prioritaire
      t.integer :type_budget_id
      t.integer :projet_id

      t.timestamps
    end
  end

  def self.down
    drop_table :budgets
  end
end

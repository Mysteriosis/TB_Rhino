# -*- encoding : utf-8 -*-
class CreateRoadMapActions < ActiveRecord::Migration
   def self.up
     create_table :road_map_actions do |t|
      t.string :action
      t.timestamps
     end
   end

   def self.down
     drop_table :road_map_actions
   end
 end



# -*- encoding : utf-8 -*-
class CreateUniteEnseignementOrientations < ActiveRecord::Migration
  def self.up
   create_table :unite_enseignement_orientations do |t|
        t.integer :unite_enseignement_id
	t.integer :orientation_id
	t.timestamps
   end
  end

  def self.down
    drop_table :unite_enseignement_orientations
  end
end

# -*- encoding : utf-8 -*-
class CreateConstantes < ActiveRecord::Migration
  def self.up
    create_table :constantes do |t|
      t.string :description
      t.string :valeur
      t.integer :annee_academique_id
      t.integer :annee_civile_id

      t.timestamps
    end
  end

  def self.down
    drop_table :constantes
  end
end

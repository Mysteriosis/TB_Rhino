# -*- encoding : utf-8 -*-
class CreateTypeBudgets < ActiveRecord::Migration
  def self.up
    create_table :type_budgets do |t|
      t.string :nom
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :type_budgets
  end
end

# -*- encoding : utf-8 -*-
class CreateInteretUnites < ActiveRecord::Migration
   def self.up
     create_table :interet_unites do |t|
      t.integer :id
      t.string  :interet
      t.timestamps
     end
   end

   def self.down
     drop_table :interet_unites
   end
 end

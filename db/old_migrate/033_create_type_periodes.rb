# -*- encoding : utf-8 -*-
class CreateTypePeriodes < ActiveRecord::Migration
  def self.up
    create_table :type_periodes do |t|
      t.string :typePeriode

      t.timestamps
    end
  end

  def self.down
    drop_table :type_periodes
  end
end

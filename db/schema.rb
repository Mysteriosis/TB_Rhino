# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150625125100) do

  create_table "affectation_projets", force: :cascade do |t|
    t.integer  "projet_id",                       limit: 4
    t.integer  "personne_id",                     limit: 4
    t.date     "date_debut"
    t.date     "date_fin"
    t.integer  "engagement_id",                   limit: 4,     null: false
    t.integer  "cout_horaire",                    limit: 4
    t.text     "description",                     limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "avec_saisie_heures"
    t.integer  "taux",                            limit: 4
    t.string   "type",                            limit: 255
    t.boolean  "export_sagex"
    t.boolean  "est_active"
    t.float    "charge_horaire",                  limit: 24
    t.string   "display_couleur",                 limit: 30
    t.string   "display_label",                   limit: 30
    t.boolean  "saisie_heures_par_RH_uniquement"
  end

  create_table "annee_academiques", force: :cascade do |t|
    t.integer  "debut",      limit: 4,   null: false
    t.string   "annee",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "annee_civiles", force: :cascade do |t|
    t.string   "annee",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "axe_strategiques", force: :cascade do |t|
    t.string   "nom",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sigle",      limit: 10,  null: false
  end

  create_table "budgets", force: :cascade do |t|
    t.string   "horaire",        limit: 255
    t.string   "financier",      limit: 255
    t.string   "prioritaire",    limit: 255
    t.integer  "type_budget_id", limit: 4
    t.integer  "projet_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cahier_charge_assistants", force: :cascade do |t|
    t.integer  "heures_RD",           limit: 4
    t.integer  "heures_infra",        limit: 4
    t.integer  "heures_ens",          limit: 4
    t.string   "definition_poste",    limit: 255
    t.string   "remarque",            limit: 255
    t.integer  "personne_id",         limit: 4
    t.integer  "charge_assistant_id", limit: 4
    t.integer  "annee_civile_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "charge_assistanats", force: :cascade do |t|
    t.integer  "priorite",              limit: 4
    t.float    "taux",                  limit: 24
    t.float    "heures",                limit: 24
    t.string   "numGroupe",             limit: 1
    t.string   "typeGroupe",            limit: 1
    t.boolean  "est_effective",                    default: false
    t.integer  "personne_id",           limit: 4
    t.integer  "unite_enseignement_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "charge_assistants", force: :cascade do |t|
    t.integer  "nbr_heure",                  limit: 4
    t.integer  "cahier_charge_assistant_id", limit: 4
    t.integer  "projet_id",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "charge_enseignements", force: :cascade do |t|
    t.float    "taux",                  limit: 24
    t.integer  "heures",                limit: 4
    t.string   "numGroupe",             limit: 1
    t.string   "typeGroupe",            limit: 1
    t.boolean  "est_effective",                    default: false
    t.integer  "personne_id",           limit: 4
    t.integer  "unite_enseignement_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "charges", force: :cascade do |t|
    t.integer  "nbHeuresAnnuelles", limit: 4
    t.integer  "coutHoraire",       limit: 4
    t.string   "texte",             limit: 255
    t.integer  "personne_id",       limit: 4
    t.integer  "projet_id",         limit: 4
    t.integer  "annee_civile_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commentaires", force: :cascade do |t|
    t.integer "personne_id", limit: 4
    t.integer "planification_id", limit: 4, null: false
    t.text    "texte",       limit: 65535, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "commentaires", ["personne_id"], name: "auteur_idx", using: :btree
  add_index "commentaires", ["planification_id"], name: "planif_idx", using: :btree

  create_table "competence_domaine_desideratas", force: :cascade do |t|
    t.integer  "competence_domaine_id", limit: 4
    t.integer  "desiderata_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "competence_domaines", force: :cascade do |t|
    t.string   "domaine",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "competence_personne_domaines", force: :cascade do |t|
    t.integer  "personne_id",           limit: 4
    t.integer  "competence_domaine_id", limit: 4
    t.integer  "niveau_competence_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "competence_personne_unites", force: :cascade do |t|
    t.integer  "personne_id",           limit: 4
    t.integer  "unite_enseignement_id", limit: 4
    t.integer  "interet_unite_id",      limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conditions", force: :cascade do |t|
    t.integer  "critere1_id",  limit: 4, null: false
    t.integer  "critere2_id",  limit: 4, null: false
    t.integer  "condition_id", limit: 4
    t.string   "comparaison",  limit: 2, null: false
    t.string   "operation",    limit: 2
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "conditions", ["condition_id"], name: "fk_conditions_conditions1_idx", using: :btree
  add_index "conditions", ["critere1_id"], name: "fk_conditions_critere1_idx", using: :btree
  add_index "conditions", ["critere2_id"], name: "fk_conditions_critere2_idx", using: :btree

  create_table "configurations", force: :cascade do |t|
    t.string   "description", limit: 80, null: false
    t.string   "valeur",      limit: 80, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "configurations", ["description"], name: "description", using: :btree

  create_table "configurations_planifications", force: :cascade do |t|
    t.string   "variable",    limit: 45,  null: false
    t.string   "description", limit: 255, null: false
    t.string   "valeur",      limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "constantes", force: :cascade do |t|
    t.string   "description",         limit: 255
    t.string   "valeur",              limit: 255
    t.integer  "annee_academique_id", limit: 4
    t.integer  "annee_civile_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contrats", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "pourcentage",   limit: 4
    t.integer  "engagement_id", limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "criteres", force: :cascade do |t|
    t.integer  "element1_id", limit: 4
    t.integer  "element2_id", limit: 4
    t.integer  "type",        limit: 4, default: 1, null: false
    t.integer  "unite",       limit: 1, default: 1, null: false
    t.integer  "valeur",      limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "criteres", ["element1_id"], name: "fk_criteres_elements1_idx", using: :btree
  add_index "criteres", ["element2_id"], name: "fk_criteres_elements2_idx", using: :btree

  create_table "cst_heures", force: :cascade do |t|
    t.string   "sigle",        limit: 15, null: false
    t.string   "descriptif",   limit: 80
    t.string   "valeur",       limit: 20
    t.integer  "annee_civile", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "date_evenement_personnes", force: :cascade do |t|
    t.date     "date"
    t.integer  "personne_id",           limit: 4
    t.integer  "evenement_personne_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "date_evenement_projets", force: :cascade do |t|
    t.date     "date"
    t.integer  "projet_id",           limit: 4
    t.integer  "evenement_projet_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "departements", force: :cascade do |t|
    t.string   "abreviation",         limit: 255
    t.string   "texte",               limit: 255
    t.integer  "annee_academique_id", limit: 4
    t.integer  "personne_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "departements", ["annee_academique_id"], name: "fk_departements_annee_academiques", using: :btree

  create_table "desiderata_projets", force: :cascade do |t|
    t.integer  "nbHeuresRetD",         limit: 4
    t.integer  "nbHeuresEnseignement", limit: 4
    t.integer  "personne_id",          limit: 4
    t.integer  "annee_academique_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "desideratas", force: :cascade do |t|
    t.float    "tauxTheorique",         limit: 24
    t.float    "tauxPratique",          limit: 24
    t.integer  "nbGroupes",             limit: 4
    t.integer  "personne_id",           limit: 4
    t.integer  "unite_enseignement_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "desir_personne_unites", force: :cascade do |t|
    t.integer  "personne_id",           limit: 4
    t.integer  "unite_enseignement_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "elements", force: :cascade do |t|
    t.string   "nom",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "engagements", force: :cascade do |t|
    t.integer  "type_engagement_id", limit: 4
    t.integer  "personne_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "evenement_personnes", force: :cascade do |t|
    t.string   "texte",                      limit: 255
    t.integer  "type_evenement_personne_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "evenement_projets", force: :cascade do |t|
    t.string   "texte",                    limit: 255
    t.integer  "type_evenement_projet_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "filieres", force: :cascade do |t|
    t.string   "abreviation",         limit: 255
    t.integer  "departement_id",      limit: 4
    t.integer  "annee_academique_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "info_heures", force: :cascade do |t|
    t.boolean  "plages_ok"
    t.boolean  "report_ok"
    t.float    "report_heures_sup",             limit: 53
    t.float    "report_heures_sup_impose",      limit: 53
    t.float    "report_heures_vacances",        limit: 53
    t.float    "report_heures_vacances_impose", limit: 53
    t.integer  "personne_id",                   limit: 4
    t.integer  "annee_civile",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interet_unites", force: :cascade do |t|
    t.string   "interet",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jalons", force: :cascade do |t|
    t.integer  "affectation_projet_id", limit: 4,     null: false
    t.integer  "mail_id",          limit: 4
    t.integer  "personne_id",      limit: 4
    t.date     "date",                           null: false
    t.string   "titre",            limit: 255,   null: false
    t.text     "description",      limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "jalons", ["mail_id"], name: "mail_idx", using: :btree
  add_index "jalons", ["personne_id"], name: "auteur_idx", using: :btree
  add_index "jalons", ["affectation_projet_id"], name: "affectation_idx", using: :btree

  create_table "jour_academiques", force: :cascade do |t|
    t.date     "date"
    t.string   "descriptif", limit: 80
    t.string   "periode",    limit: 255
    t.string   "type_jour",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mails_personnes", force: :cascade do |t|
    t.integer  "mail_id",     limit: 4, null: false
    t.integer  "personne_id", limit: 4, null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "mails_personnes", ["mail_id"], name: "fk_mails_has_personnes_mails1_idx", using: :btree
  add_index "mails_personnes", ["personne_id"], name: "fk_mails_has_personnes_personnes1_idx", using: :btree

  create_table "mails_planifications", force: :cascade do |t|
    t.string   "objet",      limit: 255, default: "Notification Rhino", null: false
    t.date     "limite",                                                null: false
    t.integer  "frequence",  limit: 1,   default: 1,                    null: false
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
  end

  create_table "mois", force: :cascade do |t|
    t.string   "mois",                 limit: 255
    t.integer  "nbJoursOuvrables",     limit: 4
    t.float    "nbHeuresCompensation", limit: 24
    t.integer  "annee_civile_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "num",                  limit: 4
  end

  create_table "niveau_competences", force: :cascade do |t|
    t.string   "niveau",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "condition_id", limit: 4,                 null: false
    t.integer  "mail_id",      limit: 4
    t.string   "titre",        limit: 255,               null: false
    t.integer  "gravite",      limit: 1,     default: 1, null: false
    t.integer  "type",         limit: 1,     default: 1, null: false
    t.integer  "active",       limit: 1,     default: 1, null: false
    t.text     "description",  limit: 65535
    t.string   "couleur",      limit: 45
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "notifications", ["condition_id"], name: "fk_notifications_conditions1_idx", using: :btree
  add_index "notifications", ["mail_id"], name: "fk_notifications_mails1_idx", using: :btree

  create_table "orientations", force: :cascade do |t|
    t.string   "abreviation",         limit: 255
    t.string   "texte",               limit: 255
    t.integer  "nb_etudiants",        limit: 4
    t.string   "filiere_id",          limit: 255
    t.integer  "annee_academique_id", limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "periodes", force: :cascade do |t|
    t.string   "descriptif",          limit: 255
    t.integer  "annee",               limit: 4
    t.string   "idPeriode",           limit: 255
    t.string   "saison",              limit: 10,  default: " ", null: false
    t.date     "date_debut"
    t.integer  "dureeSemaine",        limit: 4
    t.integer  "type_periode_id",     limit: 4
    t.integer  "annee_academique_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "date_fin"
  end

  create_table "personne_desideratas", force: :cascade do |t|
    t.integer  "priorite",      limit: 4
    t.integer  "personne_id",   limit: 4
    t.integer  "desiderata_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "personne_prives", force: :cascade do |t|
    t.date     "date_naissance"
    t.string   "rue",                         limit: 255
    t.integer  "codePostal",                  limit: 4
    t.string   "ville",                       limit: 255
    t.string   "pays",                        limit: 255
    t.string   "telExterne",                  limit: 255
    t.string   "telUrgence",                  limit: 255
    t.string   "emailPrive",                  limit: 255
    t.integer  "personne_id",                 limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "communication_tel_autorisee"
    t.boolean  "mode_saisie_simple"
    t.boolean  "saisie_par_pas_de_15mn",                  default: true
    t.boolean  "compensation"
  end

  create_table "personnes", force: :cascade do |t|
    t.string   "initiale",              limit: 255
    t.string   "prenom",                limit: 255
    t.string   "nom",                   limit: 255
    t.string   "telInterne",            limit: 255
    t.string   "emailTravail",          limit: 255
    t.string   "bureau",                limit: 255
    t.string   "prof_responsable",      limit: 255
    t.boolean  "institut"
    t.boolean  "actif"
    t.boolean  "est_fictive"
    t.string   "horaire",               limit: 10,  default: "1111111111"
    t.integer  "type_personne_id",      limit: 4
    t.integer  "axe_strategique_id",    limit: 4
    t.integer  "roles_mask",            limit: 4
    t.string   "username",              limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token",        limit: 255
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",         limit: 4,   default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",    limit: 255
    t.string   "last_sign_in_ip",       limit: 255
    t.string   "id_sagex",              limit: 255
    t.float    "solde_heures_sup_2012", limit: 24,                         null: false
  end

  create_table "plage_contractuelles", force: :cascade do |t|
    t.integer  "no_plage",     limit: 4
    t.integer  "pourcentage",  limit: 4
    t.date     "date_debut"
    t.date     "date_fin"
    t.float    "hj",           limit: 53
    t.string   "horaire",      limit: 10, default: "1111111111"
    t.boolean  "compensation",            default: true
    t.integer  "personne_id",  limit: 4
    t.integer  "annee_civile", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plage_horaire_saisies", force: :cascade do |t|
    t.integer  "affectation_projet_id", limit: 4
    t.datetime "debut"
    t.datetime "fin"
    t.float    "heures",                limit: 24
    t.string   "commentaire",           limit: 80
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "plage_horaire_saisies", ["affectation_projet_id"], name: "affectations", using: :btree

  create_table "plage_mensuelles", force: :cascade do |t|
    t.integer  "no_mois",                limit: 4
    t.date     "date_debut"
    t.date     "date_fin"
    t.float    "jo",                     limit: 53
    t.float    "jf",                     limit: 53
    t.float    "jp",                     limit: 53
    t.float    "jo_",                    limit: 53
    t.float    "jt_",                    limit: 53
    t.float    "th_",                    limit: 53
    t.float    "hv",                     limit: 53
    t.integer  "plage_contractuelle_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "planifications", force: :cascade do |t|
    t.integer  "affectation_projet_id", limit: 4, null: false
    t.integer "personne_id",            limit: 4, null: true
    t.integer  "valeur",                limit: 4, null: false
    t.date     "date",                            null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "planifications", ["affectation_projet_id"], name: "fk_planifications_affectation_projet_idx", using: :btree
  add_index "planifications", ["personne_id"], name: "auteur_idx", using: :btree

  create_table "planning_ressources_projets", force: :cascade do |t|
    t.datetime "date",                              null: false
    t.integer  "pourcentage",           limit: 4,   null: false
    t.integer  "affectation_projet_id", limit: 4,   null: false
    t.string   "commentaire",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "planning_ressources_projets", ["date", "affectation_projet_id"], name: "date", unique: true, using: :btree

  create_table "projets", force: :cascade do |t|
    t.text     "description",                     limit: 65535
    t.integer  "type_projet_id",                  limit: 4
    t.integer  "chef_id",                         limit: 4
    t.string   "unite_enseignement_id",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "id_interne",                      limit: 255
    t.string   "id_externe",                      limit: 255
    t.string   "type",                            limit: 255
    t.date     "date_debut"
    t.date     "date_fin"
    t.string   "nom_etudiant",                    limit: 255
    t.integer  "annee_academique_realisation_id", limit: 4
    t.string   "id_sagex",                        limit: 255
    t.boolean  "est_actif"
    t.boolean  "est_archive"
    t.boolean  "affichage_charge_en_jours"
  end

  create_table "road_map_actions", force: :cascade do |t|
    t.string   "action",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "road_maps", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type_personne",      limit: 4,     null: false
    t.text     "description",        limit: 65535, null: false
    t.integer  "road_map_action_id", limit: 4,     null: false
  end

  create_table "statut_projets", force: :cascade do |t|
    t.string   "statut",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_budgets", force: :cascade do |t|
    t.string   "nom",                 limit: 255
    t.text     "description",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "ressources_humaines"
  end

  create_table "type_engagements", force: :cascade do |t|
    t.string   "sigle",      limit: 10
    t.string   "libelle",    limit: 10
    t.float    "prc_RD",     limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "type_engagements", ["sigle"], name: "sigle", unique: true, using: :btree

  create_table "type_evenement_personnes", force: :cascade do |t|
    t.string   "type_evenement", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_evenement_projets", force: :cascade do |t|
    t.string   "typeEvenementProjet", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_id_externes", force: :cascade do |t|
    t.string   "nom",         limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_periodes", force: :cascade do |t|
    t.string   "typePeriode", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_personnes", force: :cascade do |t|
    t.string   "typePersonne", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_projets", force: :cascade do |t|
    t.string   "nom",                                   limit: 255
    t.integer  "type_projet_id",                        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id",                             limit: 4
    t.string   "mode_calcul_charge",                    limit: 255
    t.integer  "type_id_externe_id",                    limit: 4
    t.boolean  "est_singleton"
    t.boolean  "saisie_heures_possible_collaborateurs"
    t.boolean  "charge_financiere_renseignee"
    t.boolean  "date_debut_indeterminee"
    t.boolean  "date_fin_indeterminee"
    t.string   "genre_projet",                          limit: 255
    t.boolean  "saisie_heures_possible_profs"
    t.string   "compte_sagex_par_defaut",               limit: 255
    t.boolean  "contrat_a_renseigner",                              null: false
    t.boolean  "affichage_possible_charge_en_jours"
  end

  create_table "type_unite_enseignements", force: :cascade do |t|
    t.string   "type_unite", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unite_enseignement_orientations", force: :cascade do |t|
    t.integer  "unite_enseignement_id", limit: 4
    t.integer  "orientation_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "unite_enseignements", force: :cascade do |t|
    t.string   "abreviation",                 limit: 255
    t.string   "nom",                         limit: 255
    t.integer  "nbGroupesTh",                 limit: 4
    t.integer  "nbGroupesLab",                limit: 4
    t.float    "nbPeriodesTheoriquesSemaine", limit: 24
    t.float    "nbPeriodesPratiquesSemaine",  limit: 24
    t.boolean  "coursBloc"
    t.float    "nbPeriodesTheoriquesTotal",   limit: 24
    t.float    "nbPeriodesPratiquesTotal",    limit: 24
    t.float    "chargeExamen",                limit: 24
    t.string   "pdf",                         limit: 255
    t.string   "texte",                       limit: 255
    t.boolean  "reconductible"
    t.boolean  "assistanat_pris_en_charge",               default: true
    t.boolean  "assistanat_reconnu",                      default: true
    t.integer  "type_unite_enseignement_id",  limit: 4
    t.integer  "personne_id",                 limit: 4,   default: 46
    t.integer  "periode_id",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "variables", force: :cascade do |t|
    t.string "nom",    limit: 255, null: false
    t.string "valeur", limit: 255, null: false
  end

end

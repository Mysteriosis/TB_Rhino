# -*- encoding : utf-8 -*-
require 'test_helper'

class TypeCategoriesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_categorie" do
    assert_difference('TypeCategorie.count') do
      post :create, :type_categorie => { }
    end

    assert_redirected_to type_categorie_path(assigns(:type_categorie))
  end

  test "should show type_categorie" do
    get :show, :id => type_categories(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_categories(:one).id
    assert_response :success
  end

  test "should update type_categorie" do
    put :update, :id => type_categories(:one).id, :type_categorie => { }
    assert_redirected_to type_categorie_path(assigns(:type_categorie))
  end

  test "should destroy type_categorie" do
    assert_difference('TypeCategorie.count', -1) do
      delete :destroy, :id => type_categories(:one).id
    end

    assert_redirected_to type_categories_path
  end
end

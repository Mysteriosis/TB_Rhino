# -*- encoding : utf-8 -*-
require 'test_helper'

class UniteEnseignementsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unite_enseignements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unite_enseignement" do
    assert_difference('UniteEnseignement.count') do
      post :create, :unite_enseignement => { }
    end

    assert_redirected_to unite_enseignement_path(assigns(:unite_enseignement))
  end

  test "should show unite_enseignement" do
    get :show, :id => unite_enseignements(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => unite_enseignements(:one).id
    assert_response :success
  end

  test "should update unite_enseignement" do
    put :update, :id => unite_enseignements(:one).id, :unite_enseignement => { }
    assert_redirected_to unite_enseignement_path(assigns(:unite_enseignement))
  end

  test "should destroy unite_enseignement" do
    assert_difference('UniteEnseignement.count', -1) do
      delete :destroy, :id => unite_enseignements(:one).id
    end

    assert_redirected_to unite_enseignements_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class ProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create projet" do
    assert_difference('Projet.count') do
      post :create, :projet => { }
    end

    assert_redirected_to projet_path(assigns(:projet))
  end

  test "should show projet" do
    get :show, :id => projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => projets(:one).id
    assert_response :success
  end

  test "should update projet" do
    put :update, :id => projets(:one).id, :projet => { }
    assert_redirected_to projet_path(assigns(:projet))
  end

  test "should destroy projet" do
    assert_difference('Projet.count', -1) do
      delete :destroy, :id => projets(:one).id
    end

    assert_redirected_to projets_path
  end
end

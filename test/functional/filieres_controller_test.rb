# -*- encoding : utf-8 -*-
require 'test_helper'

class FilieresControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:filieres)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create filiere" do
    assert_difference('Filiere.count') do
      post :create, :filiere => { }
    end

    assert_redirected_to filiere_path(assigns(:filiere))
  end

  test "should show filiere" do
    get :show, :id => filieres(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => filieres(:one).id
    assert_response :success
  end

  test "should update filiere" do
    put :update, :id => filieres(:one).id, :filiere => { }
    assert_redirected_to filiere_path(assigns(:filiere))
  end

  test "should destroy filiere" do
    assert_difference('Filiere.count', -1) do
      delete :destroy, :id => filieres(:one).id
    end

    assert_redirected_to filieres_path
  end
end

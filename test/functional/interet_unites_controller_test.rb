# -*- encoding : utf-8 -*-
require 'test_helper'

class InteretUnitesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:interet_unites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create interet_unite" do
    assert_difference('InteretUnite.count') do
      post :create, :interet_unite => { }
    end

    assert_redirected_to interet_unite_path(assigns(:interet_unite))
  end

  test "should show interet_unite" do
    get :show, :id => interet_unites(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => interet_unites(:one).to_param
    assert_response :success
  end

  test "should update interet_unite" do
    put :update, :id => interet_unites(:one).to_param, :interet_unite => { }
    assert_redirected_to interet_unite_path(assigns(:interet_unite))
  end

  test "should destroy interet_unite" do
    assert_difference('InteretUnite.count', -1) do
      delete :destroy, :id => interet_unites(:one).to_param
    end

    assert_redirected_to interet_unites_path
  end
end

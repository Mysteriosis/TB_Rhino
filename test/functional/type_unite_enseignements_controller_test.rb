# -*- encoding : utf-8 -*-
require 'test_helper'

class TypeUniteEnseignementsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_unite_enseignements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_unite_enseignement" do
    assert_difference('TypeUniteEnseignement.count') do
      post :create, :type_unite_enseignement => { }
    end

    assert_redirected_to type_unite_enseignement_path(assigns(:type_unite_enseignement))
  end

  test "should show type_unite_enseignement" do
    get :show, :id => type_unite_enseignements(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_unite_enseignements(:one).id
    assert_response :success
  end

  test "should update type_unite_enseignement" do
    put :update, :id => type_unite_enseignements(:one).id, :type_unite_enseignement => { }
    assert_redirected_to type_unite_enseignement_path(assigns(:type_unite_enseignement))
  end

  test "should destroy type_unite_enseignement" do
    assert_difference('TypeUniteEnseignement.count', -1) do
      delete :destroy, :id => type_unite_enseignements(:one).id
    end

    assert_redirected_to type_unite_enseignements_path
  end
end

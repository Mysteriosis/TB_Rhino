# -*- encoding : utf-8 -*-
require 'test_helper'

class PersonneChargeEnseignementsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personne_charge_enseignements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create personne_charge_enseignement" do
    assert_difference('PersonneChargeEnseignement.count') do
      post :create, :personne_charge_enseignement => { }
    end

    assert_redirected_to personne_charge_enseignement_path(assigns(:personne_charge_enseignement))
  end

  test "should show personne_charge_enseignement" do
    get :show, :id => personne_charge_enseignements(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => personne_charge_enseignements(:one).to_param
    assert_response :success
  end

  test "should update personne_charge_enseignement" do
    put :update, :id => personne_charge_enseignements(:one).to_param, :personne_charge_enseignement => { }
    assert_redirected_to personne_charge_enseignement_path(assigns(:personne_charge_enseignement))
  end

  test "should destroy personne_charge_enseignement" do
    assert_difference('PersonneChargeEnseignement.count', -1) do
      delete :destroy, :id => personne_charge_enseignements(:one).to_param
    end

    assert_redirected_to personne_charge_enseignements_path
  end
end

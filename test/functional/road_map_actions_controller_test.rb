# -*- encoding : utf-8 -*-
require 'test_helper'

class RoadMapActionsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:road_map_actions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create road_map_action" do
    assert_difference('RoadMapAction.count') do
      post :create, :road_map_action => { }
    end

    assert_redirected_to road_map_action_path(assigns(:road_map_action))
  end

  test "should show road_map_action" do
    get :show, :id => road_map_actions(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => road_map_actions(:one).to_param
    assert_response :success
  end

  test "should update road_map_action" do
    put :update, :id => road_map_actions(:one).to_param, :road_map_action => { }
    assert_redirected_to road_map_action_path(assigns(:road_map_action))
  end

  test "should destroy road_map_action" do
    assert_difference('RoadMapAction.count', -1) do
      delete :destroy, :id => road_map_actions(:one).to_param
    end

    assert_redirected_to road_map_actions_path
  end
end

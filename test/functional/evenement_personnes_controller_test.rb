# -*- encoding : utf-8 -*-
require 'test_helper'

class EvenementPersonnesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evenement_personnes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evenement_personne" do
    assert_difference('EvenementPersonne.count') do
      post :create, :evenement_personne => { }
    end

    assert_redirected_to evenement_personne_path(assigns(:evenement_personne))
  end

  test "should show evenement_personne" do
    get :show, :id => evenement_personnes(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => evenement_personnes(:one).id
    assert_response :success
  end

  test "should update evenement_personne" do
    put :update, :id => evenement_personnes(:one).id, :evenement_personne => { }
    assert_redirected_to evenement_personne_path(assigns(:evenement_personne))
  end

  test "should destroy evenement_personne" do
    assert_difference('EvenementPersonne.count', -1) do
      delete :destroy, :id => evenement_personnes(:one).id
    end

    assert_redirected_to evenement_personnes_path
  end
end

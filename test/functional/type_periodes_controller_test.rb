# -*- encoding : utf-8 -*-
require 'test_helper'

class TypePeriodesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_periodes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_periode" do
    assert_difference('TypePeriode.count') do
      post :create, :type_periode => { }
    end

    assert_redirected_to type_periode_path(assigns(:type_periode))
  end

  test "should show type_periode" do
    get :show, :id => type_periodes(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_periodes(:one).id
    assert_response :success
  end

  test "should update type_periode" do
    put :update, :id => type_periodes(:one).id, :type_periode => { }
    assert_redirected_to type_periode_path(assigns(:type_periode))
  end

  test "should destroy type_periode" do
    assert_difference('TypePeriode.count', -1) do
      delete :destroy, :id => type_periodes(:one).id
    end

    assert_redirected_to type_periodes_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class TypeEvenementProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_evenement_projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_evenement_projet" do
    assert_difference('TypeEvenementProjet.count') do
      post :create, :type_evenement_projet => { }
    end

    assert_redirected_to type_evenement_projet_path(assigns(:type_evenement_projet))
  end

  test "should show type_evenement_projet" do
    get :show, :id => type_evenement_projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_evenement_projets(:one).id
    assert_response :success
  end

  test "should update type_evenement_projet" do
    put :update, :id => type_evenement_projets(:one).id, :type_evenement_projet => { }
    assert_redirected_to type_evenement_projet_path(assigns(:type_evenement_projet))
  end

  test "should destroy type_evenement_projet" do
    assert_difference('TypeEvenementProjet.count', -1) do
      delete :destroy, :id => type_evenement_projets(:one).id
    end

    assert_redirected_to type_evenement_projets_path
  end
end

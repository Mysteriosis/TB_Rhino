# -*- encoding : utf-8 -*-
require 'test_helper'

class SousTypeProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sous_type_projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sous_type_projet" do
    assert_difference('SousTypeProjet.count') do
      post :create, :sous_type_projet => { }
    end

    assert_redirected_to sous_type_projet_path(assigns(:sous_type_projet))
  end

  test "should show sous_type_projet" do
    get :show, :id => sous_type_projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => sous_type_projets(:one).id
    assert_response :success
  end

  test "should update sous_type_projet" do
    put :update, :id => sous_type_projets(:one).id, :sous_type_projet => { }
    assert_redirected_to sous_type_projet_path(assigns(:sous_type_projet))
  end

  test "should destroy sous_type_projet" do
    assert_difference('SousTypeProjet.count', -1) do
      delete :destroy, :id => sous_type_projets(:one).id
    end

    assert_redirected_to sous_type_projets_path
  end
end

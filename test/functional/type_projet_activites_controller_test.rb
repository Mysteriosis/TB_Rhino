# -*- encoding : utf-8 -*-
require 'test_helper'

class TypeProjetActivitesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_projet_activites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_projet_activite" do
    assert_difference('TypeProjetActivite.count') do
      post :create, :type_projet_activite => { }
    end

    assert_redirected_to type_projet_activite_path(assigns(:type_projet_activite))
  end

  test "should show type_projet_activite" do
    get :show, :id => type_projet_activites(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_projet_activites(:one).id
    assert_response :success
  end

  test "should update type_projet_activite" do
    put :update, :id => type_projet_activites(:one).id, :type_projet_activite => { }
    assert_redirected_to type_projet_activite_path(assigns(:type_projet_activite))
  end

  test "should destroy type_projet_activite" do
    assert_difference('TypeProjetActivite.count', -1) do
      delete :destroy, :id => type_projet_activites(:one).id
    end

    assert_redirected_to type_projet_activites_path
  end
end

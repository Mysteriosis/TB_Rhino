# -*- encoding : utf-8 -*-
require 'test_helper'

class AnneeCivilesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:annee_civiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create annee_civile" do
    assert_difference('AnneeCivile.count') do
      post :create, :annee_civile => { }
    end

    assert_redirected_to annee_civile_path(assigns(:annee_civile))
  end

  test "should show annee_civile" do
    get :show, :id => annee_civiles(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => annee_civiles(:one).id
    assert_response :success
  end

  test "should update annee_civile" do
    put :update, :id => annee_civiles(:one).id, :annee_civile => { }
    assert_redirected_to annee_civile_path(assigns(:annee_civile))
  end

  test "should destroy annee_civile" do
    assert_difference('AnneeCivile.count', -1) do
      delete :destroy, :id => annee_civiles(:one).id
    end

    assert_redirected_to annee_civiles_path
  end
end

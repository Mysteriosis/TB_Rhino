# -*- encoding : utf-8 -*-
require 'test_helper'

class AxeStrategiquesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:axe_strategiques)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create axe_strategique" do
    assert_difference('AxeStrategique.count') do
      post :create, :axe_strategique => { }
    end

    assert_redirected_to axe_strategique_path(assigns(:axe_strategique))
  end

  test "should show axe_strategique" do
    get :show, :id => axe_strategiques(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => axe_strategiques(:one).to_param
    assert_response :success
  end

  test "should update axe_strategique" do
    put :update, :id => axe_strategiques(:one).to_param, :axe_strategique => { }
    assert_redirected_to axe_strategique_path(assigns(:axe_strategique))
  end

  test "should destroy axe_strategique" do
    assert_difference('AxeStrategique.count', -1) do
      delete :destroy, :id => axe_strategiques(:one).to_param
    end

    assert_redirected_to axe_strategiques_path
  end
end

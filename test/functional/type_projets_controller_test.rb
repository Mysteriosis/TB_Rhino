# -*- encoding : utf-8 -*-
require 'test_helper'

class TypeProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_projet" do
    assert_difference('TypeProjet.count') do
      post :create, :type_projet => { }
    end

    assert_redirected_to type_projet_path(assigns(:type_projet))
  end

  test "should show type_projet" do
    get :show, :id => type_projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_projets(:one).id
    assert_response :success
  end

  test "should update type_projet" do
    put :update, :id => type_projets(:one).id, :type_projet => { }
    assert_redirected_to type_projet_path(assigns(:type_projet))
  end

  test "should destroy type_projet" do
    assert_difference('TypeProjet.count', -1) do
      delete :destroy, :id => type_projets(:one).id
    end

    assert_redirected_to type_projets_path
  end
end

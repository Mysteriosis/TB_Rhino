# -*- encoding : utf-8 -*-
require 'test_helper'

class AxesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:axes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create axe" do
    assert_difference('Axe.count') do
      post :create, :axe => { }
    end

    assert_redirected_to axe_path(assigns(:axe))
  end

  test "should show axe" do
    get :show, :id => axes(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => axes(:one).to_param
    assert_response :success
  end

  test "should update axe" do
    put :update, :id => axes(:one).to_param, :axe => { }
    assert_redirected_to axe_path(assigns(:axe))
  end

  test "should destroy axe" do
    assert_difference('Axe.count', -1) do
      delete :destroy, :id => axes(:one).to_param
    end

    assert_redirected_to axes_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class CompetenceDomainesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:competence_domaines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create competence_domaine" do
    assert_difference('CompetenceDomaine.count') do
      post :create, :competence_domaine => { }
    end

    assert_redirected_to competence_domaine_path(assigns(:competence_domaine))
  end

  test "should show competence_domaine" do
    get :show, :id => competence_domaines(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => competence_domaines(:one).id
    assert_response :success
  end

  test "should update competence_domaine" do
    put :update, :id => competence_domaines(:one).id, :competence_domaine => { }
    assert_redirected_to competence_domaine_path(assigns(:competence_domaine))
  end

  test "should destroy competence_domaine" do
    assert_difference('CompetenceDomaine.count', -1) do
      delete :destroy, :id => competence_domaines(:one).id
    end

    assert_redirected_to competence_domaines_path
  end
end

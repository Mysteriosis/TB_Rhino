# -*- encoding : utf-8 -*-
require 'test_helper'

class ContratMensuelsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contrat_mensuels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contrat_mensuel" do
    assert_difference('ContratMensuel.count') do
      post :create, :contrat_mensuel => { }
    end

    assert_redirected_to contrat_mensuel_path(assigns(:contrat_mensuel))
  end

  test "should show contrat_mensuel" do
    get :show, :id => contrat_mensuels(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => contrat_mensuels(:one).id
    assert_response :success
  end

  test "should update contrat_mensuel" do
    put :update, :id => contrat_mensuels(:one).id, :contrat_mensuel => { }
    assert_redirected_to contrat_mensuel_path(assigns(:contrat_mensuel))
  end

  test "should destroy contrat_mensuel" do
    assert_difference('ContratMensuel.count', -1) do
      delete :destroy, :id => contrat_mensuels(:one).id
    end

    assert_redirected_to contrat_mensuels_path
  end
end

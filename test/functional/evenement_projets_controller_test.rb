# -*- encoding : utf-8 -*-
require 'test_helper'

class EvenementProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evenement_projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evenement_projet" do
    assert_difference('EvenementProjet.count') do
      post :create, :evenement_projet => { }
    end

    assert_redirected_to evenement_projet_path(assigns(:evenement_projet))
  end

  test "should show evenement_projet" do
    get :show, :id => evenement_projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => evenement_projets(:one).id
    assert_response :success
  end

  test "should update evenement_projet" do
    put :update, :id => evenement_projets(:one).id, :evenement_projet => { }
    assert_redirected_to evenement_projet_path(assigns(:evenement_projet))
  end

  test "should destroy evenement_projet" do
    assert_difference('EvenementProjet.count', -1) do
      delete :destroy, :id => evenement_projets(:one).id
    end

    assert_redirected_to evenement_projets_path
  end
end

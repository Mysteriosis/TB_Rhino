# -*- encoding : utf-8 -*-
require 'test_helper'

class ChargeEnseignementsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:charge_enseignements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create charge_enseignement" do
    assert_difference('ChargeEnseignement.count') do
      post :create, :charge_enseignement => { }
    end

    assert_redirected_to charge_enseignement_path(assigns(:charge_enseignement))
  end

  test "should show charge_enseignement" do
    get :show, :id => charge_enseignements(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => charge_enseignements(:one).to_param
    assert_response :success
  end

  test "should update charge_enseignement" do
    put :update, :id => charge_enseignements(:one).to_param, :charge_enseignement => { }
    assert_redirected_to charge_enseignement_path(assigns(:charge_enseignement))
  end

  test "should destroy charge_enseignement" do
    assert_difference('ChargeEnseignement.count', -1) do
      delete :destroy, :id => charge_enseignements(:one).to_param
    end

    assert_redirected_to charge_enseignements_path
  end
end

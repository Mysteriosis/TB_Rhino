# -*- encoding : utf-8 -*-
require 'test_helper'

class DesideratasControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:desideratas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create desiderata" do
    assert_difference('Desiderata.count') do
      post :create, :desiderata => { }
    end

    assert_redirected_to desiderata_path(assigns(:desiderata))
  end

  test "should show desiderata" do
    get :show, :id => desideratas(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => desideratas(:one).id
    assert_response :success
  end

  test "should update desiderata" do
    put :update, :id => desideratas(:one).id, :desiderata => { }
    assert_redirected_to desiderata_path(assigns(:desiderata))
  end

  test "should destroy desiderata" do
    assert_difference('Desiderata.count', -1) do
      delete :destroy, :id => desideratas(:one).id
    end

    assert_redirected_to desideratas_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class DesiderataPorjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:desiderata_porjets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create desiderata_porjet" do
    assert_difference('DesiderataPorjet.count') do
      post :create, :desiderata_porjet => { }
    end

    assert_redirected_to desiderata_porjet_path(assigns(:desiderata_porjet))
  end

  test "should show desiderata_porjet" do
    get :show, :id => desiderata_porjets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => desiderata_porjets(:one).id
    assert_response :success
  end

  test "should update desiderata_porjet" do
    put :update, :id => desiderata_porjets(:one).id, :desiderata_porjet => { }
    assert_redirected_to desiderata_porjet_path(assigns(:desiderata_porjet))
  end

  test "should destroy desiderata_porjet" do
    assert_difference('DesiderataPorjet.count', -1) do
      delete :destroy, :id => desiderata_porjets(:one).id
    end

    assert_redirected_to desiderata_porjets_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class SuivisControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:suivis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create suivi" do
    assert_difference('Suivi.count') do
      post :create, :suivi => { }
    end

    assert_redirected_to suivi_path(assigns(:suivi))
  end

  test "should show suivi" do
    get :show, :id => suivis(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => suivis(:one).id
    assert_response :success
  end

  test "should update suivi" do
    put :update, :id => suivis(:one).id, :suivi => { }
    assert_redirected_to suivi_path(assigns(:suivi))
  end

  test "should destroy suivi" do
    assert_difference('Suivi.count', -1) do
      delete :destroy, :id => suivis(:one).id
    end

    assert_redirected_to suivis_path
  end
end

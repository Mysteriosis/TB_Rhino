# -*- encoding : utf-8 -*-
require 'test_helper'

class AnneeAcademiquesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:annee_academiques)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create annee_academique" do
    assert_difference('AnneeAcademique.count') do
      post :create, :annee_academique => { }
    end

    assert_redirected_to annee_academique_path(assigns(:annee_academique))
  end

  test "should show annee_academique" do
    get :show, :id => annee_academiques(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => annee_academiques(:one).id
    assert_response :success
  end

  test "should update annee_academique" do
    put :update, :id => annee_academiques(:one).id, :annee_academique => { }
    assert_redirected_to annee_academique_path(assigns(:annee_academique))
  end

  test "should destroy annee_academique" do
    assert_difference('AnneeAcademique.count', -1) do
      delete :destroy, :id => annee_academiques(:one).id
    end

    assert_redirected_to annee_academiques_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class UniteEnseignementOrientationsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unite_enseignement_orientations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unite_enseignement_orientation" do
    assert_difference('UniteEnseignementOrientation.count') do
      post :create, :unite_enseignement_orientation => { }
    end

    assert_redirected_to unite_enseignement_orientation_path(assigns(:unite_enseignement_orientation))
  end

  test "should show unite_enseignement_orientation" do
    get :show, :id => unite_enseignement_orientations(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => unite_enseignement_orientations(:one).to_param
    assert_response :success
  end

  test "should update unite_enseignement_orientation" do
    put :update, :id => unite_enseignement_orientations(:one).to_param, :unite_enseignement_orientation => { }
    assert_redirected_to unite_enseignement_orientation_path(assigns(:unite_enseignement_orientation))
  end

  test "should destroy unite_enseignement_orientation" do
    assert_difference('UniteEnseignementOrientation.count', -1) do
      delete :destroy, :id => unite_enseignement_orientations(:one).to_param
    end

    assert_redirected_to unite_enseignement_orientations_path
  end
end

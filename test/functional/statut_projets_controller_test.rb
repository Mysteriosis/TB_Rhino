# -*- encoding : utf-8 -*-
require 'test_helper'

class StatutProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:statut_projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create statut_projet" do
    assert_difference('StatutProjet.count') do
      post :create, :statut_projet => { }
    end

    assert_redirected_to statut_projet_path(assigns(:statut_projet))
  end

  test "should show statut_projet" do
    get :show, :id => statut_projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => statut_projets(:one).id
    assert_response :success
  end

  test "should update statut_projet" do
    put :update, :id => statut_projets(:one).id, :statut_projet => { }
    assert_redirected_to statut_projet_path(assigns(:statut_projet))
  end

  test "should destroy statut_projet" do
    assert_difference('StatutProjet.count', -1) do
      delete :destroy, :id => statut_projets(:one).id
    end

    assert_redirected_to statut_projets_path
  end
end

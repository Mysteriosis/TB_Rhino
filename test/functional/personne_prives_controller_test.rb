# -*- encoding : utf-8 -*-
require 'test_helper'

class PersonnePrivesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personne_prives)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create personne_prive" do
    assert_difference('PersonnePrive.count') do
      post :create, :personne_prive => { }
    end

    assert_redirected_to personne_prive_path(assigns(:personne_prive))
  end

  test "should show personne_prive" do
    get :show, :id => personne_prives(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => personne_prives(:one).to_param
    assert_response :success
  end

  test "should update personne_prive" do
    put :update, :id => personne_prives(:one).to_param, :personne_prive => { }
    assert_redirected_to personne_prive_path(assigns(:personne_prive))
  end

  test "should destroy personne_prive" do
    assert_difference('PersonnePrive.count', -1) do
      delete :destroy, :id => personne_prives(:one).to_param
    end

    assert_redirected_to personne_prives_path
  end
end

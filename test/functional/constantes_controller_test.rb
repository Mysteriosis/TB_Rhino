# -*- encoding : utf-8 -*-
require 'test_helper'

class ConstantesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:constantes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create constante" do
    assert_difference('Constante.count') do
      post :create, :constante => { }
    end

    assert_redirected_to constante_path(assigns(:constante))
  end

  test "should show constante" do
    get :show, :id => constantes(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => constantes(:one).id
    assert_response :success
  end

  test "should update constante" do
    put :update, :id => constantes(:one).id, :constante => { }
    assert_redirected_to constante_path(assigns(:constante))
  end

  test "should destroy constante" do
    assert_difference('Constante.count', -1) do
      delete :destroy, :id => constantes(:one).id
    end

    assert_redirected_to constantes_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class DesiderataProjetsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:desiderata_projets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create desiderata_projet" do
    assert_difference('DesiderataProjet.count') do
      post :create, :desiderata_projet => { }
    end

    assert_redirected_to desiderata_projet_path(assigns(:desiderata_projet))
  end

  test "should show desiderata_projet" do
    get :show, :id => desiderata_projets(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => desiderata_projets(:one).id
    assert_response :success
  end

  test "should update desiderata_projet" do
    put :update, :id => desiderata_projets(:one).id, :desiderata_projet => { }
    assert_redirected_to desiderata_projet_path(assigns(:desiderata_projet))
  end

  test "should destroy desiderata_projet" do
    assert_difference('DesiderataProjet.count', -1) do
      delete :destroy, :id => desiderata_projets(:one).id
    end

    assert_redirected_to desiderata_projets_path
  end
end

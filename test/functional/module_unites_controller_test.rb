# -*- encoding : utf-8 -*-
require 'test_helper'

class ModuleUnitesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:module_unites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create module_unite" do
    assert_difference('ModuleUnite.count') do
      post :create, :module_unite => { }
    end

    assert_redirected_to module_unite_path(assigns(:module_unite))
  end

  test "should show module_unite" do
    get :show, :id => module_unites(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => module_unites(:one).id
    assert_response :success
  end

  test "should update module_unite" do
    put :update, :id => module_unites(:one).id, :module_unite => { }
    assert_redirected_to module_unite_path(assigns(:module_unite))
  end

  test "should destroy module_unite" do
    assert_difference('ModuleUnite.count', -1) do
      delete :destroy, :id => module_unites(:one).id
    end

    assert_redirected_to module_unites_path
  end
end

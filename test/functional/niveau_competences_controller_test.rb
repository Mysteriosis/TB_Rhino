# -*- encoding : utf-8 -*-
require 'test_helper'

class NiveauCompetencesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:niveau_competences)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create niveau_competence" do
    assert_difference('NiveauCompetence.count') do
      post :create, :niveau_competence => { }
    end

    assert_redirected_to niveau_competence_path(assigns(:niveau_competence))
  end

  test "should show niveau_competence" do
    get :show, :id => niveau_competences(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => niveau_competences(:one).id
    assert_response :success
  end

  test "should update niveau_competence" do
    put :update, :id => niveau_competences(:one).id, :niveau_competence => { }
    assert_redirected_to niveau_competence_path(assigns(:niveau_competence))
  end

  test "should destroy niveau_competence" do
    assert_difference('NiveauCompetence.count', -1) do
      delete :destroy, :id => niveau_competences(:one).id
    end

    assert_redirected_to niveau_competences_path
  end
end

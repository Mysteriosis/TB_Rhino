# -*- encoding : utf-8 -*-
require 'test_helper'

class ContratAnnuelsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contrat_annuels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contrat_annuel" do
    assert_difference('ContratAnnuel.count') do
      post :create, :contrat_annuel => { }
    end

    assert_redirected_to contrat_annuel_path(assigns(:contrat_annuel))
  end

  test "should show contrat_annuel" do
    get :show, :id => contrat_annuels(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => contrat_annuels(:one).to_param
    assert_response :success
  end

  test "should update contrat_annuel" do
    put :update, :id => contrat_annuels(:one).to_param, :contrat_annuel => { }
    assert_redirected_to contrat_annuel_path(assigns(:contrat_annuel))
  end

  test "should destroy contrat_annuel" do
    assert_difference('ContratAnnuel.count', -1) do
      delete :destroy, :id => contrat_annuels(:one).to_param
    end

    assert_redirected_to contrat_annuels_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class MoisControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mois)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create moi" do
    assert_difference('Moi.count') do
      post :create, :moi => { }
    end

    assert_redirected_to moi_path(assigns(:moi))
  end

  test "should show moi" do
    get :show, :id => mois(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => mois(:one).id
    assert_response :success
  end

  test "should update moi" do
    put :update, :id => mois(:one).id, :moi => { }
    assert_redirected_to moi_path(assigns(:moi))
  end

  test "should destroy moi" do
    assert_difference('Moi.count', -1) do
      delete :destroy, :id => mois(:one).id
    end

    assert_redirected_to mois_path
  end
end

# -*- encoding : utf-8 -*-
require 'test_helper'

class TypeEvenementPersonnesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:type_evenement_personnes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_evenement_personne" do
    assert_difference('TypeEvenementPersonne.count') do
      post :create, :type_evenement_personne => { }
    end

    assert_redirected_to type_evenement_personne_path(assigns(:type_evenement_personne))
  end

  test "should show type_evenement_personne" do
    get :show, :id => type_evenement_personnes(:one).id
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => type_evenement_personnes(:one).id
    assert_response :success
  end

  test "should update type_evenement_personne" do
    put :update, :id => type_evenement_personnes(:one).id, :type_evenement_personne => { }
    assert_redirected_to type_evenement_personne_path(assigns(:type_evenement_personne))
  end

  test "should destroy type_evenement_personne" do
    assert_difference('TypeEvenementPersonne.count', -1) do
      delete :destroy, :id => type_evenement_personnes(:one).id
    end

    assert_redirected_to type_evenement_personnes_path
  end
end
